<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Job::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'organization' => $faker->text,
        'description' => $faker->text,
        'application_form_file' => $faker->imageUrl,
        'application_form_link' => $faker->url,
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
    ];
});
