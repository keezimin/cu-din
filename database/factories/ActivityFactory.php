<?php

use Faker\Generator as Faker;

$factory->define(\PhpSoft\Activity\Models\Activity::class, function (Faker $faker) {
    return [
        'actor' => ([
            'name' => $faker->name,
            'id' => $faker->randomNumber()
        ]),
        'actor_id' => $faker->randomNumber(),
        'actor_type' => $faker->randomElement(['User', 'Page']),
        'verb' => $faker->randomElement(['save', 'share', 'like', 'report', 'message']),
        'object' => ([
            'name' => $faker->name,
            'id' => $faker->randomNumber()
        ]),
        'object_id' => $faker->randomNumber(),
        'object_type' => $faker->randomElement(['User', 'Page', 'Post', 'Photo']),
        'target' => \DB::raw('JSON_OBJECT()'),
        'meta' => \DB::raw('JSON_OBJECT()'),
    ];
});
