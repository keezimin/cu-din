<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Scholarship::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'organization' => $faker->text,
        'amount' => rand(0, 100),
        'deadline_on' => $faker->dateTimeBetween('1 month', '6 months')->format('Y-m-d'),
        'description' => $faker->text,
        'application_form_file' => $faker->imageUrl,
        'application_form_link' => $faker->url,
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'status' => 1,
    ];
});
