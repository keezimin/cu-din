<?php

use Faker\Generator as Faker;
use \App\Models\CareerCluster;

$factory->define(CareerCluster::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'description' => $faker->text,
    ];
});
