<?php

use Faker\Generator as Faker;
use \App\Models\Device;

$factory->define(Device::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'uuid' => $faker->text,
        'token' => $faker->text,
        'is_logging_in' =>true
    ];
});
