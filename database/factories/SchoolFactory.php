<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\School::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->unique()->randomNumber,
        'district_id' => function () {
            return factory(App\Models\District::class)->create()->id;
        },
    ];
});
