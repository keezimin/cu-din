<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Invitation::class, function (Faker $faker) {
    return [
        'fullname' => $faker->name,
        'email' => $faker->safeEmail,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
    ];
});
