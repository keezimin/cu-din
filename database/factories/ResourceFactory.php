<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Resource::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'link' => $faker->url,
        'image' => $faker->imageUrl,
        'summary' => $faker->text,
        'type' => $faker->randomElement($array = array ('news','resource')),
    ];
});
