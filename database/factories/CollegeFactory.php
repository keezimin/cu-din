<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\College::class, function (Faker $faker) {
    return [
        'code' => $faker->uuid,
        'name' => '',
    ];
});
