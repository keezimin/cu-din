<?php

use Faker\Generator as Faker;
use \App\Models\Pathway;

$factory->define(Pathway::class, function (Faker $faker) {
    return [
        'career_cluster_id' => function () {
            return factory(\App\Models\CareerCluster::class)->create()->id;
        },
        'name' => $faker->text,
        'short_description' => $faker->text,
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'active' => 1
    ];
});
