<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'fullname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'contact' => $faker->text,
        'graduated_on' =>  $faker->date,
        'act_english' => rand(0,10),
        'act_science' => rand(0,10),
        'act_math' => rand(0,10),
        'act_composite' => rand(0,10),
        'act_reading' => rand(0,10),
        'sat_math' => rand(0,10),
        'sat_math' => rand(0,10),
        'remember_token' => str_random(10),
        'school_id' => function () {
            return factory(App\Models\School::class)->create()->id;
        },
    ];
});
