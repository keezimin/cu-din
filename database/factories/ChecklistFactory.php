<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Checklist::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'owner_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
    ];
});
