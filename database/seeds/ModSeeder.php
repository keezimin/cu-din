<?php

use PhpSoft\Users\Models\Role;
use Illuminate\Database\Seeder;

class ModSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRoleName = 'super_admin';
        // find or create user admin
        $gg = \App\User::firstOrCreate(
            ['email' => 'cm@greenglobal.vn'],
            [
                'name' => 'Admin TCX',
                'password' => bcrypt('123!@#123'),
                'remember_token' => str_random(60),
            ]
        );

        // find or create user admin
        $seaDev = \App\User::firstOrCreate(
            ['email' => 'it@seadev.com.vn'],
            [
                'name' => 'Admin Sea Dev',
                'password' => bcrypt('123!@#123'),
                'remember_token' => str_random(60),
            ]
        );

        // find or create role admin
        $roleSuperAdmin = Role::firstOrCreate(
            ['name' => $adminRoleName],
            [
                'display_name' => 'Administrator',
                'description' => 'User is allowed to manage all system.',
            ]
        );

        // attach roles
        if (!$gg->hasRole($adminRoleName)) {
            $gg->attachRole($roleSuperAdmin);
        } // attach roles
        if (!$seaDev->hasRole($adminRoleName)) {
            $seaDev->attachRole($roleSuperAdmin);
        }
    }
}
