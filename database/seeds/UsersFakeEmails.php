<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersFakeEmails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->whereNotIn('email', [
                'user@example.com',
                'studenttest.seadev@gmail.com',
                'datest.seadev@gmail.com',
                'sctest.seadev@gmail.com',
                'botest.seadev@gmail.com',
                'prtest.seadev@gmail.com',
                'tctest.seadev@gmail.com',
            ])
            ->update([
                'email' => DB::raw('CONCAT(\'autn+\', REPLACE(`email`, \'@\', \'_\'), \'@greenglobal.vn\')')
            ])
        ;
    }
}
