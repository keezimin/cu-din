<?php

use Illuminate\Database\Seeder;
use PhpSoft\Users\Models\Role;

class DatabaseLiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'admin@cued-in.com',
            'fullname' => 'Admin',
            'password' => bcrypt('123456@123456@'),
            'school_id' => null
        ]);

        $roleSuperAdmin = Role::where('name', 'super_admin')->first();
        if (empty($roleSuperAdmin)) {
            $roleSuperAdmin = new Role;
            $roleSuperAdmin->name         = 'super_admin';
            $roleSuperAdmin->display_name = 'Administrator';
            $roleSuperAdmin->description  = 'User is allowed to manage all system.';
            $roleSuperAdmin->save();
        }

        // attach roles
        \App\User::where('email', 'admin@cued-in.com')->first()->attachRole($roleSuperAdmin);

        \App\Services\ImportSchoolService::import(storage_path('data/PublicSchool15-16.xls'));
    }
}
