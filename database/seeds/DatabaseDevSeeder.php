<?php

use Illuminate\Database\Seeder;
use PhpSoft\Users\Models\Role;

class DatabaseDevSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\School::class, 50)->create();
        factory(\App\User::class)->create([
            'email' => 'user@example.com',
            'fullname' => 'Example User',
            'password' => bcrypt('123456'),
        ]);
        factory(\App\User::class)->create([
            'email' => 'admin@example.com',
            'fullname' => 'Admin',
            'password' => bcrypt('123456'),
        ]);

        $roleSuperAdmin = Role::where('name', 'super_admin')->first();
        if (empty($roleSuperAdmin)) {
            $roleSuperAdmin = new Role;
            $roleSuperAdmin->name         = 'super_admin';
            $roleSuperAdmin->display_name = 'Administrator';
            $roleSuperAdmin->description  = 'User is allowed to manage all system.';
            $roleSuperAdmin->save();
        }

        // attach roles
        \App\User::where('email', 'admin@example.com')->first()->attachRole($roleSuperAdmin);
        factory(\App\User::class, 10)->create();
        factory(\App\Models\Scholarship::class, 15)->create(['user_id' => 5]);
        factory(\App\Models\Scholarship::class, 15)->create(['user_id' => 6]);
        factory(\App\Models\Scholarship::class, 15)->create(['user_id' => 7]);

        $ownerScholarship = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $ownerScholarship->id]);
        factory(\App\Models\Scholarship::class)->create(['user_id' => $ownerScholarship->id]);
        // counselor save
        $ownerAct = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $ownerScholarship->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);

        // student save and apply
        $user = factory(\App\User::class)->create();
        $saveAct = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);
        \App\Services\UserActionService::applyScholarship($user, $scholarship);
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        \App\Services\UserActionService::applyScholarship($user, $scholarship);
        \App\Services\UserActionService::applyScholarship(factory(\App\User::class)->create(), $scholarship);
        \App\Services\UserActionService::applyScholarship(factory(\App\User::class)->create(), $scholarship);

        factory(\App\Models\Resource::class, 15)->create();

        $m = app()->make('\App\Repositories\Eloquent\MessageBread');
        $m->add([
            'target' => [
                'type' => 'USER_ALL'
            ],
            'content' => 'hi there, welcome to CUED-IN',
        ], \App\User::find(2));
        $m->add([
            'target' => [
                'type' => 'DISTRICT',
                'id' => [$user->school->district_id],
            ],
            'content' => 'hi there, welcome to CUED-IN, this is a test message for district',
        ], \App\User::find(2));
        $m->add([
            'target' => [
                'type' => 'SCHOOL',
                'id' => [$user->school_id],
            ],
            'content' => 'hi there, welcome to CUED-IN, this is a test message for school',
        ], \App\User::find(2));
        $m->add([
            'target' => [
                'type' => 'USER',
                'id' => [$user->id],
            ],
            'content' => 'hi there, welcome to CUED-IN, this is a test message for user',
        ], \App\User::find(2));
        $m->add([
            'target' => [
                'type' => 'USER_ALL'
            ],
            'source' => [
                'type' => 'SCHOLARSHIP',
                'id' => [$scholarship->id],
            ],
            'type' => 'remind',
            'content' => 'hi there, bla bla',
        ], $user);
        $m->add([
            'target' => [
                'type' => 'USER_ALL'
            ],
            'source' => [
                'type' => 'SCHOLARSHIP',
                'id' => [$scholarship->id],
            ],
            'content' => 'hi there, bla bla bla',
        ], $user);
    }
}
