<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RemoveFoundedAt2019 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_profiles')
            ->where('founded', '=', '2019')
            ->update([
                'founded' => null
            ])
        ;
    }
}
