<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdateVideosSeeder extends Seeder
{
    public $table;
    public $field;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->updateBusinessProfilesAbout();
//        $this->updateBusinessProfilesCulture();
//        $this->updatePathwaysAbout();
    }

    /**
     * Update about of business profiles
     *
     * @return void
     */
    public function updateBusinessProfilesAbout() {
        $this->table = 'business_profiles';
        $this->field = 'about';
        $profiles = $this->getRecords(['id', 'about']);

        if (count($profiles)) {
            $this->updateData($profiles, 'Vision', 'Company video');
        }
    }

    /**
     * Update culture of business profiles
     *
     * @return void
     */
    public function updateBusinessProfilesCulture() {
        $this->table = 'business_profiles';
        $this->field = 'culture';
        $profiles = $this->getRecords(['id', 'culture']);

        if (count($profiles)) {
            $this->updateData($profiles, 'Project', 'Project Video 1');
        }
    }

    /**
     * Update about of pathways
     *
     * @return void
     */
    public function updatePathwaysAbout() {
        $this->table = 'pathways';
        $this->field = 'about';
        $pathways = $this->getRecords(['id', 'about']);

        if (count($pathways)) {
            $this->updateData($pathways, 'Apprenticeships Available', 'Career video');
        }
    }

    /**
     * Update data: add video section to json field
     *
     * @return void
     */
    public function updateData($rows, $pointName, $title) {
        $field = $this->field;
        foreach ($rows as $row) {
            $data = [];
            $fieldData = json_decode($row->$field, true);

            if ($fieldData) {
                $hasPoint = false;
                foreach ($fieldData as $item) {
                    $data[] = $item;
                    if ($item['type'] == 'paragraph' && $item['title'] == $pointName) {
                        $hasPoint = true;
                        $data[] = [
                            'type' => 'video',
                            'title' => $title,
                            'content' => null,
                            'description' => 'A video that shows the unique personality of your company: work environment, values, beliefs,  interactions, goals and expectations'
                        ];
                    }
                }
            }
            if (!empty($data)) {
                $this->updateRecord($row->id, [$field => json_encode($data)]);
            }
        }
    }

    /**
     * Get records
     *
     * @return object
     */
    public function getRecords($fields) {
        return DB::table($this->table)
            ->select($fields)
            ->where($this->field, 'not like', '%"type": "video"%')
            ->where($this->field, 'not like', '%"type":"video"%')
            ->whereNotNull($this->field)
            ->get();
    }

    /**
     * Update record
     *
     * @return object
     */
    public function updateRecord($id, $data) {
        return DB::table($this->table)
            ->where('id', '=', $id)
            ->update($data);
    }
}
