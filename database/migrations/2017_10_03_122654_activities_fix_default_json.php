<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivitiesFixDefaultJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('activities')->whereNull('target')->update(['target' => \DB::raw('JSON_OBJECT()')]);
        \DB::table('activities')->whereNull('meta')->update(['meta' => \DB::raw('JSON_OBJECT()')]);
        Schema::table('activities', function (Blueprint $table) {
            $table->json('target')->nullable(false)->change();
            $table->json('meta')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // this is a force change, no rollback
    }
}
