<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('key')->unique();
            $table->string('value')->nullable();
        });

        factory(\App\Models\Setting::class)->create([
            'key' => 'app_name',
            'value' => ''
        ]);
        factory(\App\Models\Setting::class)->create([
            'key' => 'app_link',
            'value' => ''
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
