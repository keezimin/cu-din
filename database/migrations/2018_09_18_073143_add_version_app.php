<?php

use Illuminate\Database\Migrations\Migration;

class AddVersionApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        factory(\App\Models\Setting::class)->create([
            'key' => 'app_platform',
            'value' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
