<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddInitFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fullname')->nullable();
            $table->string('contact')->nullable();
            $table->date('graduated_on')->nullable();
            $table->integer('act_english')->unsigned()->nullable();
            $table->integer('act_science')->unsigned()->nullable();
            $table->integer('act_math')->unsigned()->nullable();
            $table->integer('act_composite')->unsigned()->nullable();
            $table->integer('act_reading')->unsigned()->nullable();
            $table->integer('sat_math')->unsigned()->nullable();
            $table->integer('sat_verbal')->unsigned()->nullable();

            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('fullname');
            $table->dropColumn('contact');
            $table->dropColumn('graduated_on');
            $table->dropColumn('act_english');
            $table->dropColumn('act_science');
            $table->dropColumn('act_math');
            $table->dropColumn('act_composite');
            $table->dropColumn('act_reading');
            $table->dropColumn('sat_math');
            $table->dropColumn('sat_verbal');

            $table->dropForeign(['school_id']);
            $table->dropColumn('school_id');
        });
    }
}
