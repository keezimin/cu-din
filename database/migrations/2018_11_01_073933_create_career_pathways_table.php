<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerPathwaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_pathways', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career_cluster_id')->unsigned();
            $table->string('name');
            $table->timestamps();
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('career_pathways', function (Blueprint $table) {
            $table->dropForeign(['career_cluster_id']);
        });
        Schema::dropIfExists('career_pathways');
    }
}
