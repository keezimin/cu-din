<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropForeign('surveys_career_cluster_id_foreign');
            $table->dropColumn(['career_cluster_id', 'point']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->integer('career_cluster_id')->unsigned();
            $table->integer('point')->default(1);
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
        });
    }
}
