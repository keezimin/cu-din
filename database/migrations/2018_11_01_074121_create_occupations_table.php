<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occupations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career_cluster_id')->unsigned();
            $table->integer('career_pathway_id')->unsigned();
            $table->string('name');
            $table->string('code')->unique();
            $table->string('video')->nullable();
            $table->text('occupation')->nullable();
            $table->double('average_salary')->default(0);
            $table->timestamps();
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
            $table->foreign('career_pathway_id')
              ->references('id')->on('career_pathways')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('occupations', function (Blueprint $table) {
            $table->dropForeign(['career_cluster_id']);
            $table->dropForeign(['career_pathway_id']);
        });
        Schema::dropIfExists('occupations');
    }
}
