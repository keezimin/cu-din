<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Job;
use App\Models\Scholarship;

class ConvertDataCareerScholarshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Job::withTrashed()->whereNull('state_id')->get()->each(function ($job) {
            if (!empty($job->schools->first()->district->state_id)) {
                $job->update(['state_id' => $job->schools()->first()->district->state_id]);
            }
        });
        Job::withTrashed()->where('is_applied_to_state', 0)->doesntHave('districts')->get()->each(function ($job) {
            if ($job->schools->isNotEmpty()) {
                $job->districts()->sync([$job->schools()->first()->district_id]);
            }
        });

        Scholarship::withTrashed()->whereNull('state_id')->get()->each(function ($scholarship) {
            if (!empty($scholarship->schools->first()->district->state_id)) {
                $scholarship->update(['state_id' => $scholarship->schools()->first()->district->state_id]);
            }
        });
        Scholarship::withTrashed()->where('is_applied_to_state', 0)->doesntHave('districts')->get()->each(function ($scholarship) {
            if ($scholarship->schools->isNotEmpty()) {
                $scholarship->districts()->sync([$scholarship->schools()->first()->district_id]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
