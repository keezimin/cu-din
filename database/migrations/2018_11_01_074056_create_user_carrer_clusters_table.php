<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCarrerClustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_carrer_clusters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('career_cluster_id')->unsigned();
            $table->integer('total_point')->default(0);
            $table->timestamps();
            $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('cascade');
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_carrer_clusters', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['career_cluster_id']);
        });
        Schema::dropIfExists('user_carrer_clusters');
    }
}
