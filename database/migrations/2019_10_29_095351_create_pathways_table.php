<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePathwaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pathways', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('short_description')->nullable();
            $table->integer('career_cluster_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->json('about')->nullable();
            $table->json('skills')->nullable();
            $table->json('training')->nullable();
            $table->timestamps();
            $table->foreign('career_cluster_id')
                ->references('id')->on('career_clusters')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pathways');
    }
}
