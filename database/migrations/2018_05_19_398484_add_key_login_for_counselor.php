<?php

use Illuminate\Database\Migrations\Migration;

class AddKeyLoginForCounselor extends Migration
{
    public function up()
    {
        factory(\App\Models\Setting::class)->create([
            'key' => 'counselor_login',
            'value' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Setting::where(['key' => 'counselor_login'])->delete();
    }
}
