<?php

use App\Models\Scholarship;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPriceToScholarships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scholarships', function (Blueprint $table) {
            $table->integer('min_price')->nullable();
            $table->integer('max_price')->nullable();
        });

        \DB::table('scholarships')->whereNull('min_price')
            ->update([
                "min_price" => \DB::raw("`amount`"),
            ]);
        \DB::table('scholarships')->whereNull('max_price')
            ->update([
                "max_price" => \DB::raw("`min_price`"),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scholarships', function (Blueprint $table) {
            $table->dropColumn('min_price');
            $table->dropColumn('max_price');
        });
    }
}
