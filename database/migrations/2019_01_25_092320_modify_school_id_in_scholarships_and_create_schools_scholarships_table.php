<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySchoolIdInScholarshipsAndCreateSchoolsScholarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools_scholarships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('scholarship_id')->unsigned();
            $table->foreign('scholarship_id')->references('id')->on('scholarships')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });

        \App\Models\Scholarship::all()->each(function ($scholarship) {
            $scholarship->schools()->attach([$scholarship->school_id]);
        });

        Schema::table('scholarships', function (Blueprint $table) {
            $table->dropColumn('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scholarships', function (Blueprint $table) {
            $table->integer('school_id')->unsigned()->nullable();
        });

        Schema::table('schools_scholarships', function (Blueprint $table) {
            $table->dropForeign(['school_id']);
            $table->dropForeign(['scholarship_id']);
        });
        Schema::dropIfExists('schools_scholarships');
    }
}
