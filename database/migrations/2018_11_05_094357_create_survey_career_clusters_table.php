<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyCareerClustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_career_clusters', function (Blueprint $table) {
            $table->integer('survey_id')->unsigned();
            $table->integer('career_cluster_id')->unsigned();
            $table->integer('point')->default(1);
            $table->timestamps();
            $table->foreign('survey_id')
              ->references('id')->on('surveys')
              ->onDelete('cascade');
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_career_clusters', function (Blueprint $table) {
            $table->dropForeign(['survey_id']);
            $table->dropForeign(['career_cluster_id']);
        });
        Schema::dropIfExists('survey_career_clusters');
    }
}
