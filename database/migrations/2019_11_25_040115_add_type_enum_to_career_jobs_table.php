<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Job;

class AddTypeEnumToCareerJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = Job::$enumTypes;
        DB::statement("ALTER TABLE career_jobs CHANGE COLUMN type type ENUM(" . "'" . implode($types, "','") . "'" . ")");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
