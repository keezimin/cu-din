<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('state_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
        });

        $query = 'INSERT INTO organizations (id, name, state_id, created_at, updated_at)
                  SELECT id, name, state_id, CURTIME(), CURTIME() FROM districts';
        DB::unprepared($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
