<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned()->nullable();
            $table->boolean('is_applied_to_district')->nullable();
            $table->string('name')->index();
            $table->enum('type', ['job', 'volunteer', 'intership']);
            $table->string('organization')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('application_form_file')->nullable();
            $table->string('application_form_link')->nullable();
            $table->string('application_form_filename')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('status')->default(0);
            $table->integer('volunteer_hours')->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_jobs');
    }
}
