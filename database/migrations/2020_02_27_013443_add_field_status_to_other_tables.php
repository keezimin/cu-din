<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusToOtherTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools_scholarships', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        Schema::table('districts_scholarships', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        Schema::table('schools_jobs', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        Schema::table('districts_career_jobs', function (Blueprint $table) {
            $table->integer('status')->default(0);
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools_scholarships', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('districts_scholarships', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('schools_jobs', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('districts_career_jobs', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
