<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_category_id')->unsigned();
            $table->integer('career_cluster_id')->unsigned();
            $table->string('name');
            $table->integer('point')->default(1);
            $table->timestamps();
            $table->foreign('survey_category_id')
              ->references('id')->on('survey_categories')
              ->onDelete('cascade');
            $table->foreign('career_cluster_id')
              ->references('id')->on('career_clusters')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->dropForeign(['survey_category_id']);
            $table->dropForeign(['career_cluster_id']);
        });
        Schema::dropIfExists('surveys');
    }
}
