<?php

use App\Models\Job;
use Illuminate\Database\Migrations\Migration;

class AddTypeEnumForJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = Job::$enumTypes;
        DB::statement("ALTER TABLE career_jobs CHANGE COLUMN type type ENUM(" . "'" . implode($types, "','") . "'" . ")");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {}
}
