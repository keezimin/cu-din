<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignKeyPathwayToJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('career_jobs', function (Blueprint $table) {
            $table->dropForeign(['pathway_id']);
            $table->foreign('pathway_id')->references('id')->on('pathways')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('career_jobs', function (Blueprint $table) {
            $table->dropForeign(['pathway_id']);
            $table->foreign('pathway_id')->references('id')->on('pathways');
        });
    }
}
