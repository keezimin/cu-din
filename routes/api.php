<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::get('/', function () {
    return response()->json(['message' => 'Welcome back']);
});
Route::get('/test/email', function () {
    \App\Jobs\SendInviteEmail::dispatch(\App\User::find(1), 'http://test', request()->email ?? env('MAIL_FROM_ADDRESS'));
    echo 'done';
});

Route::get('/analytics', function () {

    $now = \Carbon\Carbon::now();
    $total = \DB::table('career_jobs')->where('status', \App\Models\Job::APPROVED)->count();

    $totalJob3Months = \DB::table('career_jobs')->where('created_at', '<', $now->toDateTimeString())->where('status', \App\Models\Job::APPROVED)->where('created_at', '>', $now->subMonths(3)->toDateTimeString())->count();

    $now1 = \Carbon\Carbon::now();
    $totalJob6Months = \DB::table('career_jobs')->where('created_at', '<', $now1->toDateTimeString())->where('status', \App\Models\Job::APPROVED)->where('created_at', '>', $now1->subMonths(6)->toDateTimeString())->count();


    $totalUserApproved3 = \PhpSoft\Activity\Models\Activity::ofVerbs(['apply'])
        ->where('meta->approved')
        ->whereIn('object_type', ['Job'])
        ->whereExists(function ($query) {
            $query->selectRaw(1)
                ->from('career_jobs')
                ->whereRaw('career_jobs.id = activities.object_id')
                ->whereNull('career_jobs.deleted_at');
        })->whereExists(function ($query) {
            $query->selectRaw(1)
                ->from('users')
                ->whereRaw('users.id = activities.actor_id')
                ->whereNull('users.deleted_at');
        });
    $totalUserApproved6 = clone $totalUserApproved3;

    $totalUserApproved = $totalUserApproved3->count();

    $now2 = \Carbon\Carbon::now();
    $totalUserApproved3Months = $totalUserApproved3->where('updated_at', '<', $now2->toDateTimeString())->where('updated_at', '>', $now2->subMonths(3)->toDateTimeString())->count();
    $now3 = \Carbon\Carbon::now();
    $totalUserApproved6Months = $totalUserApproved6->where('updated_at', '<', $now3->toDateTimeString())->where('updated_at', '>', $now3->subMonths(6)->toDateTimeString())->count();


    $queryJobs3 = \PhpSoft\Activity\Models\Activity::ofVerbs(['apply'])
        ->select(['activities.object_id'])
        ->where('meta->interested')
        ->whereIn('object_type', ['Job'])
        ->whereExists(function ($query) {
            $query->selectRaw(1)
                ->from('career_jobs')
                ->whereRaw('career_jobs.id = activities.object_id')
                ->whereNull('career_jobs.deleted_at');
        })->whereExists(function ($query) {
            $query->selectRaw(1)
                ->from('users')
                ->whereRaw('users.id = activities.actor_id')
                ->whereNull('users.deleted_at');
        })->distinct('activities.object_id');
    $queryJobs6 = clone $queryJobs3;

    $totalInterested = $queryJobs3->count();

    $getUsersOfJob = function (&$job) {
        $id = $job->object_id;
        $job = $job->toArray();

        $countUser = \App\User::with(['jobActivities' => function ($q) use ($id) {
            $q->where('object_id', $id)->where('meta->interested');
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Job')
                ->where('activities.object_id', $id);
        })->count();
        $job['name'] = \App\Models\Job::find($id)->name;
        $job['students'] = $countUser;
        return $job;
    };

    $now4 = \Carbon\Carbon::now();
    $query3 = $queryJobs3->where('updated_at', '<', $now4->toDateTimeString())->where('updated_at', '>', $now4->subMonths(3)->toDateTimeString());

    $totalUsers3 = $query3->get();

    $total3 = $query3->count();

    $results3 = [];
    foreach ($totalUsers3 as $key => $job) {
        $results3[] = $getUsersOfJob($job);
    }

    $now5 = \Carbon\Carbon::now();
    $query6 = $queryJobs6->where('updated_at', '<', $now5->toDateTimeString())->where('updated_at', '>', $now5->subMonths(6)->toDateTimeString());

    $totalUsers6 = $query6->get();

    $total6 = $query6->count();

    $results6 = [];
    foreach ($totalUsers6 as $key => $job) {
        $results6[] = $getUsersOfJob($job);
    }


    return response()->json([
        'total_job_approved' => [
            '3months_from_mow' => $totalJob3Months,
            '6months_from_now' => $totalJob6Months,
            'total' => $total
        ],
        'total_users_approved_jobs' => [
            '3months_from_mow' => $totalUserApproved3Months,
            '6months_from_now' => $totalUserApproved6Months,
            'total' => $totalUserApproved
        ],
        'total_users_interested_a_job' => [
            '3months_from_mow' => $total3,
            '6months_from_now' => $total6,
            'total' => $totalInterested,
            'details_3months_from_mow' => $results3,
            'details_6months_from_now' => $results6,
        ]
    ]);
});

Route::post('/auth/login', 'AuthController@login');
Route::post('/auth/facebook', 'AuthFacebookController@login');
Route::post('/auth/google', 'AuthGoogleController@login');

Route::post('/password/forgot/request', 'Auth\ForgotPasswordController@getResetToken');
Route::post('/adminpage/password/forgot/request', 'Auth\ForgotPasswordController@getResetTokenAdminPage');
Route::post('/password/forgot/reset', 'Auth\ResetPasswordController@reset');
Route::get('/schools', '\App\Http\Controllers\SchoolController@index');
Route::get('/districts', '\App\Http\Controllers\DistrictController@index');
Route::post('/users', '\App\Http\Controllers\UserController@store');
Route::post('/parents', '\App\Http\Controllers\ParentController@store');
Route::get('/settings', 'SettingController@index');
Route::post('/businesses/create-invited', '\App\Http\Controllers\BusinessesController@createBusinessInvited');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('/auth/logout', 'AuthController@logout');

    Route::get('/me', '\PhpSoft\Users\Controllers\UserController@authenticated');
    Route::get('/users', '\App\Http\Controllers\UserController@index');
    Route::get('/users/{id}', '\PhpSoft\Users\Controllers\UserController@show');
    Route::post('/password/change', 'AuthController@changepass');
    Route::patch('/users/{id}', '\App\Http\Controllers\UserController@update');
    Route::post('/users/{id}/assign', '\App\Http\Controllers\UserController@assign');
    Route::delete('/users/{id}', '\App\Http\Controllers\UserController@delete');
    Route::get('/archives', '\App\Http\Controllers\UserController@usersDeleted');

    Route::post('/users/{user}/colleges/arrangement', '\App\Http\Controllers\UserCollegeController@arrangeSort');
    Route::post('/users/{user}/colleges/{college_code}', '\App\Http\Controllers\UserCollegeController@store');
    Route::post('/users/{user}/colleges/{college_code}/apply', '\App\Http\Controllers\UserCollegeController@apply');
    Route::delete('/users/{user}/colleges/{college_code}', '\App\Http\Controllers\UserCollegeController@delete');
    Route::post('/users/{user}/colleges/{item_idA}/{item_idB}/arrangement', '\App\Http\Controllers\UserCollegeController@arrange');

    Route::get('/users/{id}/colleges', '\App\Http\Controllers\UserCollegeController@index');
    Route::get('/users/{id}/colleges/applied', '\App\Http\Controllers\UserCollegeController@applied');
    Route::get('/users/{id}/colleges/applying', '\App\Http\Controllers\UserCollegeController@applying');
    Route::get('/users/{id}/scholarships', '\App\Http\Controllers\UserScholarshipController@index');
    Route::get('/users/{id}/scholarships/applying', '\App\Http\Controllers\UserScholarshipController@applying');
    Route::get('/users/{id}/jobs', '\App\Http\Controllers\UserJobController@index');
    Route::get('/users/{id}/jobs/applying', '\App\Http\Controllers\UserJobController@applying');
    Route::get('/users/{id}/trades', '\App\Http\Controllers\UserTradeSchoolController@index');
    Route::get('/users/{user}/checklists', '\App\Http\Controllers\ChecklistController@getChecklists')->name('getUserChecklists');
    Route::get('/users/{id}/trades/applying', '\App\Http\Controllers\UserTradeSchoolController@applying');
    Route::post('/users/{user}/scholarships/{scholarship}/applying', '\App\Http\Controllers\UserScholarshipController@apply');
    Route::post('/users/{user}/jobs/{job}/applying', '\App\Http\Controllers\UserJobController@apply');
    Route::post('/users/{user}/trades/{tradeSchool}/applying', '\App\Http\Controllers\UserTradeSchoolController@apply');
    Route::post('/users/{user}/scholarships/{scholarship}', '\App\Http\Controllers\UserScholarshipController@store');
    Route::post('/users/{user}/jobs/{job}', '\App\Http\Controllers\UserJobController@store');
    Route::post('/users/{user}/trades/{tradeSchool}', '\App\Http\Controllers\UserTradeSchoolController@store');
    Route::delete('/users/{user}/scholarships/{scholarship}', '\App\Http\Controllers\UserScholarshipController@delete');
    Route::delete('/users/{user}/jobs/{job}', '\App\Http\Controllers\UserJobController@delete');
    Route::delete('/users/{user}/trades/{tradeSchool}', '\App\Http\Controllers\UserTradeSchoolController@delete');

    Route::get('/users/{user}/messages', '\App\Http\Controllers\MessageController@listMessagesUser');
    Route::post('/users/{user}/messages/read', '\App\Http\Controllers\MessageController@markAsRead');
    Route::get('/users/{user}/messages/statistics', '\App\Http\Controllers\MessageController@statisticsUser');

    // Survey
    Route::get('/surveys', '\App\Http\Controllers\SurveyController@index');
    Route::post('/surveys', '\App\Http\Controllers\SurveyController@store');
    Route::get('/surveys/{survey}', '\App\Http\Controllers\SurveyController@show');
    Route::patch('/surveys/{survey}', '\App\Http\Controllers\SurveyController@update')->middleware('can:admin,survey');
    Route::delete('/surveys/{survey}', '\App\Http\Controllers\SurveyController@destroy');

    Route::post('/counselors', '\App\Http\Controllers\CounselorController@store');
    Route::get('/counselors/{counselor}/students', '\App\Http\Controllers\CounselorController@listStudentByCounselor');
    Route::get('/students/{student}/parents', '\App\Http\Controllers\CounselorController@listParents');
    Route::get('/counselors/{counselor}/teachers', '\App\Http\Controllers\CounselorController@listTeacherByCounselor');
    Route::get('/counselors/{counselor}/invites', '\App\Http\Controllers\CounselorController@listInvitesByCounselor');
    Route::get('/counselors/scholarships', '\App\Http\Controllers\ScholarshipController@listScholarshipsBySchool');
    Route::get('/counselors/jobs', '\App\Http\Controllers\CareerController@listJobsBySchool');
    Route::get('/counselors/{user}/scholarships', '\App\Http\Controllers\ScholarshipController@listScholarshipsByOwner');
    Route::get('/counselors/{counselor}/businesses', '\App\Http\Controllers\CounselorController@listBusinesses');
    Route::post('/counselors/{user}/students/invitation', '\App\Http\Controllers\CounselorController@invitation');
    Route::post('/students/{user}/parents/invitation', '\App\Http\Controllers\InviteController@studentInviteParent');

    Route::post('/counselors/{counselor}/students/import', 'ImportFileController@importStudents');

    Route::get('/resources', '\App\Http\Controllers\ResourceController@index');
    Route::get('/resources/{resource}', '\App\Http\Controllers\ResourceController@show');

    Route::get('/documents', '\App\Http\Controllers\DocumentController@index');
    Route::get('/documents/{document}', '\App\Http\Controllers\DocumentController@show');

    Route::get('/categories', '\App\Http\Controllers\CategoryController@index');
    Route::get('/categories/{category}', '\App\Http\Controllers\CategoryController@show');

    // Trade School
    Route::post('/trades', '\App\Http\Controllers\TradeSchoolController@store');
    Route::get('/trades/{tradeSchool}', '\App\Http\Controllers\TradeSchoolController@show');
    Route::get('/trades', '\App\Http\Controllers\TradeSchoolController@index');
    Route::get('/getTrades', '\App\Http\Controllers\TradeSchoolController@getTrades');
    Route::get('/colleges', '\App\Http\Controllers\CollegeController@index');
    Route::get('/colleges/{college}', '\App\Http\Controllers\CollegeController@show');
    Route::get('/colleges/{collectCode}/users', '\App\Http\Controllers\UserCollegeController@listAppliedStudents');
    Route::get('/colleges/{collectCode}/users/saved', '\App\Http\Controllers\UserCollegeController@listSavedStudents');

    // Checklist && Task
    Route::post('/checklists', '\App\Http\Controllers\ChecklistController@store');
    Route::get('/checklists/{checklist}', '\App\Http\Controllers\ChecklistController@show');
    Route::get('/checklists', '\App\Http\Controllers\ChecklistController@index');
    Route::get('/tasks', '\App\Http\Controllers\TaskController@index');
    Route::get('/tasks/{task}', '\App\Http\Controllers\TaskController@show');
    Route::post('/tasks', '\App\Http\Controllers\TaskController@store');
    Route::post('/users/{user}/checklists', '\App\Http\Controllers\ChecklistController@addChecklist');

    Route::get('/schools/{school}', '\App\Http\Controllers\SchoolController@show');
    Route::get('/districts/{district}', '\App\Http\Controllers\DistrictController@show');

    Route::patch('/schools/{school}', '\App\Http\Controllers\SchoolController@update')->middleware('can:admin,school');

    Route::get('/organizations', '\App\Http\Controllers\OrganizationController@index');
    Route::get('/organizations/{organization}', '\App\Http\Controllers\OrganizationController@show');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('/students', '\App\Http\Controllers\UserController@getStudents');
        Route::post('/district-admins', '\App\Http\Controllers\DistrictAdminController@store');
        Route::post('/settings', 'SettingController@update');
        Route::post('/schools', '\App\Http\Controllers\SchoolController@store');
        Route::delete('/schools/{school}', '\App\Http\Controllers\SchoolController@delete');
        Route::delete('/schools', '\App\Http\Controllers\SchoolController@multi_delete');

        Route::post('/districts', '\App\Http\Controllers\DistrictController@store');
        Route::patch('/districts/{district}', '\App\Http\Controllers\DistrictController@update');
        Route::post('/categories', '\App\Http\Controllers\CategoryController@store');
        Route::patch('/categories/{category}', '\App\Http\Controllers\CategoryController@update');
        Route::post('/documents', '\App\Http\Controllers\DocumentController@store');
        Route::patch('/documents/{document}', '\App\Http\Controllers\DocumentController@update');
        Route::delete('/categories/{category}', '\App\Http\Controllers\CategoryController@delete');
        Route::delete('/categories', '\App\Http\Controllers\CategoryController@multi_delete');
        Route::delete('/documents/{document}', '\App\Http\Controllers\DocumentController@delete');
        Route::delete('/documents', '\App\Http\Controllers\DocumentController@multi_delete');
        Route::delete('/districts/{district}', '\App\Http\Controllers\DistrictController@delete');
        Route::delete('/districts', '\App\Http\Controllers\DistrictController@multi_delete');
        Route::post('/users/relationships/attach', '\App\Http\Controllers\UserController@makeRelationships');
        Route::post('/users/relationships/detach', '\App\Http\Controllers\UserController@removeRelationships');

        // Organization
        Route::post('/organizations', '\App\Http\Controllers\OrganizationController@store');
        Route::delete('/organizations/{organization}', '\App\Http\Controllers\OrganizationController@delete');
        Route::patch('/organizations/{organization}/schools/{school}/assign', '\App\Http\Controllers\OrganizationController@assignSchool');
        Route::patch('/organizations/{organization}/schools/{school}/unassign', '\App\Http\Controllers\OrganizationController@unassignSchool');
        Route::delete('/organizations', '\App\Http\Controllers\OrganizationController@multi_delete');
    });

    //Dashboard page
    Route::group(['prefix' => 'statistics'], function () {
        Route::get('pathways', '\App\Http\Controllers\PathwayController@getTopPathways');
        Route::get('jobs/top-types', '\App\Http\Controllers\CareerController@getTopTypeJobs');
        Route::get('jobs/popular', '\App\Http\Controllers\CareerController@getPopularJobs');
        Route::get('jobs/new-request', '\App\Http\Controllers\CareerController@getNewJobRequest');
        Route::get('jobs/student-request', '\App\Http\Controllers\CareerController@getStudentRequest');
        Route::get('scholarships/total-saving-applying', '\App\Http\Controllers\ScholarshipController@totalSavingAndApplying');
        Route::get('clusters/most-active', '\App\Http\Controllers\CareerController@getActiveClusters');
    });

    Route::group(['middleware' => 'permission'], function () {
        Route::post('/resources', '\App\Http\Controllers\ResourceController@store');
        Route::patch('/resources/{resource}', '\App\Http\Controllers\ResourceController@update')->middleware('can:owner,resource');
        Route::delete('/resources/{resource}', '\App\Http\Controllers\ResourceController@delete')->middleware('can:owner,resource');
        Route::delete('/resources', '\App\Http\Controllers\ResourceController@multi_delete');
        Route::post('/scholarships', '\App\Http\Controllers\ScholarshipController@store');
        Route::patch('/scholarships/{scholarship}', '\App\Http\Controllers\ScholarshipController@update')->middleware('can:owner-scholarship,scholarship');
        Route::put('/scholarships/{scholarship}', '\App\Http\Controllers\ScholarshipController@changeStatus')->middleware('can:our-scholarship,scholarship');
        Route::patch('/scholarships/{scholarship}/status', '\App\Http\Controllers\ScholarshipController@changeStatus')->middleware('can:our-scholarship,scholarship');
        Route::delete('/scholarships/{scholarship}', '\App\Http\Controllers\ScholarshipController@delete')->middleware('can:owner-scholarship,scholarship');
        Route::delete('/scholarships', '\App\Http\Controllers\ScholarshipController@multi_delete');

        Route::post('/users/{user}/restore', '\App\Http\Controllers\UserController@restore')->middleware('can:ourStudent,user');
        Route::get('/groups', '\App\Http\Controllers\GroupController@index');
        Route::get('/groups/{group}', '\App\Http\Controllers\GroupController@show')->middleware('can:owner,group');
        Route::delete('/groups/{group}', '\App\Http\Controllers\GroupController@delete')->middleware('can:owner,group');
        Route::delete('/groups', '\App\Http\Controllers\GroupController@multi_delete');
        Route::patch('/groups/{group}', '\App\Http\Controllers\GroupController@update')->middleware('can:owner,group');
        Route::patch('/trades/{tradeSchool}', '\App\Http\Controllers\TradeSchoolController@update')->middleware('can:owner,tradeSchool');
        Route::delete('/trades/{tradeSchool}', '\App\Http\Controllers\TradeSchoolController@delete')->middleware('can:owner,tradeSchool');
        Route::delete('/trades', '\App\Http\Controllers\TradeSchoolController@multi_delete');

        Route::get('/trades/{tradeSchool}/users', '\App\Http\Controllers\UserTradeSchoolController@listAppliedStudents');
        Route::get('/trades/{tradeSchool}/users/saved', '\App\Http\Controllers\UserTradeSchoolController@listSavedStudents');
        Route::post('/messages', 'MessageController@store');
        Route::post('/scholarships/{scholarship}/reactive', '\App\Http\Controllers\ScholarshipController@reactive')->middleware('can:owner-scholarship,scholarship');

        Route::patch('/checklists/{checklist}', '\App\Http\Controllers\ChecklistController@update')->middleware('can:owner,checklist');
        Route::patch('/tasks/{task}', '\App\Http\Controllers\TaskController@update')->middleware('can:owner,task');
        Route::delete('/checklists/{checklist}', '\App\Http\Controllers\ChecklistController@delete')->middleware('can:owner,checklist');
        Route::post('/checklists/{checklist}/restore', '\App\Http\Controllers\ChecklistController@restore')->middleware('can:owner,checklist');
        Route::delete('/checklists', '\App\Http\Controllers\ChecklistController@multi_delete');
        Route::get('/checklists', '\App\Http\Controllers\ChecklistController@index');
        Route::get('/checklists/{checklist}/tasks', '\App\Http\Controllers\ChecklistController@getTasks');
        Route::delete('/tasks/{task}', '\App\Http\Controllers\TaskController@delete')->middleware('can:owner,task');
        Route::post('/tasks/{task}/restore', '\App\Http\Controllers\TaskController@restore')->middleware('can:owner,task');
        Route::delete('/tasks', '\App\Http\Controllers\TaskController@multi_delete');
        Route::delete('/jobs', '\App\Http\Controllers\CareerController@multi_delete');
    });

    Route::group(['middleware' => 'district_admin'], function () {
        Route::patch('/organizations/{organization}', '\App\Http\Controllers\OrganizationController@update');
        Route::get('/organizations/{organization}/schools', '\App\Http\Controllers\OrganizationController@getSchools');
    });

    Route::post('/users/{user}/enable', '\App\Http\Controllers\UserController@enable')->middleware('can:ourStudent,user');
    Route::post('/groups', '\App\Http\Controllers\GroupController@store');
    Route::get('/features/scholarships', '\App\Http\Controllers\ScholarshipController@listFeatureScholarships');
    Route::post('/scholarships', '\App\Http\Controllers\ScholarshipController@store');
    Route::get('/scholarships/{scholarship}', '\App\Http\Controllers\ScholarshipController@show');
    Route::get('/scholarships/{scholarship}/students', '\App\Http\Controllers\ScholarshipController@studentsCanSee');
    Route::get('/scholarships/{scholarship}/users', '\App\Http\Controllers\UserScholarshipController@listAppliedStudents');
    Route::get('/scholarships/{scholarship}/users/saved', '\App\Http\Controllers\UserScholarshipController@listSavedStudents');
    Route::post('/scholarships/{scholarship}/reminder', '\App\Http\Controllers\ScholarshipController@sendReminder');
    Route::get('/scholarships', '\App\Http\Controllers\ScholarshipController@index');

    Route::patch('/jobs/{job}', '\App\Http\Controllers\CareerController@update')->middleware('can:owner-job,job');
    Route::put('/jobs/{job}', '\App\Http\Controllers\CareerController@changeStatus')->middleware('can:approve-job,job');
    Route::post('/jobs/{job}/reactive', '\App\Http\Controllers\CareerController@reactive');

    Route::get('/features/jobs', '\App\Http\Controllers\CareerController@listFeatureJobs');
    Route::post('/jobs', '\App\Http\Controllers\CareerController@store');
    Route::get('/jobs', '\App\Http\Controllers\CareerController@index');
    Route::get('/jobs/{job}', '\App\Http\Controllers\CareerController@show');
    Route::get('/jobs/{job}/students', '\App\Http\Controllers\CareerController@studentsCanSee');
    Route::delete('/jobs/{job}', '\App\Http\Controllers\CareerController@delete')->middleware('can:owner,job');
    Route::group(['middleware' => 'ignoreRole:business_owner'], function () {
        Route::get('/jobs/{job}/users', '\App\Http\Controllers\UserJobController@listAppliedStudents');
        Route::get('/jobs/{job}/users/saved', '\App\Http\Controllers\UserJobController@listSavedStudents');
    });
    Route::post('/jobs/{job}/reminder', '\App\Http\Controllers\CareerController@sendReminder');

    Route::group(['middleware' => 'admin'], function () {
        Route::post('/businesses', '\App\Http\Controllers\BusinessesController@store');
    });
    Route::patch('/businesses/{business}', '\App\Http\Controllers\BusinessesController@update')->middleware('can:update-ownerBiz,business');
    Route::get('/businesses', '\App\Http\Controllers\BusinessesController@index');
    Route::get('/businesses/pathways', 'BusinessesController@getBusinessByPathway');
    Route::post('/users/{user}/follow/businesses/{business}', '\App\Http\Controllers\UserBusinessController@follow');
    Route::get('/users/{id}/follow/businesses', '\App\Http\Controllers\UserBusinessController@listFollow');
    Route::delete('/users/{user}/follow/businesses/{business}', '\App\Http\Controllers\UserBusinessController@unFollow');
    Route::get('/businesses/{business}/users/followed', '\App\Http\Controllers\UserBusinessController@listFollowedBusinesses');
    Route::post('/businesses/{user}/businesses/invitation', '\App\Http\Controllers\InviteController@employerInviteEmployer');

    Route::get('/parents', '\App\Http\Controllers\ParentController@index');
    Route::patch('/parents/{parent}', '\App\Http\Controllers\ParentController@update')->middleware('can:ownerBiz,parent');
    Route::get('/parents/{parent}/childrens', '\App\Http\Controllers\ParentController@getChildrens');

    Route::get('/district-admins', '\App\Http\Controllers\DistrictAdminController@index');
    Route::patch('/district-admins/{admin}', '\App\Http\Controllers\DistrictAdminController@update')->middleware('can:ownerBiz,admin');
    Route::get('/districts/{district}/schools', '\App\Http\Controllers\DistrictAdminController@getSchools');
    Route::get('/districts/{district}/students', '\App\Http\Controllers\DistrictAdminController@getStudents');
    Route::get('/districts/{district}/teachers', '\App\Http\Controllers\DistrictAdminController@getTeachers');
    Route::get('/districts/{district}/counselors', '\App\Http\Controllers\DistrictAdminController@getCounselors');
    Route::get('/districts/{district}/businesses', '\App\Http\Controllers\DistrictAdminController@getBizs');

    Route::post('/teachers', '\App\Http\Controllers\TeacherController@store');
    Route::get('/teachers', '\App\Http\Controllers\TeacherController@index');
    Route::patch('/teachers/{teacher}', '\App\Http\Controllers\TeacherController@update')->middleware('can:ownerBiz,teacher');

    Route::post('/tools/crawler', '\App\Http\Controllers\ToolController@crawl');
    Route::post('/uploads', '\App\Http\Controllers\ImageUploadController@upload');
    Route::post('/schools/import', 'ImportFileController@importSchools');
    Route::post('/trades/import', 'ImportFileController@importTrades');
    Route::post('/jobs/import', 'ImportFileController@importJobs');
    Route::post('/scholarships/import', 'ImportFileController@importScholarships');
    // Import Career Clusters
    Route::post('/clusters/import','ImportFileController@importClusters');

    Route::get('/messages', 'MessageController@index');
    Route::post('/messages', 'MessageController@store');
    Route::delete('/messages/{message}', 'MessageController@delete');
    Route::delete('/messages', 'MessageController@multi_delete');
    Route::get('/messages/{id}', 'MessageController@show');

    // Career Clusters
    Route::get('/clusters', 'CareerClusterController@index');
    Route::post('/clusters', 'CareerClusterController@store');
    Route::get('/clusters/{careerCluster}', 'CareerClusterController@show');
    Route::patch('/clusters/{careerCluster}', 'CareerClusterController@update');
    Route::delete('/clusters/{careerCluster}', 'CareerClusterController@destroy');
    Route::post('/clusters/match-score', 'CareerClusterController@matchScore');

    //State
    Route::get('/states', 'StateController@index');
    Route::get('/states/{state}', 'StateController@show');

    //pathway
    Route::group(['middleware' => 'pathwayRole'], function () {
        Route::post('/pathways', 'PathwayController@store');
        Route::patch('/pathways/{pathway}', 'PathwayController@update');
        Route::delete('/pathways', '\App\Http\Controllers\PathwayController@multiDelete');
    });
    Route::get('/pathways/{pathway}', 'PathwayController@show');
    Route::get('/pathways', 'PathwayController@index');
    Route::get('/pathways/{pathway}/jobs', '\App\Http\Controllers\PathwayController@getJobs');
    Route::post('/users/{user}/pathways/{pathway}', '\App\Http\Controllers\UserPathwayController@store');
    Route::get('/users/{id}/pathways', '\App\Http\Controllers\UserPathwayController@index');
    Route::delete('/users/{user}/pathways/{pathway}', '\App\Http\Controllers\UserPathwayController@delete');
    Route::get('/pathways/{pathway}/users/saved', '\App\Http\Controllers\UserPathwayController@listSavedStudents');
});

Route::group(['prefix' => 'v2', 'middleware' => ['jwt.auth']], function () {
    Route::post('/users/{user}/jobs/{job}/applying', '\App\Http\Controllers\UserJobController@storeApply_v2');
    Route::get('/users/waiting-approve', '\App\Http\Controllers\UserController@listUserWaiting');
    Route::get('/users/applying-job', '\App\Http\Controllers\UserController@listUserApplying');
    Route::post('/confirm/applying', '\App\Http\Controllers\UserController@approveJob');
    Route::delete('/confirm/applying', '\App\Http\Controllers\UserController@disapproveJob');
});
