[![Build master Status](http://45.79.141.74:8080/buildStatus/icon?job=cued-in/cued-in-api-ci-master)](http://45.79.141.74:8080/job/cued-in/view/CI%20status%20build/job/cued-in-api-ci-master/)
[![Build master Status](http://45.79.141.74:8080/buildStatus/icon?job=cued-in/cued-in-api-ci-staging)](http://45.79.141.74:8080/job/cued-in/view/CI%20status%20build/job/cued-in-api-ci-staging/)
[![Build develop Status](http://45.79.141.74:8080/buildStatus/icon?job=cued-in/cued-in-api-ci-dev)](http://45.79.141.74:8080/job/cued-in/view/CI%20status%20build/job/cued-in-api-ci-dev/)
-----

## Setup ##
1. `composer install`
2. Update `.env` file for database connection
3. `composer update`


## RUN

  ```
  docker-compose up -d api
  docker-compose exec api composer migrate
  ```

  For dev env first install
  ```
  docker-compose exec api php artisan db:seed --class DatabaseDevSeeder
  ```


## Run test ##
  in container
  ```
  vendor/bin/phpunit
  ```
or in host
  ```
  docker-compose run --rm test vendor/bin/phpunit
  ```
