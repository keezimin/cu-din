<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PushNotificationAll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $text;
    protected $data;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($text, $data = null, $url = null)
    {
        $this->text = $text;
        $this->data = $data;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $contents = [
                'en' => $this->text,
            ];

            $params = [
                'contents' => $contents,
                'included_segments' => ['All'],
            ];

            if (isset($this->url)) {
                $params['url'] = $this->url;
            }

            if (isset($this->data)) {
                $params['data'] = $this->data;
            }

            $params['ios_badgeType'] = 'Increase';
            $params['ios_badgeCount'] = '1';

            $result = \OneSignal::sendNotificationCustom($params);
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
