<?php

namespace App\Jobs;

use \App\Models\Device;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $userId;
    public $text;
    public $data;
    public $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $text, $data = null, $url = null)
    {
        $this->userId = $userId;
        $this->text = $text;
        $this->data = $data;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $devices = Device::where('user_id', $this->userId)->where('is_logging_in', true)->pluck('uuid')->all();

        foreach ($devices as $value) {
            try {
                $contents = [
                    'en' => $this->text,
                ];

                $params = [
                    'contents' => $contents,
                    'include_player_ids' => [$value],
                ];

                if (isset($this->url)) {
                    $params['url'] = $this->url;
                }

                if (isset($this->data)) {
                    $params['data'] = $this->data;
                }

                $params['ios_badgeType'] = 'Increase';
                $params['ios_badgeCount'] = '1';

                \OneSignal::sendNotificationCustom($params);
            } catch (\Exception $e) {
                \Log::error($e);
            }
        }
    }
}
