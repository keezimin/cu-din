<?php

namespace App\Jobs;

use App\Models\Task;
use App\Models\ReminderLog;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $task;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->task->owner) {
            ReminderLog::create([
                'task_id' => $this->task->id,
                'due_date' => $this->task->due_date,
                'status' => 1,
            ]);
            \Mail::send('emails.reminder', ['task' => $this->task], function ($message) {
                $checklistName = $this->task->checklist->name;
                $message->to($this->task->owner->email)->subject("Cued-In Reminder - {$checklistName}");
            });
        }
    }

/**
 * Handle a job failure.
 *
 * @return void
 */
    public function failed()
    {
        ReminderLog::where(['task_id' => $this->task->id, 'due_date' => $this->task->due_date])->update(['status' => 0]);
    }
}
