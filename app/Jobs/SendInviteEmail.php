<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendInviteEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    protected $url;
    protected $email;
    protected $ios_link;
    protected $android_link;
    protected $targets = [
        'student' => 'emails.invitation',
        'parent' => 'emails.parent_invitation',
        'employer' => 'emails.employer_invitation',
    ];
    protected $view;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $url, $email, $target = 'student')
    {
        $this->user = $user;
        $this->url = $url;
        $this->email = $email;
        $this->target = $target;
        $this->ios_link = env('URL_DOWNLOAD_IOS', '');
        $this->android_link = env('URL_DOWNLOAD_ANDROID', '');
        $this->getViews();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::send($this->view, [
            'user' => $this->user,
            'url' => $this->url,
            'ios_link' => $this->ios_link,
            'android_link' => $this->android_link,
        ], function ($message) {
            $message->to($this->email)->subject('Invitation!');
        });
    }

    private function getViews()
    {
        if (in_array($this->target, array_keys($this->targets))) {
            $this->view = $this->targets[$this->target];
        }
    }
}
