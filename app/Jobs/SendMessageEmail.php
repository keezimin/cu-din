<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendMessageEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    protected $receiver;
    protected $meta;
    protected $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $receiver, $meta, $subject)
    {
        $this->user = $user;
        $this->receiver = $receiver;
        $this->meta = $meta;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.message', ['user' => $this->user, 'receiver' => $this->receiver, 'meta' => $this->meta],
            function ($message) {
                $message->to($this->receiver->email)->subject($this->subject);
            }
        );
    }
}
