<?php

namespace App\Traits;

trait ScopeFilterFieldsTrail
{
    public $filterable = ['id'];

    public function scopeFilterFields($query, $params, $filterOptions = null)
    {
        if (empty($params)) {
            return $query;
        }
        $relationship = [];
        $onlyFilter = array_get($filterOptions, 'only');
        foreach ($params as $key => $value) {
            if (empty($value)) {
                continue;
            }
            $values = is_string($value) ? explode(',', $value) : $value;
            $options = null;
            if (strpos($key, '->$_')) {
                // parse json query
                $fields = explode('->$_', $key);
                $key = $fields[0];
                $options['path'] = $fields[1];
            }
            if (in_array($key, $this->fillable) || in_array($key, $this->filterable)) {
                $canFilter = true;
                if ($onlyFilter && !in_array($key, $onlyFilter)) {
                    $canFilter = false;
                }
                if ($canFilter) {
                    $this->filterField($query, $key, $values, $this->getTable(), $options);
                }
            } elseif (strpos($key, '_')) {
                $relate = explode('_', $key, 2);

                if (in_array($relate[1], $this->fillable) || in_array($relate[1], $this->filterable)) {
                    try {
                        $this->$relate[0]()->getRelated();
                    } catch (\Exception $e) {
                        continue;
                    }
                    $relationship[$relate[0]][$relate[1]] = $values;
                } else {
                    $relate = explode('_', $key, 3);
                    try {
                        $this->$relate[0]()->getRelated();
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }
        foreach ($relationship as $name => $keys) {
            $query = $query->with($name);
            if ($this->$name() instanceof \Illuminate\Database\Eloquent\Relations\BelongsToMany) {
                $table = $this->$name()->getTable();
            } else {
                $table = $this->$name()->getRelated()->getTable();
            }
            $query->whereHas($name, function ($query) use ($keys, $table) {
                foreach ($keys as $key => $values) {
                    $this->filterField($query, $key, $values, $table);
                }
            });
        }

        return $query;
    }

    private function filterField($query, $key, $values, $table = null, $options = null)
    {
        if ($key == 'status') {
            $query->statusWhere($values[0]);

            return $query;
        }
        if ($table) {
            $key = $table . '.' . $key;
        }

        if (count($values) == 1 && (starts_with($values[0], '%') || starts_with($values[0], '*'))) {
            $values[0] = str_replace('*', '%', $values[0]);
            $query->where($key, 'like', $values[0]);

            return $query;
        }

        $range = "/(?'operate'<>|>=|<=|>|<|=)(?'value'.*)/";
        $multiRange = "/(?'operate'<>|>=|<=|>|<|=)(?'value'.*),(?'operate2'<>|>=|<=|>|<|=)(?'value2'.*)/";

        $rangeMatches = [];
        $multiRangeMatches = [];
        foreach ($values as $value) {
            if (preg_match_all($multiRange, $value, $matches, PREG_SET_ORDER)) {
                // or mutil range like this: ?salon_rating[]=>=2,<3&salon_rating[]=>=4,<5
                $multiRangeMatches[] = $matches;
            } elseif (preg_match_all($range, $value, $matches, PREG_SET_ORDER)) {
                // check if input format have only range like this
                // ?salon_rating=>=2,<=4
                // or salon_rating[]=>=2,<=4
                $rangeMatches[] = $matches;
            }
        }
        if ($options) {
            $field = \DB::raw("{$key}->'$.{$options['path']}'");
            $query->where(function ($query) use ($field, $values) {
                foreach ($values as $value) {
                    if (is_int($value)) {
                        $query->orwhere(function ($query) use ($field, $value) {
                            $query->orwhere($field, \DB::raw($value));
                            $query->orwhere($field, \DB::raw("'$value'"));
                        });
                    } else {
                        if ($value === 'null') {
                            $query->orwhereNull($field);
                        } else {
                            $query->orwhere($field, $value);
                        }
                    }
                }
            });

            return $query;
        }
        if (!empty($rangeMatches)) {
            foreach ($rangeMatches as $range) {
                $query->where($key, $range[0]['operate'], $range[0]['value']);
            }
        } elseif (!empty($multiRangeMatches)) {
            $query->where(function ($query) use ($key, $multiRangeMatches) {
                foreach ($multiRangeMatches as $ranges) {
                    $query->orWhere(function ($query) use ($key, $ranges) {
                        $query->where($key, $ranges[0]['operate'], $ranges[0]['value']);
                        $query->where($key, $ranges[0]['operate2'], $ranges[0]['value2']);
                    });
                }
            });
        } else {
            $query->whereIn($key, $values);
        }
    }

    /**
     * This determines the foreign key relations automatically to prevent the need to figure out the columns.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string $relation_name
     * @param string $operator
     * @param string $type
     * @param bool   $where
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeModelJoin($query, $relation_name, $operator = '=', $type = 'left', $where = false)
    {
        $mainTable = $this->getTable();
        $relation = $this->$relation_name();
        $relationTable = $relation->getRelated()->getTable();
        $table = $relationTable . ' as ' . $relation_name;
        $one = $relation->getQualifiedForeignKey();
        $two = $relation_name . '.' . $relation->getRelated()->getKeyName();
        if (empty($query->columns)) {
            foreach (\Schema::getColumnListing($mainTable) as $column) {
                $query->addSelect(new \Illuminate\Database\Query\Expression("`$mainTable`.`$column` AS `$column`"));
            }
        }
        foreach (\Schema::getColumnListing($relationTable) as $column) {
            $query->addSelect(new \Illuminate\Database\Query\Expression("`$relation_name`.`$column` AS `{$relation_name}.$column`"));
        }
        // $query->addSelect(new \Illuminate\Database\Query\Expression("$relation_name.*"));

        return $query->join($table, $one, $operator, $two, $type, $where);
    }
}
