<?php

namespace App\Traits;

use App\Models\Message;

trait ScopeQuerySearchTrait
{

    public function scopeSearchUser($query, $keyword)
    {
        if (!$keyword) {
            return $query;
        }

        $query->where(function ($query) use ($keyword) {
            $query->orWhere('fullname', 'like', \DB::raw("'%{$keyword}%'"));
            $query->orWhere('email', 'like', \DB::raw("'%{$keyword}%'"));
            $query->orWhere(function($q) use ($keyword) {
                $q->whereHas('profile', function($q) use ($keyword) {
                    $q->where('contact_person', '!=', null);
                    $q->where('contact_person', 'like', \DB::raw("'%{$keyword}%'"));
                });
            });
        });

        return $query;
    }
}
