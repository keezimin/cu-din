<?php

namespace App\Traits;

trait FileNameTrait
{
    /**
     * Update application_form_filename
     * @param array attributes
     * @return string
     */
    public function getApplicationFormFilenameAttribute()
    {
        return $this->attributes['application_form_filename'] ? $this->convertName($this->attributes['application_form_filename']) : null;
    }

    /**
     * parse file name
     * @param string string need to parse
     * @return string
     */
    private function convertName($string)
    {
        return preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $string);
    }
}
