<?php

namespace App\Traits;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exception\HttpResponseException;
use App\Exceptions\EmailExistException;
use App\Exceptions\PathwayMaxException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\UnprocessableEntityException;

trait ExceptionRenderTrait
{

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $data = [
            'status' => $e instanceof HttpException ? $e->getStatusCode() : 500,
            'title' => 'Internal Server Error',
            'errors' => [[
                'title' => 'Internal Server Error',
                'detail' => $e->getMessage()?:('An exception of '.get_class_name($e)),
            ]]
        ];

        if ($e instanceof \Illuminate\Auth\Access\AuthorizationException) {
            $data = array_merge($data, [
                'status' => 403,
                'errors' => [[
                    'title' => 'Permission denied.',
                    'detail' => $e->getMessage(),
                ]]
            ]);
        }

        if ($e instanceof \Illuminate\Validation\UnauthorizedException) {
            $data = array_merge($data, [
                'status' => 401,
                'errors' => [[
                    'title' => 'Authenticate Error',
                    'detail' => $e->getMessage() ?: 'Unauthenticated',
                ]]
            ]);
        }

        if ($e instanceof ModelNotFoundException) {
            $data = array_merge($data, [
                'status' => 404,
                'errors' => [[
                    'title' => 'Not Found Error',
                    'detail' => 'Not Found Model',
                ]]
            ]);
        }

        if ($e instanceof HttpException) {
            $data = array_merge($data, [
                'status' => $e->getStatusCode(),
                'errors' => [[
                    'title' => 'Not Found Error',
                    'detail' => $e->getMessage()?:('An exception of '.get_class_name($e)),
                ]]
            ]);
        }

        if ($e instanceof EmailExistException) {
            $data = array_merge($data, [
                'status' => 400,
                'title' => 'Validation Error',
            ]);

            $errors[] = [
                'title'     => $data['title'],
                'detail'    => 'The email has already been taken.',
                'source' => [
                    'pointer' => 'email'
                ],
                'user_id' => $e->user_id,
                'role' => $e->role,
            ];
            $data['errors'] = $errors;
        }

        if ($e instanceof PathwayMaxException) {
            $data = array_merge($data, [
                'status' => 400,
                'title' => 'Validation Error',
            ]);

            $errors[] = [
                'title' => $data['title'],
                'detail' => 'Exceeding Pathways - the maximum number of pathways is 3. If you would like to obtain more pathways, please contact us at info@cued-in.com',
                'source' => [
                    'pointer' => 'pathway'
                ],
            ];
            $data['errors'] = $errors;
        }

        if ($e instanceof HttpResponseException) {
            $data = array_merge($data, [
                'status' => $e->getResponse()->status(),
                'title' => 'Validation Error',
            ]);

            $errorResponses = function ($errors) use ($data) {
                foreach ($errors as $key => $error) {
                    if (!is_array($error)) {
                        $errorResponses[] = [
                            'title'     => 'Bad Request',
                            'detail'    => $error,
                        ];
                    } else {
                        foreach ($error as $detail) {
                            $errorResponses[] = [
                                'title'     => $data['title'],
                                'detail'    => $detail,
                                'source' => [
                                    'pointer' => $key
                                ]
                            ];
                        }
                    }
                }
                return $errorResponses;
            };
            $data['errors'] = $errorResponses((array)$e->getResponse()->getData());
        }

        if ($e instanceof \Illuminate\Validation\ValidationException) {
            $data = array_merge($data, [
                'status' => 400,
                'title' => 'Validation Error',
            ]);

            $errorResponses = function ($errors) use ($data) {
                foreach ($errors as $key => $error) {
                    if (!is_array($error)) {
                        $errorResponses[] = [
                            'title'     => 'Bad Request',
                            'detail'    => $error,
                        ];
                    } else {
                        foreach ($error as $detail) {
                            $errorResponses[] = [
                                'title'     => $data['title'],
                                'detail'    => $detail,
                                'source' => [
                                    'pointer' => $key
                                ]
                            ];
                        }
                    }
                }
                return $errorResponses;
            };
            $data['errors'] = $errorResponses($e->validator->errors()->toArray());
        }

        if ($e instanceof UnprocessableEntityException) {
            $data = array_merge($data, [
                'status' => 422,
                'errors' => [[
                    'title' => 'Unprocessable Entity',
                    'detail' => $e->getMessage(),
                ]]
            ]);
        }
        return response()->json(arrayView('errors.exception', $data), $data['status']);
    }
}
