<?php

namespace App\Traits;

trait DateTrait
{
    /**
     * Update created_at
     * @param array attributes
     * @return string
     */
    public function getCreatedAttribute()
    {
        return $this->created_at ? $this->created_at->toIso8601String() : '';
    }

    /**
     * Update update_at
     * @param array attributes
     * @return string
     */
    public function getUpdatedAttribute()
    {
        return $this->updated_at ? $this->updated_at->toIso8601String() : '';
    }

    /**
     * Update update_at
     * @param array attributes
     * @return string
     */
    public function getDeletedAttribute()
    {
        return $this->deleted_at ? $this->deleted_at->toIso8601String() : '';
    }
}
