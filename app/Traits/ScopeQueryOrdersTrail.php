<?php

namespace App\Traits;

trait ScopeQueryOrdersTrail
{
    public $sortable = [];

    public function scopeQueryOrders($query, $cols, $prefix = null, $filterOptions = null)
    {
        $onlyFilter = array_get($filterOptions, 'only') ?? [];
        $ordered = false;
        $canSort = true;
        foreach ($cols as $col) {
            $col = trim($col);
            if (substr($col, 0, 1) == '-') {
                $dir = 'desc';
                $col = substr($col, 1);
            } elseif (substr($col, 0, 1) == '+') {
                $dir = 'asc';
                $col = substr($col, 1);
            } else {
                $dir = 'asc';
            }

            $options = null;
            if (strpos($col, '->$.')) {
                // parse json query
                $fields = explode('->$.', $col);
                $col = $fields[0];
                $options['path'] = $fields[1];
            }
            if (in_array($col, $this->fillable) || $col == 'id' || in_array($col, $this->sortable) || ($col && in_array($col, $onlyFilter))) {
                $canSort = true;
                if ($onlyFilter && !in_array($col, $onlyFilter)) {
                    $canSort = false;
                }
                if ($canSort) {
                    if ($prefix) {
                        $col = $prefix.'.'.$col;
                    }
                    if ($options) {
                        $query->orderBy(\DB::raw("{$col}->'$.{$options['path']}'"), $dir);
                    } else {
                        $query->orderBy($col, $dir);
                    }

                    $ordered = true;
                }
            } else {
                $relate = explode('.', $col, 2);
                if ($relate && method_exists($this, $relate[0])) {
                    $query->orderBy($col, $dir);
                    $ordered = true;
                }
            }
        }
        if (!$ordered && $canSort) {
            $query->orderBy($this->getTable().'.id', 'desc');
        }

        return $query;
    }
}
