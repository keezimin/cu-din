<?php

namespace App\Traits;

use App\Models\Message;

trait ScopeQueryByRoleTrait
{

    public function scopeFilterUserByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case $currentUser->isCounselor():
                $query->whereDoesntHave('roles')->where(['school_id' => $currentUser->school_id]);
                break;
            case $currentUser->hasRole('super_admin'):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->whereIn('school_id', function ($q) use ($currentUser) {
                    $q->select('schools.id')->from('schools')
                        ->where('schools.district_id', $currentUser->district_id);
                });
                break;
            default:
                if ($this->getTable() !== 'users') {
                    $query->where(['user_id' => 0]); // need to update this line
                }
                break;
        }

        return $query;
    }

    public function scopeFilterJobByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(function ($query) use ($currentUser) {
                    $query->orWhere(['owner_id' => $currentUser->id]);
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->whereHas('schools', function ($q) use ($currentUser) {
                            $q->where(['school_id' => $currentUser->school_id]);
                        });
                    });
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_state', true);
                        $q->whereNotNull('state_id');
                        $q->where('state_id', $currentUser->school->district->state_id);
                    });
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_county', true);
                        $q->whereHas('districts', function ($q) use ($currentUser) {
                            $q->where('districts.id', $currentUser->school->district_id);
                        });
                    });
                });
                break;
            case in_array('business_owner', $currentUser->getRoles()):
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($query) use ($currentUser) {
                    $query->orWhere(['owner_id' => $currentUser->id]);
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->whereHas('districts', function ($q) use ($currentUser) {
                            $q->where('districts.id', $currentUser->district_id);
                        });
                    });
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_state', true);
                        $q->whereNotNull('state_id');
                        $q->where('state_id', $currentUser->district->state_id);
                    });
                });
                break;
            default:
                $query->where(function ($query) use ($currentUser) {
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->whereHas('schools', function ($q) use ($currentUser) {
                            $q->where(['school_id' => $currentUser->school_id]);
                        });
                    });
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_state', true);
                        $q->whereNotNull('state_id');
                        $q->where('state_id', $currentUser->school->district->state_id);
                    });
                    $query->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_county', true);
                        $q->whereHas('districts', function ($q) use ($currentUser) {
                            $q->where('districts.id', $currentUser->school->district_id);
                        });
                    });
                });

                // $query->where(['user_id' => 0]);
                break;
        }

        return $query;
    }

    public function scopeFilterScholarshipByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['user_id' => $currentUser->id]);
                    $q->orWhere(['school_id' => $currentUser->school_id]);
                });
                break;
            case in_array('business_owner', $currentUser->getRoles()):
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['user_id' => $currentUser->id]);
                    $q->orWhereIn('school_id', function ($q) use ($currentUser) {
                        $q->select('schools.id')->from('schools')
                            ->where('schools.district_id', $currentUser->district_id);
                    });
                });
                break;
            default:
                $query->where(['user_id' => 0]);
                break;
        }

        return $query;
    }

    public function scopeV2FilterScholarshipByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['user_id' => $currentUser->id]);
                    $q->orWhere(function ($q) use ($currentUser) {
                        $q->whereHas('schools', function ($q) use ($currentUser) {
                            $q->where(['school_id' => $currentUser->school_id]);
                        });
                    });
                    $q->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_state', true);
                        $q->whereNotNull('state_id');
                        $q->where('state_id', $currentUser->school->district->state_id);
                    });
                    $q->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_county', true);
                        $q->whereHas('districts', function ($q) use ($currentUser) {
                            $q->where('districts.id', $currentUser->school->district_id);
                        });
                    });
                });
                break;
            case in_array('business_owner', $currentUser->getRoles()):
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['user_id' => $currentUser->id]);
                    $q->orWhere(function ($q) use ($currentUser) {
                        $q->whereHas('districts', function ($q) use ($currentUser) {
                            $q->where('districts.id', $currentUser->district_id);
                        });
                    });
                    $q->orWhere(function ($q) use ($currentUser) {
                        $q->where('is_applied_to_state', true);
                        $q->whereNotNull('state_id');
                        $q->where('state_id', $currentUser->district->state_id);
                    });
                });
                break;
            default:
                $query->where(['user_id' => 0]);
                break;
        }

        return $query;
    }

    public function scopeFilterItemsByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(['owner_id' => $currentUser->id]);
                break;
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['owner_id' => $currentUser->id]);
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->where('schools.district_id', $currentUser->district_id)
                            ->where('roles.name', '<>', 'super_admin');
                    });
                });
                break;
            default:
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->where('roles.name', 'super_admin');
                    });
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->where('roles.name', 'counselor')
                            ->where('users.school_id', $currentUser->school_id);
                    });
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('roles', 'role_user.role_id', '=', 'roles.id')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->where('roles.name', 'district_admin')
                            ->where('schools.district_id', 'users.district_id')
                            ->where('schools.id', $currentUser->school_id);
                    });
                });
                break;
        }

        return $query;
    }

    public function scopeFilterGroupByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(['owner_id' => $currentUser->id]);
                break;
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['owner_id' => $currentUser->id]);
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->where('schools.district_id', $currentUser->district_id);
                    });
                });
                break;
            default:
                $query;
                break;
        }

        return $query;
    }

    public function scopeFilterTradeByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(['owner_id' => $currentUser->id]);
                break;
            case in_array('super_admin', $currentUser->getRoles()):
                $query;
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->orWhere(['owner_id' => $currentUser->id]);
                    $q->orWhereIn('owner_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->where('schools.district_id', $currentUser->district_id);
                    });
                });
                break;
            default:
                $query;
                break;
        }

        return $query;
    }

    public function scopeFilterMessageByRole($query)
    {
        $currentUser = \Auth::user();

        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->where(['actor_id' => $currentUser->id, 'status' => Message::PUBLISH]);
                break;
            case in_array('super_admin', $currentUser->getRoles()):
                $query->where(['status' => Message::PUBLISH]);
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(['status' => Message::PUBLISH])
                    ->where(function ($q) use ($currentUser) {
                        $q->orWhere(['actor_id' => $currentUser->id]);
                        $q->orWhereIn('actor_id', function ($q) use ($currentUser) {
                            $q->select('users.id')
                                ->from('users')
                                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                                ->join('schools', 'users.school_id', '=', 'schools.id')
                                ->where('schools.district_id', $currentUser->district_id);
                        });
                    });
                break;
            default:
                $query;
                break;
        }

        return $query;
    }

    public function scopeFilterActivityByRole($query)
    {
        $currentUser = \Auth::user();
        switch (true) {
            case in_array('counselor', $currentUser->getRoles()):
                $query->whereIn('actor_id', function ($q) use ($currentUser) {
                    $q->select('users.id')
                        ->from('users')
                        ->join('schools', 'users.school_id', '=', 'schools.id')
                        ->where('schools.id', $currentUser->school_id);
                });
                break;
            case $currentUser->isDistrictAdmin():
                $query->where(function ($q) use ($currentUser) {
                    $q->whereIn('actor_id', function ($q) use ($currentUser) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->where('schools.district_id', $currentUser->district_id);
                    });
                });
                break;
            case in_array('business_owner', $currentUser->getRoles()):
                $query->where(['actor_id' => $currentUser->id]);
                break;
            default:
                $query;
                break;
        }

        return $query;
    }
}
