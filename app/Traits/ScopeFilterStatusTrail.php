<?php

namespace App\Traits;

trait ScopeFilterStatusTrail
{
    public function scopeStatusWhere($query, $value)
    {
        if ($value == null) {
            return $query;
        }
        $value = explode('!', $value);
        if (count($value) == 1) {
            return $query->whereRaw($this->getTable().'.status & ?', [$value[0]]);
        } else {
            return $query->whereRaw('not '.$this->getTable().'.status & ?', [$value[1]]);
        }
    }
}
