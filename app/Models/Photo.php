<?php

namespace App\Models;

class Photo extends BaseModel
{

    public $fillable = [
        'profile_id', 'photo',
    ];
}
