<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends BaseModel
{
    use SoftDeletes;

    const APPROVED = 1;

    protected $dates = [
        'deleted_at',
    ];

    protected $table = 'career_jobs';

    public $fillable = [
        'name', 'organization', 'type', 'description', 'application_form_filename', 'application_form_file',
        'application_form_link', 'user_id', 'location', 'status', 'volunteer_hours', 'owner_id', 'deadline_on',
        'career_cluster_id', 'occupation_id', 'deleted_at', 'is_applied_to_state', 'state_id', 'is_applied_to_county', 'pathway_id', 'is_assign_to_pathway',
        'updated_at',
    ];

    public static $enumTypes = [
        'job_shadowing', 'mentorship', 'service_learning', 'internship', 'clinical_cooperative_education',
        'cooperative_education', 'youth_registered_apprenticeship', 'registered_apprenticeships', 'general_employment',
        'volunteer', 'school_based_enterprise', 'externship', 'entrepreneurship', 'clinical_experience', 'speaking_engagement', 'field_trip'
    ];

    public $filterable = ['id', 'user_id', 'name', 'organization', 'type', 'description', 'location', 'owner_id', 'career_cluster_id', 'occupation_id'];

    public $sortable = ['id', 'name', 'career_cluster_id', 'occupation_id', 'updated_at', 'created_at'];

    public function setDeadlineOnAttribute($value) {
        if ($value) {
            $this->attributes['deadline_on'] = (new \Carbon\Carbon($value))->format('Y-m-d');
        }
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'object_type' => 'Job',
        ];
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, 'schools_jobs', 'career_job_id', 'school_id');
    }

    public function districts()
    {
        return $this->belongsToMany(\App\Models\District::class, 'districts_career_jobs', 'career_job_id', 'district_id');
    }

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id');
    }

    public function activities()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Job')
            ->ofVerbs(['save', 'apply']);
    }

    public function applies()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Job')
            ->ofVerbs(['apply']);
    }

    public function savedActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Job')
            ->ofVerbs(['save']);
    }

    public function appliedUsers()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Job')
                ->where('activities.object_id', $id);
        });
    }

    public function applying()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Job')
                ->where('activities.object_id', $id);
        })->count();
    }

    public function careerCluster()
    {
        return $this->belongsTo(CareerCluster::class, 'career_cluster_id');
    }

    public function occupation()
    {
        return $this->belongsTo(Occupation::class, 'occupation_id');
    }

    public function pathway()
    {
        return $this->belongsTo(\App\Models\Pathway::class);
    }
}
