<?php

namespace App\Models;

class Category extends BaseModel
{
    public $fillable = [
        'name', 'description',
    ];

    public function documents()
    {
        return $this->belongsTo(\App\Models\Document::class);
    }
}
