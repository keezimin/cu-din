<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use \App\Traits\DateTrait;
    use \App\Traits\FileNameTrait;
    use \App\Traits\ScopeFilterFieldsTrail;
    use \App\Traits\ScopeQueryOrdersTrail;
    use \App\Traits\ScopeQueryByRoleTrait;
    use \App\Traits\ScopeQuerySearchTrait;

    /**
     * Convert id to string
     * @param array attributes
     * @return string
     */
    public function getIdAttribute()
    {
        return empty($this->attributes['id']) ? null : (string) $this->attributes['id'];
    }


    public function setUpdatedAtAttribute($value) {
        $this->attributes['updated_at'] = now();
    }
}
