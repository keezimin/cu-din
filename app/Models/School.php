<?php

namespace App\Models;

class School extends BaseModel
{
    public $fillable = [
        'name', 'district_id', 'code', 'website', 'photo', 'photo_filename', 'organization_id'
    ];

    public function district()
    {
        return $this->belongsTo(\App\Models\District::class);
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Organization::class);
    }

    public function jobs()
    {
        return $this->belongsToMany(\App\Models\Job::class, 'schools_jobs', 'school_id', 'career_job_id');
    }

    public function scholarships()
    {
        return $this->belongsToMany(\App\Models\Scholarship::class, 'schools_scholarships', 'school_id', 'scholarship_id');
    }

    public function users()
    {
        return $this->hasMany(\App\User::class, 'school_id', 'id');
    }

    public function getCouselor()
    {
        $couselor = [];
        $users = $this->users()->get();
        foreach($users as $user) {
            if ($user->isCounselor()) {
                $couselor['fullname'] = $user->fullname;
                $couselor['email'] = $user->email;
            }
        }

        return $couselor;
    }
}
