<?php

namespace App\Models;

class TradeSchool extends BaseModel
{
    protected $table = 'trade_schools';

    public $fillable = [
        'name', 'phone', 'website', 'photo', 'email', 'description', 'application_form_filename',
        'application_form_file', 'application_form_link', 'district_id', 'owner_id', 'photo_filename'
    ];

    public $filterable = ['id', 'name', 'email', 'description', 'district_id', 'owner_id'];

    public $sortable = ['id', 'name', 'updated_at', 'created_at'];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function district()
    {
        return $this->belongsTo(\App\Models\District::class, 'district_id');
    }

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'object_type' => 'TradeSchool',
        ];
    }

    public function activities()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'TradeSchool')
            ->ofVerbs(['save', 'apply']);
    }

    public function applies()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'TradeSchool')
            ->ofVerbs(['apply']);
    }

    public function savedActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'TradeSchool')
            ->ofVerbs(['save']);
    }

    public function appliedUsers()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'TradeSchool')
                ->where('activities.object_id', $id);
        });
    }

    public function applying()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'TradeSchool')
                ->where('activities.object_id', $id);
        })->count();
    }
}
