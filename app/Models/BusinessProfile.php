<?php

namespace App\Models;

class BusinessProfile extends BaseModel
{
    protected $table = 'business_profiles';

    protected $fillable = [
        'user_id', 'active', 'about', 'phone', 'culture', 'industry', 'type', 'comapny_size', 'founded', 'specialties', 'facebook', 'instagram', 'linkedin', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}