<?php

namespace App\Models;

class Survey extends BaseModel
{
    protected $table = "surveys";

    protected $fillable = [
        'survey_category_id', 'name', 'created_at', 'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo(\App\Models\SurveyCategory::class, 'survey_category_id');
    }

    public function careerClusters()
    {
        return $this->hasMany(\App\Models\SurveyCareerCluster::class, 'survey_id', 'id');
    }
}
