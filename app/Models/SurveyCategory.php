<?php

namespace App\Models;

class SurveyCategory extends BaseModel
{
    protected $table = 'survey_categories';
    
    public $fillable = [
        'name',
    ];
}
