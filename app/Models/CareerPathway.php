<?php

namespace App\Models;

class CareerPathway extends BaseModel
{
    protected $table = "career_pathways";

    protected $fillable = [
        'career_cluster_id', 'name', 'created_at', 'updated_at'
    ];
}
