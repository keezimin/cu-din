<?php

namespace App\Models;

class ReminderLog extends BaseModel
{

    public $fillable = [
        'task_id', 'status', 'due_date',
    ];
}
