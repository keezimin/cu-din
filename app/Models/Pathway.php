<?php

namespace App\Models;

class Pathway extends BaseModel
{
    protected $table = 'pathways';

    protected $fillable = [
        'name', 'short_description', 'career_cluster_id', 'active', 'about', 'skills','training', 'user_id', 'owner_id', 'created_at', 'updated_at', 'photo'
    ];

    public function careerCluster()
    {
        return $this->belongsTo(\App\Models\CareerCluster::class, 'career_cluster_id');
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'object_type' => get_class_name($this),
        ];
    }

    public function savedActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Pathway')
            ->ofVerbs(['save']);
    }

    public function activities()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Pathway')
            ->ofVerbs(['save', 'apply']);
    }
}
