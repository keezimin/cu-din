<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends BaseModel
{
    use SoftDeletes;

    public $fillable = [
        'name', 'checklist_id', 'due_date', 'owner_id',
    ];

    public $filterable = ['id', 'name', 'owner_id'];

    public $sortable = ['id', 'name', 'updated_at', 'created_at', 'due_date'];

    protected $dates = ['deleted_at'];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function checklist()
    {
        return $this->belongsTo(\App\Models\Checklist::class, 'checklist_id');
    }

    public function logs()
    {
        return $this->hasMany(\App\Models\ReminderLog::class);
    }
}
