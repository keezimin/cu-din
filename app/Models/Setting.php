<?php

namespace App\Models;

class Setting extends BaseModel
{
    const ENABLE_LOGIN = 1;
    const DISABLE_LOGIN = 0;

    public $fillable = [
        'key', 'value',
    ];

    public $timestamps = false;
}
