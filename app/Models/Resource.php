<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;

class Resource extends BaseModel
{

    const TYPE_NEWS = 'news';
    const TYPE_RESOURCE = 'resource';

    public $fillable = [
        'title', 'link', 'summary', 'image', 'type', 'owner_id', 'image_filename',
    ];

    public $sortable = ['updated_at'];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }
}
