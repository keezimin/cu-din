<?php

namespace App\Models;

class CareerCluster extends BaseModel
{
    protected $table = "career_clusters";

    protected $fillable = [
        'name', 'description', 'image', 'created_at', 'updated_at'
    ];

    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'user_carrer_clusters', 'career_cluster_id', 'user_id');
    }

    public function occupations()
    {
        return $this->hasMany('App\Models\Occupation', 'career_cluster_id');
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\CareerClusterPhoto::class, 'career_cluster_id');
    }

    public function getPhotos()
    {
        return $this->photos()->orderBy('career_cluster_id')->pluck('photo')->toArray();
    }
}
