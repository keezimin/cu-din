<?php

namespace App\Models;

class College extends BaseModel
{
    public $fillable = [
        'code', 'name',
    ];

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'code' => $this->code,
            'object_type' => get_class_name($this),
        ];
    }

    public function activities()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'College')
            ->ofVerbs(['save', 'apply']);
    }

    public function applies()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'College')
            ->ofVerbs(['apply']);
    }

    public function savedActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'College')
            ->ofVerbs(['save']);
    }

    public function appliedUsers()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'College')
                ->where('activities.object_id', $id);
        });
    }

    public function applying()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'College')
                ->where('activities.object_id', $id);
        })->count();
    }
}
