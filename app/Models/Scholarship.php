<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Scholarship extends BaseModel
{
    use SoftDeletes;

    const APPROVED = 1;

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name', 'organization', 'amount', 'min_price', 'max_price', 'deadline_on', 'quantity', 'description',
        'application_form_filename', 'application_form_file', 'application_form_link', 'user_id',
        'status', 'deleted_at', 'is_applied_to_state', 'state_id', 'is_applied_to_county', 'updated_at',
    ];

    public $filterable = ['id', 'user_id', 'name', 'organization', 'description', 'min_price', 'max_price'];

    public $sortable = ['updated_at'];

    public function setDeadlineOnAttribute($value) {
        if ($value) {
            $this->attributes['deadline_on'] = (new \Carbon\Carbon($value))->format('Y-m-d');
        }
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function schools()
    {
        return $this->belongsToMany(\App\Models\School::class, 'schools_scholarships', 'scholarship_id', 'school_id');
    }

    public function districts()
    {
        return $this->belongsToMany(\App\Models\District::class, 'districts_scholarships', 'scholarship_id', 'district_id');
    }

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id');
    }

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'object_type' => get_class_name($this),
        ];
    }

    public function activities()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Scholarship')
            ->ofVerbs(['save', 'apply']);
    }

    public function applies()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Scholarship')
            ->ofVerbs(['apply']);
    }

    public function savedActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'Scholarship')
            ->ofVerbs(['save']);
    }

    public function appliedUsers()
    {
        $id = $this->id;

        return \App\User::whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Scholarship')
                ->where('activities.object_id', $id);
        });
    }
}
