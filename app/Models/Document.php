<?php

namespace App\Models;

class Document extends BaseModel
{
    public $fillable = [
        'title',
        'description',
        'image',
        'content',
        'owner_id',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id');
    }
}
