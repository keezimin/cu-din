<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends BaseModel
{
    public $fillable = [
        'name', 'code', 'abbr',
    ];

    public function districts()
    {
        return $this->hasMany(\App\Models\District::class, 'state_id');
    }
}
