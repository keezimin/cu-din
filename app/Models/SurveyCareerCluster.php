<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyCareerCluster extends Model
{
    protected $table = 'survey_career_clusters';
    
    public $fillable = [
        'survey_id', 'career_cluster_id', 'point', 'created_at', 'updated_at'
    ];
}
