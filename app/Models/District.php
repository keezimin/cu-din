<?php

namespace App\Models;

class District extends BaseModel
{
    public $fillable = [
        'name', 'code', 'state_id',
    ];

    public function schools()
    {
        return $this->hasMany(\App\Models\School::class, 'district_id');
    }

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id', 'id');
    }
}
