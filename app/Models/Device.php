<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    protected $fillable = [
        'id', 'user_id', 'uuid', 'token', 'is_logging_in'
    ];
    public $timestamps = false;
}
