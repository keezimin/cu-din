<?php

namespace App\Models;

class UserCarrerCluster extends BaseModel
{
    protected $table = "user_carrer_clusters";

    protected $fillable = [
        'user_id', 'career_cluster_id', 'total_point', 'created_at', 'updated_at'
    ];
}
