<?php

namespace App\Models;

class Group extends BaseModel
{
    public $fillable = [
        'name', 'owner_id',
    ];

    public $filterable = ['id', 'name', 'owner_id'];

    public $sortable = ['id', 'name', 'updated_at', 'created_at'];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'group_users', 'group_id', 'user_id');
    }
}
