<?php

namespace App\Models;

class Profile extends BaseModel
{
    public $fillable = [
        'website_link',
        'description',
        'photo',
        'logo',
        'address',
        'contact_person',
        'location',
        'application_form_filename',
        'user_id',
        'subscription'
    ];

    public function district()
    {
        return $this->belongsTo(\App\Models\District::class);
    }

    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->toIso8601String();
    }
}
