<?php

namespace App\Models;

class Occupation extends BaseModel
{
    protected $table = "occupations";

    protected $fillable = [
        'career_cluster_id', 'career_pathway_id', 'name', 'video', 'code', 'occupation', 'average_salary','created_at', 'updated_at'
    ];

    public function careerCluster()
    {
        return $this->belongsTo('App\Models\CareerCluster', 'career_cluster_id');
    }
}
