<?php

namespace App\Models;

class CareerClusterPhoto extends BaseModel
{
    protected $table = 'photo_career_clusters';

    public $fillable = [
        'career_cluster_id', 'photo',
    ];
}
