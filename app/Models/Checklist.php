<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Checklist extends BaseModel
{
    use SoftDeletes;

    public $fillable = [
        'name', 'type', 'object_id', 'owner_id',
    ];

    public $filterable = ['id', 'name', 'owner_id'];

    public $sortable = ['id', 'name', 'updated_at', 'created_at'];

    protected $dates = ['deleted_at'];

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function object()
    {
        return $this->belongsTo($this->getModelByType(), 'object_id');
    }

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class)->orderBy('due_date', 'asc');
    }

    public function getModelByType()
    {
        if (!$this->type) {
            return \App\Models\College::class;
        }

        $definedType = [
            'colleges' => \App\Models\College::class,
            'scholarships' => \App\Models\Scholarship::class,
            'career_jobs' => \App\Models\Job::class,
            'trade_schools' => \App\Models\TradeSchool::class,
        ];

        return $definedType[$this->type];
    }
}
