<?php

namespace App\Models;

class Organization extends BaseModel
{
    public $fillable = [
        'name', 'user_id', 'district_id', 'updated_by'
    ];

    public function schools()
    {
        return $this->hasMany(\App\Models\School::class, 'organization_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function district()
    {
        return $this->belongsTo(\App\Models\District::class);
    }
}
