<?php

namespace App\Models;

class Invitation extends BaseModel
{
    public $fillable = [
        'fullname', 'email', 'user_id', 'registed_user_id', 'invited_at',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}
