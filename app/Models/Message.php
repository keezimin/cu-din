<?php

namespace App\Models;

class Message extends \PhpSoft\Activity\Models\Activity
{
    public $table = 'activities';

    const UNPUBLISH = 0;
    const PUBLISH = 1;

    public static $MAP_TARGET_MODELS = [
        'USER' => '\App\User',
        'DISTRICT' => '\App\Models\District',
        'SCHOOL' => '\App\Models\School',
        'GROUP' => '\App\Models\Group',
    ];

    public static $MAP_SOURCE_MODELS = [
        'SCHOLARSHIP' => '\App\Models\Scholarship',
        'JOB' => '\App\Models\Job',
        'RESOURCE' => '\App\Models\Resource',
        'TRADESCHOOL' => '\App\Models\TradeSchool',
        'COLLEGE' => '\App\Models\College',
    ];

    public function message()
    {
        return $this->hasOne('App\Models\Message', 'id', 'object_id')->where('object_type', 'Message');
    }

    public function sender()
    {
        return $this->belongsTo('App\User', 'actor_id');
    }

    public function targets()
    {
        $model = self::$MAP_TARGET_MODELS[array_get($this, 'meta.target.type')] ?? null;
        if (!$model) {
            return;
        }

        $query = $model::whereIn('id', array_get($this, 'meta.target.id', []));

        if (array_get($this, 'meta.target.type') === 'USER') {
            $query = $query->withTrashed();
        }

        return $query->get();
    }

    public function sources()
    {
        $model = self::$MAP_SOURCE_MODELS[array_get($this, 'meta.source.type')] ?? null;
        if (!$model) {
            return;
        }

        return $model::whereIn('id', array_get($this, 'meta.source.id', []))->get();
    }
}
