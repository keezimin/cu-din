<?php
namespace App\Services;

use \App\Models\State;
use \App\Models\District;
use \App\Models\TradeSchool;

class ImportTradeSchoolService extends BaseImportService
{
    protected $columns = [
        'county_name', 'county_id', 'name', 'email', 'phone', 'website', 'state_id',
        'state_name', 'state_abbr',
    ];

    protected $rules = [
        'rows' => 'required|array',
        'rows.*.county_id' => 'required|max:255',
        'rows.*.county_name' => 'required|max:255',
        'rows.*.name' => 'required|string',
        'rows.*.email' => 'string|email|nullable',
        'rows.*.website' => 'nullable|string|url',
        'rows.*.state_id'   => 'required|max:255',
        'rows.*.state_name' => 'required|string|max:255',
    ];

    protected function import()
    {
        $existents = [];

        foreach ($this->inputs as $row) {
            $row = (object) $row;
            $state = State::firstOrCreate(['code' => $row->state_id],
                    [
                         'name' => $row->state_name,
                         'abbr' => $row->state_abbr,
                     ]);

            $district = District::where('code', $row->county_id)->first();
            // case 1: tạo district khi district chưa có
            // case 2: update district khi district không thuộc state trong file excel
            if (!$district) {
                $district = District::create([
                    'name'     => $row->county_name,
                    'code'     => $row->county_id,
                    'state_id' => $state['id'],
                ]);
            } elseif ($district->state_id != $state->id) {
                $district->update([
                    'state_id' => $state['id'],
                ]);
            }

            $school = TradeSchool::where('name', $row->name)->first();
            // tạo TradeSchool khi TradeSchool chưa có
            if (!$school) {
                TradeSchool::create([
                    'name' => $row->name,
                    'district_id' => $district['id'],
                    'phone' => $row->phone,
                    'email' => $row->email,
                    'website' => $row->website,
                    'owner_id' => currentUser()->id,
                ]);
            } else {
                // update school khi school không thuộc district trong file excel
                if ($school->district_id != $district->id) {
                    $school->update([
                        'district_id' => $district['id'],
                    ]);
                }
                $existents[] = $row;
            }
        }

        return $existents;
    }
};
