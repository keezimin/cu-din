<?php
namespace App\Services;

class UserActionService
{
    public static function applyScholarship($user, $scholarship)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $scholarship->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);
        if (!$activity) {
            $meta = [
                'status' => 'applying',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];
            $activity = app('activity')->add($actor, $verb, $object, $target, $meta);
        } else {
            $meta = $activity->meta;
            $status = $meta['status'] ?? null;
            if (!$status) {
                $meta['status'] = 'applying';
                $meta['applied_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'applying') {
                $meta['status'] = 'completed';
                $meta['completed_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'completed') {
                $meta['status'] = '';
            }
            $activity->meta = $meta;
            $activity->save();
        }

        return $activity;
    }

    public static function applyJob($user, $job)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $job->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);
        if (!$activity) {
            $meta = [
                'status' => 'applying',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];
            $activity = app('activity')->add($actor, $verb, $object, $target, $meta);
        } else {
            $meta = $activity->meta;
            $status = $meta['status'] ?? null;
            if (!$status) {
                $meta['status'] = 'applying';
                $meta['applied_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'applying') {
                $meta['status'] = 'completed';
                $meta['completed_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'completed') {
                $meta['status'] = '';
            }
            $activity->meta = $meta;
            $activity->save();
        }

        return $activity;
    }

    public static function apply_v2($user, $model)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $model->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);

        $pushNotify = function () use ($actor, $target, $user, $model) {
            $metaRequest = [
                'type' => 'request',
                'status' => 'waiting',
                'source' => [
                    'id' => [$model->id],
                    'type' => strtoupper(get_class_name($model)),
                ],
                'title' => '{{SENDER}} sent you a request to apply to the {{OBJECT}}. If you have questions, contact the sender.',
            ];
            $text = $metaRequest['title'];
            $text = str_replace('{{SENDER}}', $user->fullname, $text);
            $text = str_replace('{{OBJECT}}', $model->name, $text);

            $object = (object) [
                'id' => '0',
                'object_type' => 'Message',
            ];

            $push = app('activity')->add($actor, 'say', $object, $target, $metaRequest);

            $ids = \App\User::where(function ($q) use ($user) {
                $q->orWhere(function ($q) use ($user) {
                    $q->where('school_id', $user->school_id);
                    $q->withRole('counselor');
                });
                $q->orWhere(function ($q) use ($user) {
                    $q->withRole('district_admin');
                    $q->where('users.district_id', function ($q) use ($user) {
                        $q->select('schools.district_id')
                            ->from('schools')
                            ->where('schools.id', $user->school_id);
                    });
                });
            })->pluck('id');

            $object->id = $push->id;

            foreach ($ids as $id) {
                $target = (object) [
                    'id' => $id,
                    'target_type' => 'User',
                ];

                $m = app('activity')->add($actor, 'message', $object, $target);

                \App\Services\PushNotificationService::push([$id], $text, [
                    'message_id' => $m->id,
                ]);
            }
        };

        if (!$activity) {
            $meta = [
                'status' => 'interested',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];
            $activity = app('activity')->add($actor, $verb, $object, $target, $meta);
            $pushNotify();
        } else {
            $meta = $activity->meta;
            $status = $meta['status'] ?? null;
            if ($status == '') {
                $meta['status'] = 'interested';
                $meta['applied_at'] = \Carbon\Carbon::now()->toIso8601String();
                $pushNotify();
            }
            if ($status === 'applying') {
                $meta['status'] = 'completed';
                $meta['completed_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            $activity->meta = $meta;
            $activity->save();
        }

        return $activity;
    }

    public static function approve($user, $model)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $model->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);

        $objectMessage = (object) [
            'id' => '0',
            'object_type' => 'Message',
        ];

        $act = app('activity')->last($actor, 'say', $objectMessage);

        if (!$activity) {
            throwError('Not found.', 404);
        }

        $meta = $activity->meta;
        $metaAct = $act->meta;

        $status = $meta['status'] ?? null;

        if ($status !== 'interested') {
            return $activity;
        }

        $meta['status'] = 'applying';
        $metaAct['status'] = 'approved';

        $activity->meta = $meta;
        $act->meta = $metaAct;
        $activity->save();
        $act->save();

        $metaRequest = [
            'source' => [
                'id' => [$model->id],
                'type' => strtoupper(get_class_name($model)),
            ],
            'title' => '{{SENDER}} approved your request to apply to the {{OBJECT}}. If you have additional questions, contact the sender.',
        ];
        $text = $metaRequest['title'];
        $text = str_replace('{{SENDER}}', currentUser()->fullname, $text);
        $text = str_replace('{{OBJECT}}', $model->name, $text);

        $object = (object) [
            'id' => '0',
            'object_type' => 'Message',
        ];

        $actor = currentUser()->toActivityObject();

        $push = app('activity')->add($actor, 'say', $object, $target, $metaRequest);

        $object->id = $push->id;

        $target = (object) [
            'id' => $user->id,
            'target_type' => 'User',
        ];

        $m = app('activity')->add($actor, 'message', $object, $target);

        \App\Services\PushNotificationService::push([$user->id], $text, [
            'message_id' => $m->id,
        ]);

        return $activity;
    }

    public static function disapprove($user, $model)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $model->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);

        $objectMessage = (object) [
            'id' => '0',
            'object_type' => 'Message',
        ];

        $act = app('activity')->last($actor, 'say', $objectMessage);

        if (!$activity) {
            throwError('Not found.', 404);
        }

        $meta = $activity->meta;
        $metaAct = $act->meta;

        $status = $meta['status'] ?? null;

        if ($status !== 'interested') {
            return $activity;
        }

        $meta['status'] = '';
        $metaAct['status'] = 'not approve';

        $activity->meta = $meta;
        $act->meta = $metaAct;
        $activity->save();
        $act->save();

        $metaRequest = [
            'source' => [
                'id' => [$model->id],
                'type' => strtoupper(get_class_name($model)),
            ],
            'title' => '{{SENDER}} has not approved you to apply for {{OBJECT}}. If you have additional questions, contact the sender.',
        ];
        $text = $metaRequest['title'];
        $text = str_replace('{{SENDER}}', currentUser()->fullname, $text);
        $text = str_replace('{{OBJECT}}', $model->name, $text);

        $object = (object) [
            'id' => '0',
            'object_type' => 'Message',
        ];

        $actor = currentUser()->toActivityObject();

        $push = app('activity')->add($actor, 'say', $object, $target, $metaRequest);

        $object->id = $push->id;

        $target = (object) [
            'id' => $user->id,
            'target_type' => 'User',
        ];

        $m = app('activity')->add($actor, 'message', $object, $target);

        \App\Services\PushNotificationService::push([$user->id], $text, [
            'message_id' => $m->id,
        ]);

        return $activity;
    }

    public static function applyTradeSchool($user, $tradeSchool)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $tradeSchool->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);
        if (!$activity) {
            $meta = [
                'status' => 'applying',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];
            $activity = app('activity')->add($actor, $verb, $object, $target, $meta);
        } else {
            $meta = $activity->meta;
            $status = $meta['status'] ?? null;
            if (!$status) {
                $meta['status'] = 'applying';
                $meta['applied_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'applying') {
                $meta['status'] = 'completed';
                $meta['completed_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'completed') {
                $meta['status'] = '';
            }
            $activity->meta = $meta;
            $activity->save();
        }

        return $activity;
    }

    public static function applyCollege($user, $college)
    {
        $actor = $user->toActivityObject();
        $verb = 'apply';
        $object = $college->toActivityObject();
        $target = null;
        $activity = app('activity')->read($actor, $verb, $object);
        if (!$activity) {
            $meta = [
                'status' => 'applying',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];
            $activity = app('activity')->add($actor, $verb, $object, $target, $meta);
        } else {
            $meta = $activity->meta;
            $status = $meta['status'] ?? null;
            if (!$status) {
                $meta['status'] = 'applying';
                $meta['applied_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'applying') {
                $meta['status'] = 'completed';
                $meta['completed_at'] = \Carbon\Carbon::now()->toIso8601String();
            }
            if ($status === 'completed') {
                $meta['status'] = '';
            }
            $activity->meta = $meta;
            $activity->save();
        }

        return $activity;
    }
}
