<?php
namespace App\Services;

use \App\Models\School;
use \App\Models\Scholarship;

class ImportScholarshipService extends BaseImportService
{
    protected $columns = [
        'name', 'school_ids', 'quantity', 'organization', 'min_price', 'max_price', 'description', 'state_id'
    ];

    protected $rules = [
        'rows'              => 'required|array',
        'rows.*.name'       => 'required|string|max:255',
        'rows.*.min_price'  => 'required|numeric|digits_between:0,10',
        'rows.*.max_price'  => 'numeric|digits_between:0,10',
        'rows.*.school_ids' => 'required|regex:/^[,0-9]+$/',
        'rows.*.quantity'   => 'numeric|nullable',
        'rows.*.state_id'   => 'required|exists:states,code',
    ];

    protected function import()
    {
        $existents = [];
        foreach ($this->inputs as $index => $row) {
            $row         = (object) $row;
            $scholarship = Scholarship::where(['name' => $row->name])->first();
            if (!$scholarship) {
                $ids          = [];
                $district_ids = [];
                $user         = currentUser();
                $state        = \App\Models\State::where('code', $row->state_id)->first();
                if ($row->school_ids) {
                    $districtsState = $state->districts()->pluck('id')->toArray();
                    $codes          = explode(',', $row->school_ids);
                    $codes          = array_where(array_filter($codes), function ($value, $key) use (&$ids, &$district_ids, $districtsState, $user, $state, $index) {
                        $school = School::where('code', $value)->first();

                        if (!$school && $value !== '') {
                            throw new \App\Exceptions\UnprocessableEntityException("row.$index.school_ids => Invalid data. This school id not found: " . $value);
                        } else {
                            if (!in_array($school->district_id, $districtsState)) {
                                throw new \App\Exceptions\UnprocessableEntityException("row.$index.school_ids => The school $value does not belong to state $state->code");
                            }
                        }

                        // if ($school->id != $user->school_id && $user->isCounselor()) {
                        //     throw new \App\Exceptions\UnprocessableEntityException('Invalid data. Cannot import for this school: ' . $row->school_id);
                        // }
                        $ids[]          = $school->id;
                        $district_ids[] = $school->district_id;

                        return $school && $school->id;
                    });
                }

                $scholarship  = Scholarship::create([
                    'name'         => $row->name,
                    'quantity'     => $row->quantity ?? null,
                    'organization' => empty($row->organization) ? '' : $row->organization,
                    'min_price'    => $row->min_price,
                    'amount'       => $row->min_price,
                    'max_price'    => $row->max_price && $row->max_price > $row->min_price ? $row->max_price : $row->min_price,
                    'description'  => empty($row->description) ? '' : $row->description,
                    'user_id'      => $user->id,
                    'state_id'     => $state->id,
                ]);
                if ($ids && $district_ids) {
                    $scholarship->schools()->attach($ids);
                    $scholarship->districts()->attach(array_unique($district_ids));
                }
            } else {
                $existents[] = $row;
            }
        }

        return $existents;
    }
}
