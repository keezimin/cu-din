<?php
namespace App\Services;

use App\Jobs\SendInviteEmail;
use App\Models\Invitation;

class InviteService
{
    public static function exec($emails, $user)
    {
        if (!is_array($emails)) {
            $q = Invitation::select('user_id', 'email', 'registed_user_id')
                ->where('user_id', $user->id)
                ->whereNull('registed_user_id');
            $emails = $q->pluck('email');
        }

        $sourceLink = "app/invited-account";
        $data = [
            'branch_key' => env('DEEPLINKING_SECRET'),
            'sdk' => 'api',
            'channel' => 'Email',
            'campaign' => 'Invite Student',
            'data' => json_encode([
                '$deeplink_path' => $sourceLink,
                'custom_object' => [
                    'page' => 'WelcomePage',
                ],
            ]),
        ];
        $url = \PhpSoft\DeepLinking\GetLink::get(env('DEEPLINKING_PROVIDER'), $sourceLink, compact('data'));
        foreach ($emails as $email) {
            SendInviteEmail::dispatch($user, $url, $email);
        }
    }

    public static function inviteParent($emails, $user)
    {
        if (!is_array($emails)) {
            $q = Invitation::select('user_id', 'email', 'registed_user_id')
                ->where('user_id', $user->id)
                ->whereNull('registed_user_id');
            $emails = $q->pluck('email');
        }

        $sourceLink = "app/invited-account";
        $data = [
            'branch_key' => env('DEEPLINKING_SECRET'),
            'sdk' => 'api',
            'channel' => 'Email',
            'campaign' => 'Invite Parent',
            'data' => json_encode([
                '$deeplink_path' => $sourceLink,
                'custom_object' => [
                    'page' => 'WelcomePage',
                ],
            ]),
        ];
        $url = \PhpSoft\DeepLinking\GetLink::get(env('DEEPLINKING_PROVIDER'), $sourceLink, compact('data'));
        foreach ($emails as $email) {
            SendInviteEmail::dispatch($user, $url, $email, 'parent');
        }
    }

    public static function inviteEmployer($emails, $user)
    {
        if (!is_array($emails)) {
            $q = Invitation::select('user_id', 'email', 'registed_user_id')
                ->where('user_id', $user->id)
                ->whereNull('registed_user_id');
            $emails = $q->pluck('email');
        }
        $url = 'https://cued-in.com/request-an-account/';
        foreach ($emails as $email) {
            SendInviteEmail::dispatch($user, $url, $email, 'employer');
        }
    }
}
