<?php
namespace App\Services;

use \App\Models\Job;
use \App\Models\School;
use \App\User;

class ImportJobService extends BaseImportService
{
    protected $columns = [
        'name', 'type', 'email_business', 'location', 'description', 'school_ids', 'volunteer_hours', 'state_id'
    ];

    protected $messages = [
        'regex' => 'The :attribute format is invalid. It must be same: id1,id2,id3',
    ];

    public function __construct() {
        $this->rules = [
            'rows'                   => 'required|array',
            'rows.*.name'            => 'required|string|max:255',
            'rows.*.type'            => 'required|string|in:' . implode(Job::$enumTypes, ','),
            'rows.*.email_business'  => 'required|email|exists:users,email',
            'rows.*.school_ids'      => 'required|regex:/^[,0-9]+$/',
            'rows.*.volunteer_hours' => 'numeric|nullable',
            'rows.*.state_id'        => 'required|exists:states,code',
        ];
    }

    protected function import()
    {
        $existents = [];
        foreach ($this->inputs as $index => $row) {
            $row = (object) $row;
            $job = Job::where(['name' => $row->name, 'type' => $row->type])->first();
            if (!$job) {
                $ids          = [];
                $district_ids = [];
                $state        = \App\Models\State::where('code', $row->state_id)->first();
                $business     = User::where('email', $row->email_business)->first();
                $user         = currentUser();

                if (!$business->isBizOwner()) {
                    throw new \App\Exceptions\UnprocessableEntityException("row.$index.email_business => Invalid data. This email $row->email_business is not business owner");
                }

                if ($row->school_ids) {
                    $districtsState = $state->districts()->pluck('id')->toArray();
                    $codes          = explode(',', $row->school_ids);
                    $codes          = array_where(array_filter($codes), function ($value, $key) use (&$ids, &$district_ids, $districtsState, $state, $index) {
                        $school = School::where('code', $value)->first();

                        if (!$school && $value !== '') {
                            throw new \App\Exceptions\UnprocessableEntityException("row.$index.school_ids => Invalid data. This school id not found: " . $value);
                        } else {
                            if (!in_array($school->district_id, $districtsState)) {
                                throw new \App\Exceptions\UnprocessableEntityException("row.$index.school_ids => The school $value does not belong to state $state->code");
                            }
                        }

                        $ids[]          = $school->id;
                        $district_ids[] = $school->district_id;

                        return $school && $school->id;
                    });
                }
                $job = Job::create([
                    'name'            => $row->name,
                    'type'            => $row->type,
                    'user_id'         => $user->isBizOwner() ? $user->id : $business->id,
                    'location'        => $row->location ?? null,
                    'description'     => $row->description ?? null,
                    'owner_id'        => $user->id,
                    'volunteer_hours' => $row->type === 'volunteer' && $row->volunteer_hours ? $row->volunteer_hours : 0,
                    'state_id'        => $state->id
                ]);

                if ($ids && $district_ids) {
                    $job->schools()->attach($ids);
                    $job->districts()->attach(array_unique($district_ids));
                }
            } else {
                $existents[] = $row;
            }
        }

        return $existents;
    }
}
