<?php
namespace App\Services;

use Validator;

class BaseImportService
{
    protected $columns = [];

    protected $rules = [];

    protected $inputs = [];

    protected $messages = [];

    protected function import() {}

    public function run($file)
    {
        $existents = [];

        \Excel::selectSheetsByIndex(0)->load($file, function ($reader) use (&$existents) {
            $sheet = $reader->get($this->columns);
            $results = $sheet->first();
            if ($diff = array_diff($this->columns, $results->getHeading())) {
                throw new \App\Exceptions\UnprocessableEntityException('Invalid format data. Missing column(s): ' . implode(', ', $diff));
            }

            $results = $results->filter(function ($row, $key) {
                return $row->filter()->isNotEmpty();
            });

            $this->inputs = $results->toArray();
        });

        $inputValidate = ['rows' => $this->inputs];
        $validator = Validator::make($inputValidate, $this->rules, $this->messages);

        //validate dateformat
        if ($validator->fails()) {
            throwError($validator->errors()->toArray(), 400);
        }

        return $this->import();
    }
};
