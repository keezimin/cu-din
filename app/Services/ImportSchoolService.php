<?php
namespace App\Services;

use \App\Models\School;
use \App\Models\District;

class ImportSchoolService extends BaseImportService
{
    protected $columns = [
        'county_name', 'county_number', 'school_name', 'school_id',
    ];

    protected $rules = [
        'rows' => 'required|array',
        'rows.*.county_number' => 'required|max:255',
        'rows.*.school_id' => 'required|string',
    ];

    protected function import()
    {
        $existents = [];
        foreach ($this->inputs as $row) {
            $row = (object) $row;

            $district = District::where('code', $row->county_number)->first();
            $school = School::where('code', $row->school_id)->first();
            if (!$district) {
                $district = District::create([
                    'name' => $row->county_name,
                    'code' => $row->county_number,
                ]);
            }
            if (!$school) {
                School::create([
                    'name' => $row->school_name,
                    'code' => $row->school_id,
                    'district_id' => $district['id'],
                ]);
            } else {
                $existents[] = $row;
            }
        }

        return $existents;
    }
}
