<?php
namespace App\Services;

use \App\User;
use \App\Models\Invitation;

class ImportStudentService extends BaseImportService
{
    protected $columns = [
        'full_name', 'email',
    ];

    protected $rules = [
        'rows' => 'required|array',
        'rows.*.full_name' => 'required|string|max:255',
        'rows.*.email' => 'required|email',
    ];

    protected $counselor;

    public function __construct(User $counselor)
    {
        $this->counselor = $counselor;
    }

    protected function import()
    {
        $existents = [];

        foreach ($this->inputs as $row) {
            $row = (object) $row;
            $registed = User::where('email', $row->email)->first();
            if (!$registed) {
                $student = Invitation::where('email', $row->email)->where('user_id', $this->counselor->id)->first();
                if (!$student) {
                    $data[$row->email] = [
                        'fullname' => $row->full_name,
                        'email' => $row->email,
                        'user_id' => $this->counselor->id,
                    ];
                } else {
                    $existents[] = $row;
                }
            }
        }

        if (!empty($data)) {
            Invitation::insert(array_values($data));
            \App\Services\InviteService::exec(array_keys($data), $this->counselor);
        }

        return $existents;
    }
}
