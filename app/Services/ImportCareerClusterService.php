<?php
namespace App\Services;

use App\Models\SurveyCategory;
use App\Models\CareerCluster;
use App\Models\CareerPathway;
use App\Models\Survey;
use App\Models\Occupation;
use App\Models\SurveyCareerCluster;

class ImportCareerClusterService 
{
    public function run($files)
    {
        $existents = [];
        \Excel::batch($files, function($rows, $file) {
            if ($file->getClientOriginalName() == '2.csv') {
                $data = SurveyCategory::all();
                if ($data->isEmpty()) {
                    $results = $rows->first()->toArray();
                    foreach ($results as $result) {
                        if (isset($result['adapted_from_oklahoma_careertech_survey_please_credit_in_app'])) {
                            if ($result['adapted_from_oklahoma_careertech_survey_please_credit_in_app'] == 'Activites' ||
                            $result['adapted_from_oklahoma_careertech_survey_please_credit_in_app'] == 'Personal Qualities' || 
                            $result['adapted_from_oklahoma_careertech_survey_please_credit_in_app'] == 'School Subjects') {
                                $surveyCategory = SurveyCategory::firstOrCreate(['name' => $result['adapted_from_oklahoma_careertech_survey_please_credit_in_app']]);
                            } elseif (isset($result[0])) {
                                foreach ($result as $keyItem => $item) {
                                    $arItem = [
                                        'name'               => $result['adapted_from_oklahoma_careertech_survey_please_credit_in_app'],
                                        'survey_category_id' => $surveyCategory->id
                                    ];
                                    $survey = Survey::firstOrCreate($arItem);
                                    $surveyCluster['survey_id'] = $survey->id;
                                    if ($keyItem == 'adapted_from_oklahoma_careertech_survey_please_credit_in_app' || $keyItem == 0) {
                                        $surveyCluster['career_cluster_id'] = $result[0];
                                    } else {
                                        $surveyCluster['career_cluster_id'] = $keyItem;
                                    }
                                    SurveyCareerCluster::firstOrCreate($surveyCluster);
                                }
                            }
                        }
                    }
                } else {
                    $existents[] = "Data's key ". $file->getClientOriginalName() ." has exist!";
                }
            } elseif ($file->getClientOriginalName() == '1.csv') {
                $data = CareerCluster::all();
                if ($data->isEmpty()) {
                    $results = $rows->first()->toArray();
                    foreach ($results as $result) {
                        $cluster = CareerCluster::firstOrCreate(['name' => $result['career_cluster']]);
                        $pathway = CareerPathway::firstOrCreate([
                            'name'             => $result['career_pathway'],
                            'career_cluster_id'=> $cluster->id
                        ]);
                        Occupation::firstOrCreate([
                            'name'             => $result['occupation'],
                            'career_cluster_id'=> $cluster->id,
                            'career_pathway_id'=> $pathway->id,
                            'video'            => $result['videos'] ?? '',
                            'code'             => $result['code']
                        ]);
                    }
                } else {
                    $existents[] = "Data's key ". $file->getClientOriginalName() ." has exist!";
                }
            } elseif ($file->getClientOriginalName() == '3.csv') {
                $results = $rows->first()->toArray();
                foreach ($results as $result) {
                    $occupation = Occupation::where('name', $result['title'])->where('code', $result['onet_soc_code'])->update(['occupation' => $result['description']]);
                }
            } elseif ($file->getClientOriginalName() == '4.csv') {
                $results = $rows->first()->toArray();
                foreach ($results as $result) {
                    // get Mean Annual Salary of state's Virginia
                    if (isset($result['mean_annual_salary']) && strpos($result['mean_annual_salary'], ",") && $result['state'] == 'Virginia') {
                        Occupation::where('name', $result['occ_title'])->update(['average_salary' => $result['mean_annual_salary']]);
                    }
                }
            }
        });
        return $existents;
    }
}
