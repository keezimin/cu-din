<?php
namespace App\Services;

class PushNotificationService
{
    public static function push($userIds, $text, $data = null, $limitText = 200)
    {
        $text = str_limit($text, env('LIMIT_TEXT_MESSAGE', $limitText));
        if ($userIds === null) {
            dispatch(new \App\Jobs\PushNotificationAll($text, $data));
            return;
        }

        foreach ($userIds as $id) {
            dispatch(new \App\Jobs\PushNotification($id, $text, $data));
        }
    }
}
