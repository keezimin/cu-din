<?php
namespace App\Repositories\Eloquent;

use App\Models\Profile;
use App\Models\Photo;
use PhpSoft\Users\Models\Role;
use App\Repositories\Contracts\BusinessBreadInterface;

class BusinessBread extends BreadRepository implements BusinessBreadInterface
{
    public function getModel()
    {
        return \App\User::class;
    }

    public function add($data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }
        $data['owner_id'] = currentUser()->id;
        $user = parent::add($data);
        $role = Role::firstOrCreate(
            [
                'name' => 'business_owner',
            ]
        );
        $subscription = isset($data['subscription']) ? $data['subscription'] : '';
        $user->profile()->create(['user_id' => $user->id, 'address' => "", 'subscription' => $subscription, 'contact_person' => $data['contact_person']]);
        $user->roles()->attach($role->id);

        return $user;
    }

    public function updateProfile($user, $data)
    {
        if (isset($data['users'])) {
            $first_name = $data['users']['first_name'] ?? $user->first_name;
            $last_name = $data['users']['last_name'] ?? $user->last_name;
            $data['users']['fullname'] = $first_name. " ". $last_name;
        }
        $data['users']['updated_at'] = now();
        $user->update($data['users']);
        unset($data['users']);

        return $user->profile()->updateOrCreate(['user_id' => $user->id], $data);
    }

    public function updateNewBusinessProfile($user, $data)
    {
        return $user->businessProfile()->updateOrCreate(['user_id' => $user->id], $data);
    }

    public function updatePhoto($user, $data)
    {
        $user->photos()->delete();
        $photo = [];
        if (isset($data['dataImage']) && is_array($data['dataImage'])) {
            foreach ($data['dataImage'] as $key => $value) {
                $photo[] = new \App\Models\Photo(['photo' => $value]);
            }
        }

        return  $user->photos()->saveMany($photo);
    }
}
