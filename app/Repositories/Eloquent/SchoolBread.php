<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\SchoolBreadInterface;
use App\Models\School;

class SchoolBread extends BreadRepository implements SchoolBreadInterface
{
    public function getModel()
    {
        return School::class;
    }
}
