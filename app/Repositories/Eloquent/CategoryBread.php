<?php
namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Repositories\Contracts\CategoryBreadInterface;

class CategoryBread extends BreadRepository implements CategoryBreadInterface
{
    public function getModel()
    {
        return Category::class;
    }
}
