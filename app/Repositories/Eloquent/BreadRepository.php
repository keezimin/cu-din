<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\BreadInterface;

abstract class BreadRepository implements BreadInterface
{
    private function filter($filter = [])
    {
        request()->merge($filter);
        $filter = request()->input();

        $sort = explode(',', request()->input('sort', '-id'));

        $model = $this->getModel();
        $query = $model::queryOrders($sort)->filterFields($filter);
        return $query;
    }

    public function browse($filter = [])
    {
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);

        $query = $this->filter($filter);
        return $query->paginate($size, ['*'], 'page[number]', $number);
    }

    public function count($filter = [])
    {
        $query = $this->filter($filter);
        return $query->count();
    }

    public function add($data)
    {
        return $this->getModel()::create($data);
    }

    public function read($id)
    {
        return $this->getModel()::findOrFail($id);
    }
}
