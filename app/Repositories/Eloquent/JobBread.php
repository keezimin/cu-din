<?php
namespace App\Repositories\Eloquent;

use App\Models\Job;
use App\Repositories\Contracts\JobBreadInterface;
use PhpSoft\Activity\Models\Activity;

class JobBread extends BreadRepository implements JobBreadInterface
{
    public function getModel()
    {
        return \App\Models\Job::class;
    }

    public function getTopTypeJobs()
    {
        $query = \DB::select(\DB::raw
            (
                "SELECT COUNT(DISTINCT c.id) as number_job, c.type, COUNT(a.id) as number_student
                FROM career_jobs c
                INNER JOIN activities a
                ON a.object_id = c.id
                WHERE a.actor_type= \"user\"
                AND a.verb = \"apply\"
                AND a.object_type = \"Job\"
                GROUP BY c.type
                ORDER BY number_job desc
                LIMIT 5"
            )
        );

        return $query;
    }

    public function getPopularJob()
    {
        $query = \DB::select(\DB::raw
            (
                "SELECT COUNT(*) as total,
                    a.object_id,
                    c.name,
                    SUM(CASE WHEN a.verb = \"save\" THEN 1 ELSE 0 END) 'save',
                    SUM(CASE WHEN a.verb = \"apply\" THEN 1 ELSE 0 END) 'applying'
                FROM activities a
                INNER JOIN career_jobs c ON a.object_id = c.id
                WHERE object_type = 'Job'
                GROUP BY object_id
                ORDER BY total DESC
                LIMIT 10"
            )
        );

        return $query;
    }

    public function getStudentRequest()
    {
        $status = Job::APPROVED;

        $query = \DB::select(\DB::raw(
            "SELECT *
            FROM (
                SELECT c.name as job_name,
                    c.type, p.contact_person as employer_name,
                    (
                        SELECT meta
                        FROM activities
                        WHERE object_id = c.id
                        AND JSON_EXTRACT(`meta` , '$.status') = 'interested'
                        AND verb = \"apply\"
                        AND actor_type = \"user\"
                        AND object_type = \"Job\"
                        ORDER BY JSON_EXTRACT(`meta` , '$.applied_at') desc LIMIT 1
                    ) as meta,
                    (
                     SELECT u.fullname as name_student
                        FROM activities
                        INNER JOIN users as u on activities.actor_id = u.id
                        WHERE object_id = c.id
                        AND JSON_EXTRACT(`meta` , '$.status') = 'interested'
                        AND verb = \"apply\"
                        AND actor_type = \"user\"
                        AND object_type = \"Job\"
                        ORDER BY JSON_EXTRACT(`meta` , '$.applied_at') desc LIMIT 1
                    ) as fullname
                FROM career_jobs c
                INNER JOIN profiles p
                    ON p.user_id = c.user_id
                WHERE c.status = ?
            ) tmp
            WHERE meta is not NUll
            ORDER BY JSON_EXTRACT(`meta` , '$.applied_at') desc
            LIMIT 5"
        ), [$status]);

        return $query;
    }

    public function getNewJobRequest()
    {
        $jobs = Job::select('career_jobs.type', 'career_jobs.name as job_name', 'users.name as employer_name', 'career_jobs.created_at')
            ->join('users', 'users.id', '=', 'career_jobs.user_id')
            ->where('status', !Job::APPROVED)
            ->orderBy('career_jobs.created_at', 'desc')
            ->limit(5)
            ->get();
        return $jobs;
    }

    public function getCareerCluster()
    {
        $query = Job::select(\DB::raw('count(*) as job_post, career_jobs.career_cluster_id, career_clusters.name '))
            ->where(['status' => Job::APPROVED, 'deleted_at' => null])
            ->join('career_clusters', 'career_jobs.career_cluster_id', 'career_clusters.id')
            ->groupBy('career_jobs.career_cluster_id')
            ->orderBy('job_post', 'DESC')
            ->limit(5)
            ->get()->toArray();

        foreach ($query as $key => &$value) {
            $cluster = $value['career_cluster_id'];
            $queryJob = Job::where(['career_cluster_id' => $cluster, 'status' => Job::APPROVED, 'deleted_at' => NULL])
                ->get()->pluck('id')->toArray();
            $queries = Activity::where(['verb' => 'apply', 'object_type' => 'Job'])->whereIn('object_id', $queryJob)->get();
            $value['total_apply'] = $queries->count();
        
        }

        return $query;
    }
}
