<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ResourceBreadInterface;
use App\Models\Resource;

class ResourceBread extends BreadRepository implements ResourceBreadInterface
{
    public function getModel()
    {
        return Resource::class;
    }
}
