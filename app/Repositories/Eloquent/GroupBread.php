<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\GroupBreadInterface;

class GroupBread extends BreadRepository implements GroupBreadInterface
{
    public function getModel()
    {
        return \App\Models\Group::class;
    }
}
