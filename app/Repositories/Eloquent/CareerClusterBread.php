<?php
namespace App\Repositories\Eloquent;

use App\Models\CareerCluster;
use App\Repositories\Contracts\CareerClusterInterface;

class CareerClusterBread extends BreadRepository implements CareerClusterInterface
{
    public function getModel()
    {
        return CareerCluster::class;
    }

    public function updatePhoto($careerCluster, $images = [])
    {
        $careerCluster->photos()->delete();
        $photo = [];
        if (is_array($images)) {
            foreach ($images as $key => $image) {
                $photo[] = new \App\Models\CareerClusterPhoto(['photo' => $image]);
            }
        }

        return  $careerCluster->photos()->saveMany($photo);
    }
}
