<?php
namespace App\Repositories\Eloquent;

use App\Models\Document;
use App\Repositories\Contracts\DocumentBreadInterface;

class DocumentBread extends BreadRepository implements DocumentBreadInterface
{
    public function getModel()
    {
        return Document::class;
    }
}
