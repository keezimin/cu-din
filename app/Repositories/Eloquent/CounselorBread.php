<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\CounselorBreadInterface;
use PhpSoft\Users\Models\Role;

class CounselorBread extends BreadRepository implements CounselorBreadInterface
{
    public function getModel()
    {
        return \App\User::class;
    }

    public function add($data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }
        $user = parent::add($data);
        $role = Role::firstOrCreate(
            [
                'name' => 'counselor',
            ]
        );
        $user->roles()->attach($role->id);
        return $user;
    }
}
