<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\DistrictBreadInterface;
use App\Models\District;

class DistrictBread extends BreadRepository implements DistrictBreadInterface
{
    public function getModel()
    {
        return District::class;
    }
}
