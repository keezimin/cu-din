<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ScholarshipBreadInterface;

class ScholarshipBread extends BreadRepository implements ScholarshipBreadInterface
{
    public function getModel()
    {
        return \App\Models\Scholarship::class;
    }

    public function getSaveAndApplying()
    {
        $query = \DB::select(\DB::raw(
            "SELECT COUNT(*) as total,
                a.object_id,
                s.name,
                SUM(CASE WHEN a.verb = \"save\" THEN 1 ELSE 0 END) 'save',
                SUM(CASE WHEN a.verb = \"apply\" THEN 1 ELSE 0 END) 'applying'
            FROM activities a
            INNER JOIN scholarships s ON a.object_id = s.id
            WHERE object_type = 'Scholarship'
            GROUP BY object_id
            ORDER BY total DESC
            LIMIT 5"
        ));

        return $query;
    }
}
