<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\TradeSchoolBreadInterface;

class TradeSchoolBread extends BreadRepository implements TradeSchoolBreadInterface
{
    public function getModel()
    {
        return \App\Models\TradeSchool::class;
    }
}
