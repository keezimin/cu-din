<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\OrganizationBreadInterface;
use App\Models\Organization;

class OrganizationBread extends BreadRepository implements OrganizationBreadInterface
{
    public function getModel()
    {
        return Organization::class;
    }
}
