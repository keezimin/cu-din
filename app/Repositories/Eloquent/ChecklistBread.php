<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ChecklistBreadInterface;

class ChecklistBread extends BreadRepository implements ChecklistBreadInterface
{
    public function getModel()
    {
        return \App\Models\Checklist::class;
    }
}
