<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\TaskBreadInterface;

class TaskBread extends BreadRepository implements TaskBreadInterface
{
    public function getModel()
    {
        return \App\Models\Task::class;
    }
}
