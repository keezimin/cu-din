<?php
namespace App\Repositories\Eloquent;

use App\Models\Pathway;
use App\Repositories\Contracts\PathWayBreadInterface;

class PathWayBread extends BreadRepository implements PathWayBreadInterface
{
    public function getModel()
    {
        return Pathway::class;
    }
}
