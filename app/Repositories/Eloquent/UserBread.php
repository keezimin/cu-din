<?php
namespace App\Repositories\Eloquent;

use App\Models\Invitation;
use App\Models\Profile;
use App\Repositories\Contracts\UserBreadInterface;
use PhpSoft\Users\Models\Role;

class UserBread extends BreadRepository implements UserBreadInterface
{
    public function getModel()
    {
        return \App\User::class;
    }

    public function addParent($data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }

        if (currentUser()) {
            $data['owner_id'] = currentUser()->id;
        }
        $user = parent::add($data);

        $role = Role::firstOrCreate(
            [
                'name' => 'parent',
            ]
        );

        $user->profile()->create([
            'user_id' => $user->id,
            'address' => isset($data['address']) ? $data['address'] : '',
        ]);

        $q = Invitation::select('user_id', 'email', 'registed_user_id')
            ->where('email', $data['email'])
            ->whereHas('user', function ($q) {
                $q->whereDoesntHave('roles');
            })
            ->whereNull('registed_user_id');

        $ids = $q->pluck('user_id');

        if ($ids) {
            $user->childrens()->attach($ids);
            Invitation::where('email', $user->email)->whereNull('registed_user_id')->update(['registed_user_id' => $user->id]);
        }

        $user->roles()->attach($role->id);

        return $user;
    }

    public function addTeacher($data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }

        if (currentUser()) {
            $data['owner_id'] = currentUser()->id;
        }

        $user = parent::add($data);

        $role = Role::firstOrCreate(
            [
                'name' => 'teacher',
            ]
        );

        $user->profile()->create([
            'user_id' => $user->id,
            'address' => isset($data['address']) ? $data['address'] : '',
        ]);

        $user->roles()->attach($role->id);

        return $user;
    }

    public function addDistrictAdmin($data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }

        if (currentUser()) {
            $data['owner_id'] = currentUser()->id;
        }

        $user = parent::add($data);

        $role = Role::firstOrCreate(
            [
                'name' => 'district_admin',
            ]
        );

        $user->profile()->create([
            'user_id' => $user->id,
            'address' => isset($data['address']) ? $data['address'] : '',
        ]);

        $user->roles()->attach($role->id);

        return $user;
    }

    public function updateProfile($user, $data)
    {
        if (isset($data['users'])) {
            $first_name = $data['users']['first_name'] ?? $user->first_name;
            $last_name = $data['users']['last_name'] ?? $user->last_name;
            $data['users']['fullname'] = $first_name. " ". $last_name;
            $user->update($data['users']);
            unset($data['users']);
        }

        return $user->profile()->updateOrCreate(['user_id' => $user->id], $data);
    }
}
