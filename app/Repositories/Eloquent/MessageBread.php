<?php
namespace App\Repositories\Eloquent;

use App\User;
use App\Models\College;
use App\Jobs\SendMessageEmail;
use App\Repositories\Contracts\MessageBreadInterface;

class MessageBread extends BreadRepository implements MessageBreadInterface
{
    public function getModel()
    {
        return \App\Models\Message::class;
    }

    public function update($filter, $data)
    {
        return app('activity')->update($filter, $data);
    }

    public function add($data, $sender = null)
    {
        // build source message
        $user = $sender ?? \Auth::user();

        // build target messages
        $targetIds = array_get($data, 'target.id');
        switch (array_get($data, 'target.type')) {
            case 'USER':
                if (!$targetIds) {
                    break;
                }
                $ids = $targetIds;
                // we dont want to send to message creator
                $key = array_search($user->id, $ids);
                if ($key !== false) {
                    unset($ids[$key]);
                }

                switch (array_get($data, 'source.type')) {
                    case 'SCHOLARSHIP':
                        $scholarship = \App\Models\Scholarship::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Scholarship::APPROVED])->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'RESOURCE':
                        $resource = \App\Models\Resource::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'JOB':
                        $job = \App\Models\Job::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Job::APPROVED])->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'TRADESCHOOL':
                        $tradeSchool = \App\Models\TradeSchool::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'COLLEGE':
                        $college = College::updateOrCreate(['code' => array_first(array_get($data, 'source.id'))], ['name' => array_get($data, 'source.name')]);
                        //@TODO: we may now valid user within same school
                        break;
                }
                break;
            case 'USER_ALL':
                $isNotificationAll = false;
                $query = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles');
                switch (array_get($data, 'source.type')) {
                    case 'SCHOLARSHIP':
                        unset($isNotificationAll);
                        $scholarship = \App\Models\Scholarship::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Scholarship::APPROVED])->firstOrFail();
                        $query = $scholarship->appliedUsers()->where('id', '!=', $user->id)->where('id', '!=', $scholarship->user_id);
                        break;
                    case 'RESOURCE':
                        $resource = \App\Models\Resource::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        $query = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles')->where('school_id', $user->school_id);
                        //@TODO: we may now valid user within same school
                        break;
                    case 'JOB':
                        $job = \App\Models\Job::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Job::APPROVED])->firstOrFail();
                        $query = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles')->where('school_id', $user->school_id);
                        //@TODO: we may now valid user within same school
                        break;
                    case 'TRADESCHOOL':
                        $tradeSchool = \App\Models\TradeSchool::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        $query = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles')->where('school_id', $user->school_id);
                        //@TODO: we may now valid user within same school
                        break;
                    case 'COLLEGE':
                        $college = College::updateOrCreate(['code' => array_first(array_get($data, 'source.id'))], ['name' => array_get($data, 'source.name')]);
                        $query = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles')->where('school_id', $user->school_id);
                        break;
                }
                $ids = array_pluck($query->get(['id']), 'id');

                if ($user->hasRole('teacher')) {
                    $builder = \App\User::where('id', '!=', $user->id)->whereDoesntHave('roles')->where('school_id', $user->school_id);
                    $ids = array_pluck($builder->get(['id']), 'id');
                    unset($isNotificationAll);
                }

                if ($user->hasRole('parent')) {
                    $ids = array_pluck($user->childrens()->get(['user_id']), 'user_id');
                    unset($isNotificationAll);
                }
                if ($user->isCounselor()) {
                    $builder = User::whereDoesntHave('roles')
                        ->where('school_id', $user->school_id)
                        ->where('id', '!=', $user->id);
                    $ids = array_pluck($builder->get(['id']), 'id');
                    unset($isNotificationAll);
                }
                if ($user->isDistrictAdmin()) {
                    $builder = User::whereDoesntHave('roles')
                        ->whereIn('school_id', function ($q) use ($user) {
                            $q->select('schools.id')->from('schools')
                                ->where('schools.district_id', $user->district_id);
                        });
                    $ids = array_pluck($builder->get(['id']), 'id');
                    unset($isNotificationAll);
                }
                unset($data['target']['id']);
                break;
            case 'STUDENT_AND_TEACHER':
                $query = \App\User::where('id', '!=', $user->id)->where(function ($q) {
                    $q->orWhere(function ($q) {
                        $q->whereDoesntHave('roles');
                    });
                    $q->orWhere(function ($q) {
                        $q->withRole('teacher');
                    });
                });
                switch (array_get($data, 'source.type')) {
                    case 'RESOURCE':
                        $resource = \App\Models\Resource::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                }
                $ids = array_pluck($query->get(['id']), 'id');

                if ($user->isCounselor()) {
                    $builder = \App\User::where(function ($q) use ($user) {
                        $q->orWhere(function ($q) use ($user) {
                            $q->whereDoesntHave('roles')
                                ->where('school_id', $user->school_id);
                        });
                        $q->orWhereIn('id', function ($q) use ($user) {
                            $q->select('users.id')
                                ->from('users')
                                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                                ->where('roles.name', 'teacher')
                                ->where('users.school_id', $user->school_id);
                        });
                    });
                    $ids = array_pluck($builder->get(['id']), 'id');
                }
                if ($user->isDistrictAdmin()) {
                    $builder = \App\User::where(function ($q) use ($user) {
                        $q->orWhere(function ($q) use ($user) {
                            $q->whereDoesntHave('roles')
                                ->whereIn('school_id', function ($q) use ($user) {
                                    $q->select('schools.id')->from('schools')
                                        ->where('schools.district_id', $user->district_id);
                                });
                        });
                        $q->orWhereIn('id', function ($q) use ($user) {
                            $q->select('users.id')
                                ->from('users')
                                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                                ->join('schools', 'users.school_id', '=', 'schools.id')
                                ->where('roles.name', 'teacher')
                                ->where('schools.district_id', $user->district_id);
                        });
                    });
                    $ids = array_pluck($builder->get(['id']), 'id');
                }
                unset($data['target']['id']);
                break;
            case 'SCHOOL':
                if (!$targetIds) {
                    break;
                }
                $ids = array_pluck(\App\User::where('id', '!=', $user->id)->whereIn('school_id', $targetIds)->get(['id']), 'id');
                break;
            case 'DISTRICT':
                if (!$targetIds) {
                    break;
                }
                $ids = array_pluck(\App\User::where('users.id', '!=', $user->id)
                        ->join('schools', 'schools.id', '=', 'users.school_id')
                        ->join('districts', 'districts.id', '=', 'schools.district_id')
                        ->whereIn('schools.district_id', $targetIds)->get(['users.id']), 'id');
                break;
            case 'GROUP':
                if (!$targetIds) {
                    break;
                }
                switch (array_get($data, 'source.type')) {
                    case 'SCHOLARSHIP':
                        $scholarship = \App\Models\Scholarship::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Scholarship::APPROVED])->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'RESOURCE':
                        $resource = \App\Models\Resource::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'JOB':
                        $job = \App\Models\Job::whereIn('id', array_get($data, 'source.id'))->where(['status' => \App\Models\Job::APPROVED])->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'TRADESCHOOL':
                        $tradeSchool = \App\Models\TradeSchool::whereIn('id', array_get($data, 'source.id'))->firstOrFail();
                        //@TODO: we may now valid user within same school
                        break;
                    case 'COLLEGE':
                        $college = College::updateOrCreate(['code' => array_first(array_get($data, 'source.id'))], ['name' => array_get($data, 'source.name')]);

                        //@TODO: we may now valid user within same school
                        break;
                }
                $ids = \App\User::whereHas('groups', function ($q) use ($targetIds) {
                    $q->whereIn('groups.id', $targetIds);
                })->pluck('id');
                break;
        }

        if (empty($ids)) {
            if (array_get($data, 'target.type') == 'STUDENT_AND_TEACHER') {
                return true;
            }
            throw new \App\Exceptions\UnprocessableEntityException('Invalid target');
        }

        $actor = $user->toActivityObject();
        $verb = 'say';
        $object = (object) [
            'id' => '0',
            'object_type' => 'Message',
        ];
        $target = null;
        $status = \App\Models\Message::PUBLISH;
        $meta = array_only($data, ['target', 'content', 'link', 'image', 'source']);
        $text = array_get($meta, 'content');
        $cot = 'believes you might be interested in';
        switch (array_get($data, 'source.type')) {
            case 'SCHOLARSHIP':
                if (!$user->hasRole('parent')) {
                    $cot = 'has sent you a scholarship to review.';
                }
                $title = "{{SENDER}} {$cot} {{SCHOLARSHIP}}";
                if (array_get($data, 'type') === 'remind') {
                    $title = '{{SENDER}} sent you a reminder to complete your scholarship application that is coming due {{SCHOLARSHIP}}';
                    $meta['type'] = 'remind';
                }
                $text = $title;
                $text = str_replace('{{SENDER}}', $user->fullname, $text);
                $text = str_replace('{{SCHOLARSHIP}}', $scholarship->name, $text);
                $meta['title'] = $title;
                break;
            case 'RESOURCE':
                if (!$user->hasRole('parent')) {
                    $cot = 'sent you News';
                }
                $title = "{{SENDER}} {$cot} {{RESOURCE}}";
                if (array_get($data, 'type') === 'remind') {
                    $title = '{{SENDER}} remind you to complete the form for {{RESOURCE}}';
                    $meta['type'] = 'remind';
                }
                $text = $title;
                $text = str_replace('{{SENDER}}', $user->fullname, $text);
                $text = str_replace('{{RESOURCE}}', $resource->title, $text);
                $meta['title'] = $title;
                $status = \App\Models\Message::UNPUBLISH;
                break;
            case 'JOB':
                if (!$user->hasRole('parent')) {
                    $cot = 'sent you Job Info';
                }
                $title = "{{SENDER}} {$cot} {{JOB}}";
                if (array_get($data, 'type') === 'remind') {
                    $title = '{{SENDER}} remind you to complete the form for {{JOB}}';
                    $meta['type'] = 'remind';
                }
                $text = $title;
                $text = str_replace('{{SENDER}}', $user->fullname, $text);
                $text = str_replace('{{JOB}}', $job->name, $text);
                $meta['title'] = $title;
                break;
            case 'TRADESCHOOL':
                if (!$user->hasRole('parent')) {
                    $cot = 'sent you Technical School Info';
                }
                $title = "{{SENDER}} {$cot} {{TRADESCHOOL}}";
                if (array_get($data, 'type') === 'remind') {
                    $title = '{{SENDER}} remind you to complete the form for {{TRADESCHOOL}}';
                    $meta['type'] = 'remind';
                }
                $text = $title;
                $text = str_replace('{{SENDER}}', $user->fullname, $text);
                $text = str_replace('{{TRADESCHOOL}}', $tradeSchool->name, $text);
                $meta['title'] = $title;
                break;
            case 'COLLEGE':
                if (!$user->hasRole('parent')) {
                    $cot = 'sent you College Info';
                }
                $title = "{{SENDER}} {$cot} {{COLLEGE}}";
                if (array_get($data, 'type') === 'remind') {
                    $title = '{{SENDER}} remind you to complete the form for {{COLLEGE}}';
                    $meta['type'] = 'remind';
                }
                $text = $title;
                $text = str_replace('{{SENDER}}', $user->fullname, $text);
                $text = str_replace('{{COLLEGE}}', $college->name, $text);
                $meta['title'] = $title;
                break;
        }
        $activity = app('activity')->add($actor, $verb, $object, $target, $meta, $status);

        // we need more 1 query to update its actual ID
        // but it's unnecessary for now, we can back to process later
        $object->id = $activity->id;

        $verb = 'message';
        foreach ($ids as $id) {
            $target = (object) [
                'id' => $id,
                'target_type' => 'User',
            ];
            $m = app('activity')->add($actor, $verb, $object, $target);
            if (!isset($isNotificationAll) || !$isNotificationAll) {
                \App\Services\PushNotificationService::push([$id], $text, [
                    'message_id' => $m->id,
                ]);
            }
            if (isset($data['via']) && $data['via'] === 'EMAIL') {
                $userTarget = User::find($id);
                SendMessageEmail::dispatch($user, $userTarget, $meta, $text);
            }
        }
        if (isset($isNotificationAll) && $isNotificationAll) {
            \App\Services\PushNotificationService::push(null, $text);
        }

        $model = $this->getModel();
        $message = new $model;
        $message->setRawAttributes($activity->getAttributes(), true);

        return $message;
    }
}
