<?php
namespace App\Repositories\Contracts;

interface BreadInterface
{
    public function browse();
    public function add($data);
    public function getModel();
}
