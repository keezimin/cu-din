<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\SchoolBreadInterface',
            'App\Repositories\Eloquent\SchoolBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\UserBreadInterface',
            'App\Repositories\Eloquent\UserBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\CounselorBreadInterface',
            'App\Repositories\Eloquent\CounselorBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\DistrictBreadInterface',
            'App\Repositories\Eloquent\DistrictBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\ScholarshipBreadInterface',
            'App\Repositories\Eloquent\ScholarshipBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\ResourceBreadInterface',
            'App\Repositories\Eloquent\ResourceBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\MessageBreadInterface',
            'App\Repositories\Eloquent\MessageBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\JobBreadInterface',
            'App\Repositories\Eloquent\JobBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\BusinessBreadInterface',
            'App\Repositories\Eloquent\BusinessBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\GroupBreadInterface',
            'App\Repositories\Eloquent\GroupBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\TradeSchoolBreadInterface',
            'App\Repositories\Eloquent\TradeSchoolBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\ChecklistBreadInterface',
            'App\Repositories\Eloquent\ChecklistBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\TaskBreadInterface',
            'App\Repositories\Eloquent\TaskBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\CategoryBreadInterface',
            'App\Repositories\Eloquent\CategoryBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\DocumentBreadInterface',
            'App\Repositories\Eloquent\DocumentBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\PathWayBreadInterface',
            'App\Repositories\Eloquent\PathWayBread'
        );
        $this->app->bind(
            'App\Repositories\Contracts\OrganizationBreadInterface',
            'App\Repositories\Eloquent\OrganizationBread'
        );
    }
}
