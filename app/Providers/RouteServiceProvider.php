<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::bind('user', function ($value) {
            return \App\User::withTrashed()->findOrFail($value);
        });
        Route::bind('business', function ($value) {
            return \App\User::withTrashed()->findOrFail($value);
        });
        Route::bind('checklist', function ($value) {
            return \App\Models\Checklist::withTrashed()->findOrFail($value);
        });
        Route::bind('task', function ($value) {
            return \App\Models\Task::withTrashed()->findOrFail($value);
        });
        Route::bind('collectCode', function ($value) {
            return \App\Models\College::where(['code' => $value])->firstOrFail();
        });
        Route::bind('teacher', function ($value) {
            return \App\User::withTrashed()->findOrFail($value);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix(null)
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
