<?php

namespace App\Providers;

use App\User;
use App\Models\School;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('owner', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }
            if ($user->isDistrictAdmin()) {
                $users = User::whereIn('id', function ($q) use ($user) {
                    $q->select('users.id')
                        ->from('users')
                        ->join('role_user', 'users.id', '=', 'role_user.user_id')
                        ->join('schools', 'users.school_id', '=', 'schools.id')
                        ->join('roles', 'role_user.role_id', '=', 'roles.id')
                        ->where('schools.district_id', $user->district_id)
                        ->where('roles.name', '<>', 'super_admin');
                })->pluck('id')->toArray();

                if (in_array($model->owner_id, $users)) {
                    return true;
                }
            }

            return $user->id == $model->owner_id;
        });
        Gate::define('admin', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isDistrictAdmin()) {
                return $user->district_id == $model->district_id;
            }

            return false;
        });
        Gate::define('ownerBiz', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isDistrictAdmin()) {
                if ($model->isBizOwner()) {
                    $users = User::whereIn('id', function ($q) use ($user) {
                        $q->select('users.id')
                            ->from('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->join('schools', 'users.school_id', '=', 'schools.id')
                            ->where('schools.district_id', $user->district_id);
                    })->pluck('id')->toArray();

                    if (in_array($model->owner_id, $users)) {
                        return true;
                    }
                }

                $count = School::where('schools.id', $model->school_id)->whereIn('schools.id', function ($q) use ($user) {
                    $q->select('schools.id')
                        ->from('schools')
                        ->join('districts', 'districts.id', '=', 'schools.district_id')
                        ->where('schools.district_id', $user->district_id);
                })->count();

                if ($count) {
                    return true;
                }
            }

            return $user->id == $model->owner_id || $user->id == $model->id;
        });

        Gate::define('update-ownerBiz', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            return $user->id == $model->id;
        });

        Gate::define('owner-scholarship', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isDistrictAdmin()) {
                $count = $model->districts()->where('districts.id', $user->district_id)->count();

                if ($count) {
                    return true;
                }
            }

            return $user->id == $model->user_id;
        });
        Gate::define('our-scholarship', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            $schools = $model->schools()->pluck('schools.id')->toArray();

            if ($user->isDistrictAdmin()) {
                if (!empty($model->is_applied_to_state)) {
                    $countState = $model->where('state_id', $user->district->state_id)->count();
                    if ($countState) {
                        return true;
                    }
                }

                $countCounty = $model->districts()->where('districts.id', $user->district_id)->count();
                if ($countCounty) {
                    return true;
                }
            }

            if ($user->isCounselor()) {
                if (!empty($model->is_applied_to_state)) {
                    $countState = $model->where('state_id', $user->school->district->state_id)->count();
                    if ($countState) {
                        return true;
                    }
                } elseif (!empty($model->is_applied_to_county)) {
                    $countCounty = $model->districts()->where('districts.id', $user->school->district_id)->count();
                    if ($countCounty) {
                        return true;
                    }
                }

                $schools = $model->schools()->pluck('schools.id')->toArray();
                if (in_array($user->school_id, $schools)) {
                    return true;
                }
            }

            return $user->id == $model->user_id;
        });

        Gate::define('owner-job', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isDistrictAdmin()) {
                $count = $model->districts()->where('districts.id', $user->district_id)->count();

                if ($count) {
                    return true;
                }
            }

            return $user->id == $model->owner_id;
        });

        Gate::define('approve-job', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isCounselor()) {
                if (!empty($model->is_applied_to_state)) {
                    $countState = $model->where('state_id', $user->school->district->state_id)->count();
                    if ($countState) {
                        return true;
                    }
                } elseif (!empty($model->is_applied_to_county)) {
                    $countCounty = $model->districts()->where('districts.id', $user->school->district_id)->count();
                    if ($countCounty) {
                        return true;
                    }
                }

                $schools = $model->schools()->pluck('schools.id')->toArray();
                if (in_array($user->school_id, $schools)) {
                    return true;
                }
            }
            if ($user->isDistrictAdmin()) {
                if (!empty($model->is_applied_to_state)) {
                    $countState = $model->where('state_id', $user->district->state_id)->count();
                    if ($countState) {
                        return true;
                    }
                }

                $countCounty = $model->districts()->where('districts.id', $user->district_id)->count();
                if ($countCounty) {
                    return true;
                }
            }

            return !$user->hasRole('business_owner') && $user->id == $model->owner_id;
        });

        Gate::define('ourStudent', function ($user, $model) {
            if ($user->hasRole('super_admin')) {
                return true;
            }

            if ($user->isDistrictAdmin()) {
                $schools = School::whereIn('schools.id', function ($q) use ($user) {
                    $q->select('schools.id')
                        ->from('schools')
                        ->join('districts', 'districts.id', '=', 'schools.district_id')
                        ->where('schools.district_id', $user->district_id);
                })->pluck('schools.id')->toArray();

                if (in_array($model->school_id, $schools)) {
                    return true;
                }
            }

            return $user->school_id == $model->school_id;
        });
        //
    }
}
