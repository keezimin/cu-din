<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixDuplicateStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'states:fix-dup {from} {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move duplicate state into valid state';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = $this->argument('from');
        $to = $this->argument('to');

        $this->info('Updating `career_jobs` table...');
        DB::table('career_jobs')
            ->where('state_id', '=', $from)
            ->update([
                'state_id' => $to
            ])
        ;
        $this->info('Updating `districts` table...');
        DB::table('districts')
            ->where('state_id', '=', $from)
            ->update([
                'state_id' => $to
            ])
        ;
        $this->info('Updating `scholarships` table...');
        DB::table('scholarships')
            ->where('state_id', '=', $from)
            ->update([
                'state_id' => $to
            ])
        ;

        $this->info('Deleting from `states` table...');
        DB::table('states')->where('id', '=', $from)->delete();
    }
}
