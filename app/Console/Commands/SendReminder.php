<?php

namespace App\Console\Commands;

use \App\Models\Task;
use Illuminate\Console\Command;
use \App\Jobs\SendEmailReminder;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder task to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::whereRaw(\DB::raw('DATE(tasks.`due_date`) = CURDATE()'))->where(function ($q) {
            $q->whereHas('logs', function ($q) {
                $q->whereRaw(\DB::raw('DATE(reminder_logs.`due_date`) = CURDATE()'));
                $q->where(['status' => 0]);
            });
            $q->orWhereDoesntHave('logs', function ($q) {
                $q->whereRaw(\DB::raw('DATE(reminder_logs.`due_date`) = CURDATE()'));
            })
                ->orDoesntHave('logs');
        })->whereHas('checklist', function ($q) {
            $q->where('deleted_at', null);
        })->with('owner', 'checklist')->get();

        $tasks->each(function ($item) {
            dispatch(new \App\Jobs\SendEmailReminder($item));
        });
    }
}
