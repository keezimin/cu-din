<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:send {--e|lang=en} {--m|msg=hello!} {--i|idMsg=} {tokens*} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App::setLocale($this->option('lang'));
        $tokens = $this->argument('tokens');
        $text = $this->option('msg');
        \App\Services\PushNotificationService::push($tokens, $text);
        $this->info('done');
    }
}
