<?php

namespace App;

use PhpSoft\Users\Models\UserTrait;
use Illuminate\Auth\Authenticatable;
use PhpSoft\AuthSocials\Models\Social;
use PhpSoft\Users\Models\User as PhpSoftUser;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends PhpSoftUser implements AuthenticatableContract, CanResetPasswordContract
{
    use UserTrait, Authenticatable, CanResetPassword;
    use \PhpSoft\AuthSocials\Traits\SocialsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'fullname', 'email', 'password', 'contact', 'graduated_on',
        'act_english', 'act_science', 'act_math', 'act_composite', 'act_reading', 'sat_math', 'sat_verbal',
        'school_id', 'district_id', 'owner_id', 'deleted_at', 'updated_at',
    ];

    protected $dates = ['deleted_at'];

    public function setGraduatedOnAttribute($value)
    {
        if ($value) {
            $this->attributes['graduated_on'] = \Carbon\Carbon::parse($value)->toDateTimeString();
        } else {
            $this->attributes['graduated_on'] = null;
        }
    }

    /**
     * Update update_at
     * @param array attributes
     * @return string
     */
    public function getGraduatedOnAttribute($value)
    {
        return $value ? \Carbon\Carbon::parse($value)->toIso8601String() : null;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function school()
    {
        return $this->belongsTo(\App\Models\School::class);
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id');
    }

    public function socials()
    {
        return $this->hasMany(Social::class, 'user_id');
    }

    public function myUsers()
    {
        return $this->hasMany(\App\User::class, 'owner_id');
    }

    public function district()
    {
        return $this->belongsTo(\App\Models\District::class, 'district_id');
    }

    public function groups()
    {
        return $this->belongsToMany(\App\Models\Group::class, 'group_users', 'user_id', 'group_id');
    }

    public function checklists()
    {
        return $this->hasMany(\App\Models\Checklist::class, 'owner_id');
    }

    public function parents()
    {
        return $this->belongsToMany(\App\User::class, 'users_parents', 'user_id', 'parent_id');
    }

    public function childrens()
    {
        return $this->belongsToMany(\App\User::class, 'users_parents', 'parent_id', 'user_id');
    }

    public function tasks()
    {
        return $this->hasMany(\App\Models\Task::class, 'owner_id')->where(function ($q) {
            $q->whereRaw(\DB::raw('DATE(`due_date`) > CURDATE()'));
        })->orderBy('due_date', 'asc');
    }

    public function allTasks()
    {
        return $this->hasMany(\App\Models\Task::class, 'owner_id');
    }

    public function scholarships()
    {
        return $this->hasMany(\App\Models\Scholarship::class, 'user_id');
    }

    public function myGroups()
    {
        return $this->hasMany(\App\Models\Group::class, 'owner_id');
    }

    public function resources()
    {
        return $this->hasMany(\App\Models\Resource::class, 'owner_id');
    }

    public function myJobs()
    {
        return $this->hasMany(\App\Models\Job::class, 'owner_id');
    }

    public function otherJobs()
    {
        return $this->hasMany(\App\Models\Job::class, 'user_id');
    }

    public function trades()
    {
        return $this->hasMany(\App\Models\TradeSchool::class, 'owner_id');
    }

    public function profile()
    {
        return $this->hasOne(\App\Models\Profile::class);
    }

    public function businessProfile()
    {
        return $this->hasOne(\App\Models\BusinessProfile::class);
    }

    public function scholarshipActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'Scholarship');
        });
    }

    public function activities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
        });
    }

    public function jobActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'Job');
        });
    }

    public function tradeSchoolActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'TradeSchool');
        });
    }

    public function collegeActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'College');
        });
    }

    public function pathwayActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'Pathway');
        });
    }

    public function businessActivities()
    {
        return $this->hasMany(\PhpSoft\Activity\Models\Activity::class, 'actor_id')->where(function ($q) {
            $q->where('actor_type', 'User');
            $q->where('object_type', 'User');
        });
    }

    public function organization()
    {
        return $this->hasOne('App\Models\Organization');
    }

    public function toActivityObject()
    {
        return (object) [
            'id' => $this->id,
            'fullname' => $this->fullname,
            'object_type' => get_class_name($this),
        ];
    }

    public function followActivity()
    {
        return $this->hasMany('\PhpSoft\Activity\Models\Activity', 'object_id')
            ->ofObject($this->id, 'User')
            ->ofVerbs(['follow']);
    }

    public function isAdmin()
    {
        return in_array('super_admin', $this->getRoles());
    }

    public function isAdminOrCounselor()
    {
        return in_array('super_admin', $this->getRoles()) || in_array('counselor', $this->getRoles());
    }

    public function isCounselor()
    {
        return in_array('counselor', $this->getRoles());
    }

    public function isDistrictAdmin()
    {
        return in_array('district_admin', $this->getRoles());
    }

    public function isParent()
    {
        return in_array('parent', $this->getRoles());
    }

    public function isBizOwner()
    {
        return in_array('business_owner', $this->getRoles());
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\Photo::class, 'user_id');
    }

    public function careerClusters()
    {
        return $this->belongsToMany(\App\Models\CareerCluster::class, 'user_carrer_clusters', 'user_id', 'career_cluster_id')
            ->withPivot('total_point')
            ->orderBy('pivot_total_point', 'desc');
    }

    public function pathways()
    {
        return $this->hasMany(\App\Models\Pathway::class, 'user_id');
    }
}
