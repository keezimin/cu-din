<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\TradeSchool;
use Illuminate\Http\Request;

class UserTradeSchoolController extends Controller
{
    public function index($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = TradeSchool::whereHas('savedActivity', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $tradeSchools = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('tradeSchool/browse', [
            'tradeSchools' => $tradeSchools,
        ]), 200);
    }

    public function applying($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = TradeSchool::whereHas('applies', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $tradeSchools = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('tradeSchool/browse', [
            'tradeSchools' => $tradeSchools,
        ]), 200);
    }

    public function store(User $user, TradeSchool $tradeSchool, Request $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'save';
        $object = $tradeSchool->toActivityObject();
        $target = null;
        $meta = null;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function apply(User $user, TradeSchool $tradeSchool, Request $request)
    {
        $activity = \App\Services\UserActionService::applyTradeSchool($user, $tradeSchool);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function delete(User $user, TradeSchool $tradeSchool, Request $request)
    {
        $actor = $user;
        $verb = 'save';
        $object = $tradeSchool;

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }

    public function listAppliedStudents(TradeSchool $tradeSchool, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $tradeSchool->id;
        $query = User::with(['tradeSchoolActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'TradeSchool')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listSavedStudents(TradeSchool $tradeSchool, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $tradeSchool->id;
        $query = User::with(['tradeSchoolActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'save')
                ->where('activities.object_type', 'TradeSchool')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
