<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\User;
use Illuminate\Http\Request;

class UserJobController extends Controller
{
    public function index($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Job::whereHas('savedActivity', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function applying($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Job::whereHas('applies', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function store(User $user, Job $job, Request $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'save';
        $object = $job->toActivityObject();
        $target = null;
        $meta = null;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function apply(User $user, Job $job, Request $request)
    {
        $activity = \App\Services\UserActionService::applyJob($user, $job);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function storeApply_v2(User $user, Job $job, Request $request)
    {
        $activity = \App\Services\UserActionService::apply_v2($user, $job);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function delete(User $user, Job $job, Request $request)
    {
        $actor = $user;
        $verb = 'save';
        $object = $job;

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }

    public function listAppliedStudents(Job $job, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $job->id;
        $query = User::with(['jobActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Job')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listSavedStudents(Job $job, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $job->id;
        $query = User::with(['jobActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'save')
                ->where('activities.object_type', 'Job')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
