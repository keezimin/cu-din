<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\School;
use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Repositories\Contracts\UserBreadInterface;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class DistrictAdminController extends Controller
{
    public function __construct(UserBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->whereHas('roles', function ($q) {
            $q->where(['name' => 'district_admin']);
        })->filterFields($filter)->searchUser($search);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function store(RegisterUserRequest $request)
    {
        $user_exist = User::withTrashed()->where('email', $request->email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }
        $data = $request->all();
        $data['fullname'] = $request->first_name . ' '. $request->last_name;
        $user = $this->bread->addDistrictAdmin($data);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 201);
    }

    public function update(User $admin, ProfileRequest $request)
    {
        $profile = $this->bread->updateProfile($admin, $request->all());

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $admin,
        ]), 200);
    }

    public function getStudents(User $district, Request $request)
    {
        $search = request()->input('search', '');
        $trashed = request()->input('trashed', false);
        $invited = request()->input('invited', false);
        $filter = request()->except(['trashed']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));
        $onlyAllow = ['only' => ['fullname', 'email', 'registed_user_id', 'updated_at']];
        $state_id = request()->input('state_id', null);
        $ids = array_filter(explode(',', request()->input('ids')));
        $applied = request()->input('applied', '');
        $district_ids = array_filter(explode(',', request()->input('district_ids')));

        if ($invited) {
            $q = Invitation::select('id', 'fullname', 'email', 'registed_user_id', 'updated_at', 'created_at', 'deleted_at')
                ->whereIn('user_id', function ($q) use ($district) {
                    $q->select('users.id')
                        ->from('users')
                        ->join('role_user', 'users.id', '=', 'role_user.user_id')
                        ->join('schools', 'users.school_id', '=', 'schools.id')
                        ->where('schools.district_id', $district->district_id);
                })
                ->whereNull('registed_user_id');
            $filterInvite = $filter;
            if (array_get($filter, 'fullname')) {
                $filterInvite = array_except($filter, ['fullname']);
            }
            $q = $q->filterFields($filterInvite, $onlyAllow);
        }

        $builder = User::select('id', 'fullname', 'email', \DB::raw('id as registed_user_id'), 'updated_at', 'created_at', 'deleted_at')
            ->whereDoesntHave('roles')
            ->where(function ($q) use ($district, $state_id, $district_ids) {
                // case 1: get student in state
                // case 2: get student in district
                if ($state_id) {
                    $q->whereHas('school', function ($q) use ($state_id) {
                        $q->whereHas('district', function ($q) use ($state_id) {
                            $q->where('state_id', $state_id);
                        });
                    });
                } elseif ($district_ids) {
                    $q->whereHas('school', function ($q) use ($district_ids) {
                        $q->whereIn('district_id', $district_ids);
                    });
                }

                $q->whereHas('school', function ($q) use ($district) {
                    $q->where('district_id', $district->district_id);
                });
            })->filterFields($filter, $onlyAllow)
            ->queryOrders($sort, null, $onlyAllow)
            ->searchUser($search);

        if (in_array($applied, ['job', 'tradeSchool', 'scholarship']) && $ids) {
            $releted = "{$applied}Activities";
            $builder = $builder->whereDoesntHave($releted, function ($q) use ($ids) {
                $q->whereIn('object_id', $ids);
            });

            if (in_array($applied, ['job', 'scholarship'])) {
                $tableName = ($applied == 'job') ? 'career_jobs' : 'scholarships';
                $school_ids = \App\Models\School::whereHas("{$applied}s", function ($q) use ($ids, $tableName) {
                    $q->whereIn("{$tableName}.id", $ids);
                })->pluck('id')->toArray();

                if (!$state_id && !empty($school_ids)) {
                    $builder = $builder->whereIn('school_id', $school_ids);
                }
            }
        }

        if ($invited) {
            $builder = $builder->union($q)->queryOrders($sort, null, $onlyAllow);
        }

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $total = \DB::select("select count(*) as total_count from ({$builder->toSql()}) as count_table", $builder->getBindings())[0]->total_count;
        $skip = ($number - 1) * $size;
        $items = $builder->skip($skip)->take($size)->get();

        $users = new Paginator($items, $total, $size, $number, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page[number]',
        ]);

        return response()->json(arrayView('student/browse', [
            'users' => $users,
        ]), 200);
    }

    public function getTeachers(User $district, Request $request)
    {
        $search = request()->input('search', '');
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));

        $builder = User::withRole('teacher')
            ->whereIn('school_id', function ($q) use ($district) {
                $q->select('schools.id')->from('schools')
                    ->where('schools.district_id', $district->district_id);
            })
            ->filterFields($filter)
            ->queryOrders($sort, null)
            ->searchUser($search);

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $users = $builder->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function getCounselors(User $district, Request $request)
    {
        $search = request()->input('search', '');
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));

        $builder = User::withRole('counselor')
            ->whereIn('school_id', function ($q) use ($district) {
                $q->select('schools.id')->from('schools')
                    ->where('schools.district_id', $district->district_id);
            })
            ->filterFields($filter)
            ->queryOrders($sort, null)
            ->searchUser($search);

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $users = $builder->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function getSchools(User $district, Request $request)
    {
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'id'));

        $builder = School::where('district_id', $district->district_id)
            ->filterFields($filter)
            ->queryOrders($sort, null);

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $schools = $builder->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('school/browse', [
            'schools' => $schools,
        ]), 200);
    }

    public function getBizs(User $district, Request $request)
    {
        $search = request()->input('search', '');
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));

        $builder = User::withRole('business_owner')->where(function ($q) use ($district) {
            $q->orWhere(['owner_id' => $district->id]);
            $q->orWhereIn('owner_id', function ($q) use ($district) {
                $q->select('users.id')
                    ->from('users')
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('schools', 'users.school_id', '=', 'schools.id')
                    ->where('schools.district_id', $district->district_id);
            });
        })
            ->filterFields($filter)
            ->queryOrders($sort, null)
            ->searchUser($search);

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $users = $builder->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
