<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Requests\ResetPasswordRequest;
use App\User;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getResetToken(ResetPasswordRequest $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        $token = $this->broker()->createToken($user);
        $email = $request->get('email');
        $platform = $request->get('platform', 'app');
        if ($user->isAdmin() || $user->isDistrictAdmin() || $user->isBizOwner()) {
            $params = [
                'token' => $token,
                'email' => $email
            ];
            $url = env('URL_ADMIN_PAGE'). '/password-forgot-resset'.'?'. http_build_query($params);
        } else {
            if ($platform == 'app') {
                $sourceLink = "app/create-new-password/$email/$token";
                $data = [
                    'branch_key' => env('DEEPLINKING_SECRET'),
                    'sdk' => 'api',
                    'channel' => 'Email',
                    'campaign' => 'Forgot Password',
                    'data' => json_encode([
                        '$deeplink_path' => $sourceLink,
                        'custom_object' =>  [
                            'page' =>  'CreateNewPasswordPage',
                            'params' => compact('email', 'token'),
                        ]
                    ]),
                ];
                $url = \PhpSoft\DeepLinking\GetLink::get(env('DEEPLINKING_PROVIDER'), $sourceLink, compact('data'));
            } elseif ($platform == 'webapp') {
                $url = env('URL_WEBAPP_PAGE'). "/create-new-password/$email/$token";
            } else {
                $params = [
                    'token' => $token,
                    'email' => $email
                ];
                $url = env('URL_ADMIN_PAGE'). '/password-forgot-resset'.'?'. http_build_query($params);
            }
        }

        Mail::send('emails.forgot_password', compact('user', 'url'), function ($message) use ($email) {
            $message->to($email)->subject('Reset your password!');
        });
        return response()->json(null, 204);
    }

    public function getResetTokenAdminPage(ResetPasswordRequest $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        $token = $this->broker()->createToken($user);
        $email = $request->get('email');
        $param = [
            'token' => $token,
            'email' => $email
        ];

        $url = env('URL_ADMIN_PAGE'). '/password-forgot-resset'.'?'. http_build_query($param);
        Mail::send('emails.forgot_password', compact('user', 'url'), function ($message) use ($email) {
            $message->to($email)->subject('Reset your password!');
        });

        return response()->json(null, 204);
    }
}
