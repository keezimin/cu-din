<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function sendResetResponse($response) {
        return response()->json(null, 204);
    }

    protected function credentials(Request $request)
    {
        $request->merge(['password_confirmation' => $request->password]);
        return $request->only(
            'email', 'password', 'token', 'password_confirmation'
        );
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        $data = [
            'errors' => array([
                'title' => 'Not Found Error',
                'detail' => trans($response)
            ])
        ];
        return response()->json(arrayView('errors.exception', $data), 404);
    }
}
