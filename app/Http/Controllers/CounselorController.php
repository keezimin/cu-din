<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Models\Invitation;
use App\Repositories\Contracts\CounselorBreadInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class CounselorController extends Controller
{
    public function __construct(CounselorBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function store(RegisterUserRequest $request)
    {
        $user_exist = User::withTrashed()->where('email', $request->email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }
        $data = $request->all();
        $data['fullname'] = $request->first_name . ' '. $request->last_name;
        $user = $this->bread->add($data);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 201);
    }

    public function listStudentByCounselor(User $counselor, Request $request)
    {
        $search = request()->input('search', '');
        $applied = request()->input('applied', '');
        $ids = array_filter(explode(',', request()->input('ids')));
        $district_ids = array_filter(explode(',', request()->input('district_ids')));
        $trashed = request()->input('trashed', false);
        $invited = request()->input('invited', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));
        $onlyAllow = ['only' => ['fullname', 'email', 'registed_user_id', 'updated_at']];
        $state_id = request()->input('state_id', null);

        if ($invited) {
            $q = Invitation::select('id', 'fullname', 'email', 'registed_user_id', 'updated_at', 'created_at', 'deleted_at')
                ->where('user_id', $counselor->id)
                ->whereNull('registed_user_id');
            $filterInvite = $filter;
            if (array_get($filter, 'fullname')) {
                $filterInvite = array_except($filter, ['fullname']);
            }
            $q = $q->filterFields($filterInvite, $onlyAllow);
        }

        $builder = User::select('id', 'fullname', 'email', \DB::raw('id as registed_user_id'), 'updated_at', 'created_at', 'deleted_at')
            ->whereDoesntHave('roles')
            ->where(function ($q) use($counselor, $state_id, $district_ids) {
                // case 1: get student in state
                // case 2: get student in district
                if ($state_id) {
                    $q->whereHas('school', function ($q) use ($state_id) {
                        $q->whereHas('district', function ($q) use ($state_id) {
                            $q->where('state_id', $state_id);
                        });
                    });
                } elseif ($district_ids) {
                    $q->whereHas('school', function ($q) use ($district_ids) {
                        $q->whereIn('district_id', $district_ids);
                    });
                }
                $q->where('school_id', $counselor->school_id);
            })->filterFields($filter, $onlyAllow)
            ->queryOrders($sort, null, $onlyAllow)
            ->searchUser($search);

        if ($invited) {
            $builder = $builder->union($q)->queryOrders($sort, null, $onlyAllow);
        }

        if (in_array($applied, ['job', 'tradeSchool', 'scholarship']) && request()->ids) {
            $releted = "{$applied}Activities";
            $builder = $builder->whereDoesntHave($releted, function ($q) use ($ids) {
                $q->whereIn('object_id', $ids);
            });
        }

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $total = \DB::select("select count(*) as total_count from ({$builder->toSql()}) as count_table", $builder->getBindings())[0]->total_count;
        $skip = ($number - 1) * $size;
        $items = $builder->skip($skip)->take($size)->get();

        $users = new Paginator($items, $total, $size, $number, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page[number]',
        ]);

        return response()->json(arrayView('student/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listParents(User $student, Request $request)
    {
        $search = request()->input('search', '');
        $ids = explode(',', request()->input('ids'));
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));
        $onlyAllow = ['only' => ['fullname', 'email', 'registed_user_id']];

        $q = Invitation::select('id', 'fullname', 'email', 'registed_user_id', 'updated_at', 'created_at', 'deleted_at')
            ->where('user_id', $student->id)
            ->whereNull('registed_user_id');
        $filterInvite = $filter;
        if (array_get($filter, 'fullname')) {
            $filterInvite = array_except($filter, ['fullname']);
        }
        $q = $q->filterFields($filterInvite, $onlyAllow);

        $builder = User::select('id', 'fullname', 'email', \DB::raw('id as registed_user_id'), 'updated_at', 'created_at', 'deleted_at')
            ->whereHas('childrens', function ($q) use ($student) {
                $q->where('users_parents.user_id', $student->id);
            })
            ->whereHas('roles')
            ->filterFields($filter, $onlyAllow)
            ->searchUser($search);

        $builder = $builder->union($q)->queryOrders($sort, null, $onlyAllow);

        $total = \DB::select("select count(*) as total_count from ({$builder->toSql()}) as count_table", $builder->getBindings())[0]->total_count;
        $skip = ($number - 1) * $size;
        $items = $builder->skip($skip)->take($size)->get();

        $users = new Paginator($items, $total, $size, $number, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page[number]',
        ]);

        return response()->json(arrayView('student/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listTeacherByCounselor(User $counselor, Request $request)
    {
        $search = request()->input('search', '');
        $ids = explode(',', request()->input('ids'));
        $trashed = request()->input('trashed', false);
        $filter = request()->except(['applied', 'trashed', 'ids']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));
        $onlyAllow = ['only' => ['fullname', 'email']];

        $builder = User::withRole('teacher')
            ->where('school_id', $counselor->school_id)
            ->filterFields($filter, $onlyAllow)
            ->queryOrders($sort, null, $onlyAllow)
            ->searchUser($search);

        if ($trashed) {
            $builder = $builder->onlyTrashed();
        }

        $users = $builder->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listInvitesByCounselor(User $counselor, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', 'fullname'));
        $onlyAllow = ['only' => ['fullname', 'email', 'registed_user_id']];
        $q = Invitation::select('id', 'fullname', 'email', 'registed_user_id')
            ->where('user_id', $counselor->id)
            ->whereNull('registed_user_id');
        $filterInvite = $filter;
        if (array_get($filter, 'fullname')) {
            $filterInvite = array_except($filter, ['fullname']);
        }
        $q->filterFields($filterInvite, $onlyAllow);

        $total = \DB::select("select count(*) as total_count from ({$q->toSql()}) as count_table", $q->getBindings())[0]->total_count;
        $skip = ($number - 1) * $size;
        $items = $q->skip($skip)->take($size)->get();

        $users = new Paginator($items, $total, $size, $number, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page[number]',
        ]);

        return response()->json(arrayView('student/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listBusinesses(User $counselor)
    {
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->whereHas('roles', function ($q) {
            $q->where(['name' => 'business_owner']);
        })->where(['owner_id' => $counselor->id])
        ->filterFields($filter)
        ->searchUser($search);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function invitation(User $user, Request $request)
    {
        \App\Services\InviteService::exec($request->email, $user);

        return response()->json(null, 204);
    }
}
