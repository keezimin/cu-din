<?php

namespace App\Http\Controllers;

use App\Http\Requests\PathWayRequest;
use App\Http\Requests\UpdatePathwayRequest;
use App\Http\Requests\DeletePathwayRequest;
use App\Repositories\Contracts\PathWayBreadInterface;
use App\Models\Pathway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PathwayController extends Controller
{
    public function __construct(PathWayBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $user = currentUser();
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        if ($user->isAdmin()) {
            $query = Pathway::queryOrders($sort)->filterFields($filter)->searchUser($search);
        } else {
            $query = Pathway::queryOrders($sort)->whereHas('user', function ($q) {
                $q->whereHas('businessProfile', function ($query) {
                    $query->where('active', true);
                });
            })->filterFields($filter)->searchUser($search);
        }

        $pathways = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('pathway/browse', [
            'pathways' => $pathways,
        ]), 200);
    }

    public function store(PathWayRequest $request)
    {
        $user = currentUser();

        if ($request->user_id && $request->active) {
            $count = Pathway::where('user_id', $request->user_id)->where('active', true)->count();
            if ($count >= 3) {
                throw new \App\Exceptions\PathwayMaxException();
            }
        }
        $data = $request->except(['about', 'skills', 'training']);

        if ($request->about) {
            $data['about'] = json_encode($request->about);
        }
        if ($request->skills) {
            $data['skills'] = json_encode($request->skills);
        }
        if ($request->training) {
            $data['training'] = json_encode($request->training);
        }

        $data['active'] = $request->active ?? false;
        $data['owner_id'] = $user->id;
        $pathway = $this->bread->add($data);

        return response()->json(arrayView('pathway/read', [
            'pathway' => $pathway,
        ]), 201);
    }

    public function update(Pathway $pathway, UpdatePathwayRequest $request)
    {
        if ($request->active && $pathway->active == false) {
            $count = Pathway::where('user_id', $pathway->user_id)->where('active', true)->count();
            if ($count >= 3) {
                throw new \App\Exceptions\PathwayMaxException();
            }
        }
        $data = $request->except(['about', 'skills', 'training']);

        if (isset($request->skills)){
            $data['skills'] = json_encode($request->skills);
        }
        if (isset($request->about)){
            $data['about'] = json_encode($request->about);
        }
        if (isset($request->training)){
            $data['training'] = json_encode($request->training);
        }

        $pathway->update($data);

        return response()->json(arrayView('pathway/read', [
            'pathway' => $pathway,
        ]), 200);
    }

    public function show(Pathway $pathway)
    {
        if ($this->checkUserIsActive($pathway)) {
            return response()->json(arrayView('pathway/read', ['pathway' => $pathway]), 200);
        }

        return response()->json(null, 403);
    }

    public function multiDelete(DeletePathwayRequest $request)
    {
        $idArray = $request->get('ids');

        $userId = $request->get('user_id');
        $data = [];
        $data['meta']['errors'] = [];
        if (is_array($idArray)) {
            foreach ($idArray as $key => $id) {
                $pathway = Pathway::find($id);
                if ($pathway) {
                    if ($pathway->user_id != $userId) {
                        $data['status'] = 400;
                        $data['meta']['errors'][] = [
                            'title' => 'Bad Request',
                            'detail' => 'Can\'t delete Pathway. Pathway not belong to BO',
                            'meta' => [
                                'id' => $id,
                            ],
                        ];
                        continue;
                    }
                    $pathway->delete();
                } else {
                    $data['status'] = 404;
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Pathway. Pathway is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), $data['status']);
        } else {
            return response()->json(null, 204);
        }
    }

    public function getJobs(Pathway $pathway, Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        if (empty($filter['assign'])) {
            $query = $pathway->jobs()->withTrashed()->queryOrders($sort)->filterJobByRole()->filterFields($filter);
        } else {
            $query = $pathway->jobs()->where('is_assign_to_pathway', true)
                ->withTrashed()
                ->queryOrders($sort)
                ->filterJobByRole()
                ->filterFields($filter);
        }

        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    //Recent Activity - New Pathways
    //Data: Top 5 Most recent pathways [Business, cluster, pathway name, date]
    public function getTopPathways()
    {
        $userRole = Auth::user()->isBizOwner();
        $pathways = [];
        if (!$userRole) {
            $size = request()->input('page.size', 5);
            $number = request()->input('page.number', 1);
            $query = Pathway::with(['careerCluster', 'user'])->latest('created_at');
            $pathways = $query->paginate($size, ['*'], 'page[number]', $number);
        }

        return response()->json(arrayView('pathway/browse', [
            'pathways' => $pathways,
        ]), 200);
    }

    private function checkUserIsActive($pathway) {
        $user = currentUser();
        if ($user->isAdmin() || !empty($pathway->user->businessProfile->active)) {
            return true;
        }

        return false;
    }
}
