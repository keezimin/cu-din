<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use JWTAuth;
use App\User;
use JWTAuthException;
use App\Models\Device;
use App\Models\Setting;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $object_type = $request->get('object_type');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $data = [
                    'jsonapi' => [
                        'version' => '1.0',
                    ],
                    'errors' => [
                        'title' => 'Authenticate Error',
                        'detail' => 'Invalid email or password',
                    ],
                ];

                return response()->json($data, 401);
            }
        } catch (JWTAuthException $e) {
            $data = [
                'jsonapi' => [
                    'version' => '1.0',
                ],
                'errors' => [
                    'title' => 'Authenticate Error',
                    'detail' => 'Failed to create token',
                ],
            ];

            return response()->json($data, 500);
        }

        $user         = JWTAuth::toUser($token);
        $customClaims = [
            'password' => $user->password,
            'domain'   => env('APP_URL'),
        ];
        $token        = JWTAuth::fromUser($user, $customClaims);

        if ($object_type === 'app' && $user->isCounselor()) {
            $counselor_login = Setting::where('key', 'counselor_login')->first();
            if ($counselor_login && (int) $counselor_login->value === Setting::DISABLE_LOGIN) {
                $data = [
                    'jsonapi' => [
                        'version' => '1.0',
                    ],
                    'errors' => [
                        'title' => 'Authenticate Error',
                        'detail' => 'Account cannot login in app',
                    ],
                ];

                return response()->json($data, 406);
            }
        }

        if ($object_type === 'app' && $user->isDistrictAdmin()) {
            $da_login = Setting::where('key', 'da_login')->first();
            if ($da_login && (int) $da_login->value === Setting::DISABLE_LOGIN) {
                $data = [
                    'jsonapi' => [
                        'version' => '1.0',
                    ],
                    'errors' => [
                        'title' => 'Authenticate Error',
                        'detail' => 'Account cannot login in app',
                    ],
                ];

                return response()->json($data, 406);
            }
        }

        $data = [
            'id' => $token,
            'type' => 'Token',
            'attributes' => [
                'accessToken' => $token,
                'tokenType' => 'Bearer',
                'expiresIn' => config('jwt.ttl'),
                'user_id' => $user['id'],
            ],
        ];
        $device_uuid = $request->input('device_uuid');
        $device_token = $request->input('device_token');
        if ($device_uuid && $device_token) {
            $uuid = Device::where('uuid', $device_uuid)->first();
            if ($uuid) {
                //update token device
                $uuid->user_id = $user['id'];
                $uuid->token = $device_token;
                $uuid->is_logging_in = true;
                $uuid->save();
            } else {
                //save new device
                $device = new Device();
                $device->user_id = $user['id'];
                $device->uuid = $device_uuid;
                $device->token = $device_token;
                $device->is_logging_in = true;
                $device->save();
            }
        }

        return response()->json(arrayView('users.login', $data));
    }

    public function logout(Request $request)
    {
        $user = \Auth::user();
        $device_uuid = $request->input('device_uuid');
        if ($device_uuid && $user) {
            $uuid = Device::where('uuid', $device_uuid)->where('user_id', $user->id)->first();
            if ($uuid) {
                $uuid->is_logging_in = false;
                $uuid->save();
            }
        }

        $token = JWTAuth::getToken();
        if ($token) {
            JWTAuth::setToken($token)->invalidate();
        }

        return response()->json(null, 204);
    }

    public function changepass(ChangePasswordRequest $request)
    {
        $request_data = $request->all();
        $user = \Auth::user();
        if (Hash::check($request_data['current_password'], $user->password)) {
            $user->password = Hash::make($request_data['password']);
            $user->save();

            $token = JWTAuth::getToken();
            if ($token) {
                JWTAuth::setToken($token)->invalidate();
            }

            return response()->json(null, 204);
        } else {
            $data = [
                'errors' => [
                    [
                        'detail' => 'The current password not match',
                    ],
                ],
            ];

            return response()->json(arrayView('errors.exception', $data), 403);
        }
    }
}
