<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Models\Group;
use App\Repositories\Contracts\GroupBreadInterface;
use App\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public $bread;

    public function __construct(GroupBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Group::queryOrders($sort)->filterFields($filter);

        if (currentUser() && currentUser()->isDistrictAdmin()) {
            $query->filterGroupByRole();
        }

        $groups = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('group/browse', [
            'groups' => $groups,
        ]), 200);
    }

    public function update(Group $group, GroupUpdateRequest $request)
    {
        $ids = $request->get('ids');

        $group->update($request->except(['ids']));

        if ($ids) {
            $group->users()->sync($ids);
        }

        return response()->json(arrayView('group/read', ['group' => $group]), 200);
    }

    public function store(GroupRequest $request)
    {
        $user = \Auth::user();

        $ids = $request->get('ids');

        $group = Group::create([
            'name' => $request->get('name'),
            'owner_id' => $user->id,
        ]);

        $group->users()->attach($ids);

        return response()->json(arrayView('group/read', ['group' => $group]), 201);
    }

    public function delete(Group $group)
    {
        $group->users()->detach();
        $group->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $group = Group::find($id);
                if ($group) {
                    $group->users()->detach();
                    $group->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Group. Group is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(Group $group)
    {
        return response()->json(arrayView('group/read', ['group' => $group]), 200);
    }
}
