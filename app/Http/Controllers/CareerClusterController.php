<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CareerCluster;
use App\Models\SurveyCareerCluster;
use App\Models\Survey;
use App\Models\UserCarrerCluster;
use App\Http\Requests\CareerClusterRequest;
use App\Http\Requests\CareerClusterUpdateRequest;
use App\Repositories\Eloquent\CareerClusterBread;

class CareerClusterController extends Controller
{
    protected $bread;

    public function __construct(CareerClusterBread $bread)
    {
        $this->bread = $bread;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $careerClusters = $this->bread->browse();
        return response()->json(arrayView('careerCluster/browse', [
            'careerClusters' => $careerClusters,
        ]), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareerClusterRequest $request)
    {
        $data = $request->all();
        $careerCluster = CareerCluster::create($data);

        return response()->json(arrayView('careerCluster/read', ['careerCluster' => $careerCluster]), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CareerCluster $careerCluster)
    {
        $currentUser = \Auth::user();
        $userCarrerCluster = UserCarrerCluster::where('user_id', $currentUser->id)->where('career_cluster_id', $careerCluster->id)->first();
        if (!empty($userCarrerCluster)) {
            $totalPoint = $userCarrerCluster->total_point;

            return response()->json(arrayView('careerCluster/read', ['careerCluster' => $careerCluster, 'totalPoint' => $totalPoint]), 200);
        }

        return response()->json(arrayView('careerCluster/read', ['careerCluster' => $careerCluster]), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\CareerCluster  $careerCluster
     * @return \Illuminate\Http\Response
     */
    public function update(CareerCluster $careerCluster, CareerClusterUpdateRequest $request)
    {
        $careerCluster->update($request->all());

        if (isset($request->dataImage)) {
            $photo = $this->bread->updatePhoto($careerCluster, $request->dataImage);
        }

        return response()->json(arrayView('careerCluster/read', ['careerCluster' => $careerCluster]), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\CareerCluster $careerCluster
     * @return \Illuminate\Http\Response
     */
    public function destroy(CareerCluster $careerCluster)
    {
        $careerCluster->delete();

        return response()->json(null, 204);
    }

    /**
     * Match score for user do survey
     * @param  CareerClusterRequest $request
     * @return response
     */
    public function matchScore(Request $request) {
        $currentUser = \Auth::user();
        $listIds = $request->listIds;
        if (empty($listIds)) {
            return response()->json(null, 500);
        }
        $listItems = CareerCluster::all()->toArray();
        foreach ($listItems as $item) {
            $listCareerClusters[$item['id']] = 0;
        }
        foreach ($listIds as $id) {
            $survey = Survey::findOrFail($id);
            $arCareerClusters = $survey->careerClusters->toArray() ?? [];
            foreach ($arCareerClusters as $item) {
                if (array_key_exists($item['career_cluster_id'], $listCareerClusters)) {
                    $listCareerClusters[$item['career_cluster_id']]++;
                }
            }
        }
        foreach ($listItems as $item) {
            UserCarrerCluster::updateOrCreate([
                'user_id' => $currentUser->id,
                'career_cluster_id' => $item['id']
            ],[
                'total_point' => $listCareerClusters[$item['id']]
            ]);
        }
        return response()->json(null, 200);
    }
}
