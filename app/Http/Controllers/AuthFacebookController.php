<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Device;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthFacebookRequest;

class AuthFacebookController extends Controller
{
    public function login(AuthFacebookRequest $request)
    {
        $profile = \AuthSocial::getProfile('facebook', $request->token);
        $object_type = $request->get('object_type');

        if (!$profile->email) {
            throw new \Illuminate\Validation\UnauthorizedException('Invalid email');
        }

        $social = \AuthSocial::getSocial('facebook', $profile->id);

        if ($social->user_id) {
            $user = User::withTrashed()->find($social->user_id);
        } else {
            $user = User::withTrashed()->where(['email' => $profile->email])->first();
            if (!$user) {
                $user = new User;
                $user->fullname = $profile->name;
                $user->name = $profile->name;
                $user->email = $profile->email;
                $user->password = bcrypt($profile->id . time());
                $user->save();
            }
            $social->user_id = $user->id;
            $social->save();
        }

        if ($user->deleted_at) {
            $errors = [
                'jsonapi' => [
                    'version' => '1.0',
                ],
                'errors' => [
                    'title' => 'Authenticate Error',
                    'detail' => 'Account_cannot_login_in_app',
                ],
            ];

            return response()->json($errors, 406);
        }

        if ($object_type === 'app' && $user->isCounselor()) {
            $counselor_login = Setting::where('key', 'counselor_login')->first();
            if ($counselor_login && (int) $counselor_login->value === Setting::DISABLE_LOGIN) {
                $data = [
                    'jsonapi' => [
                        'version' => '1.0',
                    ],
                    'errors' => [
                        'title' => 'Authenticate Error',
                        'detail' => 'Account_cannot_login_in_app',
                    ],
                ];

                return response()->json($data, 406);
            }
        }

        if ($object_type === 'app' && $user->isDistrictAdmin()) {
            $da_login = Setting::where('key', 'da_login')->first();
            if ($da_login && (int) $da_login->value === Setting::DISABLE_LOGIN) {
                $data = [
                    'jsonapi' => [
                        'version' => '1.0',
                    ],
                    'errors' => [
                        'title' => 'Authenticate Error',
                        'detail' => 'Account_cannot_login_in_app',
                    ],
                ];

                return response()->json($data, 406);
            }
        }

        $device_uuid = $request->get('device_uuid');
        $device_token = $request->get('device_token');
        if ($device_uuid && $device_token) {
            $device = Device::firstOrNew([
                'uuid' => $device_uuid,
                'user_id' => $user->id,
            ]);
            $device->token = $device_token;
            $device->is_logging_in = true;
            $device->save();
        }

        $token = \JWTAuth::fromUser($user);

        return response()->json(arrayView('phpsoft.users::tokens/show', compact('token', 'user')));
    }
}
