<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Scholarship;
use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Requests\ReminderRequest;
use App\Http\Requests\ScholarshipRequest;
use App\Http\Requests\ScholarshipUpdateRequest;
use App\Http\Requests\ReactiveScholarshipRequest;
use App\Repositories\Contracts\MessageBreadInterface;
use App\Repositories\Contracts\ScholarshipBreadInterface;

class ScholarshipController extends Controller
{
    public $bread;

    public function __construct(ScholarshipBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function update(Scholarship $scholarship, ScholarshipUpdateRequest $request)
    {
        $user = \Auth::user();
        $district_ids = $request->get('district_ids');

        $data               = $request->all();

        $school_ids = $request->get('school_ids');

         if ($user->isCounselor()) {
            $data       = $request->except(['user_id', 'school_ids', 'district_ids']);
            // $school_ids = [$user->school_id];
        } else {
            $data       = $request->except(['school_ids', 'district_ids']);
         }

        $minPrice = isset($data['amount']) && !isset($data['min_price']) ? $data['amount'] : (isset($data['min_price']) ? $data['min_price'] : 0);

        if ($minPrice) {
            $data['amount'] = $minPrice;
            $data['min_price'] = $minPrice;
            $data['max_price'] = isset($data['max_price']) ? $data['max_price'] : $minPrice;
        }

        if ($request->get('is_applied_to_state')) {
            $data['is_applied_to_county'] = 0;
        } elseif ($request->get('is_applied_to_county')) {
            $data['is_applied_to_state']  = 0;
        } else {
            $data['is_applied_to_county'] = 0;
            $data['is_applied_to_state']  = 0;
        }

        $data['updated_at'] = now();
        $scholarship->update($data);

        if (empty($request->get('is_applied_to_state'))) {
            if (empty($request->get('is_applied_to_county'))) {
                $scholarship->schools()->sync($school_ids);
                $scholarship->districts()->sync($district_ids);
            } else {
                $scholarship->schools()->sync([]);
                $scholarship->districts()->sync($district_ids);
            }
        } else {
            $scholarship->schools()->sync([]);
            $scholarship->districts()->sync([]);
        }

        return response()->json(arrayView('scholarship/read', ['scholarship' => $scholarship]), 200);
    }

    public function changeStatus(Scholarship $scholarship, ScholarshipUpdateRequest $request)
    {
        $scholarship->update($request->only('status'));

        return response()->json(arrayView('scholarship/read', ['scholarship' => $scholarship]), 200);
    }

    public function index(Request $request)
    {
        $filter = request()->input();
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        $query = Scholarship::queryOrders($sort)->v2FilterScholarshipByRole()->filterFields($filter);

        $scholarships = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function listScholarshipsByOwner(User $user, Request $request)
    {
        $scholarships = $this->bread->browse(['user_id' => $user->id]);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function listScholarshipsBySchool(Request $request)
    {
        $filter = request()->input();
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        $query = Scholarship::whereHas('schools', function ($q) {
                $q->where(['school_id' => \Auth::user()->school_id]);
            })->queryOrders($sort)->filterFields($filter);

        $scholarships = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function listFeatureScholarships(Request $request)
    {
        $user_id = request()->input('user_id', null);
        $filter  = request()->except(['user_id']);
        $size    = request()->input('page.size', 10);
        $number  = request()->input('page.number', 1);
        $sort    = explode(',', request()->input('sort', '-id'));

        $query = Scholarship::where(function ($q) use ($user_id) {
            //get all scholarship in school
            $q->orWhere(function ($q) use ($user_id) {
                $q->where(function ($q) use ($user_id) {
                    $q->whereHas('schools', function ($q) use ($user_id) {
                        if ($user_id) {
                            $q->whereIn('school_id', function ($q) use ($user_id) {
                                $q->select(\DB::raw('users.school_id'))
                                    ->from('users')
                                    ->join('users_parents', function ($join) {
                                        $join->on('users_parents.user_id', '=', 'users.id')
                                            ->where('users_parents.parent_id', \Auth::user()->id);
                                    })->where('users.id', $user_id);
                            });
                        } else {
                            $q->where(['school_id' => \Auth::user()->school_id]);
                        }
                    });
                });
            });
            // get all job in district
            $q->orWhere(function ($q) use ($user_id) {
                $q->where('is_applied_to_county', true);
                if($user_id) {
                    $q->whereHas('districts', function ($q) use ($user_id) {
                        $q->whereIn('districts_scholarships.district_id', function ($q) use ($user_id) {
                            $q->select('districts.id')
                            ->from('districts')
                            ->join('schools', 'districts.id', '=', 'schools.district_id')
                            ->join('users', function ($join) use ($user_id) {
                                $join->on('schools.id', '=', 'users.school_id')
                                ->where('users.id', $user_id);
                            })->join('users_parents', function ($join) {
                                $join->on('users_parents.user_id', '=', 'users.id')
                                ->where('users_parents.parent_id', \Auth::user()->id);
                            });
                        });
                    });
                } else {
                    $q->whereHas('districts', function ($q) {
                        $q->where('districts_scholarships.district_id', \Auth::user()->school->district_id);
                    });
                }
            });
            //get all scholarship is_applied_to_state
            $q->orWhere(function ($q) use ($user_id) {
                $q->where('scholarships.is_applied_to_state', true);
                $q->whereNotNull('scholarships.state_id');
                if($user_id) {
                    $q->where('state_id', function ($q) use ($user_id) {
                        $q->select('districts.state_id')
                        ->from('districts')
                        ->join('schools', 'districts.id', '=', 'schools.district_id')
                        ->join('users', function ($join) use ($user_id) {
                            $join->on('schools.id', '=', 'users.school_id')
                            ->where('users.id', $user_id);
                        })->join('users_parents', function ($join) {
                            $join->on('users_parents.user_id', '=', 'users.id')
                            ->where('users_parents.parent_id', \Auth::user()->id);
                        });
                    });
                } else {
                    $q->where('scholarships.state_id', \Auth::user()->school->district->state_id);
                }
            });
        })->where(['status' => Scholarship::APPROVED])
        ->whereRaw('IF (`deadline_on` IS NULL, `deadline_on` IS NULL, `deadline_on` >= CURDATE())')
        ->queryOrders($sort)->filterFields($filter);

        $scholarships = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function store(ScholarshipRequest $request)
    {
        $user       = \Auth::user();
        $school_ids = $request->get('school_ids');
        $district_ids = $request->get('district_ids');

        if ($user->isCounselor() && !$user->school_id) {
            throw new \App\Exceptions\UnprocessableEntityException('User need update School before create Scholarship');
        }

        if ($user->isCounselor()) {
            $school_ids = [$user->school_id];
        } elseif ($user->isDistrictAdmin()) {
            $district_ids = [$user->district_id];
        }

        $minPrice = $request->get('amount') && !$request->get('min_price')
            ? $request->get('amount')
            : $request->get('min_price');

        $data = [
            'name'                      => $request->get('name'),
            'organization'              => $request->get('organization'),
            'amount'                    => $minPrice,
            'min_price'                 => $minPrice,
            'max_price'                 => $request->get('max_price') ? $request->get('max_price') : $minPrice,
            'deadline_on'               => $request->get('deadline_on'),
            'description'               => $request->get('description'),
            'quantity'                  => $request->get('quantity'),
            'application_form_file'     => $request->get('application_form_file'),
            'application_form_link'     => $request->get('application_form_link'),
            'application_form_filename' => $request->get('application_form_filename'),
            'user_id'                   => $user->id,
            'status'                    => $user->isBizOwner() ? 0 : 1,
            'state_id'                  => $request->get('state_id')
        ];

        if ($request->get('is_applied_to_state')) {
            $data['is_applied_to_state'] = $request->get('is_applied_to_state');
        } elseif ($request->get('is_applied_to_county')) {
            $data['is_applied_to_county'] = $request->get('is_applied_to_county');
        }

        $scholarship = \App\Models\Scholarship::create($data);

        if (empty($request->get('is_applied_to_state'))) {
            if (empty($request->get('is_applied_to_county'))) {
                $scholarship->schools()->attach($school_ids);
                $scholarship->districts()->attach($district_ids);
            } else {
                $scholarship->districts()->attach($district_ids);
            }
        }

        return response()->json(arrayView('scholarship/read', ['scholarship' => $scholarship]), 201);
    }

    public function reactive(Scholarship $scholarship, ReactiveScholarshipRequest $request)
    {
        $scholarship->update($request->only('deadline_on'));

        \DB::table('activities')
            ->where('activities.actor_type', 'User')
            ->where('activities.verb', 'apply')
            ->where('activities.object_type', 'Scholarship')
            ->where('activities.object_id', $scholarship->id)->delete();

        return response()->json(arrayView('scholarship/read', ['scholarship' => $scholarship]), 200);
    }

    public function delete(Scholarship $scholarship)
    {
        $scholarship->activities()->delete();
        $scholarship->forceDelete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $resource = Scholarship::find($id);
                if ($resource) {
                    $resource->activities()->delete();
                    $resource->forceDelete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Scholarship. Scholarship is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(Scholarship $scholarship)
    {
        return response()->json(arrayView('scholarship/read', ['scholarship' => $scholarship]), 200);
    }

    public function studentsCanSee(Scholarship $scholarship)
    {
        $filter = request()->input();
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        // case 1: Lấy các students nằm trong districts là có thể thấy
        // case 2: Lấy các students nằm trong schools là có thể thấy
        $query = User::where(function ($q) use ($scholarship) {
            // Là parent chỉ xem được của children
            $q->whereHas('parents', function ($q) {
                $q->where('parent_id', currentUser()->id);
            });
            // Lọc điều kiện student nào được nhìn thấy $scholarship
            $q->where(function ($q) use ($scholarship) {
                if ($scholarship->is_applied_to_district) {
                    $q->where(function ($q) use ($scholarship) {
                        $q->whereIn('school_id', function ($q) use ($scholarship) {
                            $q->select(\DB::raw('schools.id'))
                                ->from('schools')
                                ->join('districts', function ($join) use ($scholarship) {
                                    $join->on('districts.id', '=', 'schools.district_id')
                                        ->whereIn('districts.id', function ($q) use ($scholarship) {
                                            $q->select(\DB::raw('schools.district_id'))
                                            ->from('schools')
                                            ->join('schools_scholarships', function ($join) use ($scholarship) {
                                                $join->on('schools.id', '=', 'schools_scholarships.school_id')
                                                    ->where('schools_scholarships.scholarship_id', $scholarship->id);
                                            });
                                        });
                                });
                        });
                    });
                } else {
                    $q->where(function ($q) use ($scholarship) {
                        $q->whereIn('school_id', function ($q) use ($scholarship) {
                            $q->select(\DB::raw('schools.id'))
                            ->from('schools')
                            ->join('schools_scholarships', function ($join) use ($scholarship) {
                                $join->on('schools.id', '=', 'schools_scholarships.school_id')
                                    ->where('schools_scholarships.scholarship_id', $scholarship->id);
                            });
                        });
                    });
                }
            });
        })->queryOrders($sort)->filterFields($filter);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function sendReminder(Scholarship $scholarship, ReminderRequest $request, MessageBreadInterface $breadMessage)
    {
        $request->merge([
            'source' => [
                'type' => 'SCHOLARSHIP',
                'id' => [$scholarship->id],
            ],
            'type' => 'remind',
        ]);
        $data = $request->all();
        $message = $breadMessage->add($data);

        return response()->json(arrayView('messages/read', [
            'message' => $message,
        ]), 201);
    }

    public function totalSavingAndApplying()
    {
        $data = $this->bread->getSaveAndApplying();
        foreach ($data as $key => &$value) {
            $value->save = (int)$value->save;
            $value->applying = (int)$value->applying;
        }

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }
}
