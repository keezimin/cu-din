<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Pathway;
use Illuminate\Http\Request;

class UserPathwayController extends Controller
{
    public function index($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Pathway::whereHas('savedActivity', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $pathways = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('pathway/browse', [
            'pathways' => $pathways,
        ]), 200);
    }

    public function store(User $user, Pathway $pathway, Request $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'save';
        $object = $pathway->toActivityObject();
        $target = null;
        $meta = null;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function delete(User $user, Pathway $pathway, Request $request)
    {
        $actor = $user;
        $verb = 'save';
        $object = $pathway;

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }

    public function listSavedStudents(Pathway $pathway, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $pathway->id;
        $query = User::with(['pathwayActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'save')
                ->where('activities.object_type', 'Pathway')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
