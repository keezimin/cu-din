<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use App\Http\Requests\TaskUpdateRequest;
use App\Repositories\Contracts\TaskBreadInterface;

class TaskController extends Controller
{
    public $bread;

    public function __construct(TaskBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Task::queryOrders($sort)->filterItemsByRole()->withTrashed()->filterFields($filter);

        $tasks = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('task/browse', [
            'tasks' => $tasks,
        ]), 200);
    }

    public function update(Task $task, TaskUpdateRequest $request)
    {
        $task->update($request->all());

        return response()->json(arrayView('task/read', ['task' => $task]), 200);
    }

    public function store(TaskRequest $request)
    {
        $user = \Auth::user();

        $task = Task::create([
            'name' => $request->get('name'),
            'due_date' => $request->get('due_date'),
            'checklist_id' => $request->get('checklist_id'),
            'owner_id' => $user->id,
        ]);

        return response()->json(arrayView('task/read', ['task' => $task]), 201);
    }

    public function delete(Task $task, Request $request)
    {
        if ($request->get('force') === 'true' || $request->get('force') === true) {
            $task->forceDelete();
        } else {
            $task->delete();
        }
        $task->delete();

        return response()->json(null, 204);
    }

    public function restore(Task $task, Request $request)
    {
        $task->restore();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $task = Task::find($id);
                if ($task) {
                    if ($request->get('force') === 'true' || $request->get('force') === true) {
                        $task->forceDelete();
                    } else {
                        $task->delete();
                    }
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Task. Task is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(Task $task)
    {
        return response()->json(arrayView('task/read', ['task' => $task]), 200);
    }
}
