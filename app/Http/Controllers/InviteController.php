<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationRequest;
use App\Repositories\Contracts\CounselorBreadInterface;
use App\User;
use \App\Models\Invitation;

class InviteController extends Controller
{
    public function __construct(CounselorBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function studentInviteParent(User $user, InvitationRequest $request)
    {
        $emails = $request->email;

        foreach ($emails as $email) {
            $data = [
                'fullname' => '',
                'email' => $email,
                'user_id' => $user->id,
            ];
            $parent = User::where('email', $email)->first();
            if ($parent && !$user->roles->first()) {
                $user->parents()->syncWithoutDetaching($parent->id);
            }
            Invitation::updateOrCreate($data, ['invited_at' => \Carbon\Carbon::now(), 'registed_user_id' => $parent ? $parent->id : null]);
        }

        \App\Services\InviteService::inviteParent($request->email, $user);

        return response()->json(null, 204);
    }

    public function employerInviteEmployer(User $user, InvitationRequest $request)
    {
        $emails = $request->email;

        foreach ($emails as $email) {
            $data = [
                'fullname' => '',
                'email' => $email,
                'user_id' => $user->id,
            ];
            $employer = User::where('email', $email)->first();

            Invitation::updateOrCreate($data, ['invited_at' => \Carbon\Carbon::now(), 'registed_user_id' => $employer ? $employer->id : null]);
        }

        \App\Services\InviteService::inviteEmployer($request->email, $user);

        return response()->json(null, 204);
    }
}
