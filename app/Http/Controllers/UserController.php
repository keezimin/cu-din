<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\Models\Message;
use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Http\Requests\AssignRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\RestoreUserRequest;
use App\Http\Requests\ApproveApplyRequest;
use App\Http\Requests\RegisterStudentRequest;
use App\Http\Requests\RelationshipRequest;
use Illuminate\Contracts\Auth\Access\Gate;
use App\Repositories\Contracts\UserBreadInterface;

class UserController extends Controller
{
    public function __construct(UserBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $trashed = request()->input('trashed', false);
        $search = request()->input('search', '');
        $district_of = request()->input('district_of');
        $roleName = request()->input('role');
        $filter = request()->except(['trashed', 'role', 'district_of']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->filterFields($filter)->searchUser($search);

        if ($roleName) {
            $query->withRole($roleName);
        }

        if (Auth::user()->isAdminOrCounselor() && $trashed) {
            $query->withTrashed();
        }

        if ($district_of) {
            $query->where(function ($q) use ($district_of) {
                $q->where('district_id', function ($q) use ($district_of) {
                    $q->select('schools.district_id')->from('schools')
                        ->where('schools.id', $district_of);
                });
                $q->orWhere('school_id', $district_of);
            });
            $query->whereHas('roles', function ($q) {
                $q->whereIn('roles.name', ['counselor', 'district_admin']);
            });
        }

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(
            arrayView(
                'phpsoft.users::user/browse',
                [
                    'users' => $users,
                ]
            ),
            200
        );
    }

    public function usersDeleted()
    {
        $filter = request()->input();
        $search = request()->input('search', '');
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->filterUserByRole()->filterFields($filter)->searchUser($search);

        $query->onlyTrashed();

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function getStudents()
    {
        $search = request()->input('search', '');
        $object_ids = array_filter(explode(',', request()->input('object_ids', '')));
        $trashed = request()->input('trashed', false);
        $ids = array_filter(explode(',', request()->input('ids', '')));
        $filter = request()->except(['trashed', 'ids']);
        $state_id = request()->input('state_id', null);
        $applied = request()->input('applied', []);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));
        $district_ids = array_filter(explode(',', request()->input('district_ids')));

        $query = User::queryOrders($sort)->whereDoesntHave('roles')->filterFields($filter)->searchUser($search);

        // case 1: get student in state
        // case 2: get student in district
        // case 3: get student in school
        if ($state_id) {
            $query->whereHas('school', function ($q) use ($state_id) {
                $q->whereHas('district', function ($q) use ($state_id) {
                    $q->where('state_id', $state_id);
                });
            });
        } elseif ($district_ids) {
            $query->whereHas('school', function ($q) use ($district_ids) {
                $q->whereIn('district_id', $district_ids);
            });
        } else {
            // $ids of schools in job
            if ($ids) {
                $query->whereIn('school_id', $ids);
            }
        }

        //$object_ids of scholarship/job
        if (in_array($applied, ['job', 'tradeSchool', 'scholarship']) && $object_ids) {
            $releted = "{$applied}Activities";
            $query->whereDoesntHave($releted, function ($q) use ($object_ids) {
                $q->whereIn('object_id', $object_ids);
            });
        }

        if (Auth::user()->isAdminOrCounselor() && $trashed) {
            $query->withTrashed();
        }

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function store(RegisterStudentRequest $request)
    {
        $user_exist = User::withTrashed()->where('email', $request->email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'fullname' => $request->get('first_name') . ' '. $request->get('last_name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'contact' => $request->get('contact'),
            'graduated_on' => $request->get('graduated_on'),
            'act_english' => $request->get('act_english'),
            'act_science' => $request->get('act_science'),
            'act_math' => $request->get('act_math'),
            'act_composite' => $request->get('act_composite'),
            'act_reading' => $request->get('act_reading'),
            'sat_math' => $request->get('sat_math'),
            'sat_verbal' => $request->get('sat_verbal'),
            'school_id' => $request->get('school_id'),
        ]);

        $invitation = Invitation::where('email', $user->email)->whereNull('registed_user_id')->update(['registed_user_id' => $user->id]);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 201);
    }

    public function restore(User $user, RestoreUserRequest $request)
    {
        if ($request->role) {
            $user->updateRole($request->role);
        }
        $user->update(array_merge($request->only('graduated_on'), ['deleted_at' => null]));

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 200);
    }

    public function enable(User $user)
    {
        $user->restore();
        $user->scholarships()->restore();

        if ($user->hasRole('business_owner')) {
            $user->otherJobs()->restore();
        } else {
            $user->myJobs()->restore();
        }

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 200);
    }

    public function update($id, UpdateUserRequest $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $owner = \Auth::user();

        $first_name = empty($request->first_name) ? $user->first_name : $request->first_name;
        $last_name = empty($request->last_name) ? $user->last_name : $request->last_name;

        if ($owner->isAdminOrCounselor() || $owner->isDistrictAdmin()) {
            if ($owner->isCounselor() && (($user->isCounselor() && !app(Gate::class)->allows('update', $user)) || $user->hasRole('super_admin'))) {
                return response()->json(null, 403);
            }
            if ($owner->isCounselor() && $user->school && $user->school_id != $owner->school_id) {
                return response()->json(null, 403);
            }
            if ($owner->isDistrictAdmin() && (($user->isDistrictAdmin() && !app(Gate::class)->allows('update', $user)) || $user->hasRole('super_admin'))) {
                return response()->json(null, 403);
            }
            if ($owner->isDistrictAdmin() && (!$user->isDistrictAdmin() && $user->school) && $user->school->district && $user->school->district->id != $owner->district_id) {
                return response()->json(null, 403);
            }

            $data = $request->all();

            if ($owner->isCounselor() && $user->isCounselor()) {
                $data = $request->except(['school_id']);
            }
            $first_name = empty($request->first_name) ? $user->first_name : $request->first_name;
            $last_name = empty($request->last_name) ? $user->last_name : $request->last_name;
            $data['fullname'] = $first_name. ' '. $last_name;
    
            $photo = $this->bread->updateProfile($user, $data);

            $user->saveUser($data);
        } else {
            $data = $request->except('password');
            $data['fullname'] = $first_name. ' '. $last_name;
            if (!app(Gate::class)->allows('update', $user)) {
                return response()->json(null, 403);
            }
            $user->update($data);
            $this->bread->updateProfile($user, $data);
        }

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 200);
    }

    public function makeRelationships(RelationshipRequest $request)
    {
        foreach ($request->students as $id) {
            $student = User::find($id);
            $student->parents()->syncWithoutDetaching($request->parents);
        }

        return response()->json(null, 204);
    }

    public function removeRelationships(RelationshipRequest $request)
    {
        foreach ($request->students as $id) {
            $student = User::find($id);
            $student->parents()->detach($request->parents);
        }

        return response()->json(null, 204);
    }

    public function approveJob(ApproveApplyRequest $request)
    {
        $currentUser = currentUser();
        $model = Message::$MAP_SOURCE_MODELS[$request->target];
        $object = $model::findOrFail($request->target_id);

        if($currentUser->isAdmin()) {
            $user = \App\User::where('id', $request->user_id)->firstOrFail();
        } else {
            $user = \App\User::where('id', $request->user_id)->where(function ($q) use ($currentUser) {
                $q->orWhere('school_id', $currentUser->school_id);
                $q->orWhereIn('school_id', function ($q) use ($currentUser) {
                    $q->select('schools.id')
                        ->from('schools')
                        ->join('districts', 'schools.district_id', '=', 'districts.id')
                        ->where('schools.district_id', $currentUser->district_id);
                });
            })->firstOrFail();
        }

        $activity = \App\Services\UserActionService::approve($user, $object);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function disapproveJob(ApproveApplyRequest $request)
    {
        $currentUser = currentUser();
        $model = Message::$MAP_SOURCE_MODELS[$request->target];
        $object = $model::findOrFail($request->target_id);

        if($currentUser->isAdmin()) {
            $user = \App\User::where('id', $request->user_id)->firstOrFail();
        } else {
            $user = \App\User::where('id', $request->user_id)->where(function ($q) use ($currentUser) {
                $q->orWhere('school_id', $currentUser->school_id);
                $q->orWhereIn('school_id', function ($q) use ($currentUser) {
                    $q->select('schools.id')
                        ->from('schools')
                        ->join('districts', 'schools.district_id', '=', 'districts.id')
                        ->where('schools.district_id', $currentUser->district_id);
                });
            })->firstOrFail();
        }

        $activity = \App\Services\UserActionService::disapprove($user, $object);

        return response()->json(null, 204);
    }

    public function listUserWaiting(Request $request)
    {
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        $query = \PhpSoft\Activity\Models\Activity::ofVerbs(['apply'])
            ->filterActivityByRole()
            ->OfStatus('interested')
            ->whereIn('object_type', ['Job'])
            ->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('career_jobs')
                    ->whereRaw('career_jobs.id = activities.object_id')
                    ->whereNull('career_jobs.deleted_at');
            })->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('users')
                    ->whereRaw('users.id = activities.actor_id')
                    ->whereNull('users.deleted_at');
            })->queryOrders($sort);

        $activities = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('activities/browse_v2', [
            'activities' => $activities,
        ]), 200);
    }

    public function listUserApplying(Request $request)
    {
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        $query = \PhpSoft\Activity\Models\Activity::ofVerbs(['apply'])
            ->filterActivityByRole()
            ->where('meta->status', '<>', 'interested')
            ->whereIn('object_type', ['Job'])
            ->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('career_jobs')
                    ->whereRaw('career_jobs.id = activities.object_id')
                    ->whereNull('career_jobs.deleted_at');
            })->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('users')
                    ->whereRaw('users.id = activities.actor_id')
                    ->whereNull('users.deleted_at');
            })->queryOrders($sort);

        $activities = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('activities/browse_v2', [
            'activities' => $activities,
        ]), 200);
    }

    public function delete($id)
    {
        $force = request()->get('force', false);

        $user = User::withTrashed()->findOrFail($id);
        $owner = \Auth::user();

        if ($owner->isAdminOrCounselor() || $owner->isDistrictAdmin()) {
            if ($owner->isCounselor() && (($user->isCounselor() && !app(Gate::class)->allows('update', $user)) || $user->hasRole('super_admin'))) {
                return response()->json(null, 403);
            }
            if ($owner->isCounselor() && $user->school && $user->school_id != $owner->school_id) {
                return response()->json(null, 403);
            }
            if ($owner->isDistrictAdmin() && (($user->isDistrictAdmin() && !app(Gate::class)->allows('update', $user)) || $user->hasRole('super_admin'))) {
                return response()->json(null, 403);
            }
            if ($owner->isDistrictAdmin() && $user->school && $user->school->district && $user->school->district->id != $owner->district_id) {
                return response()->json(null, 403);
            }
        } else {
            if (!app(Gate::class)->allows('update', $user)) {
                return response()->json(null, 403);
            }
        }

        if ($force == true) {
            if ($user->isCounselor()) {
                $query = $user->withTrashed();
                if ($query->where('id', $user->id)->whereHas('scholarships')->exists() ||
                    $query->where('id', $user->id)->whereHas('myJobs')->exists() ||
                    $query->where('id', $user->id)->whereHas('otherJobs')->exists() ||
                    $query->where('id', $user->id)->whereHas('myGroups')->exists() ||
                    $query->where('id', $user->id)->whereHas('resources')->exists() ||
                    $query->where('id', $user->id)->whereHas('myUsers')->exists() ||
                    $query->where('id', $user->id)->whereHas('checklists')->exists()) {
                    return throwError(['messages' => 'Please assign for another account.'], 406);
                }
            }
            $this->removeUser($user);

            return response()->json(null, 204);
        }

        if (!$user->hasRole('super_admin')) {
            $time = $user->freshTimestamp();
            $user->scholarships()->update(['deleted_at' => $time->toDateTimeString()]);

            if ($user->hasRole('business_owner')) {
                $user->otherJobs()->update(['deleted_at' => $time->toDateTimeString()]);
            } else {
                $user->myJobs()->update(['deleted_at' => $time->toDateTimeString()]);
            }

            $user->update(['deleted_at' => $user->fromDateTime($time)]);

            return response()->json(null, 204);
        }

        throw new \Illuminate\Auth\Access\AuthorizationException('Cannot delete user');
    }

    public function assign($id, AssignRequest $request)
    {
        $force = request()->get('force', false);
        $user = User::withTrashed()->findOrFail($id);
        $assignee = User::withTrashed()->findOrFail($request->get('user_id'));
        $owner = \Auth::user();
        if (($owner->isAdminOrCounselor() || $owner->isDistrictAdmin())
            && ($user->isCounselor() && ($assignee->isCounselor() || $assignee->isDistrictAdmin()))) {
            try {
                DB::transaction(function () use ($user, $assignee) {
                    DB::table('scholarships')->where('user_id', $user->id)
                        ->update(['user_id' => $assignee->id]);
                    DB::table('career_jobs')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                    DB::table('career_jobs')->where('user_id', $user->id)
                        ->update(['user_id' => $assignee->id]);
                    DB::table('groups')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                    DB::table('resources')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                    DB::table('checklists')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                    DB::table('users')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                    DB::table('trade_schools')->where('owner_id', $user->id)
                        ->update(['owner_id' => $assignee->id]);
                }, 5);
            } catch (Exception $e) {
                return response()->json($e->getMessage(), 500);
            }

            return response()->json(null, 204);
        }

        return response()->json(null, 403);
    }

    private function removeUser(User $user)
    {
        try {
            DB::beginTransaction();
            $user->scholarships()->withTrashed()->each(function ($scholarship) {
                $scholarship->activities()->delete();
            });
            $user->scholarships()->withTrashed()->forceDelete();
            $user->myJobs()->withTrashed()->each(function ($job) {
                $job->activities()->delete();
                $job->schools()->detach();
            });
            $user->myJobs()->withTrashed()->forceDelete();
            $user->otherJobs()->withTrashed()->each(function ($job) {
                $job->activities()->delete();
                $job->schools()->detach();
            });
            $user->otherJobs()->withTrashed()->forceDelete();
            $user->trades()->delete();
            $user->resources()->delete();
            $user->myGroups()->delete();
            $user->allTasks()->forceDelete();
            $user->checklists()->forceDelete();
            $user->parents()->detach();
            $user->childrens()->detach();
            $user->profile()->delete();
            $user->socials()->delete();
            $user->activities()->delete();
            $user->forceDelete();

            return DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }
}
