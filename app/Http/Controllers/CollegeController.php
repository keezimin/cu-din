<?php

namespace App\Http\Controllers;

use App\Models\College;
use Illuminate\Http\Request;

class CollegeController extends Controller
{
    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = College::queryOrders($sort)->filterFields($filter);

        $colleges = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('college/browse', [
            'colleges' => $colleges,
        ]), 200);
    }

    public function show(College $college)
    {
        return response()->json(arrayView('college/read', ['college' => $college]), 200);
    }
}
