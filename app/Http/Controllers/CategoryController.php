<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Contracts\CategoryBreadInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public $bread;

    public function __construct(CategoryBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $categories = $this->bread->browse();
        return response()->json(arrayView('category/browse', [
            'categories' => $categories,
        ]), 200);
    }

    public function show(Category $category)
    {
        return response()->json(arrayView('category/read', ['category' => $category]), 200);
    }

    public function store(CategoryRequest $request)
    {
        $category = Category::create([
            'name' => $request->get('name'),
        ]);

        return response()->json(arrayView('category/read', ['category' => $category]), 201);
    }

    public function update(Category $category, CategoryRequest $request)
    {
        $category->update($request->all());

        return response()->json(arrayView('category/read', ['category' => $category]), 200);
    }

    public function delete(Category $category)
    {
        $count = \App\Models\Document::where('category_id', $category->id)->count();

        if ($count) {
            throw new \Illuminate\Auth\Access\AuthorizationException('Cannot delete Category');
        }

        $category->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $count = \App\Models\Document::where('category_id', $id)->count();

                if ($count) {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Category. Category has linked with documents',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                    continue;
                }
                $category = Category::find($id);
                if ($category) {
                    $category->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete category. Category is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }
}
