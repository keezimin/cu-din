<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Requests\OrganizationRequest;
use App\Http\Requests\OrganizationUpdateRequest;
use App\Http\Requests\MultiDeleteOrganizationRequest;
use App\Repositories\Contracts\OrganizationBreadInterface;

class OrganizationController extends Controller
{
    public $bread;

    public function __construct(OrganizationBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Organization::queryOrders($sort)->filterFields($filter);
        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'like', \DB::raw("'%{$search}%'"));
            });
        }
        $organizations = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('organization/browse', [
            'organizations' => $organizations,
        ]), 200);
    }

    public function show(Organization $organization)
    {
        return response()->json(arrayView('organization/read', ['organization' => $organization]), 200);
    }

    public function store(OrganizationRequest $request)
    {
        $user = currentUser();
        $organization = Organization::create([
            'name'     => $request->get('name'),
            'user_id' => $request->get('user_id'),
            'district_id' => $request->get('district_id'),
            'updated_by' => $user->id
        ]);

        return response()->json(arrayView('organization/read', ['organization' => $organization]), 201);
    }

    public function update(Organization $organization, OrganizationUpdateRequest $request)
    {
        $user = currentUser();
        if ($user->isAdmin()) {
            $data = $request->all();
        } else {
            $data = $request->except(['user_id', 'district_id']);
        }
        $data['updated_by'] = $user->id;

        $organization->update($data);

        return response()->json(arrayView('organization/read', ['organization' => $organization]), 200);
    }

    public function delete(Organization $organization)
    {
        $count = \App\Models\School::where('organization_id', $organization->id)->count();
        if ($count) {
            throw new \Illuminate\Auth\Access\AuthorizationException('Can\'t delete Organization. Organization has linked with schools');
        }
        $organization->delete();

        return response()->json(null, 204);
    }

    public function getSchools(Organization $organization, Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $search = request()->input('search', []);
        $query = $organization->schools()->queryOrders($sort);

        foreach ($search as $field => $value) {
            if (!empty($value['id']) && isset($value['operator'])) {
                switch ($field) {
                    case 'state':
                        $query->whereHas('district.state', function($q) use ($value) {
                            if ($value['operator'] === '=') {
                                $q->whereIn('states.id', $value['id']);
                            }

                            if ($value['operator'] === '<>') {
                                $q->whereNotIn('states.id', $value['id']);
                            }
                        });
                        break;
                    case 'county':
                        $query->whereHas('district', function($q) use ($value) {
                            if ($value['operator'] === '=') {
                                $q->whereIn('districts.id', $value['id']);
                            }

                            if ($value['operator'] === '<>') {
                                $q->whereNotIn('districts.id', $value['id']);
                            }
                        });
                        break;
                }
            }
        }

        $query->filterFields($filter);
        $schools = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('school/browse', [
            'schools' => $schools,
        ]), 200);
    }

    public function assignSchool(Organization $organization, School $school, Request $request)
    {
        $school->update(['organization_id' => $organization->id]);

        return response()->json(null, 204);
    }

    public function unassignSchool(Organization $organization, School $school, Request $request)
    {
        $school->update(['organization_id' => null]);

        return response()->json(null, 204);
    }

    public function multi_delete(MultiDeleteOrganizationRequest $request)
    {
        $id_array = $request->get('ids');
        $data['meta']['errors'] = [];
        if (!empty($id_array)) {
            foreach ($id_array as $key => $id) {
                $count = \App\Models\School::where('organization_id', $id)->count();
                if ($count) {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Organization. Organization has linked with schools',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                    continue;
                }
                $organization = Organization::find($id);
                if ($organization) {
                    $organization->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Organization. Organization is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 400);
        } else {
            return response()->json(null, 204);
        }
    }
}
