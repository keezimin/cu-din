<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\College;
use Illuminate\Http\Request;
use PhpSoft\Activity\Models\Activity;
use App\Http\Requests\SaveCollegeRequest;

class UserCollegeController extends Controller
{
    public function index($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = [
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'verb' => 'save',
            'object_type' => 'College',
        ];

        $activities = app('activity')->browse($filter);

        return response()->json(arrayView('activities/browse', [
            'activities' => $activities,
        ]), 200);
    }

    public function applied($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);

        $filter = [
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'verb' => 'apply',
            'object_type' => 'College',
        ];

        $activities = app('activity')->browse($filter);

        return response()->json(arrayView('activities/browse', [
            'activities' => $activities,
        ]), 200);
    }

    public function applying($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = College::whereHas('applies', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $colleges = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('college/browse', [
            'colleges' => $colleges,
        ]), 200);
    }

    public function listSavedStudents(College $collectCode, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $collectCode->id;
        $query = User::with(['collegeActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'save')
                ->where('activities.object_type', 'College')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listAppliedStudents(College $collectCode, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $collectCode->id;
        $query = User::with(['collegeActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'College')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function store(User $user, SaveCollegeRequest $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'save';
        if ($request->college_name) {
            $object = College::updateOrCreate(['code' => $request->college_code], ['name' => $request->college_name])->toActivityObject();
        } else {
            $object = College::firstOrCreate(['code' => $request->college_code])->toActivityObject();
        }
        $target = null;
        $meta = $request->meta;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);
        $activity = app('activity')->edit($actor, $verb, $object, $target, ['order' => $activity->id]);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function apply(User $user, SaveCollegeRequest $request)
    {
        $object = College::updateOrCreate(['code' => $request->college_code], ['name' => $request->college_name]);

        $activity = \App\Services\UserActionService::applyCollege($user, $object);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function arrange(User $user, $item_idA, $item_idB, Request $request)
    {
        $a = Activity::findOrFail($item_idA);
        $b = Activity::findOrFail($item_idB);
        $metaA = $a->meta ?? [];
        $metaB = $b->meta ?? [];
        $tmp = $metaA['order'] ?? $a->id;
        $metaA['order'] = $metaB['order'] ?? $b->id;
        $metaB['order'] = $tmp;
        $a->meta = $metaA;
        $b->meta = $metaB;
        $a->save();
        $b->save();

        return response()->json(null, 204);
    }

    public function arrangeSort(User $user, Request $request)
    {
        $orders = $request->id;
        if ($orders) {
            $items = [];
            $sort = [];
            foreach ($orders as $i) {
                $item = Activity::findOrFail($i);
                $items[] = $item;
                $sort[] = $item->meta['order'];
            }
            rsort($sort);
            foreach ($items as $i => $item) {
                $tmpMeta = $item->meta;
                $tmpMeta['order'] = $sort[$i];
                $item->meta = $tmpMeta;
            }
            foreach ($items as $item) {
                $item->save();
            }
        }

        return response()->json(null, 204);
    }

    public function delete(User $user, Request $request)
    {
        $actor = $user;
        $verb = 'save';
        $object = College::where(['code' => $request->college_code])->firstOrFail();

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }
}
