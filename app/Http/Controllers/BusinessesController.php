<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\BusinessProfileRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Repositories\Contracts\BusinessBreadInterface;
use Illuminate\Http\Request;
use App\Models\Invitation;
use PhpSoft\Users\Models\Role;

class BusinessesController extends Controller
{
    public function __construct(BusinessBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->whereHas('roles', function ($q) {
            $q->where(['name'=> 'business_owner']);
        })->filterFields($filter)
        ->searchUser($search);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function store(RegisterUserRequest $request)
    {
        $user_exist = User::withTrashed()->where('email', $request->email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }
        $user = currentUser();
        $data = $user->isAdmin() ? $request->all() : $request->except(['active']);
        $data['fullname'] = $request->first_name . ' '. $request->last_name;

        $user = $this->bread->add($data);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 201);
    }

    public function update(User $business, BusinessProfileRequest $request)
    {
        $user = currentUser();
        $data = $user->isAdmin() ? $request->except(['about', 'culture']) : $request->except(['about', 'culture', 'active']);
        $profile = $this->bread->updateProfile($business, $data);

        if (isset($data['dataImage'])) {
            $photo = $this->bread->updatePhoto($business, $data);
        }

        if (isset($request->about)) {
            $data['about'] = json_encode($request->about);
        }
        if (isset($request->culture)) {
            $data['culture'] = json_encode($request->culture);
        }

        $newBusiness = $this->bread->updateNewBusinessProfile($business, $data);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $business,
        ]), 200);
    }

    public function getBusinessByPathway(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));
        $careerClusterId = $filter['career_cluster_id'] ?? null;

        $query = User::query()->select('users.id');
        if (!empty($careerClusterId)) {
            $query->join('pathways', 'users.id', '=', 'pathways.user_id');
            $query->where('pathways.career_cluster_id', $careerClusterId)->where('pathways.active', true);
        }
        $query->join('business_profiles', 'users.id', '=', 'business_profiles.user_id')
            ->where('business_profiles.active', true)
            ->where('business_profiles.id', '!=', null);
        $query->join('profiles', 'users.id', '=', 'profiles.user_id');
        $query->orderBy('profiles.contact_person');

        $ids = $query->pluck('id')->all();
        $query = User::query()->whereIn('id', $ids)->orderByRaw('FIELD(id,'. implode(",", $ids). ')');
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function createBusinessInvited(RegisterUserRequest $request)
    {
        $email = $request->email;
        $user_exist = User::withTrashed()->where('email', $email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }
        $invitation = Invitation::whereNull('registed_user_id')->get();

        $userInvite = null;
        foreach ($invitation as $key => $value) {
            // check email invited
            if ($value->email == $email) {
                $userInvite = User::find($value->user_id);
                break;
            }
        }
        if ($userInvite && $userInvite->isBizOwner()) {
            $data = $request->all();
            $data['fullname'] = $request->first_name . ' '. $request->last_name;
            if ($data['password']) {
                $data['password'] = bcrypt($data['password']);
            }

            $newEmployer = User::create($data);
            $role = Role::firstOrCreate(
                [
                    'name' => 'business_owner',
                ]
            );
            $subscription = isset($data['subscription']) ? $data['subscription'] : '';
            $newEmployer->profile()->create(
                [
                    'user_id' => $newEmployer->id,
                    'address' => '',
                    'subscription' => $subscription,
                    'contact_person' => $data['contact_person']
                ]
            );
            $newEmployer->businessProfile()->create(
                [
                    'user_id' => $newEmployer->id,
                    'active' => false
                ]
            );
            $newEmployer->roles()->attach($role->id);

            return response()->json(arrayView('phpsoft.users::user/read', [
                'user' => $newEmployer,
            ]), 201);

        } else {
            return response()->json(null, 403);
        }
    }
}
