<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;

class StateController extends Controller
{
    public function index(Request $request)
    {
        $filter = request()->input();
        $size   = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort   = explode(',', request()->input('sort', '-id'));

        $states = State::queryOrders($sort)
                    ->filterFields($filter)
                    ->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('state/browse', [
            'states' => $states,
        ]), 200);
    }

    public function show(State $state)
    {
        return response()->json(arrayView('state/read', ['state' => $state]), 200);
    }
}
