<?php

namespace App\Http\Controllers;

use App\Models\Pathway;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function pathways()
    {
        $size = request()->input('page.size', 5);
        $number = request()->input('page.number', 1);
        $query = Pathway::with(['careerCluster', 'user'])->latest('created_at');
        $pathways = $query->paginate($size, ['*'], 'page[number]', $number);
    
        return response()->json(arrayView('pathway/browse', [
            'pathways' => $pathways,
        ]), 200);
    }
}
