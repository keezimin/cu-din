<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\Models\School;
use App\Repositories\Contracts\SchoolBreadInterface;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public $bread;

    public function __construct(SchoolBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $schools = $this->bread->browse();

        return response()->json(arrayView('school/browse', [
            'schools' => $schools,
        ]), 200);
    }

    public function show(School $school)
    {
        return response()->json(arrayView('school/read', ['school' => $school]), 200);
    }

    public function store(SchoolRequest $request)
    {
        $school = School::create([
            'name' => $request->get('name'),
            'code' => $request->get('code'),
            'photo' => $request->get('photo'),
            'website' => $request->get('website'),
        ]);
        return response()->json(arrayView('school/read', ['school' => $school]), 201);
    }

    public function delete(School $school)
    {
        $count = \App\User::where('school_id', $school->id)->count();
        if ($count) {
            throw new \Illuminate\Auth\Access\AuthorizationException('Cannot delete School');
        }
        $school->delete();
        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $count = \App\User::where('school_id', $id)->count();
                if ($count) {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete School. School has linked with users',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                    continue;
                }
                $school = School::find($id);
                if ($school) {
                    $school->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete School. School is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function update(School $school, SchoolRequest $request)
    {
        $school->update($request->all());
        return response()->json(arrayView('school/read', ['school' => $school]), 200);
    }
}
