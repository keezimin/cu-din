<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MessageCreateRequest;
use App\User;
use App\Models\Message;
use App\Repositories\Contracts\MessageBreadInterface;

class MessageController extends Controller
{
    public $bread;

    public function __construct(MessageBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = [
            'object_type' => 'Message',
            'actor_type' => 'User',
            'verb' => 'say',
        ];
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Message::queryOrders($sort)->filterMessageByRole()->filterFields($filter);

        $messages = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('messages/browse', [
            'messages' => $messages,
        ]), 200);
    }

    public function store(MessageCreateRequest $request)
    {
        $message = $this->bread->add($request->all());
        return response()->json(arrayView('messages/read', [
            'message' => $message,
        ]), 201);
    }

    public function show($id)
    {
        $user = \Auth::user();
        $message = $this->bread->read($id);
        if ($user->hasRole('super_admin') || $user->isOwner($message, 'actor_id') || $user->isOwner($message, 'target_id') ) {
            return response()->json(arrayView('messages/read', [
                'message' => $message,
            ]), 200);
        }
        throw new \Illuminate\Database\Eloquent\ModelNotFoundException();
    }

    public function listMessagesUser(\App\User $user)
    {
        $filter = [
            'object_type' => 'Message',
            'target_type' => 'User',
            'target_id' => $user->id,
            'verb' => 'message',
        ];
        $messages = $this->bread->browse($filter);
        return response()->json(arrayView('messages/browse', [
            'messages' => $messages,
        ]), 200);
    }

    public function markAsRead(\App\User $user, Request $request)
    {
        $filter = [
            'object_type' => 'Message',
            'target_type' => 'User',
            'target_id' => $user->id,
            'verb' => 'message',
        ];
        if ($request->id) {
            $filter['id'] = $request->id;
        }
        $result = $this->bread->update($filter, ['meta->seen_at' => \Carbon\Carbon::now()->toIso8601String()]);
        return response()->json(null, 204);
    }

    public function statisticsUser(\App\User $user, Request $request)
    {
        $filter = [
            'object_type' => 'Message',
            'target_type' => 'User',
            'target_id' => $user->id,
            'verb' => 'message',
            'meta->$_seen_at' => 'null',
        ];
        $unread_messages = $this->bread->count($filter);
        $data = [
            'count' => compact('unread_messages'),
        ];
        return response()->json(arrayView('data/read', compact('data')), 200);
    }

    public function delete(Message $message)
    {
        $id = $message->id;
        Message::where('object_type', 'Message')->where(function ($q) use ($id) {
            $q->orWhere(function ($q) use ($id) {
                $q->whereIn('id', [$id]);
            });
            $q->orWhere(function ($q) use ($id) {
                $q->whereIn('object_id', [$id]);
            });
        })->delete();
        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $messageIds = $request->get('id');
        if(is_array($messageIds)) {
            Message::where('object_type', 'Message')->where(function ($q) use ($messageIds) {
                $q->orWhere(function ($q) use ($messageIds) {
                    $q->whereIn('id', $messageIds);
                });
                $q->orWhere(function ($q) use ($messageIds) {
                    $q->whereIn('object_id', $messageIds);
                });
            })->delete();
        }
        return response()->json(null, 204);
    }
}
