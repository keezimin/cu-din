<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Repositories\Contracts\UserBreadInterface;
use App\User;

class ParentController extends Controller
{
    public function __construct(UserBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $search = request()->input('search', '');
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::queryOrders($sort)->whereHas('roles', function ($q) {
            $q->where(['name' => 'parent']);
        })->filterFields($filter)->searchUser($search);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function getChildrens(User $parent)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = $parent->childrens()->queryOrders($sort)->whereDoesntHave('roles')->filterFields($filter);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function store(RegisterUserRequest $request)
    {
        $user_exist = User::withTrashed()->where('email', $request->email)->first();
        if ($user_exist) {
            $roles = $user_exist->getRoles();
            if (empty($roles)) {
                $role = 'student';
            } else {
                $role = $roles[0];
            }
            throw new \App\Exceptions\EmailExistException($user_exist->id, $role);
        }
        $data = $request->all();
        $data['fullname'] = $request->first_name . ' '. $request->last_name;
        $user = $this->bread->addParent($data);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 201);
    }

    public function update(User $parent, ProfileRequest $request)
    {
        $profile = $this->bread->updateProfile($parent, $request->all());

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $parent,
        ]), 200);
    }
}
