<?php

namespace App\Http\Controllers;

use Auth;
use Input;
use JWTAuth;
use Validator;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    public $bread;

    /**
     * Check authentication
     *
     * @return boolean
     */
    public function checkAuth()
    {
        return !empty(Auth::user());
    }

    public function getUser()
    {
        $user = null;
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
        }
        return $user;
    }

    /**
     * thow Error
     *
     * @return object
     */
    public function throwError($message, $code)
    {
        if (!is_array($message)) {
            $message = [$message];
        }
        throw new \Illuminate\Http\Exception\HttpResponseException(new \Illuminate\Http\JsonResponse($message, $code));
    }
}
