<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SettingUpdateRequest;

class SettingController extends Controller
{
    public function index()
    {
        $filter = request()->input();

        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-key'));

        $query = \App\Models\Setting::queryOrders($sort)->filterFields($filter);
        $settings = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('setting/browse', [
            'settings' => $settings,
        ]), 200);
    }

    public function update(SettingUpdateRequest $request)
    {
        $data = $request->get('data');
        foreach ($data as $key => $value) {
            if ($key == 'app_platform') {
                $value = json_encode($value);
            }
            \DB::table('settings')
                ->where('key', $key)
                ->update(['value' => $value]);
        }

        return response()->json(null, 204);
    }
}
