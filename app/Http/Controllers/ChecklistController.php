<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\College;
use App\Models\Checklist;
use Illuminate\Http\Request;
use App\Http\Requests\ChecklistRequest;
use App\Http\Requests\AddChecklistRequest;
use App\Http\Requests\ChecklistUpdateRequest;
use App\Repositories\Contracts\ChecklistBreadInterface;

class ChecklistController extends Controller
{
    public $bread;

    public function __construct(ChecklistBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Checklist::queryOrders($sort)->filterItemsByRole()->where('object_id', '<>', null)->filterFields($filter);

        $checklists = $query->paginate($size, ['*'], 'page[number]', $number);

        $checklists->load('object');

        return response()->json(arrayView('checklist/browse', [
            'checklists' => $checklists,
        ]), 200);
    }

    public function getChecklists(User $user, Request $request)
    {
        $filter = $request->all();

        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = $user->checklists()->withTrashed()->queryOrders($sort)->filterFields($filter);

        $checklists = $query->paginate($size, ['*'], 'page[number]', $number);

        $checklists->load('object');

        return response()->json(arrayView('checklist/browse', [
            'checklists' => $checklists,
        ]), 200);
    }

    public function getTasks(Checklist $checklist, Request $request)
    {
        $filter = $request->all();

        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = $checklist->tasks()->withTrashed()->queryOrders($sort)->filterFields($filter);

        $tasks = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('task/browse', [
            'tasks' => $tasks,
        ]), 200);
    }

    public function update(Checklist $checklist, ChecklistUpdateRequest $request)
    {
        $tasks = $request->get('tasks');

        if ($request->get('type') === 'colleges') {
            $college = College::firstOrCreate(['code' => $request->get('college_code'), 'name' => $request->get('college_name')]);
        }

        $attributes = $request->except(['tasks']);

        if ($attributes) {
            $attributes['object_id'] = isset($college) ? $college->id : $request->get('object_id');
        }

        $checklist->update($attributes);

        if ($tasks) {
            $checklist->tasks()->createMany($tasks);
        }

        return response()->json(arrayView('checklist/read', ['checklist' => $checklist]), 200);
    }

    public function store(ChecklistRequest $request)
    {
        $user = \Auth::user();

        $tasks = $request->get('tasks');

        if ($request->get('type') === 'colleges') {
            $college = College::firstOrCreate(['code' => $request->get('college_code'), 'name' => $request->get('college_name')]);
        }

        $checklist = Checklist::create([
            'name' => $request->get('name'),
            'object_id' => isset($college) ? $college->id : $request->get('object_id'),
            'type' => $request->get('type'),
            'owner_id' => $user->id,
        ]);

        if ($tasks) {
            collect($tasks)->map(function ($task) use ($user) {
                $task['owner_id'] = $user->id;

                return $task;
            });
            $checklist->tasks()->createMany($tasks);
        }

        return response()->json(arrayView('checklist/read', ['checklist' => $checklist]), 201);
    }

    public function addChecklist(User $user, AddChecklistRequest $request)
    {
        $object_id = $request->object_id;
        if ($request->type === 'colleges') {
            $college = College::firstOrCreate(['code' => $request->college_code, 'name' => $request->college_name]);
            $object_id = $college->id;
        }
        $checklists = Checklist::select('name', 'id')->whereHas('owner', function ($q) use ($user) {
            $q->where(['school_id' => $user->school_id]);
        })->where(['type' => $request->type, 'object_id' => $object_id])->with('tasks')->get();

        if (count($checklists) > 0) {
            $checklists->each(function ($checklist) use ($user) {
                $attributes = $checklist->toArray();
                $attributes['owner_id'] = $user->id;
                $stChecklist = Checklist::create($attributes);
                if ($checklist->tasks) {
                    $tasks = $checklist->tasks()->withTrashed()->select('name', 'due_date')->get();

                    $tasks->map(function ($task) use ($user) {
                        $task['owner_id'] = $user->id;

                        return $task;
                    });

                    $stChecklist->tasks()->createMany($tasks->toArray());
                }
            });
        } else {
            $object = \DB::table($request->type)->find($object_id);
            $checklist = Checklist::create([
                'name' => $object->name,
                'owner_id' => $user->id,
            ]);
        }

        return response()->json(null, 204);
    }

    public function delete(Checklist $checklist, Request $request)
    {
        if ($request->get('force') === 'true' || $request->get('force') === true) {
            $checklist->tasks()->forceDelete();
            $checklist->forceDelete();
        } else {
            $checklist->delete();
        }

        return response()->json(null, 204);
    }

    public function restore(Checklist $checklist, Request $request)
    {
        $checklist->restore();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $checklist = Checklist::find($id);
                if ($checklist) {
                    if ($request->get('force') === 'true' || $request->get('force') === true) {
                        $checklist->tasks()->forceDelete();
                        $checklist->forceDelete();
                    } else {
                        $checklist->tasks()->delete();
                        $checklist->delete();
                    }
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Checklist. Checklist is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(Checklist $checklist)
    {
        return response()->json(arrayView('checklist/read', ['checklist' => $checklist]), 200);
    }
}
