<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Document;
use App\Repositories\Contracts\DocumentBreadInterface;
use App\User;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public $bread;

    public function __construct(DocumentBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function store(DocumentRequest $request)
    {
        $user = \Auth::user();

        $type = $request->get('type');

        $document = Document::create([
            'image' => $request->get('image'),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'title' => $request->get('title'),
            'category_id' => $request->get('category_id'),
            'owner_id' => $user->id,
        ]);

        return response()->json(arrayView('document/read', [
            'document' => $document,
        ]), 201);
    }

    public function update(Document $document, UpdateDocumentRequest $request)
    {
        $document->update($request->all());

        return response()->json(arrayView('document/read', [
            'document' => $document,
        ]), 200);
    }

    public function show(Document $document)
    {
        return response()->json(arrayView('document/read', ['document' => $document]), 200);
    }

    public function index()
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Document::queryOrders($sort)->filterFields($filter);

        $documents = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('document/browse', [
            'documents' => $documents,
        ]), 200);
    }

    public function delete(Document $document)
    {
        $document->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $document = Document::find($id);
                if ($document) {
                    $document->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete document. Document is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }
}
