<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Resource;
use Illuminate\Http\Request;
use App\Http\Requests\ResourceRequest;
use App\Http\Requests\UpdateResourceRequest;
use App\Repositories\Contracts\MessageBreadInterface;
use App\Repositories\Contracts\ResourceBreadInterface;

class ResourceController extends Controller
{
    public $bread;

    public function __construct(ResourceBreadInterface $bread, MessageBreadInterface $messageBread)
    {
        $this->bread = $bread;
        $this->messageBread = $messageBread;
    }

    public function store(ResourceRequest $request)
    {
        $user = \Auth::user();

        $type = $request->get('type');

        $resource = Resource::create([
            'image' => $request->get('image'),
            'image_filename' => $request->get('image_filename'),
            'link' => $request->get('link'),
            'summary' => $request->get('summary'),
            'title' => $request->get('title'),
            'type' => $type,
            'owner_id' => $user->id,
        ]);

        if ($type === Resource::TYPE_NEWS) {
            $data = [
                'target' => [
                    'type' => 'STUDENT_AND_TEACHER',
                ],
                'source' => [
                    'type' => "RESOURCE",
                    "id" => [$resource->id],
                ],
            ];
            $this->messageBread->add($data);
        }

        return response()->json(arrayView('resource/read', [
            'resource' => $resource,
        ]), 201);
    }

    public function update(Resource $resource, UpdateResourceRequest $request)
    {
        $resource->update($request->all());

        return response()->json(arrayView('resource/read', [
            'resource' => $resource,
        ]), 200);
    }

    public function show(Resource $resource)
    {
        return response()->json(arrayView('resource/read', ['resource' => $resource]), 200);
    }

    public function index()
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Resource::queryOrders($sort)->filterItemsByRole()->filterFields($filter);

        $resources = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('resource/browse', [
            'resources' => $resources,
        ]), 200);
    }

    public function delete(Resource $resource)
    {
        $resource->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $resource = Resource::find($id);
                if ($resource) {
                    $resource->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Resource. Resource is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }
}
