<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Http\Requests\SurveyRequest;
use App\Http\Requests\SurveyUpdateRequest;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $size = $request->input('page.size', 10);
        $number = $request->input('page.number', 1);
        $sort = explode(',', $request->input('sort', '-id'));

        $query = Survey::queryOrders($sort)->filterFields($filter);

        $surveys = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('survey/browse', [
            'surveys' => $surveys,
        ]), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SurveyRequest $request)
    {
        $data = $request->all();
        $survey = Survey::create($data);

        return response()->json(arrayView('survey/read', ['survey' => $survey]), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        return response()->json(arrayView('survey/read', ['survey' => $survey]), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Survey $survey, SurveyUpdateRequest $request)
    {
        $survey->update($request->all());
        return response()->json(arrayView('survey/read', ['survey' => $survey]), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        $survey->delete();

        return response()->json(null, 204);
    }
}
