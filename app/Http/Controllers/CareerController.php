<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Job;
use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Requests\JobRequest;
use App\Http\Requests\ReminderRequest;
use App\Http\Requests\JobUpdateRequest;
use App\Http\Requests\ReactiveJobRequest;
use App\Repositories\Contracts\JobBreadInterface;
use App\Repositories\Contracts\MessageBreadInterface;
use Illuminate\Support\Facades\Auth;

class CareerController extends Controller
{
    public $bread;

    public function __construct(JobBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Job::queryOrders($sort)->filterJobByRole()->filterFields($filter);

        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function studentsCanSee(Job $job)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::where(function ($q) use ($job) {
            $q->whereHas('parents', function ($q) {
                $q->where('parent_id', currentUser()->id);
            });

            $q->where(function ($q) use ($job) {
                $q->whereIn('school_id', function ($q) use ($job) {
                    $q->select(\DB::raw('schools_jobs.school_id'))
                        ->from('schools_jobs')
                        ->join('schools', function ($join) use ($job) {
                            $join->on('schools.id', '=', 'schools_jobs.school_id')
                                ->where('schools_jobs.career_job_id', $job->id);
                        });
                });
            });
        })->queryOrders($sort)->filterFields($filter);

        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function update(Job $job, JobUpdateRequest $request)
    {
        $user               = \Auth::user();
        $school_ids         = $request->get('school_ids');
        $district_ids       = $request->get('district_ids');
        $data               = $request->except(['school_ids', 'district_ids']);
        $data['updated_at'] = now();

        if ($request->get('is_applied_to_state')) {
            $data['is_applied_to_county'] = 0;
        } elseif ($request->get('is_applied_to_county')) {
            $data['is_applied_to_state']  = 0;
        } else {
            $data['is_applied_to_county'] = 0;
            $data['is_applied_to_state']  = 0;
        }

        $job->update($data);

        if (empty($request->get('is_applied_to_state'))) {
            if (empty($request->get('is_applied_to_county'))) {
                $job->schools()->sync($school_ids);
                $job->districts()->sync($district_ids);
            } else {
                $job->schools()->sync([]);
                $job->districts()->sync($district_ids);
            }
        } else {
            $job->schools()->sync([]);
            $job->districts()->sync([]);
        }

        return response()->json(arrayView('job/read', ['job' => $job]), 200);
    }

    public function listJobsByOwner(User $user, Request $request)
    {
        $jobs = $this->bread->browse(['user_id' => $user->id]);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function listJobsBySchool(Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Job::whereHas('schools', function ($q) {
            $q->where(['school_id' => \Auth::user()->school_id]);
        })->queryOrders($sort)->filterFields($filter);

        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function listFeatureJobs(Request $request)
    {
        $user_id = request()->input('user_id', null);
        $search = request()->input('search', '');
        $filter = request()->except(['search', 'user_id']);
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Job::where(function ($q) use ($user_id) {
            // get all jobs in school of student
            $q->orWhere(function ($q) use ($user_id) {
                $q->where(function ($q) use ($user_id) {
                    $q->whereHas('schools', function ($q) use ($user_id) {
                        if ($user_id) {
                            $q->whereIn('school_id', function ($q) use ($user_id) {
                                $q->select(\DB::raw('users.school_id'))
                                ->from('users')
                                ->join('users_parents', function ($join) {
                                    $join->on('users_parents.user_id', '=', 'users.id')
                                        ->where('users_parents.parent_id', \Auth::user()->id);
                                })->where('users.id', $user_id);
                            });
                        } else {
                            $q->where(['school_id' => \Auth::user()->school_id]);
                        }
                    });
                });
            });
            // get all job in district
            $q->orWhere(function ($q) use ($user_id) {
                $q->where('is_applied_to_county', true);
                if($user_id) {
                    $q->whereHas('districts', function ($q) use ($user_id) {
                        $q->whereIn('districts_career_jobs.district_id', function ($q) use ($user_id) {
                            $q->select('districts.id')
                            ->from('districts')
                            ->join('schools', 'districts.id', '=', 'schools.district_id')
                            ->join('users', function ($join) use ($user_id) {
                                $join->on('schools.id', '=', 'users.school_id')
                                ->where('users.id', $user_id);
                            })->join('users_parents', function ($join) {
                                $join->on('users_parents.user_id', '=', 'users.id')
                                ->where('users_parents.parent_id', \Auth::user()->id);
                            });
                        });
                    });
                } else {
                    $q->whereHas('districts', function ($q) {
                        $q->where('districts_career_jobs.district_id', \Auth::user()->school->district_id);
                    });
                }
            });
            // get all job is is_applied_to_state
            $q->orWhere(function ($q) use ($user_id) {
                $q->where('career_jobs.is_applied_to_state', true);
                $q->whereNotNull('career_jobs.state_id');
                if($user_id) {
                    $q->where('state_id', function ($q) use ($user_id) {
                        $q->select('districts.state_id')
                        ->from('districts')
                        ->join('schools', 'districts.id', '=', 'schools.district_id')
                        ->join('users', function ($join) use ($user_id) {
                            $join->on('schools.id', '=', 'users.school_id')
                            ->where('users.id', $user_id);
                        })->join('users_parents', function ($join) {
                            $join->on('users_parents.user_id', '=', 'users.id')
                            ->where('users_parents.parent_id', \Auth::user()->id);
                        });
                    });
                } else {
                    $q->where('career_jobs.state_id', \Auth::user()->school->district->state_id);
                }
            });
        })->where(['status' => Job::APPROVED])
        ->whereRaw('IF (`deadline_on` IS NULL, `deadline_on` IS NULL, `deadline_on` >= CURDATE())')
        ->queryOrders($sort)
        ->filterFields($filter);

        if ($search) {
            if ((starts_with($search, '%') || starts_with($search, '*'))) {
                $search = str_replace('*', '%', $search);
                $query = $query->where(function ($q) use ($search) {
                    $q->orWhere(function ($q) use ($search) {
                        $q->where('name', 'like', $search);
                    });
                    $q->orWhereHas('user', function ($q) use ($search) {
                        $q->where('fullname', 'like', $search);
                    });
                });
            }
        }

        $jobs = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('job/browse', [
            'jobs' => $jobs,
        ]), 200);
    }

    public function store(JobRequest $request)
    {
        $user = \Auth::user();

        $businessOwnerId = $request->get('user_id');
        $school_ids = $request->get('school_ids');
        $district_ids = $request->get('district_ids');
        $status = 0;

        if ($user->isBizOwner()) {
            $businessOwnerId = $user->id;
        }

        if ($user->isAdminOrCounselor() || $user->isDistrictAdmin()) {
            if ($user->isCounselor()) {
                $school_ids = [$user->school_id];
            } elseif ($user->isDistrictAdmin()) {
                $district_ids = [$user->district_id];
            }

            $status = Job::APPROVED;
        }

        $data = [
            'name'                      => $request->get('name'),
            'organization'              => $request->get('organization'),
            'description'               => $request->get('description'),
            'type'                      => $request->get('type'),
            'location'                  => $request->get('location'),
            'volunteer_hours'           => $request->get('volunteer_hours') ?? 0,
            'application_form_file'     => $request->get('application_form_file'),
            'application_form_link'     => $request->get('application_form_link'),
            'application_form_filename' => $request->get('application_form_filename'),
            'user_id'                   => $businessOwnerId,
            'owner_id'                  => $user->id,
            'status'                    => $status,
            'deadline_on'               => $request->get('deadline_on'),
            'career_cluster_id'         => $request->get('career_cluster_id'),
            'occupation_id'             => $request->get('occupation_id'),
            'state_id'                  => $request->get('state_id'),
            'pathway_id'                => $request->get('pathway_id'),
            'is_assign_to_pathway'      => $request->get('is_assign_to_pathway')
        ];

        if ($request->get('is_applied_to_state')) {
            $data['is_applied_to_state'] = $request->get('is_applied_to_state');
        } elseif ($request->get('is_applied_to_county')) {
            $data['is_applied_to_county'] = $request->get('is_applied_to_county');
        }

        $job = Job::create($data);

        if (empty($request->get('is_applied_to_state'))) {
            if (empty($request->get('is_applied_to_county'))) {
                $job->schools()->attach($school_ids);
                $job->districts()->attach($district_ids);
            } else {
                $job->districts()->attach($district_ids);
            }
        }
        if ($job && $job->status === Job::APPROVED) {
            $businessOwnerId = $job->user_id;
            $business = User::find($job->user_id);
            $actor = $business->toActivityObject();
            $object = (object) [
                'id' => '0',
                'object_type' => 'Message',
            ];
            $target = null;
            $metaRequest = [
                'source' => [
                    'id' => [$job->id],
                    'type' => 'JOB',
                ],
                'title' => '{{SENDER}} send you a notification. {{SENDER}} has added a new opportunity: {{OBJECT}}',
            ];
            $text = $metaRequest['title'];
            $text = str_replace('{{SENDER}}', $business->fullname, $text);
            $text = str_replace('{{OBJECT}}', $job->name, $text);
            $push = app('activity')->add($actor, 'say', $object, $target, $metaRequest);
            $object->id = $push->id;

            $query = User::with(['businessActivities' => function ($q) use ($businessOwnerId) {
                $q->where('object_id', $businessOwnerId);
            }])->whereIn('id', function ($q) use ($businessOwnerId) {
                $q->select(\DB::raw('activities.actor_id'))
                    ->from('activities')
                    ->where('activities.actor_type', 'User')
                    ->where('activities.verb', 'follow')
                    ->where('activities.object_type', 'User')
                    ->where('activities.object_id', $businessOwnerId);
            })->filterUserByRole();

            $idStudentFollow = $query->get()->pluck('id')->all();

            // push Notification
            foreach ($idStudentFollow as $key => $id) {
                $target = (object) [
                    'id' => $id,
                    'target_type' => 'User',
                ];
                $m = app('activity')->add($actor, 'message', $object, $target);

                \App\Services\PushNotificationService::push([$id], $text, [
                    'message_id' => $m->id,
                ]);
            }
        }

        return response()->json(arrayView('job/read', ['job' => $job]), 201);
    }

    public function delete(Job $job)
    {
        $job->schools()->detach();
        $job->activities()->delete();
        $job->forceDelete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $job = Job::find($id);
                if ($job) {
                    $job->schools()->detach();
                    $job->activities()->delete();
                    $job->forceDelete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Job. Job is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(Job $job)
    {
        return response()->json(arrayView('job/read', ['job' => $job]), 200);
    }

    public function sendReminder(Job $job, ReminderRequest $request, MessageBreadInterface $breadMessage)
    {
        $request->merge([
            'source' => [
                'type' => 'Job',
                'id' => [$job->id],
            ],
            'type' => 'remind',
        ]);
        $data = $request->all();
        $message = $breadMessage->add($data);

        return response()->json(arrayView('messages/read', [
            'message' => $message,
        ]), 201);
    }

    public function reactive(Job $job, ReactiveJobRequest $request)
    {
        $job->update($request->only('deadline_on'));

        \DB::table('activities')
            ->where('activities.actor_type', 'User')
            ->where('activities.verb', 'apply')
            ->where('activities.object_type', 'Job')
            ->where('activities.object_id', $job->id)->delete();

        return response()->json(arrayView('job/read', ['job' => $job]), 201);
    }

    public function changeStatus(Job $job, JobUpdateRequest $request)
    {
        $job->update($request->only('status'));
        if ($job && $job->status === Job::APPROVED) {
            $businessOwnerId = $job->user_id;
            $business = User::find($job->user_id);
            $actor = $business->toActivityObject();
            $object = (object) [
                'id' => '0',
                'object_type' => 'Message',
            ];
            $target = null;
            $metaRequest = [
                'source' => [
                    'id' => [$job->id],
                    'type' => 'JOB',
                ],
                'title' => '{{SENDER}} send you a notification. {{SENDER}} has added a new opportunity: {{OBJECT}}',
            ];
            $text = $metaRequest['title'];
            $text = str_replace('{{SENDER}}', $business->fullname, $text);
            $text = str_replace('{{OBJECT}}', $job->name, $text);
            $push = app('activity')->add($actor, 'say', $object, $target, $metaRequest);
            $object->id = $push->id;

            $query = User::with(['businessActivities' => function ($q) use ($businessOwnerId) {
                $q->where('object_id', $businessOwnerId);
            }])->whereIn('id', function ($q) use ($businessOwnerId) {
                $q->select(\DB::raw('activities.actor_id'))
                    ->from('activities')
                    ->where('activities.actor_type', 'User')
                    ->where('activities.verb', 'follow')
                    ->where('activities.object_type', 'User')
                    ->where('activities.object_id', $businessOwnerId);
            })->filterUserByRole();

            $idStudentFollow = $query->get()->pluck('id')->all();

            // push Notification
            foreach ($idStudentFollow as $key => $id) {
                $target = (object) [
                    'id' => $id,
                    'target_type' => 'User',
                ];
                $m = app('activity')->add($actor, 'message', $object, $target);

                \App\Services\PushNotificationService::push([$id], $text, [
                    'message_id' => $m->id,
                ]);
            }
        }

        return response()->json(arrayView('job/read', ['job' => $job]), 200);
    }

    public function getTopTypeJobs(Request $request)
    {
        $data = $this->bread->getTopTypeJobs();

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }

    public function getPopularJobs(Request $request)
    {
        $data = $this->bread->getPopularJob();

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }

    public function getStudentRequest()
    {
        $userRole = Auth::user()->isBizOwner();
        $data = [];
        if (!$userRole) {
            $data = $this->bread->getStudentRequest();
            foreach ($data as $key => &$value) {
                $value->meta = json_decode($value->meta, true);
            }
        }

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }

    public function getNewJobRequest(Request $request)
    {
        $userRole = Auth::user()->isBizOwner();
        $data = [];
        if (!$userRole) {
            $data = $this->bread->getNewJobRequest();
        }

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }

    public function getActiveClusters(Request $request)
    {
        $data = $this->bread->getCareerCluster();

        return response()->json(arrayView('dashboard/read', ['data' => $data]), 200);
    }

}
