<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Requests\DistrictRequest;
use App\Http\Requests\DistrictUpdateRequest;
use App\Repositories\Contracts\DistrictBreadInterface;

class DistrictController extends Controller
{
    public $bread;

    public function __construct(DistrictBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index()
    {
        $districts = $this->bread->browse();

        return response()->json(arrayView('district/browse', [
            'districts' => $districts,
        ]), 200);
    }

    public function show(District $district)
    {
        return response()->json(arrayView('district/read', ['district' => $district]), 200);
    }

    public function store(DistrictRequest $request)
    {
        $district = District::create([
            'name'     => $request->get('name'),
            'code'     => $request->get('code'),
            'state_id' => $request->get('state_id'),
        ]);

        return response()->json(arrayView('district/read', ['district' => $district]), 201);
    }

    public function update(District $district, DistrictUpdateRequest $request)
    {
        $data = [
            'state_id' => empty($request->state_id) ? $district->state_id : $request->state_id,
            'name'     => empty($request->name) ? $district->name : $request->name,
            'code'     => empty($request->code) ? $district->code : $request->code,
        ];
        $district->update($data);

        return response()->json(arrayView('district/read', ['district' => $district]), 200);
    }

    public function delete(District $district)
    {
        $count = \App\Models\School::where('district_id', $district->id)->count();
        if ($count) {
            throw new \Illuminate\Auth\Access\AuthorizationException('Can\'t delete District. District has linked with schools');
        }
        $district->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $count = \App\Models\School::where('district_id', $id)->count();
                if ($count) {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete District. District has linked with schools',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                    continue;
                }
                $district = District::find($id);
                if ($district) {
                    $district->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete District. District is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }
}
