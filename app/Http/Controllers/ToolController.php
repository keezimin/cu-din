<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Embed\Embed;

class ToolController extends Controller
{
    public function crawl(Request $request) {
        $info = Embed::create($request->link);
        return response()->json(arrayView('media/embed/read', ['info' => $info]), 200);
    }
}
