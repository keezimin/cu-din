<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserBusinessController extends Controller
{
    public function listFollow($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = User::whereHas('followActivity', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $business = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $business,
        ]), 200);
    }

    public function follow(User $user, User $business, Request $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'follow';
        $object = $business->toActivityObject();
        $target = null;
        $meta = null;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function unFollow(User $user, User $business, Request $request)
    {
        $actor = $user;
        $verb = 'follow';
        $object = $business;

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }

    public function listFollowedBusinesses(User $business, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $business->id;
        $query = User::with(['businessActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'follow')
                ->where('activities.object_type', 'User')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
