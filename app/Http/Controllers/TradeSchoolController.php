<?php

namespace App\Http\Controllers;

use App\Http\Requests\TradeSchoolRequest;
use App\Http\Requests\TradeSchoolUpdateRequest;
use App\Models\TradeSchool;
use App\Repositories\Contracts\TradeSchoolBreadInterface;
use App\User;
use Illuminate\Http\Request;

class TradeSchoolController extends Controller
{
    public $bread;

    public function __construct(TradeSchoolBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function index(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = TradeSchool::queryOrders($sort)->filterTradeByRole()->filterFields($filter);
        $tradeSchools = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('tradeSchool/browse', [
            'tradeSchools' => $tradeSchools,
        ]), 200);
    }

    public function getTrades(Request $request)
    {
        $filter = $request->all();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = TradeSchool::queryOrders($sort)->filterFields($filter);
        $tradeSchools = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('tradeSchool/browse', [
            'tradeSchools' => $tradeSchools,
        ]), 200);
    }

    public function update(TradeSchool $tradeSchool, TradeSchoolUpdateRequest $request)
    {
        $tradeSchool->update($request->all());

        return response()->json(arrayView('tradeSchool/read', ['tradeSchool' => $tradeSchool]), 200);
    }

    public function store(TradeSchoolRequest $request)
    {
        $user = \Auth::user();

        $tradeSchool = TradeSchool::create([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'website' => $request->get('website'),
            'photo' => $request->get('photo'),
            'description' => $request->get('description'),
            'district_id' => $request->get('district_id'),
            'application_form_file' => $request->get('application_form_file'),
            'application_form_link' => $request->get('application_form_link'),
            'application_form_filename' => $request->get('application_form_filename'),
            'owner_id' => $user->id,
        ]);

        return response()->json(arrayView('tradeSchool/read', ['tradeSchool' => $tradeSchool]), 201);
    }

    public function delete(TradeSchool $tradeSchool)
    {
        $tradeSchool->delete();

        return response()->json(null, 204);
    }

    public function multi_delete(Request $request)
    {
        $id_array = $request->get('id');
        $data['meta']['errors'] = [];
        if (is_array($id_array)) {
            foreach ($id_array as $key => $id) {
                $tradeSchool = TradeSchool::find($id);
                if ($tradeSchool) {
                    $tradeSchool->delete();
                } else {
                    $data['meta']['errors'][] = [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Trade School. Trade School is not found',
                        'meta' => [
                            'id' => $id,
                        ],
                    ];
                }
            }
        }
        if ($data['meta']['errors']) {
            return response()->json(arrayView('errors.exception', $data), 200);
        } else {
            return response()->json(null, 204);
        }
    }

    public function show(TradeSchool $tradeSchool)
    {
        return response()->json(arrayView('tradeSchool/read', ['tradeSchool' => $tradeSchool]), 200);
    }
}
