<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Scholarship;
use Illuminate\Http\Request;

class UserScholarshipController extends Controller
{
    public function index($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Scholarship::whereHas('savedActivity', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $scholarships = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function applying($id, Request $request)
    {
        $user = User::withTrashed()->findOrFail($id);
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $query = Scholarship::whereHas('applies', function ($q) use ($user) {
            $q->ofActor($user->id);
        })->queryOrders($sort)->filterFields($filter);
        $scholarships = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('scholarship/browse', [
            'scholarships' => $scholarships,
        ]), 200);
    }

    public function store(User $user, Scholarship $scholarship, Request $request)
    {
        $actor = $user->toActivityObject();
        $verb = 'save';
        $object = $scholarship->toActivityObject();
        $target = null;
        $meta = null;

        $activity = app('activity')->edit($actor, $verb, $object, $target, $meta);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 201);
    }

    public function apply(User $user, Scholarship $scholarship, Request $request)
    {
        $activity = \App\Services\UserActionService::applyScholarship($user, $scholarship);

        return response()->json(arrayView('activities/read', [
            'activity' => $activity,
        ]), 200);
    }

    public function delete(User $user, Scholarship $scholarship, Request $request)
    {
        $actor = $user;
        $verb = 'save';
        $object = $scholarship;

        app('activity')->delete($actor, $verb, $object);

        return response()->json(null, 204);
    }

    public function listAppliedStudents(Scholarship $scholarship, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $scholarship->id;
        $query = User::with(['scholarshipActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'apply')
                ->where('activities.object_type', 'Scholarship')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }

    public function listSavedStudents(Scholarship $scholarship, Request $request)
    {
        $filter = request()->input();
        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $id = $scholarship->id;
        $query = User::with(['scholarshipActivities' => function ($q) use ($id) {
            $q->where('object_id', $id);
        }])->whereIn('id', function ($q) use ($id) {
            $q->select(\DB::raw('activities.actor_id'))
                ->from('activities')
                ->where('activities.actor_type', 'User')
                ->where('activities.verb', 'save')
                ->where('activities.object_type', 'Scholarship')
                ->where('activities.object_id', $id);
        })->queryOrders($sort)->filterFields($filter)->filterUserByRole();
        $users = $query->paginate($size, ['*'], 'page[number]', $number);

        return response()->json(arrayView('phpsoft.users::user/browse', [
            'users' => $users,
        ]), 200);
    }
}
