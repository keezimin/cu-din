<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UploadImageRequest;
use Illuminate\Support\Facades\Storage;

class ImageUploadController extends Controller
{
    public function upload(UploadImageRequest $request) {
        $path = $request->file('file')->store('public');
        $url = env('APP_URL') . Storage::url($path);
        return response()->json(arrayView('media/image/read', ['url' => $url]), 200);
    }
}
