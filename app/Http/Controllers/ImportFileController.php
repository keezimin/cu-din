<?php

namespace App\Http\Controllers;

use \App\User;
use Illuminate\Support\Facades\Input;

class ImportFileController extends Controller
{
    public function importSchools()
    {
        $existents = (new \App\Services\ImportSchoolV2Service())->run(Input::file('file'));

        return response()->json($existents, 200);
    }

    public function importTrades()
    {
        $existents = (new \App\Services\ImportTradeSchoolService())->run(Input::file('file'));

        return response()->json($existents, 200);
    }

    public function importJobs()
    {
        $existents = (new \App\Services\ImportJobService())->run(Input::file('file'));

        return response()->json($existents, 200);
    }

    public function importScholarships()
    {
        $existents = (new \App\Services\ImportScholarshipService())->run(Input::file('file'));

        return response()->json($existents, 200);
    }

    public function importStudents(User $counselor)
    {
        $existents = (new \App\Services\ImportStudentService($counselor))->run(Input::file('file'));

        return response()->json($existents, 200);
    }

    public function importClusters()
    {
        $existents = (new \App\Services\ImportCareerClusterService())->run(Input::file('file'));
        
        return response()->json($existents, 200);
    }
}
