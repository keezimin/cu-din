<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = currentUser();

        switch (true) {
            case $user->isBizOwner():
            case $user->isAdminOrCounselor():
                \App\Models\Resource::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->where('owner_id', '=', $user->id);
                    }
                });
                \App\Models\Scholarship::addGlobalScope('user_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->where('user_id', '=', $user->id);
                    }
                });
                \App\User::addGlobalScope('school_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder = $builder->where('school_id', '=', $user->school_id);
                    } else {
                        $builder->withTrashed();
                    }
                });
                \App\Models\Group::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->where('owner_id', '=', $user->id);
                    }
                });
                \App\Models\TradeSchool::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->where('owner_id', '=', $user->id);
                    }
                });
                \App\Models\Checklist::addGlobalScope('owner_id', function (Builder $builder) use ($user, $request) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->withTrashed();
                        if ($request->id && $request->route()->getName() === 'getUserChecklists') {
                            $student = \App\User::find($request->id);
                            if (!($user->school_id == $student->school_id && !$student->isAdminOrCounselor())) {
                                $builder->where('owner_id', '=', $user->id);
                            }
                        } else {
                            $builder->where('owner_id', '=', $user->id);
                        }
                    }
                });
                \App\Models\Job::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                    if ($user->isCounselor() || $user->isBizOwner()) {
                        $builder->where('owner_id', '=', $user->id);
                    }
                });

                return $next($request);
            case $user->isDistrictAdmin():
                return $next($request);
            default:
                switch ($request->method()) {
                    case 'PATCH':
                    case 'POST':
                    case 'DELETE':
                        \App\Models\Checklist::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                            $builder->withTrashed()->where('owner_id', '=', $user->id);
                        });
                        \App\Models\Task::addGlobalScope('owner_id', function (Builder $builder) use ($user) {
                            $builder->withTrashed()->where('owner_id', '=', $user->id);
                        });

                        return $next($request);

                    default:
                        # code...
                        break;
                }

                return response()->json(null, 403);
        }
    }
}
