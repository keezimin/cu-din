<?php

namespace App\Http\Middleware;

use Closure;
use View;

class IgnoreIncluded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = explode(',', $request->get('ignore_included'));

        if (is_array($params)) {
            $request->ignore_included = $params;
        }

        return $next($request);
    }
}
