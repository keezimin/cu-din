<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = currentUser();

        if (in_array('super_admin', $user->getRoles())) {
            return $next($request);
        }

        return response()->json(null, 403);
    }
}
