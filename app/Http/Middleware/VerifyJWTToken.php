<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class VerifyJWTToken extends \Tymon\JWTAuth\Middleware\GetUserFromToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $this->auth->setRequest($request);
            $passwordConfirm = JWTAuth::parseToken()->getPayload()->get('password');
            $urlConfirm      = JWTAuth::parseToken()->getPayload()->get('domain');
            $user            = JWTAuth::parseToken()->authenticate();

            if (!JWTAuth::parseToken()->authenticate()) {
                return response()->json(arrayView('phpsoft.users::errors/authenticate', [
                    'error' => 'Unauthenticated.',
                ]), 401);
            }

            // check domain in token and current domain
            if (strpos($urlConfirm, env('APP_URL')) === false) {
                $token = JWTAuth::getToken();
                if ($token) {
                    JWTAuth::setToken($token)->invalidate();
                }

                return response()->json(arrayView('phpsoft.users::errors/authenticate', [
                    'error' => 'Unauthenticated.',
                ]), 401);
            }

            // check password in token and password of user
            if ($passwordConfirm != $user->password) {
                $token = JWTAuth::getToken();
                if ($token) {
                    JWTAuth::setToken($token)->invalidate();
                }

                return response()->json([
                    'Message' => 'Unauthenticated. Your password has been changed.'
                ], 401);
            }
        } catch (\Exception $e) {
            return response()->json(arrayView('phpsoft.users::errors/authenticate', [
                'error' => $e->getMessage(),
            ]), 401);
        }

        return $next($request);
    }
}
