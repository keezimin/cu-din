<?php

namespace App\Http\Middleware;

use Closure;

class IgnoreRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = currentUser();

        if (in_array($role, $user->getRoles())) {
            return response()->json(null, 403);
        }

        return $next($request);
    }
}