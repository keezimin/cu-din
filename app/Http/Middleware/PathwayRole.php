<?php

namespace App\Http\Middleware;

use Closure;

class PathwayRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = currentUser();

        switch ($request->method()) {
            case 'PATCH' :
                if ($user->isAdmin() || ($user->isBizOwner() && !empty($user->businessProfile->active))) {
                    return $next($request);
                }

                return response()->json(null, 403);
            default:
                switch (true) {
                    case $user->isAdmin():
                    case $user->isCounselor():
                    case $user->isDistrictAdmin():
                    case $user->isBizOwner():
                        return $next($request);

                    default:
                        return response()->json(null, 403);
                }
        }
    }
}