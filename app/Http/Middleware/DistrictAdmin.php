<?php

namespace App\Http\Middleware;

use Closure;

class DistrictAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = currentUser();

        switch (true) {
            case $user->isAdmin():
            case $user->isDistrictAdmin():
                return $next($request);
        }

        return response()->json(null, 403);
    }
}
