<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistrictUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:255|unique:districts,name,' . request()->route()->district->id,
            'code'     => 'sometimes|max:255|unique:districts,code,' . request()->route()->district->id,
            'state_id' => 'sometimes|exists:states,id|max:255',
        ];
    }
}
