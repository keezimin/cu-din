<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddChecklistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:colleges,scholarships,trade_schools,career_jobs',
            'object_id' => 'required|numeric|exists:' . request()->type . ',id',
            'college_code' => 'required_if:type,==,colleges|numeric',
            'college_name' => 'required_if:type,==,colleges|string',
            'object_id' => request()->type === 'colleges' ? 'numeric' : 'required|numeric|exists:' . request()->type . ',id',
        ];
    }
}
