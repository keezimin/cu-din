<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterStudentRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         => 'required|email',
            'password'      => 'required|string',
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'contact'       => 'nullable|string',
            'graduated_on'  => 'nullable|date',
            'act_english'   => 'nullable|numeric',
            'act_science'   => 'nullable|numeric',
            'act_math'      => 'nullable|numeric',
            'act_composite' => 'nullable|numeric',
            'act_reading'   => 'nullable|numeric',
            'sat_math'      => 'nullable|numeric|between:200,800',
            'sat_verbal'    => 'nullable|numeric|between:200,800',
            'school_id'     => 'required|numeric|exists:schools,id',
        ];
    }
}
