<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChecklistUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string',
            'due_date' => 'sometimes|date|after_or_equal:today',
            'type' => 'sometimes|string|in:colleges,scholarships,trade_schools,career_jobs',
            'object_id' => request()->type === 'colleges' ? 'sometimes|numeric' : 'sometimes|numeric|exists:' . request()->type . ',id',
            'tasks' => 'sometimes|array',
            'tasks.*' => 'required|array',
            'tasks.*.name' => 'required|string',
            'tasks.*.due_date' => 'sometimes|date',
        ];
    }
}
