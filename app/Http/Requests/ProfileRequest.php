<?php

namespace App\Http\Requests;

class ProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checkContactPerson = strpos(url()->current(),'businesses') ? 'sometimes|required' : 'nullable';
        return [
            'users.first_name' => 'nullable|string',
            'users.last_name' => 'nullable|string',
            'users.contact' => 'nullable|string',
            'logo' => 'nullable|string',
            'photo' => 'nullable|string',
            'address' => 'nullable|string',
            'location' => 'nullable|string',
            'contact_person' => 'string|'.$checkContactPerson,
            'website_link' => 'nullable|url',
            'district_id' => 'sometimes|exists:districts,id',
            'description' => 'nullable|string',
            'application_form_filename' => 'nullable|url',
        ];
    }
}
