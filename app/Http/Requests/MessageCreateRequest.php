<?php

namespace App\Http\Requests;

class MessageCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target' => 'required|array',
            'target.type' => 'required|string|in:USER,USER_ALL,DISTRICT,SCHOOL,GROUP',
            'target.id' => 'nullable|array',
            'source' => 'sometimes|array',
            'source.type' => 'sometimes|required|string|in:SCHOLARSHIP,JOB,RESOURCE,TRADESCHOOL,COLLEGE',
            'source.id' => 'sometimes|nullable|array',
            'source.name' => 'required_if:source.type,COLLEGE|string',
            'content' => 'required_without:source|nullable|string',
            'via' => 'sometimes|nullable|in:EMAIL',
            'link' => 'nullable|url',
            'image' => 'nullable|url',
        ];
    }
}
