<?php

namespace App\Http\Requests;

class UpdatePathwayRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'career_cluster_id' => 'numeric|exists:career_clusters,id',
            'name' => 'string',
            'short_description' => 'string',
            'user_id' => 'sometimes|required|numeric|exists:users,id',
            'active' => 'boolean',
        ];  
    }
}