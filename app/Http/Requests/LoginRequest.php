<?php

namespace App\Http\Requests;

class LoginRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'object_type' => 'sometimes|required|string|max:255|in:app',
            'device_uuid' => 'sometimes|required|string|max:255',
            'device_token' => 'sometimes|required|string|max:255',
        ];
    }
}
