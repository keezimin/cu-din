<?php

namespace App\Http\Requests;

class ReminderRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target'    => 'required|array',
            'target.type'   => 'required|string|in:USER,USER_ALL',
            'target.id'   => 'nullable|array',
            'content'    => 'nullable|string',
        ];
    }
}
