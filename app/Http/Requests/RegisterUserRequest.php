<?php

namespace App\Http\Requests;

class RegisterUserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $checkContactPerson = strpos(url()->current(),'businesses') ? 'required' : 'nullable';
        return [
            'email' => 'required|email',
            'password' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'contact' => 'nullable|string',
            'graduated_on' => 'date',
            'act_english' => 'numeric',
            'act_science' => 'numeric',
            'act_math' => 'numeric',
            'act_composite' => 'numeric',
            'act_reading' => 'numeric',
            'sat_math' => 'numeric',
            'sat_verbal' => 'numeric',
            'district_id' => 'sometimes|nullable|numeric|exists:districts,id',
            'contact_person' => 'string|'.$checkContactPerson,
        ];
    }
}
