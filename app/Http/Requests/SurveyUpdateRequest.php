<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey_category_id' => 'numeric|exists:survey_categories,id',
            'career_cluster_id'  => 'numeric|exists:career_clusters,id',
            'name'               => 'string'
        ];
    }
}
