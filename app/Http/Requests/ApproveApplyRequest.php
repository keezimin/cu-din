<?php

namespace App\Http\Requests;

class ApproveApplyRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'target' => 'required|string|in:SCHOLARSHIP,JOB,RESOURCE,TRADESCHOOL,COLLEGE',
            'target_id' => 'required|nullable|numeric',
        ];
    }
}
