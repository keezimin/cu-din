<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ExistApplyState;

class ScholarshipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checkSchools = empty(request()->is_applied_to_state) 
            && empty(request()->is_applied_to_county) ? '|required' : '';

        $checkAppliedState = empty(request()->is_applied_to_state) ? '|required' : '';

        return [
            'name'                      => 'required|string',
            'school_ids'                => 'array' . $checkSchools,
            'school_ids.*'              => 'required|numeric|exists:schools,id',
            'district_ids'              => 'array' . $checkAppliedState,
            'district_ids.*'            => 'required|numeric|exists:districts,id',
            'quantity'                  => 'sometimes|nullable|numeric',
            'organization'              => 'nullable|string',
            'amount'                    => 'sometimes|numeric|digits_between:0,10',
            'min_price'                 => 'sometimes|numeric|digits_between:0,10',
            'max_price'                 => 'nullable|numeric|digits_between:0,10|min:' . (int) request()->min_price,
            'deadline_on'               => 'nullable|date',
            'description'               => 'nullable|string',
            'application_form_file'     => 'nullable|string',
            'application_form_filename' => 'nullable|string',
            'application_form_link'     => 'nullable|string',
            'is_applied_to_state'       => 'in:0,1',
            'is_applied_to_county'      => 'in:0,1',
            'state_id'                  => 'required|numeric|exists:states,id',
        ];
    }
}
