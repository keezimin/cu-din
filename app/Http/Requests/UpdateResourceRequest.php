<?php

namespace App\Http\Requests;

class UpdateResourceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'sometimes|required|max:255',
            'type'    => 'sometimes|required|in:resource,news',
            'link'    => 'nullable|url|max:255',
            'summary' => 'sometimes|required',
            'image'   => 'nullable',
            'image_filename'   => 'nullable|max:255',
        ];
    }
}
