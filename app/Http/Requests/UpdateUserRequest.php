<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Auth\Access\Gate;
use \App\User;

class UpdateUserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        $user = \Auth::user();

        return app(Gate::class)->allows('update', $user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes|required|email|max:255|unique:users,email,' . request()->id,
            'password' => 'sometimes|required|max:255',
            'fullname' => 'nullable|string',
            'first_name' => 'nullable|string',
            'last_name' => 'nullable|string',
            'contact' => 'nullable|string',
            'graduated_on' => 'nullable|date',
            'act_english' => 'nullable|numeric',
            'act_science' => 'nullable|numeric',
            'act_math' => 'nullable|numeric',
            'act_composite' => 'nullable|numeric',
            'act_reading' => 'nullable|numeric',
            'sat_math' => 'nullable|numeric|between:200,800',
            'sat_verbal' => 'nullable|numeric|between:200,800',
            'district_id' => 'sometimes|nullable|numeric|exists:districts,id',
        ];
    }
}
