<?php

namespace App\Http\Requests;

class BusinessProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checkContactPerson = strpos(url()->current(), 'businesses') ? 'sometimes|required' : 'nullable';
        return [
            'users.first_name' => 'nullable|string',
            'users.last_name' => 'nullable|string',
            'users.contact' => 'nullable|string',
            'logo' => 'nullable|string',
            'photo' => 'nullable|string',
            'address' => 'nullable|string',
            'location' => 'nullable|string',
            'contact_person' => 'string|' . $checkContactPerson,
            'website_link' => 'nullable|url',
            'district_id' => 'sometimes|exists:districts,id',
            'description' => 'nullable|string',
            'application_form_filename' => 'nullable|url',
            'name' => 'nullable|string',
            'active' => 'boolean',
            'industry' => 'nullable|string',
            'type' => 'nullable|string',
            'comapny_size' => 'nullable|numeric',
            'founded' => 'nullable|numeric',
            'specialties' => 'nullable|string',
            'contact' => 'nullable|string',
            'phone' => 'nullable|string',
            'email' => 'nullable|string|email',
            'facebook' => 'nullable|url|string',
            'instagram' => 'nullable|url|string',
            'linkedin' => 'nullable|url|string',
        ];
    }
}
