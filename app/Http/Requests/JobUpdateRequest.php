<?php

namespace App\Http\Requests;

use App\Models\Job;
use Illuminate\Foundation\Http\FormRequest;

class JobUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->method() == 'PATCH') {
            $checkSchools = empty(request()->is_applied_to_state) 
                && empty(request()->is_applied_to_county) ? '|required' : '';

            $checkAppliedState = empty(request()->is_applied_to_state) ? '|required' : '';

            return [
                'name'                      => 'sometimes|string',
                'user_id'                   => 'sometimes|numeric|exists:users,id',
                'organization'              => 'nullable|string',
                'description'               => 'nullable|string',
                'type'                      => 'sometimes|string|in:' . implode(Job::$enumTypes, ','),
                'volunteer_hours'           => 'sometimes|numeric',
                'status'                    => 'sometimes|numeric|in:0,1',
                'location'                  => 'nullable|string',
                'school_ids'                => 'array' . $checkSchools,
                'school_ids.*'              => 'required|numeric|exists:schools,id',
                'district_ids'              => 'array' . $checkAppliedState,
                'district_ids.*'            => 'required|numeric|exists:districts,id',
                'application_form_file'     => 'nullable|string',
                'application_form_filename' => 'nullable|string',
                'application_form_link'     => 'nullable|string',
                'deadline_on'               => 'nullable|date',
                'is_applied_to_district'    => 'nullable|in:0,1',
                'is_applied_to_state'       => 'in:0,1',
                'state_id'                  => 'sometimes|required|numeric|exists:states,id',
                'pathway_id'                => 'sometimes|numeric|exists:pathways,id',
                'is_assign_to_pathway'      => 'nullable|boolean'
            ];
        } else {
            return [
                'status'                    => 'required|numeric|in:0,1',
            ];
        }
    }
}
