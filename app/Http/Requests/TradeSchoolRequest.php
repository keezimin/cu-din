<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TradeSchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'nullable|string',
            'email' => 'nullable|email',
            'description' => 'nullable|string',
            'website' => 'nullable|string',
            'photo' => 'nullable|string',
            'district_id' => 'sometimes|numeric|exists:districts,id',
            'application_form_file' => 'nullable|string',
            'application_form_filename' => 'nullable|string',
            'application_form_link' => 'nullable|string',
            'photo_filename' => 'nullable|string'
        ];
    }
}
