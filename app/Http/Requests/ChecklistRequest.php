<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChecklistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'due_date' => 'sometimes|date|after_or_equal:today',
            'tasks' => 'sometimes|array',
            'tasks.*' => 'required|array',
            'tasks.*.name' => 'required|string',
            'tasks.*.due_date' => 'sometimes|date',
        ];

        if (currentUser()->isAdminOrCounselor()) {
            $rules = array_merge($rules, [
                'type' => 'required|string|in:colleges,scholarships,trade_schools,career_jobs',
                'college_code' => 'required_if:type,==,colleges|numeric',
                'college_name' => 'required_if:type,==,colleges|string',
                'object_id' => request()->type === 'colleges' ? 'numeric' : 'required|numeric|exists:' . request()->type . ',id',
            ]);
        }

        return $rules;
    }
}
