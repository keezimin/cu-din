<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Exception;

class EmailExistException extends Exception
{
	public $user_id;
	public $role;

    public function __construct($user_id = null, $role = null)
    {
		$this->user_id = $user_id;
		$this->role = $role;
    }
}
