<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $scholarships));
$this->set('links', $this->helper('helpers.links', $scholarships));
$this->set('data', $this->each($scholarships, function ($section, $scholarship) {
    $section->set($section->partial('partials/scholarship', [ 'scholarship' => $scholarship ]));
}));

$this->helper('helpers.included');