<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $tradeSchools));
$this->set('links', $this->helper('helpers.links', $tradeSchools));
$this->set('data', $this->each($tradeSchools, function ($section, $tradeSchool) {
    $section->set($section->partial('partials/tradeSchool', ['tradeSchool' => $tradeSchool]));
}));

$this->helper('helpers.included');
