<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $categories));
$this->set('links', $this->helper('helpers.links', $categories));
$this->set('data', $this->each($categories, function ($section, $category) {
    $section->set($section->partial('partials/category', ['category' => $category]));
}));

$this->helper('helpers.included');
