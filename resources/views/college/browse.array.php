<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $colleges));
$this->set('links', $this->helper('helpers.links', $colleges));
$this->set('data', $this->each($colleges, function ($section, $college) {
    $section->set($section->partial('partials/college', ['college' => $college]));
}));
