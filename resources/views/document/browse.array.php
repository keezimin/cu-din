<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $documents));
$this->set('links', $this->helper('helpers.links', $documents));
$this->set('data', $this->each($documents, function ($section, $document) {
    $section->set($section->partial('partials/document', ['document' => $document]));
}));

$this->helper('helpers.included');
