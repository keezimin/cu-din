<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', [
    'type' => 'Embed',
    'attributes' => [
        'title' => $info->title,
        'description' => $info->description,
        'images' => $info->images,
    ]
]);
