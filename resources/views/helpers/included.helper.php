<?php

$process = function ($included = []) {
    $items = [];
    $result = [];
    foreach ($included as $value) {
        list ($data, $id, $partial) = $value;
        $id && $items[$partial][$id] = $data;
    }
    foreach ($GLOBALS['view_included'] ?? [] as $value) {
        list ($data, $id, $partial) = $value;
        $id && $items[$partial][$id] = $data;
    }
    foreach ($items as $partial => $item) {
        foreach ($item as $data) {
            $result[] = [$data, $partial];
        }
    }
    return $result;
};

return function ($included = []) use ($process) {

    $result = $process($included);
    $this->set('included', $this->each($result, function ($section, $item) {
        list ($data, $partial) = $item;
        $section->set($section->partial($partial, $data));
    }));

    // for nested include
    $result = $process($included);
    $this->set('included', $this->each($result, function ($section, $item) {
        list ($data, $partial) = $item;
        $section->set($section->partial($partial, $data));
    }));

    unset($GLOBALS['view_included']);
};
