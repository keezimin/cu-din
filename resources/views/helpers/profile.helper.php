<?php

$videoType = function ($url = '') {
    if (empty($url)) {
        return null;
    }

    if (filter_var($url, FILTER_VALIDATE_URL) === false) {
        return null;
    }

    $data = [];
    if (strpos($url, 'youtu')) {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);

        $data = [
            'type' => 'youtube',
            'id' => !empty($match[1]) ? $match[1] : null
        ];
    }

    if (strpos($url, 'vimeo')) {
        preg_match('/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/', $url, $match);

        $data = [
            'type' => 'vimeo',
            'id' => !empty($match[5]) ? $match[5] : null
        ];
    }

    return $data;
};

return function ($data = null) use ($videoType) {
    $data = json_decode($data);
    if (!empty($data)) {
        foreach ($data as $key => &$value) {
            $content = $value->content;
            $contentStripTags = $this->helper('helpers.strip-tags', $content);
            $value->content = $contentStripTags ? $value->content : null;
            if ($value->type === 'video') {
                $dataVideo = $videoType($value->content);
                if (!empty($dataVideo)) {
                    if (!empty($dataVideo['type'])) {
                        $value->type_video = $dataVideo['type'];
                    }
                    if (!empty($dataVideo['id'])) {
                        $value->id_video = $dataVideo['id'];
                    }
                }
            } else {
                $content = $value->content;
                if ($content === '<p></p>') {
                    $value->content = null;
                }
            }
        }
    }

    return $data;
};
