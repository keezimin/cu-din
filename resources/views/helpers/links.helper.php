<?php

/**
 * Array View Helper
 *
 * @param  array  $items
 * @return array
 */
return function ($items = []) {
    if (empty($items)) {
        return ['self'  => Request::fullUrl()];
    }
    $params = Request::except('page.number');
    array_set($params, 'page.size', $items->perPage());
    $items = $items->appends($params);
    $links = [
        'self'  => Request::fullUrl(),
        'first' => $items->url(1),
        'prev'  => $items->previousPageUrl(),
        'next'  => $items->nextPageUrl(),
        'last'  => $items->url($items->lastPage())
    ];
    return $links;
};
