<?php

/**
 * Array View Helper
 *
 * @param  array  $items
 * @return array
 */
return function ($text = '') {
    $allowable_tags = '<img><video><audio>';
    $text = strip_tags($text, $allowable_tags);

    return $text;
};
