<?php

/**
 * Array View Helper
 *
 * @param  array  $items
 * @return array
 */
return function ($func = null) {
    $relationship = $func();
    $this->set('relationships', $relationship ?: (object)[]);
};
