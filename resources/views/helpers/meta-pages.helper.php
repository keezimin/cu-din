<?php

/**
 * Array View Helper
 *
 * @param  array  $items
 * @return array
 */
return function ($items = []) {

    if (empty($items)) {
        return [];
    }

    $metaPages = [
        'totalPages'  => $items->lastPage(),
        'totalObjects' => $items->total()
    ];
    return $metaPages;
};
