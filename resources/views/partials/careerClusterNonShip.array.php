<?php

$this->set('type', 'CareerCluster');
$this->set('id', $careerCluster->id);
$this->set('attributes', function ($session) use ($careerCluster) {
    $session->set('id', $careerCluster->id);
    if (isset($careerCluster->pivot->total_point)) {
        $session->set('total_point', $careerCluster->pivot->total_point);
    }
    $session->extract($careerCluster, [
        'name', 'description'
    ]);
});