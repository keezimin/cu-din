<?php

$this->set('type', 'District');
$this->set('id', $district->id);
$this->set('attributes', function ($session) use ($district) {
    $session->set('id', $district->id);
    $session->extract($district, [
        'name', 'code', 'state_id',
     ]);

    if($district->state) {
      $session->set('name_state', $district->state->name);
    } else {
      $session->set('name_state', null);
    }
 });

$this->helper('helpers.relationships', function () use ($district) {
    $relationships = [];

    if (!in_array('District.state', app('request')->ignore_included)) {
        if ($district->state) {
        $relationships['state'] = [
            'id' => $district->state->id,
            'type' => 'State',
        ];
        $this->helper('helpers.included-add', ['state' => $district->state], array_get($district->state, 'id'), 'partials/state');
        }
    }

    if (!in_array('District.schools', app('request')->ignore_included)) {
        if (!empty($schools = $district->schools()->get())) {
            foreach ($schools as $value) {
                $relationships['schools'][] = [
                    'id' => $value->id,
                    'type' => 'School',
                ];
                $this->helper('helpers.included-add', ['school' => $value], array_get($value, 'id'), 'partials/school');
            }
        }
    }
    return $relationships;
});
