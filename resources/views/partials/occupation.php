<?php

$this->set('type', 'Occupation');
$this->set('id', $occupation->id);
$this->set('attributes', function ($session) use ($occupation) {
    $session->set('id', $occupation->id);
    $session->extract($occupation, [
        'career_cluster_id', 'career_pathway_id', 'name', 'code', 'video', 'occupation', 'average_salary'
    ]);
    $session->set('created_at', $occupation->created);
    $session->set('updated_at', $occupation->updated);
    $session->set('deleted_at', $occupation->deleted);
});

