<?php

$this->set('id', (string) $user->registed_user_id);
$this->set('type', 'User');
$this->set('attributes', function ($section) use ($user) {
    $section->set('id', $user->id);
    $section->extract($user, [
        'first_name',
        'last_name',
        'fullname',
        'email',
        'registed_user_id',
    ]);
    $section->set('created_at', $user->created);
    $section->set('updated_at', $user->updated);
    $section->set('deleted_at', $user->deleted);
});
