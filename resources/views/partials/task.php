<?php

$this->set('type', 'Task');
$this->set('id', $task->id);
$this->set('attributes', function ($session) use ($task) {
    $session->set('id', $task->id);
    $session->extract($task, [
        'name', 'due_date', 'checklist_id',
    ]);
    $session->set('created_at', $task->created);
    $session->set('updated_at', $task->updated);
    $session->set('deleted_at', $task->deleted);
});

$this->helper('helpers.relationships', function () use ($task) {
    $relationships = [];

    if (!in_array('Task.owner', app('request')->ignore_included)) {
        if ($task->owner) {
            $relationships['owner'] = [
                'id' => $task->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $task->owner], array_get($task->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    return $relationships;
});
