<?php

$this->set('type', 'Document');
$this->set('id', $document->id);
$this->set('attributes', function ($session) use ($document) {
    $session->set('id', $document->id);
    $session->extract($document, [
        'title', 'description', 'image', 'content',
    ]);
    $session->set('created_at', $document->created);
});

$this->helper('helpers.relationships', function () use ($document) {
    $relationships = [];

    if (!in_array('Document.owner', app('request')->ignore_included)) {
        if ($document->owner) {
            $relationships['owner'] = [
                'id' => $document->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $document->owner], array_get($document->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Document.category', app('request')->ignore_included)) {
        if ($document->category) {
            $relationships['category'] = [
                'id' => $document->category->id,
                'type' => 'Category',
            ];
            $this->helper('helpers.included-add', ['category' => $document->category], array_get($document->category, 'id'), 'partials/category');
        }
    }

    return $relationships;
});
