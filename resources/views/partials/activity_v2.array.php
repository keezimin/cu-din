<?php

$this->set('type', 'Activity');
$this->set('id', $activity->id);
$this->set('attributes', function ($section) use ($activity) {
    $section->set('id', $activity->id);
    $section->extract($activity, [
        'actor', 'actor_id', 'actor_type', 'verb', 'object', 'object_id', 'object_type', 'target', 'target_id', 'target_type', 'meta', 'status',
    ]);
    $section->set('created_at', $activity->created);
    $section->set('updated_at', $activity->updated);
});

$this->helper('helpers.relationships', function () use ($activity) {
    $relationships = [];

    $objectModel = "\App\Models\\{$activity->object_type}";
    $object = $objectModel::find($activity->object_id);

    if (!in_array('Activity.'.$object, app('request')->ignore_included)) {
        if ($object) {
            $relationships['object'] = [
                'id' => $object->id,
                'type' => $activity->object_type,
            ];

            $type = title_case($activity->object_type);
            $typePath = strtolower($type);
            $partial = 'partials/' . $typePath;
            if ($typePath === 'user') {
                $partial = 'phpsoft.users::partials/user';
            }
            $this->helper('helpers.included-add', ['job' => $object], array_get($object, 'id'), $partial);
        }
    }

    if (!in_array('Activity.user', app('request')->ignore_included)) {
        $user = \App\User::find($activity->actor_id);
        if($user) {
            $relationships['user'] = [
                'id' => $user->id,
                'type' => 'User',
            ];

            $partial = 'phpsoft.users::partials/user';
            $this->helper('helpers.included-add', ['user' => $user], array_get($user, 'id'), $partial);
        }
    }

    return $relationships;
});
