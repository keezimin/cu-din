<?php

$this->set('type', 'Activity');
$this->set('id', $activity->id);
$this->set('attributes', function ($section) use ($activity) {
    $section->set('id', $activity->id);
    $section->extract($activity, [
        'actor', 'actor_id', 'actor_type', 'verb', 'object', 'object_id', 'object_type', 'target', 'target_id', 'target_type', 'meta', 'status',
    ]);
    $section->set('created_at', $activity->created);
    $section->set('updated_at', $activity->updated);
});
