<?php

$this->set('type', 'Scholarship');
$this->set('id', $scholarship->id);
$this->set('attributes', function ($session) use ($scholarship) {
    $session->set('id', $scholarship->id);
    $session->extract($scholarship, [
        'name', 'organization', 'amount', 'min_price', 'max_price', 'deadline_on', 'description',
        'quantity', 'application_form_file', 'application_form_filename', 'application_form_link',
        'school_id', 'status', 'is_applied_to_state', 'state_id', 'is_applied_to_county',
    ]);
});

$this->helper('helpers.relationships', function () use ($scholarship) {
    $relationships = [];

    if (!in_array('Scholarship.user', app('request')->ignore_included)) {
        if ($scholarship->user) {
            $relationships['user'] = [
                'id' => $scholarship->user->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $scholarship->user], array_get($scholarship->user, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Scholarship.schools', app('request')->ignore_included)) {
        $schools = $scholarship->schools;
        foreach ($schools as $value) {
            $relationships['schools'][] = [
                'id' => $value->id,
                 'type' => 'School',
             ];
            $this->helper('helpers.included-add', ['school' => $value], $value->id, 'partials/school');
        }
    }

    if (!in_array('Scholarship.activities', app('request')->ignore_included)) {
        $user = \Auth::user();
        if ($user) {
            $activities = $scholarship->activities()->ofActor($user->id)->get();
            foreach ($activities as $value) {
                $relationships['activities'][] = [
                    'id' => $value->id,
                    'type' => 'Activity',
                ];
                $this->helper('helpers.included-add', ['activity' => $value], $value->id, 'partials/activity');
            }
        }
    }

    if (!in_array('Scholarship.districts', app('request')->ignore_included)) {
        $districts = $scholarship->districts;
        foreach ($districts as $value) {
            $relationships['districts'][] = [
                'id' => $value->id,
                 'type' => 'District',
             ];
            $this->helper('helpers.included-add', ['district' => $value], $value->id, 'partials/district');
        }
    }

    if (!in_array('Scholarship.state', app('request')->ignore_included)) {
        if ($scholarship->state) {
            $relationships['state'] = [
                'id' => $scholarship->state->id,
                'type' => 'State'
            ];
            $this->helper('helpers.included-add', ['state' => $scholarship->state], array_get($scholarship->state, 'id'), 'partials/state');
        }
    }

    return $relationships;
});
