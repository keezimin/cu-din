<?php

$this->set('type', 'Survey');
$this->set('id', $survey->id);
$this->set('attributes', function ($section) use ($survey) {
    $section->set('id', $survey->id);
    $section->extract($survey, [
        'survey_category_id', 'name'
    ]);
    $section->set('created_at', $survey->created);
    $section->set('updated_at', $survey->updated);
});

$this->helper('helpers.relationships', function () use ($survey) {
    $relationships = [];

    if (!in_array('Survey.category', app('request')->ignore_included)) {
        if ($survey->category) {
            $relationships['category'] = [
                'id' => $survey->category->id,
                'type' => 'SurveyCategory',
            ];
            $this->helper('helpers.included-add', ['surveyCategory' => $survey->category], array_get($survey->category, 'id'), 'partials/surveyCategory');
        }
    }

    if (!in_array('Survey.careerCluster', app('request')->ignore_included)) {
        if($survey->careerCluster) {
            $relationships['careerCluster'] = [
                'id' => $survey->careerCluster->id,
                'type' => 'CareerCluster',
            ];
            $this->helper('helpers.included-add', ['careerCluster' => $survey->careerCluster], array_get($survey->careerCluster, 'id'), 'partials/careerCluster');
        }
    }

    return $relationships;
});