<?php

$this->set('type', 'Resource');
$this->set('id', $resource->id);
$this->set('attributes', function ($session) use ($resource) {
    $session->set('id', $resource->id);
    $session->extract($resource, [
        'title', 'image', 'link', 'type', 'summary', 'image_filename',
    ]);
    $session->set('created_at', $resource->created);
});

$this->helper('helpers.relationships', function () use ($resource) {
    $relationships = [];

    if (!in_array('Resource.owner', app('request')->ignore_included)) {
        if ($resource->owner) {
            $relationships['owner'] = [
                'id' => $resource->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $resource->owner], array_get($resource->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    return $relationships;
});
