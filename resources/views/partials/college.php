<?php

$this->set('type', 'College');
$this->set('id', $college->id);
$this->set('attributes', function ($session) use ($college) {
    $session->set('id', $college->id);
    $session->extract($college, [
        'name', 'code',
    ]);
    $session->set('applying', $college->applying());
    $session->set('created_at', $college->created);
});
