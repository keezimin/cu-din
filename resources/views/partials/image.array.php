<?php

$this->set('type', 'File');
$this->set('attributes', function ($session) use ($url) {
    $session->set('url', $url);
});
