<?php

$this->set('type', 'Group');
$this->set('id', $group->id);
$this->set('attributes', function ($session) use ($group) {
    $session->set('id', $group->id);
    $session->extract($group, [
        'name',
    ]);
    $session->set('created_at', $group->created);
    $session->set('updated_at', $group->updated);
});

$this->helper('helpers.relationships', function () use ($group) {
    $relationships = [];

    if (!in_array('Group.owner', app('request')->ignore_included)) {
        if ($group->owner) {
            $relationships['owner'] = [
                'id' => $group->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $group->owner], array_get($group->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Group.users', app('request')->ignore_included)) {
        $users = $group->users()->get();
        foreach ($users as $value) {
            $relationships['users'][] = [
                'id' => $value->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $value], $value->id, 'phpsoft.users::partials/user');
        }
    }

    return $relationships;
});
