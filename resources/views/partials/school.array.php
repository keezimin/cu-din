<?php

$this->set('type', 'School');
$this->set('id', $school->id);
$this->set('attributes', function ($session) use ($school) {
    $session->set('id', $school->id);
    $session->extract($school, [
        'name', 'code', 'website', 'photo', 'photo_filename'
    ]);
    if (!empty($school->getCouselor())) {
        $advisor = $school->getCouselor();
        $session->set('advisor_email', $advisor['email']);
        $session->set('advisor_name', $advisor['fullname']);
    } else {
        $session->set('advisor_email', null);
        $session->set('advisor_name', null);
    }

});

$this->helper('helpers.relationships', function () use ($school) {
    $relationships = [];

    if (!in_array('School.dictrict', app('request')->ignore_included)) {
        if ($dictrict = $school->district) {
            $relationships['district'] = [
                'id' => $school->district->id,
                'type' => 'District',
            ];
           $this->helper('helpers.included-add', ['district' => $dictrict], array_get($dictrict, 'id'), 'partials/district');
        }
    }
    if (!in_array('School.organization', app('request')->ignore_included)) {
        if ($organization = $school->organization) {
            $relationships['organization'] = [
                'id' => $school->organization->id,
                'type' => 'Organization',
            ];
           $this->helper('helpers.included-add', ['organization' => $organization], array_get($organization, 'id'), 'partials/organization');
        }
    }
    return $relationships;
});
