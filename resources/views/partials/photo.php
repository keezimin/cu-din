<?php

$this->set('type', 'Photo');
$this->set('id', $photo->id);
$this->set('attributes', function ($session) use ($photo) {
    $session->set('id', $photo->id);
    $session->extract($photo, [
        'user_id', 'photo',
    ]);
    $session->set('created_at', $photo->created);
    $session->set('updated_at', $photo->updated);
    $session->set('deleted_at', $photo->deleted);
});

