<?php 
$this->set('type', 'State');
$this->set('id', $state->id);
$this->set('attributes', function ($session) use ($state) {
    $session->set('id', $state->id);
    $session->extract($state, [
        'name', 'code', 'abbr',
    ]);
});

$this->helper('helpers.relationships', function () use ($state) {
    $relationships = [];
    if (!in_array('State.districts', app('request')->ignore_included)) {
        if (!empty($districts = $state->districts()->get())) {
            foreach ($districts as $value) {
                $relationships['districts'][] = [
                    'id' => $value->id,
                    'type' => 'District',
                ];
                $this->helper('helpers.included-add', ['district' => $value], array_get($value, 'id'), 'partials/district');
            }
        }
    }
    return $relationships;
});