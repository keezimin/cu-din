<?php

$this->set('type', 'Message');
$this->set('id', $message->id);
$this->set('attributes', function ($section) use ($message) {
    $section->set('id', $message->id);
    if (\Auth::user() && !(\Auth::user()->hasRole('super_admin') || \Auth::user()->isOwner($message, 'actor_id'))) {
        $meta = $message->meta;
        unset($meta['target']);
        $message->meta = $meta;
    }
    $section->extract($message, [
        'actor', 'actor_id', 'actor_type', 'verb', 'object', 'object_id', 'object_type', 'target', 'target_id', 'target_type', 'meta',
    ]);
    $section->set('created_at', $message->created);
    $section->set('updated_at', $message->updated);
});

$this->helper('helpers.relationships', function () use ($message) {
    $relationships = [];

    if (!in_array('Message.sender', app('request')->ignore_included)) {
        if ($message->sender) {
            $relationships['sender'] = [
                'id' => $message->actor_id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $message->sender], array_get($message->sender, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Message.message', app('request')->ignore_included)) {
        if ($message->message) {
            $relationships['message'] = [
                'id' => $message->message->id,
                'type' => 'Message',
            ];
            $this->helper('helpers.included-add', ['message' => $message->message], array_get($message->message, 'id'), 'partials/message');
        }
    }

    if (!in_array('Message.targets', app('request')->ignore_included)) {
        if ($targets = $message->targets()) {
            $type = title_case(array_get($message, 'meta.target.type'));
            foreach ($targets as $value) {
                $relationships['targets'][] = [
                    'id' => $value->id,
                    'type' => $type,
                ];
                $typePath = strtolower($type);
                $partial = 'partials/' . $typePath;
                if ($typePath === 'user') {
                    $partial = 'phpsoft.users::partials/user';
                }
                $this->helper('helpers.included-add', [$typePath => $value], array_get($value, 'id'), $partial);
            }
        }
    }

    if (!in_array('Message.sources', app('request')->ignore_included)) {
        if ($targets = $message->sources()) {
            $type = title_case(array_get($message, 'meta.source.type'));
            foreach ($targets as $value) {
                $relationships['sources'][] = [
                    'id' => $value->id,
                    'type' => $type === 'Tradeschool' ? 'TradeSchool' : $type,
                ];
                $typePath = strtolower($type);
                $typePath = camel_case($type);
                if ($typePath === 'tradeschool') {
                    $typePath = 'tradeSchool';
                }
                $partial = 'partials/' . $typePath;
                if ($typePath === 'user') {
                    $partial = 'phpsoft.users::partials/user';
                }
                $this->helper('helpers.included-add', [$typePath => $value], array_get($value, 'id'), $partial);
            }
        }
    }

    return $relationships;
});
