<?php

$this->set('type', 'Job');
$this->set('id', $job->id);
$this->set('attributes', function ($session) use ($job) {
    $session->set('id', $job->id);
    $session->extract($job, [
        'name', 'status', 'type', 'organization', 'volunteer_hours', 'description', 'application_form_file',
        'application_form_filename', 'application_form_link', 'location', 'deadline_on', 'occupation_id',
        'career_cluster_id', 'is_applied_to_state', 'state_id', 'pathway_id', 'is_applied_to_county', 'is_assign_to_pathway'
    ]);
    $session->set('created_at', $job->created);
    $session->set('updated_at', $job->updated);
    $session->set('applying', $job->applying());
});

$this->helper('helpers.relationships', function () use ($job) {
    $relationships = [];

    if (!in_array('Job.user', app('request')->ignore_included)) {
            if ($job->user) {
            $relationships['user'] = [
                'id' => $job->user->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $job->user], array_get($job->user, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Job.owner', app('request')->ignore_included)) {
        if ($job->owner) {
            $relationships['owner'] = [
                'id' => $job->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $job->owner], array_get($job->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Job.activities', app('request')->ignore_included)) {
        $user = \Auth::user();
        if ($user) {
            $activities = $job->activities()->ofActor($user->id)->get();
            foreach ($activities as $value) {
                $relationships['activities'][] = [
                    'id' => $value->id,
                    'type' => 'Activity',
                ];
                $this->helper('helpers.included-add', ['activity' => $value], $value->id, 'partials/activity');
            }
        }
    }

    if (!in_array('Job.schools', app('request')->ignore_included)) {
        $schools = $job->schools()->get();
        foreach ($schools as $value) {
            $relationships['schools'][] = [
                'id' => $value->id,
                'type' => 'School',
            ];
            $this->helper('helpers.included-add', ['school' => $value], $value->id, 'partials/school');
        }
    }

    if (!in_array('Job.occupation', app('request')->ignore_included)) {
        if ($job->occupation) {
            $relationships['occupation'] = [
                'id' => $job->occupation->id,
                'type' => 'Occupation'
            ];
            $this->helper('helpers.included-add', ['occupation' => $job->occupation, 'totalPoint' => $totalPoint ?? 0], $job->occupation->id, 'partials/occupation');
        }
    }

    if (!in_array('Job.careerCluster', app('request')->ignore_included)) {
        if ($job->careerCluster) {
            $relationships['careerCluster'] = [
                'id' => $job->careerCluster->id,
                'type' => 'CareerCluster'
            ];
            $this->helper('helpers.included-add', ['careerCluster' => $job->careerCluster], array_get($job->careerCluster, 'id'), 'partials/careerClusterNonShip');
        }
    }

    if (!in_array('Job.districts', app('request')->ignore_included)) {
        $districts = $job->districts()->get();
        foreach ($districts as $value) {
            $relationships['districts'][] = [
                'id' => $value->id,
                'type' => 'District',
            ];
            $this->helper('helpers.included-add', ['district' => $value], $value->id, 'partials/district');
        }
    }

    if (!in_array('Job.state', app('request')->ignore_included)) {
        if ($job->state) {
            $relationships['state'] = [
                'id' => $job->state->id,
                'type' => 'State'
            ];
            $this->helper('helpers.included-add', ['state' => $job->state], array_get($job->state, 'id'), 'partials/state');
        }
    }

    if (!in_array('Job.pathway', app('request')->ignore_included)) {
        if ($job->pathway) {
            $relationships['pathway'] = [
                'id' => $job->pathway->id,
                'type' => 'Pathway'
            ];
            $this->helper('helpers.included-add', ['pathway' => $job->pathway], array_get($job->pathway, 'id'), 'partials/pathway');
        }
    }


    return $relationships;
});
