<?php

$this->set('type', 'TradeSchool');
$this->set('id', $tradeSchool->id);
$this->set('attributes', function ($session) use ($tradeSchool) {
    $session->set('id', $tradeSchool->id);
    $session->extract($tradeSchool, [
        'name', 'phone', 'email', 'photo', 'website', 'description', 'application_form_file', 'application_form_filename', 'application_form_link', 'district_id', 'photo_filename'
    ]);
    $session->set('created_at', $tradeSchool->created);
    $session->set('updated_at', $tradeSchool->updated);
    $session->set('applying', $tradeSchool->applying());
});

$this->helper('helpers.relationships', function () use ($tradeSchool) {
    $relationships = [];

    if (!in_array('TradeSchool.owner', app('request')->ignore_included)) {
        if ($tradeSchool->owner) {
            $relationships['owner'] = [
                'id' => $tradeSchool->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $tradeSchool->owner], array_get($tradeSchool->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('TradeSchool.district', app('request')->ignore_included)) {
        if ($tradeSchool->district) {
            $relationships['district'] = [
                'id' => $tradeSchool->district->id,
                'type' => 'District',
            ];
            $this->helper('helpers.included-add', ['district' => $tradeSchool->district], array_get($tradeSchool->district, 'id'), 'partials/district');
        }
    }

    if (!in_array('TradeSchool.activities', app('request')->ignore_included)) {
        $user = \Auth::user();
        if ($user) {
            $activities = $tradeSchool->activities()->ofActor($user->id)->get();
            foreach ($activities as $value) {
                $relationships['activities'][] = [
                    'id' => $value->id,
                    'type' => 'Activity',
                ];
                $this->helper('helpers.included-add', ['activity' => $value], $value->id, 'partials/activity');
            }
        }
    }

    return $relationships;
});
