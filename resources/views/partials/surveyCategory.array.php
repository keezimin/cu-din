<?php

$this->set('type', 'SurveyCategory');
$this->set('id', $surveyCategory->id);
$this->set('attributes', function ($session) use ($surveyCategory) {
    $session->set('id', $surveyCategory->id);
    $session->extract($surveyCategory, [
        'name',
    ]);
});
