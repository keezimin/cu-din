<?php

$this->set('type', 'Pathway');
$this->set('id', $pathway->id);
$this->set('attributes', function ($session) use ($pathway) {
    $session->set('id', $pathway->id);
    $session->extract($pathway, [
        'career_cluster_id', 'name', 'short_description', 'photo', 'user_id', 'owner_id', 'active'
    ]);

    $skills = $this->helper('helpers.profile', $pathway->skills);
    $training = $this->helper('helpers.profile', $pathway->training);
    $about = $this->helper('helpers.profile', $pathway->about);

    $session->set('about', $about);
    $session->set('skills', $skills);
    $session->set('training', $training);
    $session->set('created_at', $pathway->created);
    $session->set('updated_at', $pathway->updated);
});

$this->helper('helpers.relationships', function () use ($pathway) {
    $relationships = [];

    if (!in_array('Pathway.owner', app('request')->ignore_included)) {
        if ($pathway->owner) {
            $relationships['owner'] = [
                'id' => $pathway->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $pathway->owner], array_get($pathway->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }
    if (!in_array('Pathway.user', app('request')->ignore_included)) {
        if ($pathway->user) {
            $relationships['user'] = [
                'id' => $pathway->user->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $pathway->user], array_get($pathway->user, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Pathway.careerCluster', app('request')->ignore_included)) {
        if ($pathway->careerCluster) {
            $relationships['careerCluster'] = [
                'id' => $pathway->careerCluster->id,
                'type' => 'careerCluster',
            ];
            $this->helper('helpers.included-add', ['careerCluster' => $pathway->careerCluster], array_get($pathway->careerCluster, 'id'), 'partials/careerCluster');
        }
    }

    if (!in_array('Pathway.activities', app('request')->ignore_included)) {
        $user = \Auth::user();
        if ($user) {
            $activities = $pathway->activities()->ofActor($user->id)->get();
            foreach ($activities as $value) {
                $relationships['activities'][] = [
                    'id' => $value->id,
                    'type' => 'Activity',
                ];
                $this->helper('helpers.included-add', ['activity' => $value], $value->id, 'partials/activity');
            }
        }
    }

    return $relationships;
});