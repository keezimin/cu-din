<?php

$this->set('type', 'Category');
$this->set('id', $category->id);
$this->set('attributes', function ($session) use ($category) {
    $session->set('id', $category->id);
    $session->extract($category, [
        'name',
    ]);
});
