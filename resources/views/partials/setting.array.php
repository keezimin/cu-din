<?php

$this->set('type', 'Setting');
$this->set('attributes', function ($session) use ($setting) {
    $session->extract($setting, [
        'key',
    ]);
    $value = $setting->value;

    if ($setting->key === 'app_platform') {
        $value = json_decode($setting->value);
    }

    $session->set('value', $value);
});
