<?php

$this->set('type', 'Checklist');
$this->set('id', $checklist->id);
$this->set('attributes', function ($session) use ($checklist) {
    $session->set('id', $checklist->id);
    $session->extract($checklist, [
        'name', 'object_id', 'type',
    ]);
    $session->set('created_at', $checklist->created);
    $session->set('updated_at', $checklist->updated);
    $session->set('deleted_at', $checklist->deleted);
});

$this->helper('helpers.relationships', function () use ($checklist) {
    $relationships = [];

    if (!in_array('Checklist.owner', app('request')->ignore_included)) {
        if ($checklist->owner) {
            $relationships['owner'] = [
                'id' => $checklist->owner->id,
                'type' => 'User',
            ];
            $this->helper('helpers.included-add', ['user' => $checklist->owner], array_get($checklist->owner, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Checklist.object', app('request')->ignore_included)) {
        if ($checklist->object) {
            $type = camel_case(get_class_name($checklist->object));
            $relationships['object'] = [
                'id' => $checklist->object->id,
                'type' => ucfirst($type),
            ];
            $this->helper('helpers.included-add', [$type => $checklist->object], array_get($checklist->object, 'id'), 'partials/' . $type);
        }
    }

    if (!in_array('Checklist.tasks', app('request')->ignore_included)) {
        $tasks = $checklist->tasks()->withTrashed()->get();
        foreach ($tasks as $value) {
            $relationships['tasks'][] = [
                'id' => $value->id,
                'type' => 'Task',
            ];
            $this->helper('helpers.included-add', ['task' => $value], $value->id, 'partials/task');
        }
    }

    return $relationships;
});
