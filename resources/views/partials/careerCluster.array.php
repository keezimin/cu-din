<?php
$totalPoint = (isset($totalPoint) ? $totalPoint : '');
$this->set('type', 'CareerCluster');
$this->set('id', $careerCluster->id);
$this->set('attributes', function ($session) use ($careerCluster, $totalPoint) {
    $session->set('id', $careerCluster->id);
    if (isset($careerCluster->pivot->total_point)) {
        $totalPoint = $careerCluster->pivot->total_point;
    }
    if ((isset($totalPoint) && $totalPoint === 0) || !empty($totalPoint)) {
        $session->set('total_point', $totalPoint);
    }
    $session->extract($careerCluster, [
        'name', 'description'
    ]);
    $session->set('logo', $careerCluster->image);
    $session->set('photos', $careerCluster->getPhotos());
});

$this->helper('helpers.relationships', function () use ($careerCluster) {
    $relationships = [];
    if (!in_array('CareerCluster.occupations', app('request')->ignore_included)) {
        foreach ($careerCluster->occupations ?? [] as $value) {
            $relationships['occupations'][] = [
                'id' => $value->id,
                'type' => 'Occupation',
            ];

            $this->helper('helpers.included-add', ['occupation' => $value], array_get($value, 'id'), 'partials/occupation');
        }
    }

    if (!in_array('CareerCluster.photos', app('request')->ignore_included)) {
        foreach ($careerCluster->photos ?? [] as $value) {
            $relationships['photos'][] = [
                'id' => $value->id,
                'type' => 'Photo',
            ];
            $this->helper('helpers.included-add', ['photo' => $value], array_get($value, 'id'), 'partials/photo');
        }
    }

    return $relationships;
});