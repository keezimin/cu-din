<?php

$this->set('type', 'Organization');
$this->set('id', $organization->id);
$this->set('attributes', function ($session) use ($organization) {
    $session->set('id', $organization->id);
    $session->extract($organization, [
        'name', 'user_id', 'district_id', 'updated_by',
     ]);

    if(isset($organization->district)) {
      $session->set('name_district', $organization->district->name);
    } else {
      $session->set('name_district', null);
    }

    if(isset($organization->district->state)) {
        $session->set('state_id', $organization->district->state->id);
        $session->set('name_state', $organization->district->state->name);
      } else {
        $session->set('state_id', null);
        $session->set('name_state', null);
    }

    if(isset($organization->user)) {
        $session->set('name_partner', $organization->user->first_name. ' '. $organization->user->last_name);
      } else {
        $session->set('name_partner', null);
    }
 });

$this->helper('helpers.relationships', function () use ($organization) {
    $relationships = [];

    if (!in_array('Organization.district', app('request')->ignore_included)) {
        if ($organization->district) {
        $relationships['district'] = [
            'id' => $organization->district->id,
            'type' => 'District',
        ];
        $this->helper('helpers.included-add', ['district' => $organization->district], array_get($organization->district, 'id'), 'partials/district');
        }
    }

    if (!in_array('Organization.partner', app('request')->ignore_included)) {
        if ($organization->user) {
        $relationships['user'] = [
            'id' => $organization->user->id,
            'type' => 'User',
        ];
        $this->helper('helpers.included-add', ['user' => $organization->user], array_get($organization->user, 'id'), 'phpsoft.users::partials/user');
        }
    }

    if (!in_array('Organization.district.state', app('request')->ignore_included)) {
        if (isset($organization->district->state)) {
        $relationships['state'] = [
            'id' => $organization->district->state->id,
            'type' => 'State',
        ];
        $this->helper('helpers.included-add', ['state' => $organization->district->state], array_get($organization->district->state, 'id'), 'partials/state');
        }
    }

    if (!in_array('Organization.schools', app('request')->ignore_included)) {
        if (!empty($schools = $organization->schools()->get())) {
            foreach ($schools as $value) {
                $relationships['schools'][] = [
                    'id' => $value->id,
                    'type' => 'School',
                ];
                $this->helper('helpers.included-add', ['school' => $value], array_get($value, 'id'), 'partials/school');
            }
        }
    }
    return $relationships;
});
