<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $settings));
$this->set('links', $this->helper('helpers.links', $settings));
$this->set('data', $this->each($settings, function ($section, $setting) {
    $section->set($section->partial('partials/setting', ['setting' => $setting]));
}));
