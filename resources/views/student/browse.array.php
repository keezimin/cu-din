<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $users));
$this->set('links', $this->helper('helpers.links', $users));
$this->set('data', $this->each($users, function ($section, $user) {

    $section->set($section->partial('partials/student', [ 'user' => $user ]));
}));

foreach ($users as $user) {
    $this->helper('helpers.included-add', ['school' => $user->school], array_get($user->school, 'id'), 'partials/school');
}

$this->helper('helpers.included');
