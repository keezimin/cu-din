<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $jobs));
$this->set('links', $this->helper('helpers.links', $jobs));
$this->set('data', $this->each($jobs, function ($section, $job) {
    $section->set($section->partial('partials/job', ['job' => $job]));
}));

$this->helper('helpers.included');
