<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $pathways));
$this->set('links', $this->helper('helpers.links', $pathways));
$this->set('data', $this->each($pathways, function ($section, $pathway) {
    $section->set($section->partial('partials/pathway', [ 'pathway' => $pathway ]));
}));

$this->helper('helpers.included');
