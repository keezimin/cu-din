<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $checklists));
$this->set('links', $this->helper('helpers.links', $checklists));
$this->set('data', $this->each($checklists, function ($section, $checklist) {
    $section->set($section->partial('partials/checklist', ['checklist' => $checklist]));
}));

$this->helper('helpers.included');
