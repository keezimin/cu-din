<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/checklist', ['checklist' => $checklist]));

$this->helper('helpers.included');
