<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $activities));
$this->set('links', $this->helper('helpers.links', $activities));
$this->set('data', $this->each($activities, function ($section, $activity) {
    $section->set($section->partial('partials/activity_v2', ['activity' => $activity]));
}));

$this->helper('helpers.included');
