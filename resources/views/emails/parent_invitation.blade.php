<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CUED-IN - Invitation</title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%; }

      body {
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 16px;
        line-height: 1.4;
        margin: 0;
        padding: 50px;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        background:#F5F5F5; }

      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body {
        background-color: #fff;
        width:600px;margin:0px auto;padding: 45px; border:1px solid #e3e3e3;border-radius:4px;}

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        Margin: 0 auto !important;
        /* makes it centered */
        width: auto !important;
        width: 580px; }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        Margin: 0 auto;
        }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #fff;
        border-radius: 3px;
        line-height: 1.5;
        text-align: center;
        width: 100%; }
      .main .content p {
        color: #3C3737;
        font-size: 16px; }
      .main .content .color-gray {
        padding: 10px 0 5px;
        color: #888; }
      .center {
        max-width: 600px;
        margin: 0 auto;
      }
      .content-title {
        color: #3C3737;
        font-size: 24px;
        text-align: center;
        margin-bottom: 20px;
        line-height: 1.5;
      }
      .wrapper {
        box-sizing: border-box; }
      .header {
        clear: both;
        margin-bottom: 35px;
        text-align: center;
        width: 100%;
      }
      .footer {
        border-top: 1px solid #EEE;
        clear: both;
        color: #A3A3A3;
        padding: 30px 0 0;
        margin-top: 20px;
        text-align: center;
        width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #A3A3A3;
          line-height: 1.6;
          font-size: 14px;
          margin-bottom: 8px; }
        .footer .list {
          text-align: center;
          padding: 0;
          margin: 0; }
        .footer .list li {
          display: inline-block;
          margin: 0;
        }
        .footer .list li:before {
          background-color: #A3A3A3;
          content: '';
          border-radius: 10px;
          display: inline-block;
          width: 3px;
          height: 3px;
          margin: 11px 10px 10px;
          float: left;
        }
        .footer .list li:first-child:before {
          display: none;
        }
        .footer .list li a {
          text-decoration: underline;
        }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        Margin-bottom: 15px; }

      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize; }

      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 16px;
        line-height: 24px;
        font-weight: normal;
        margin: 0;
        Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px; }

      a {
        color: #19ADFF;
        text-decoration: none; }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center; }
        .btn a {
          background-color: #83cc14;
          border: solid 1px #83cc14;
          border-radius: 5px;
          box-sizing: border-box;
          color: #fff;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0 auto;
          padding: 12px 25px;
          width: 70%;
          text-decoration: none;
          text-transform: capitalize; }

      .btn-primary table td {
        background-color: #83cc14; }

      .btn-primary a {
        background-color: #83cc14;
        border-color: #83cc14;
        color: #ffffff; }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0; }

      .first {
        margin-top: 0; }

      .align-center {
        text-align: center; }

      .align-right {
        text-align: right; }

      .align-left {
        text-align: left; }

      .clear {
        clear: both; }

      .mt0 {
        margin-top: 0; }

      .mb0 {
        margin-bottom: 0; }

      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0; }

      .powered-by a {
        text-decoration: none; }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        Margin: 20px 0; }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important; }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important; }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important; }
        table[class=body] .content {
          padding: 0 !important; }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important; }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; }
        table[class=body] .btn table {
          width: 100% !important; }
        table[class=body] .btn a {
          width: 100% !important; }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; }
        .footer .content-block {
          padding: 0 10px;
        }}

      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%; }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%; }
        .contact-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important;
          text-transform: uppercase !important; }
        .btn-primary table td:hover {
          background-color: #34495e !important; }
        .btn-primary a:hover {
          background-color: #34495e !important;
          border-color: #34495e !important; } }

    </style>
  </head>
  <body class="">
    <table border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td class="container">
          <div class="content">

            <!-- START CENTERED WHITE CONTAINER -->
            <!-- START HEADER -->
            <div class="header">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <a href="http://cued-in.com/"><img src="{{url('logo.png')}}" alt="CUED-IN" width="103" /></a>
                  </td>
                </tr>
              </table>
            </div>

            <!-- END HEADER -->
            <table class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  <div class="center">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td>
                          <p class="content-title"><b>You’ve been invited to join a <br /> CUED-In account.</b></p>
                          <div class="content">
                            <p>Welcome to CUED-In.</p>
                            <p>{{$user->fullname}} would like your assistance in career planning.</p>
                            <p>Please download the CUED-In app to get started: <a class="button-primary" href="{{$ios_link}}">iOS</a> and <a class="button-primary" href="{{$android_link}}">Android</a>.</p>
                            <p>Thank you for encouraging your child.</p>
                            <p>If you have any questions, contact <a href="maito:damien@gmail.com">info@cued-in.com</a></p>
                            <p class="color-gray">Join CUED-In, click the appropriate link below</p>
                          </div>
                          <p class="btn"><a class="button-primary" href="{{$url}}">JOIN THIS ACCOUNT</a></p>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
              <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
            <div class="footer">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <p>&copy; 2017 CUED-In. All rights reserved</p>
                  </td>
                </tr>
                <tr>
                  <td class="content-block">
                    <ul class="list">
                      <li><a href="#">Contact Us</a></li>
                      <li><a href="#">Term of Use</a></li>
                      <li><a href="#">Privacy Policy</a></li>
                    </ul>
                  </td>
                </tr>
              </table>
            </div>

            <!-- END FOOTER -->
            <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
