<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $groups));
$this->set('links', $this->helper('helpers.links', $groups));
$this->set('data', $this->each($groups, function ($section, $group) {
    $section->set($section->partial('partials/group', ['group' => $group]));
}));

$this->helper('helpers.included');
