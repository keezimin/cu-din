<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $messages));
$this->set('links', $this->helper('helpers.links', $messages));
$this->set('data', $this->each($messages, function ($section, $message) {
    $section->set($section->partial('partials/message', [ 'message' => $message ]));
}));

$this->helper('helpers.included');