<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $schools));
$this->set('links', $this->helper('helpers.links', $schools));
$this->set('data', $this->each($schools, function ($section, $school) {
    $section->set($section->partial('partials/school', [ 'school' => $school ]));
}));

foreach ($schools as $school) {
    $this->helper('helpers.included-add', ['district' => $school->district], array_get($school->district, 'id'), 'partials/district');
}

$this->helper('helpers.included');