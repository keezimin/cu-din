<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/school', [ 'school' => $school]));

$this->helper('helpers.included', [
    [['district' => $school->district], array_get($school->district, 'id'), 'partials/district'],
]);
