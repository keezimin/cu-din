<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
if (isset($totalPoint)) {
	$this->set('data', $this->partial('partials/careerCluster', ['careerCluster' => $careerCluster, 'totalPoint' => $totalPoint]));
} else {
	$this->set('data', $this->partial('partials/careerCluster', ['careerCluster' => $careerCluster]));
}

$this->helper('helpers.included');
