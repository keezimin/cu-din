<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $careerClusters));
$this->set('links', $this->helper('helpers.links', $careerClusters));
$this->set('data', $this->each($careerClusters, function ($section, $careerCluster) {
    $section->set($section->partial('partials/careerCluster', ['careerCluster' => $careerCluster]));
}));
