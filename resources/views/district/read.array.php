<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/district', [ 'district' => $district]));
$this->helper('helpers.included');