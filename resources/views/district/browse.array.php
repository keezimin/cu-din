<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $districts));
$this->set('links', $this->helper('helpers.links', $districts));
$this->set('data', $this->each($districts, function ($section, $district) {
    $section->set($section->partial('partials/district', ['district' => $district]));
}));

foreach ($districts as $district) {
   $this->helper('helpers.included-add', ['state' => $district->state], array_get($district->state, 'id'), 'partials/state');
}

$this->helper('helpers.included');