<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $resources));
$this->set('links', $this->helper('helpers.links', $resources));
$this->set('data', $this->each($resources, function ($section, $resource) {
    $section->set($section->partial('partials/resource', [ 'resource' => $resource ]));
}));