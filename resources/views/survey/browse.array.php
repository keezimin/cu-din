<?php
$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $surveys));
$this->set('links', $this->helper('helpers.links', $surveys));
$this->set('data', $this->each($surveys, function ($section, $survey) {
    $section->set($section->partial('partials/survey', [ 'survey' => $survey ]));
}));

$this->helper('helpers.included');
