<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/task', ['task' => $task]));

$this->helper('helpers.included');
