<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $tasks));
$this->set('links', $this->helper('helpers.links', $tasks));
$this->set('data', $this->each($tasks, function ($section, $task) {
    $section->set($section->partial('partials/task', ['task' => $task]));
}));

$this->helper('helpers.included');
