<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/organization', ['organization' => $organization]));
$this->helper('helpers.included');