<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $organizations));
$this->set('links', $this->helper('helpers.links', $organizations));
$this->set('data', $this->each($organizations, function ($section, $organization) {
    $section->set($section->partial('partials/organization', ['organization' => $organization]));
}));

foreach ($organizations as $organization) {
    if (isset($organization->district)) {
        $this->helper('helpers.included-add', ['district' => $organization->district], array_get($organization->district, 'id'), 'partials/district');
    }
}

$this->helper('helpers.included');