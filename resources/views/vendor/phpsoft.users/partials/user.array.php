<?php

$this->set('type', 'User');
$this->set('id', $user->id);
$this->set('attributes', function ($section) use ($user) {
    $section->set('id', $user->id);
    $section->extract($user, [
        'first_name',
        'last_name',
        'fullname',
        'email',
        'contact',
        'graduated_on',
        'act_english',
        'act_science',
        'act_math',
        'act_composite',
        'act_reading',
        'sat_math',
        'sat_verbal',
        'school_id',
        'district_id',
    ]);
    $section->set('created_at', $user->created);
    $section->set('updated_at', $user->updated);
    $section->set('deleted_at', $user->deleted_at);
    $section->set('profile', function ($section) use ($user) {
        $section->extract($user->profile, [
            'id',
            'website_link',
            'description',
            'address',
            'photo',
            'logo',
            'application_form_filename',
            'user_id',
            'contact_person',
            'location',
            'subscription',
            'updated_at'
        ]);
    });
    if ($user->businessProfile) {
        $section->set('business_profile', function ($section) use ($user) {
            $section->extract($user->businessProfile, [
                'id',
                'user_id',
                'active',
                'phone',
                'industry',
                'type',
                'comapny_size',
                'founded',
                'specialties',
                'facebook',
                'instagram',
                'linkedin',
            ]);
            $about = $this->helper('helpers.profile', $user->businessProfile->about);
            $culture = $this->helper('helpers.profile', $user->businessProfile->culture);
            $section->set('about', $about);
            $section->set('culture', $culture);
        });
    }
    $section->set('roles', $user->getRoles());
});

$this->helper('helpers.relationships', function () use ($user) {
    $relationships = [];

    if (!in_array('User.owner', app('request')->ignore_included)) {
        $user->owner && $relationships['owner'] = [
            'id' => $user->owner->id,
            'type' => 'User',
        ];
        $this->helper('helpers.included-add', ['user' => $user->owner], array_get($user->owner, 'id'), 'phpsoft.users::partials/user');
    }

    if (!in_array('User.district', app('request')->ignore_included)) {
        $user->district && $relationships['district'] = [
            'id' => $user->district->id,
            'type' => 'District',
        ];
        $this->helper('helpers.included-add', ['district' => $user->district], array_get($user->district, 'id'), 'partials/district');
    }

    if (!in_array('User.school', app('request')->ignore_included)) {
        $user->school && $relationships['school'] = [
            'id' => $user->school->id,
            'type' => 'School',
        ];
        $this->helper('helpers.included-add', ['school' => $user->school], array_get($user->school, 'id'), 'partials/school');
    }

    if (!in_array('User.scholarshipActivities', app('request')->ignore_included)) {
        foreach ($user->scholarshipActivities ?? [] as $value) {
            $relationships['scholarshipActivities'][] = [
                'id' => $value->id,
                'type' => 'Activity',
            ];
            $this->helper('helpers.included-add', ['activity' => $value], array_get($value, 'id'), 'partials/activity');
        }
    }

    if (!in_array('Business.activities', app('request')->ignore_included)) {
        $business = \Auth::user();
        if ($business) {
            $activities = $business->businessActivities()->ofActor($business->id)->get();

            foreach ($activities as $value) {
                $relationships['activities'][] = [
                    'id' => $value->id,
                    'type' => 'Activity',
                ];
                $this->helper('helpers.included-add', ['activity' => $value], $value->id, 'partials/activity');
            }
        }
    }

    if (!in_array('User.tasks', app('request')->ignore_included)) {
        foreach ($user->tasks ?? [] as $value) {
            $relationships['tasks'][] = [
                'id' => $value->id,
                'type' => 'Task',
            ];
            $this->helper('helpers.included-add', ['task' => $value], array_get($value, 'id'), 'partials/task');
        }
    }

    if (!in_array('User.photos', app('request')->ignore_included)) {
        foreach ($user->photos ?? [] as $value) {
            $relationships['photos'][] = [
                'id' => $value->id,
                'type' => 'Photo',
            ];
            $this->helper('helpers.included-add', ['photo' => $value], array_get($value, 'id'), 'partials/photo');
        }
    }

    if (!in_array('User.careerClusters', app('request')->ignore_included)) {
        foreach ($user->careerClusters ?? [] as $value) {
            $relationships['careerCluster'][] = [
                'id' => $value->id,
                'type' => 'careerCluster',
            ];
            $this->helper('helpers.included-add', ['careerCluster' => $value], array_get($value, 'id'), 'partials/careerCluster');
        }
    }

    if (!in_array('User.organization', app('request')->ignore_included)) {
        $user->organization && $relationships['organization'] = [
            'id' => $user->organization->id,
            'type' => 'Organization',
        ];
        $this->helper('helpers.included-add', ['organization' => $user->organization], array_get($user->organization, 'id'), 'partials/organization');
    }

    return $relationships;
});
