<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('phpsoft.users::partials/user', [ 'user' => $user ]));

$this->helper('helpers.included');
