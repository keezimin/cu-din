<?php

$this->set('json', ['version' => '1.0']);
$this->set('meta', ['version' => '1.0']);
$this->set('data', $this->partial('partials/state', ['state' => $state]));

$this->helper('helpers.included');
