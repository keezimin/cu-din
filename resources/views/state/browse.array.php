<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $states));
$this->set('links', $this->helper('helpers.links', $states));
$this->set('data', $this->each($states, function ($section, $state) {
    $section->set($section->partial('partials/state', ['state' => $state]));
}));

$this->helper('helpers.included');
