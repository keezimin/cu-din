<?php
use PhpSoft\Users\Models\Role;

class RoleControllerTest extends TestCase
{
    public function testAddRoleSalonOwnerBySeeder()
    {
        $roleSalonOwner = Role::where('name', 'salon_owner')->first();
        $this->assertNotNull($roleSalonOwner);
        $this->assertNotEmpty($roleSalonOwner);
        $this->assertEquals('salon_owner', $roleSalonOwner->name);
        $this->assertEquals('Salon Owner', $roleSalonOwner->display_name);
        $this->assertEquals('User is allowed to manage all salons that user is their owner.', $roleSalonOwner->description);
    }
}
