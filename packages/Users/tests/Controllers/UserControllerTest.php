<?php

use App\User as AppUser;
use PhpSoft\Users\Models\User;
use PhpSoft\Users\Models\Role;
use PhpSoft\Swivel\Models\HairType;
use PhpSoft\Swivel\Models\Booking;

class UserControllerTest extends TestCase
{
    public function testGetAuthenticatedUserNormail()
    {
        $user = factory(AppUser::class)->create(['email' => 'user@example.com', 'password' => bcrypt('123456') ]);
        $token = JWTAuth::attempt(['email' => 'user@example.com', 'password' => '123456']);
        $user = JWTAuth::toUser($token);
        $res = $this->call('GET', '/me', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals($user->username, $results->data->attributes->username);
        $this->assertEquals($user->email, $results->data->attributes->email);
    }

    public function getMe()
    {
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);

        $res = $this->call('GET', '/me', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        return $results->data;
    }
    public function testGetAuthenticatedUserNotSendToken()
    {
        $res = $this->call('GET', '/me');
        $this->assertEquals(401, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('Token is not provided.', $results->errors[0]->detail);
    }

    public function testCheckAuthGetMe()
    {
        $this->withoutMiddleware();
        $res = $this->call('GET', '/me');
        $this->assertEquals(401, $res->getStatusCode());
    }

    public function testGetAuthenticatedUser()
    {
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $user = JWTAuth::toUser($token);

        $res = $this->call('GET', '/me', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertInternalType('string', $results->data->attributes->id);
        $this->assertEquals($user->username, $results->data->attributes->username);
        $this->assertEquals($user->email, $results->data->attributes->email);
    }

    public function testRegisterValidateFailure()
    {
        $res = $this->call('POST', '/users');
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The username field is required.', $results->errors[0]->detail);
        $this->assertEquals('The email field is required.', $results->errors[1]->detail);
        $this->assertEquals('The password field is required.', $results->errors[2]->detail);

        $res = $this->call('POST', '/users', [
            'username'      => 'Fish Bone',
            'email'     => 'Invalid email',
            'password'  => '123',
        ]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The email must be a valid email address.', $results->errors[0]->detail);
        $this->assertEquals('The password confirmation does not match.', $results->errors[1]->detail);
        $this->assertEquals('The password must be at least 6 characters.', $results->errors[2]->detail);

        $res = $this->call('POST', '/users', [
            'username'      => 'Fish Bone',
            'email'     => 'admin@example.com',
            'password'  => '123456',
            'password_confirmation'  => '123456',
        ]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The email has already been taken.', $results->errors[0]->detail);
    }

    public function testRegisterSuccess()
    {
        Mail::shouldReceive('send')->andReturn(true);
        $data = [
            'username'           => 'Fish Bone',
            'email'                 => 'fish@example.com',
            'password'              => '123456',
            'password_confirmation' => '123456',
        ];
        $res = $this->call('POST', '/users', $data);
        $results = json_decode($res->getContent());
        $this->assertEquals(201, $res->getStatusCode());

        $userId = $results->data->id;
        $user = \App\User::find($userId);
        $this->assertEquals($data['username'], $user->username);
        $this->assertEquals($data['email'], $user->email);

        // User name unique
        $data = [
            'username'           => 'Fish Bone',
            'email'                 => 'snake@example.com',
            'password'              => '123456',
            'password_confirmation' => '123456',
        ];
        $res = $this->call('POST', '/users', $data);
        $results = json_decode($res->getContent());
        $this->assertEquals(400, $res->getStatusCode());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The username has already been taken.', $results->errors[0]->detail);
    }

    public function testCheckAuthUpdateProfile()
    {
        $res = $this->call('PATCH', '/users/1');
        $this->assertEquals(401, $res->getStatusCode());
    }

    public function testUpdateProfileFailure()
    {
        // test auth
        $res = $this->call('PATCH', '/users/1', [
           'username' => 'Steven',
        ]);

        $results = json_decode($res->getContent());
        $this->assertEquals(401, $res->getStatusCode());

        $this->assertObjectHasAttribute('errors', $results);

        // test invalid validate input
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        //

        $user = factory(AppUser::class)->create(['email' => 'email@gmail.com']);

        $res = $this->call('PATCH', '/users/{id}', [
           'username' => 'Steven Adam',
           'email'      => 'admin@example.com',
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(404, $res->getStatusCode());

        $res = $this->call('PATCH', '/users/1', [
           'username' => 'Steven Adam',
           'email'      => 'email@gmail.com',
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('The email has already been taken.', $results->errors[0]->detail);

        $data = [
            'hair_type_id'          => "123123123132"
        ];
        $res = $this->call('PATCH', '/users/1', $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);

        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
    }

    public function testUpdateWithPermission()
    {
        $user1 = factory(AppUser::class)->create(['email' => 'email1@gmail.com']);
        $user2 = factory(AppUser::class)->create(['email' => 'email2@gmail.com']);
        $credentials = [ 'email' => 'email1@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);

        $res = $this->call('PATCH', '/users/' . $user2->id, [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(403, $res->getStatusCode());

        $res = $this->call('PATCH', '/users/' . $user1->id, [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
    }

    public function testUpdateEmail()
    {
        $user1 = factory(AppUser::class)->create(['email' => 'email1@gmail.com']);
        $user2 = factory(AppUser::class)->create(['email' => 'email2@gmail.com']);
        $credentials = [ 'email' => 'email1@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        JWTAuth::toUser($token);

        $res = $this->call('PATCH', '/users/' . $user1->id, [
            'email' => 'email3@gmail.com'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());

        $res = $this->call('PATCH', '/users/' . $user1->id, [
            'email' => 'email2@gmail.com'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());

        $res = $this->call('PATCH', '/users/' . $user1->id, [
            'email' => 'email3@gmail.com'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());

        $credentials = [ 'email' => 'email2@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        JWTAuth::toUser($token);

        $res = $this->call('PATCH', '/users/' . $user2->id, [
            'email' => 'email3@gmail.com'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());

        $credentials = [ 'email' => 'email2@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $user = JWTAuth::toUser($token);

        $res = $this->call('PATCH', '/users/' . $user->id, [
            'email' => 'email3@gmail.com'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
    }

    public function testUpdateProfileSuccess()
    {
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        JWTAuth::toUser($token);

        $hairtype = factory(HairType::class)->create();
        $data = [
            'username'            => 'Steven Adam',
            'email'                 => 'email@gmail.com',
            'avatar'                => 'http://4.bp.blogspot.com/-PYPXhjtfYio/Uc5gvnATFyI/AAAAAAAACDs/CjaI9fFmSK8/s1600/Garen_splash.jpg',
            'hair_type_id'          => $hairtype->id
        ];
        $res = $this->call('PATCH', '/users/1', $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);

        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $userId = $results->data->id;
        $user = \App\User::find($userId);
        $this->assertInternalType('string', $results->data->id);
        $this->assertEquals($data['username'], $user->username);
        $this->assertEquals($data['email'], $user->email);
        $this->assertEquals($data['avatar'], $results->data->attributes->avatar);
        $this->assertEquals($data['hair_type_id'], $results->data->attributes->hair_type_id);
        $this->assertEquals($hairtype->id, $results->data->relationships->hairtype->id);
        $this->assertEquals("Hairtype", $results->data->relationships->hairtype->type);

        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        JWTAuth::toUser($token);

        $hairtype = factory(HairType::class)->create();
        $user = factory(\App\User::class)->create();
        $data = [
            'username'            => 'Steven Adam 2',
            'email'                 => 'email122@gmail.com',
            'avatar'                => 'http://4.bp.blogspot.com/-PYPXhjtfYio/Uc5gvnATFyI/AAAAAAAACDs/CjaI9fFmSK8/s1600/Garen_splash.jpg',
        ];
        $res = $this->call('PATCH', '/users/'.$user->id, $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertObjectHasAttribute('type', $results->included);
        $this->assertObjectHasAttribute('id', $results->included);
        $this->assertObjectHasAttribute('attributes', $results->included);
        $this->assertEquals($data['username'], $results->data->attributes->username);
        $this->assertEquals($data['email'], $results->data->attributes->email);
        $this->assertEquals($data['avatar'], $results->data->attributes->avatar);
        $this->assertEquals("Hairtype", $results->included->type);
        $this->assertObjectHasAttribute("id", $results->included->attributes);

        $hairtype = factory(HairType::class)->create();
        $data = [
            'hair_type_id'          => $hairtype->id
        ];
        $res = $this->call('PATCH', '/users/1', $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);

        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertEquals($hairtype->id, $results->data->relationships->hairtype->id);
        $this->assertEquals("Hairtype", $results->data->relationships->hairtype->type);
        $this->assertEquals($hairtype->id, $results->included->attributes->id);
        $this->assertEquals($hairtype->id, $results->included->id);
        $this->assertEquals("Hairtype", $results->included->type);
        $this->assertObjectHasAttribute('type', $results->included);
        $this->assertObjectHasAttribute('id', $results->included);
        $this->assertObjectHasAttribute('attributes', $results->included);

        $data = [
            'hair_type_id'          => 12313123
        ];
        $res = $this->call('PATCH', '/users/1', $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);

        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());

        // test unique user
        $data = [
            'username'            => 'Steven Adam 2',
            'email'                 => 'email122@gmail.com',
            'avatar'                => 'http://4.bp.blogspot.com/-PYPXhjtfYio/Uc5gvnATFyI/AAAAAAAACDs/CjaI9fFmSK8/s1600/Garen_splash.jpg',
        ];
        $res = $this->call('PATCH', '/users/'.$user->id, $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
    }

    public function testViewUser()
    {
        // test view user failure
        $res = $this->call('GET', '/users/2');
        $this->assertEquals(404, $res->getStatusCode());
        // test view user succsess
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users/1', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $user = \App\User::find(1);
        $this->assertEquals($user->username, $results->data->attributes->username);
        $this->assertEquals('super_admin', $results->data->attributes->roles[0]);
        $this->assertEquals($user->email, $results->data->attributes->email);
        $this->assertEquals($user->id, $results->data->id);
        $this->assertInternalType('string', $results->data->id);
        $this->assertEquals(1, $results->included->id);
        $this->assertEquals("Hairtype", $results->included->type);

        $user = factory(\App\User::class)->create(['email' => 'job@gmail.com']);
        $member = new Role;
        $member->name         = 'member';
        $member->display_name = 'Member of system';
        $member->description  = '';
        $member->save();
        $user->attachRole($member);

        // test admin get another user
        $res = $this->call('GET', '/users/' . $user->id, [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals('member', $results->data->attributes->roles[0]);
        $this->assertObjectHasAttribute('fb_info', $results->data->attributes);
        $this->assertNull($results->data->attributes->fb_info);
    }

    public function testReadRole()
    {
        //test with no auth
        $res = $this->call('GET', '/users/1', [], [], [], []);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectNotHasAttribute('roles', $results->data->attributes);

        // test with auth but not admin
        $user = factory(\App\User::class)->create(['email' => 'job@gmail.com']);
        $credentials = [ 'email' => 'job@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);

        $member = new Role;
        $member->name         = 'member';
        $member->display_name = 'Member of system';
        $member->description  = '';
        $member->save();
        $user->attachRole($member);

        $res = $this->call('GET', '/users/' . $user->id, [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('member', $results->data->attributes->roles[0]);

        // tests auth not admin get another user
        $res = $this->call('GET', '/users/1', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectNotHasAttribute('roles', $results->data->attributes);
    }

    public function testBrowseNotFound()
    {
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals(1, count($results->data));
    }

    public function testBrowseFound()
    {
        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $users[] = factory(App\User::class)->create();
        }
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals(count($users), count($results->data));
    }

    public function testOrder()
    {
        $user1 = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();

        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals($user2->id, $results->data[0]->id);
        $this->assertEquals($user1->id, $results->data[1]->id);

        $res = $this->call('GET', '/users?sort=-id', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals($user2->id, $results->data[0]->id);
        $this->assertEquals($user1->id, $results->data[1]->id);

        $res = $this->call('GET', '/users?sort=+id', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals($user2->id, $results->data[2]->id);
        $this->assertEquals($user1->id, $results->data[1]->id);
    }

    public function testBrowseWithPagination()
    {
        $users = [];
        for ($i = 0; $i < 20; ++$i) {
            $users[] = factory(App\User::class)->create();
        }
        // invalid param
        $res = $this->call('GET', '/users?page[number]=1&page[size]=-1');
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('The page.size must be at least 1.', $results->errors[0]->detail);

        $res = $this->call('GET', '/users?page[number]=1&page[size]=a');
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('The page.size must be an integer.', $results->errors[0]->detail);

        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users?page[number]=1&page[size]=1', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        // 1 items next
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertEquals(1, count($results->data));

        // 5 items next
        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $res = $this->call('GET', '/users?page[number]=2&page[size]=10', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertObjectHasAttribute('username', $results->data[0]->attributes);
        $this->assertObjectHasAttribute('email', $results->data[0]->attributes);
        $this->assertEquals(10, count($results->data));
        $this->assertObjectHasAttribute('included', $results);
        $this->assertEquals(1, $results->included[0]->id);
        $this->assertEquals("Hairtype", $results->included[0]->type);
    }

    public function testNoPermission()
    {
        $res = $this->call('GET', '/search/users', [], [], [], []);
        $this->assertResponseStatus(401);

        $user = factory(App\User::class)->create();
        $token = JWTAuth::attempt(['email' => $user->email, 'password' => '123456']);

        $res = $this->call('GET', '/search/users', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertResponseStatus(403);
    }

    public function testSearchSuccess()
    {
        $token = JWTAuth::attempt(['email' => 'admin@example.com', 'password' => '123456']);

        for ($i = 0; $i < 10; ++$i) {
            $users[] = factory(App\User::class)->create([
                'username' => 'user'.($i + 1)
            ]);
        }

        $res = $this->call('GET', '/search/users', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertResponseStatus(200);

        $res = $this->call('GET', '/search/users?q=abc', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(0, count($results->data));

        $res = $this->call('GET', '/search/users?q=user', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(10, count($results->data));

        $res = $this->call('GET', '/search/users?q=user5', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(1, count($results->data));

        $res = $this->call('GET', '/search/users?q=user&page[size]=5', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(5, count($results->data));

        $res = $this->call('GET', '/search/users?q=user&page[number]=2&page[size]=5', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(5, count($results->data));

        $res = $this->call('GET', '/search/users?q=user&page[number]=3&page[size]=5', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(0, count($results->data));

        $res = $this->call('GET', '/search/users?q=user1', [], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertResponseStatus(200);
        $results = json_decode($res->getContent());
        $this->assertEquals(2, count($results->data));
    }

    public function testReadFacebookProfile()
    {
        //test with no auth
        $res = $this->call('GET', '/users/1', [], [], [], []);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectNotHasAttribute('fb_info', $results->data->attributes);

        $fbInfo = [
            'nickname' => 'steven',
            'name' => 'steven job',
            'gender' => 'male',
            'id' => '123456789',
            'avatar' => 'link avatar',
            "avatar_original" => 'link avatar original'
        ];
        // test with auth but not admin
        $user = factory(\App\User::class)->create(['email' => 'job@gmail.com', 'fb_info' => $fbInfo]);
        $credentials = [ 'email' => 'job@gmail.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);

        $member = new Role;
        $member->name         = 'member';
        $member->display_name = 'Member of system';
        $member->description  = '';
        $member->save();
        $user->attachRole($member);

        $res = $this->call('GET', '/users/' . $user->id, [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('fb_info', $results->data->attributes);
        $this->assertEquals($fbInfo['nickname'], $results->data->attributes->fb_info->nickname);
        $this->assertEquals($fbInfo['name'], $results->data->attributes->fb_info->name);
        $this->assertEquals($fbInfo['gender'], $results->data->attributes->fb_info->gender);
        $this->assertEquals($fbInfo['id'], $results->data->attributes->fb_info->id);
        $this->assertEquals($fbInfo['avatar'], $results->data->attributes->fb_info->avatar);
        $this->assertEquals($fbInfo['avatar_original'], $results->data->attributes->fb_info->avatar_original);

        // tests auth not admin get another user
        $res = $this->call('GET', '/users/1', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $results = json_decode($res->getContent());
        $this->assertEquals(200, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectNotHasAttribute('fb_info', $results->data->attributes);
    }

    public function testUpdateUsernameUnique()
    {
        $user = factory(AppUser::class)->create(['email' => 'user2@example.com', 'password' => bcrypt('123456'), 'username' => 'Minh Ngoc' ]);
        $user = factory(AppUser::class)->create(['email' => 'user@example.com', 'password' => bcrypt('123456') ]);
        $credentials = [ 'email' => 'user@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        $hairtype = factory(HairType::class)->create();
        $data = [
            'username'            => 'Minh Ngoc',
            'email'                 => 'email@gmail.com',
            'avatar'                => 'http://4.bp.blogspot.com/-PYPXhjtfYio/Uc5gvnATFyI/AAAAAAAACDs/CjaI9fFmSK8/s1600/Garen_splash.jpg',
            'hair_type_id'          => $hairtype->id
        ];
        $res = $this->call('PATCH', '/users/'.$user->id, $data, [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('The username has already been taken.', $results->errors[0]->detail);
    }

    public function testShowSalonOwner()
    {
        $credentials = ['email' => 'admin@example.com', 'password' => '123456'];
        $token = JWTAuth::attempt($credentials);
        $salon = factory(PhpSoft\Swivel\Models\Salon::class)->create();

        $res = $this->call('POST', '/salonOwners', [
            'username' => 'Anthony',
            'email' => 'admin@example.com',
            'salon_id' => $salon->id,
        ], [], [], ['HTTP_Authorization' => "Bearer $token"]);
        $this->assertEquals(201, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertEquals('Salon', $results->included[0]->type);
        $this->assertEquals($salon->id, $results->included[0]->id);
        $this->assertEquals('User', $results->included[1]->type);
        $this->assertEquals('Anthony', $results->included[1]->attributes->username);
        $this->assertEquals('admin@example.com', $results->included[1]->attributes->email);

        $res1 = $this->call('GET', '/me', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(200, $res1->getStatusCode());
        $results1 = json_decode($res1->getContent());
        $this->assertNotEmpty($results1->data->attributes->salons);
    }
}
