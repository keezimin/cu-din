<?php

use App\User as AppUser;
use Illuminate\Support\Facades\Password;
use PhpSoft\Users\Models\PasswordReset;

class PasswordControllerTest extends TestCase
{
    public function testCheckAuthChangePassword()
    {
        $res = $this->call('POST', '/password/change', [
            'old_password'          => '1234',
            'password'              => '1234',
            'password_confirmation' => '123'
        ], [], []);
        $this->assertEquals(401, $res->getStatusCode());
    }

    public function testChangePassword()
    {
        // Check authenticate
        $res = $this->call('POST', '/password/change');
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('Token is not provided.', $results->errors[0]->detail);

        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);
        // Input is empty
        $res = $this->call('POST', '/password/change', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The old password field is required.', $results->errors[0]->detail);
        $this->assertEquals('The password field is required.', $results->errors[1]->detail);

        // Check validate input
        $res = $this->call('POST', '/password/change', [
            'old_password'          => '1234',
            'password'              => '1234',
            'password_confirmation' => '123'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The old password must be at least 6 characters.', $results->errors[0]->detail);
        $this->assertEquals('The old password is incorrect.', $results->errors[1]->detail);
        $this->assertEquals('The password confirmation does not match.', $results->errors[2]->detail);
        // Old password is wrong
        $res = $this->call('POST', '/password/change', [
            'old_password'          => '123456789',
            'password'              => '12345678',
            'password_confirmation' => '12345678'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The old password is incorrect.', $results->errors[0]->detail);

        // Change password success
        $res = $this->call('POST', '/password/change', [
            'old_password'          => '123456',
            'password'              => '12345678',
            'password_confirmation' => '12345678'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(204, $res->getStatusCode());
        $checkPassword = Auth::attempt(['id' => 1, 'password' => '12345678']);
        $this->assertTrue($checkPassword);
    }

    public function testAdminResetPassword()
    {
        // Check authenticate
        $res = $this->call('POST', '/password/1/adminReset');
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('Token is not provided.', $results->errors[0]->detail);

        $credentials = [ 'email' => 'admin@example.com', 'password' => '123456' ];
        $token = JWTAuth::attempt($credentials);

        $res = $this->call('POST', '/password/100/adminReset', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(404, $res->getStatusCode());
        $results = json_decode($res->getContent());

        $user = factory(AppUser::class)->create(['email' => 'user@example.com', 'password' => bcrypt('123456')]);

        // Invalid param
        $res = $this->call('POST', '/password/' . $user->id . '/adminReset', [
            'lenght' => 'abc'
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('The lenght must be an integer.', $results->errors[0]->detail);

        // Reset password success with password
        $res = $this->call('POST', '/password/' . $user->id . '/adminReset', [
            'password' => '12345678',
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(204, $res->getStatusCode());
        $result = $res->getData();
        $this->assertEquals('12345678', $result);
        $checkPassword = Auth::attempt(['id' => $user->id, 'password' => '12345678']);
        $this->assertTrue($checkPassword);

        // Reset password with leght success without password
        $res = $this->call('POST', '/password/' . $user->id . '/adminReset', [
            'lenght' => '20',
        ], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(204, $res->getStatusCode());
        $result = $res->getData();
        $this->assertEquals(20, strlen($result));
        $checkPassword = Auth::attempt(['id' => $user->id, 'password' => $result]);
        $this->assertTrue($checkPassword);

        // Reset password success without password and leght
        $res = $this->call('POST', '/password/' . $user->id . '/adminReset', [], [], [], ['HTTP_Authorization' => "Bearer {$token}"]);
        $this->assertEquals(204, $res->getStatusCode());
        $result = $res->getData();
        $this->assertEquals(10, strlen($result));
        $checkPassword = Auth::attempt(['id' => $user->id, 'password' => $result]);
        $this->assertTrue($checkPassword);
    }

    public function testForgotPassword()
    {
        $checkSendMail = $this->call('POST', '/password/forgot/request', [
            'email'=> 'admin@example.com'
        ]);
        $this->assertEquals(204, $checkSendMail->getStatusCode());
        // check email validate
        $checkSendMail = $this->call('POST', '/password/forgot/request', [
            'email'=> 'admin@example'
        ]);
        $this->assertEquals(400, $checkSendMail->getStatusCode());
        $result = json_decode($checkSendMail->getContent());
        $this->assertEquals('The email must be a valid email address.', $result->errors[0]->detail);

        // check user is invalid
        $checkSendMail = $this->call('POST', '/password/forgot/request', [
            'email'=> 'nouser@example.com'
        ]);
        $this->assertEquals(404, $checkSendMail->getStatusCode());
    }

    public function testResetPasswordFailure()
    {
        // check input is empty
        $checkResetPassword = $this->call('PATCH', '/password/forgot/reset');
        $this->assertEquals(400, $checkResetPassword->getStatusCode());
        $result = json_decode($checkResetPassword->getContent());
        $this->assertObjectHasAttribute('errors', $result);
        $this->assertEquals('The token field is required.', $result->errors[0]->detail);
        $this->assertEquals('The new password field is required.', $result->errors[1]->detail);

        // check input incorrect
        $checkResetPassword = $this->call('PATCH', '/password/forgot/reset', [
            'token'         => 'token',
            'new_password'  => '12345678',
        ]);
        $this->assertEquals(404, $checkResetPassword->getStatusCode());
    }

    public function testResetPasswordSuccess()
    {
        $token = PasswordReset::createToken('admin@example.com');

        $checkResetPassword = $this->call('PATCH', '/password/forgot/reset', [
            'token'         => $token->token,
            'new_password'  => '123456',
        ]);
        $this->assertEquals(204, $checkResetPassword->getStatusCode());

        // Token was deleted after used
        $res = $this->call('PATCH', '/password/forgot/reset', [
            'token'         => $token->token,
            'new_password'  => '12345678',
        ]);
        $this->assertEquals(404, $res->getStatusCode());
    }

    public function testExpiredResetToken()
    {
        $token = PasswordReset::createToken('admin@example.com');
        // make token expired
        $token->created_at = date('Y-m-d H:i:s', strtotime($token->created_at) - PasswordReset::EXPIRES_IN - 1);
        $token->saveOrFail();

        // check input incorrect
        $res = $this->call('PATCH', '/password/forgot/reset', [
            'token'         => $token->token,
            'new_password'  => '12345678',
        ]);
        $this->assertEquals(400, $res->getStatusCode());
        $results = json_decode($res->getContent());
        $this->assertObjectHasAttribute('errors', $results);
        $this->assertEquals('Invalid token', $results->errors[0]->detail);
    }
}
