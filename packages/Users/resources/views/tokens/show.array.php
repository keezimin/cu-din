<?php

$this->set('jsonapi', ['version' => '1.0']);

$this->set('data', function ($section) use ($token) {
    $section->set('id', $token);
    $section->set('type', 'Token');
    $section->set('attributes', [
        'accessToken' => $token,
        'tokenType' => 'Bearer',
        'expiresIn' => \JWTAuth::getPayloadFactory()->getTTL()
    ]);
});
