<?php

$this->set('jsonapi', ['version' => '1.0']);
$this->set('meta', $this->helper('helpers.meta-pages', $users));
$this->set('links', $this->helper('helpers.links', $users));
$this->set('data', $this->each($users, function ($section, $user) {

    $section->set($section->partial('phpsoft.users::partials/user', [ 'user' => $user ]));
}));
