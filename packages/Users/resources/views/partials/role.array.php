<?php

$this->extract($role, [
    'name',
    'display_name',
    'description',
]);

$this->set('id', (string)$role->id);
