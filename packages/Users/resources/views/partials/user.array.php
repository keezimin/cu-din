<?php

$this->set('type', 'User');
$this->set('id', $user->id);
$this->set('attributes', function ($session) use ($user) {
    $session->set('id', $user->id);
    $session->extract($user, [
        'fullname',
        'email',
    ]);
    $session->set('roles', $user->getRoles());
});
