<?php

namespace PhpSoft\Users\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class SignupEvent extends Event
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\User $user)
    {
        $this->user = $user;
    }
}
