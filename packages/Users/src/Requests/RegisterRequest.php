<?php

namespace PhpSoft\Users\Requests;

use App\Http\Requests\BaseRequest as Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class RegisterRequest
 * @package PhpSoft\Users\Requests
 */
class RegisterRequest extends Request
{
    protected $forceJsonResponse = true;
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username'  => 'required|string|max:255|unique:users',
            'email'      => 'required|email|max:255|unique:users',
            'password'   => 'required|string|confirmed|min:6'
        ];
    }
}
