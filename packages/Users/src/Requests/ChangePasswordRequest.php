<?php

namespace PhpSoft\Users\Requests;

use App\Http\Requests\BaseRequest as Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class ChangePasswordRequest
 * @package PhpSoft\Users\Requests
 */
class ChangePasswordRequest extends Request
{
    // protected $forceJsonResponse = true;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'old_password' => 'required|min:6|confirm_password:' . \Auth::user()->id,
            'password'     => 'required|confirmed|min:6',
        ];
    }

    public function messages()
    {
        return [
            'old_password.confirm_password' => 'The old password is incorrect.'
        ];
    }
}
