<?php

namespace PhpSoft\Users\Requests;

use App\Http\Requests\BaseRequest as Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Auth\Access\Gate;
use PhpSoft\Users\Models\User;

/**
 * Class UpdateRequest
 * @package PhpSoft\Users\Requests
 */
class UpdateRequest extends Request
{
    protected $forceJsonResponse = true;
    public function authorize()
    {
        $id = $this->route('id');
        $user = $id ? User::findOrFail($id) : \Auth::user();
        return app(Gate::class)->allows('update', $user);
    }

    public function rules()
    {
        return [
            'fullname'      => 'sometimes|required|string|max:255',
            'email'        => 'sometimes|required|email|max:255|unique:users,email,'.request()->route('id'),
        ];
    }
}
