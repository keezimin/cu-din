<?php

namespace PhpSoft\Users\Requests;

use App\Http\Requests\BaseRequest as Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class ForgotPasswordRequest
 * @package PhpSoft\Users\Requests
 */
class ForgotPasswordRequest extends Request
{
    protected $forceJsonResponse = true;
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|max:255',
        ];
    }
}
