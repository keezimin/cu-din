<?php

namespace PhpSoft\Users\Requests;

use App\Http\Requests\BaseRequest as Request;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

/**
 * Class ResetPasswordRequest
 * @package PhpSoft\Users\Requests
 */
class ResetPasswordRequest extends Request
{
    // protected $forceJsonResponse = true;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }
}
