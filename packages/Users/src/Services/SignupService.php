<?php

namespace PhpSoft\Users\Services;

use App\User;
use PhpSoft\Users\Events\SignupEvent;

/**
 *
 */
class SignupService
{
    private function __construct(array $data)
    {
        $this->data = $data;
    }

    public function fireEvents()
    {
        event(new SignupEvent($this->user));
    }

    public function signup()
    {
        $this->user = (new User)->saveUser($this->data);
        return $this->user;
    }

    public static function exec(array $data, $fireEvents = true)
    {
        $service  = new static($data);
        $service->signup();
        if ($fireEvents) {
            $service->fireEvents();
        }
        return $service;
    }
}
