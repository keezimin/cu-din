<?php

namespace PhpSoft\Users\Models;

use Auth;
use PhpSoft\Users\Models\Role;
use App\Models\BaseModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements
AuthorizableContract
{
    use Authorizable, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $appends = ['rolesName'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname',
        'email',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Update the model in the database.
     *
     * @param  array  $attributes
     * @return bool|int
     */
    public function saveUser(array $attributes = [])
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }

        if ($this->id) {
            $this->update($attributes);

            return $this->fresh();
        } else {
            return $this->create($attributes);
        }
    }

    /**
     * get roles user
     * @return array roles
     */
    public function getRoles()
    {
        return $this->roles()->orderBy('role_id')->pluck('name')->toArray();
    }

    /**
     * update roles user
     * @return array roles
     */
    public function updateRole($name_role)
    {
        $role = Role::firstOrCreate(['name' => $name_role]);

        return $this->roles()->sync([$role->id]);
    }

    /**
     * check isOwner
     */
    public function isOwner($object, $field_check = 'user_id')
    {
        if (!empty($object->$field_check)) {
            return $object->$field_check == Auth::user()->id;
        }

        return $object->id == Auth::user()->id;
    }

    public function getRolesNameAttribute()
    {
        return implode(array_pluck($this->roles, 'name'), ' - ');
    }
}
