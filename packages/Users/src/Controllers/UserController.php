<?php

namespace PhpSoft\Users\Controllers;

use Auth;
use App\User;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    public function authenticated()
    {
        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => Auth::user(),
        ]), 200);
    }

    /**
     * View user
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $query = User::with('profile');
        if (Auth::user()->isAdminOrCounselor() || Auth::user()->isDistrictAdmin()) {
            $query->withTrashed();
        }
        // get user by id
        $user = $query->findOrFail($id);

        return response()->json(arrayView('phpsoft.users::user/read', [
            'user' => $user,
        ]), 200);
    }
}
