<?php

namespace PhpSoft\Activity\Tests\Services;

use PhpSoft\Activity\Tests\TestCase;

class ActivityServiceTest extends TestCase
{
    public function testBrowseActivity()
    {
        factory(\PhpSoft\Activity\Models\Activity::class, 15)->create(['verb' => 'save']);
        factory(\PhpSoft\Activity\Models\Activity::class, 15)->create(['verb' => 'apply']);
        $filter = [
            'verb' => 'save'
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(10, $actitivies->count());

        $filter = [
            'verb' => 'save',
            'page' => [
                'number' => 2,
            ]
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(5, $actitivies->count());

        $filter = [
            'verb' => 'save',
            'page' => [
                'number' => 3,
            ]
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(0, $actitivies->count());

        $filter = [
            'verb' => 'save',
            'page' => [
                'size' => 20,
            ]
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(15, $actitivies->count());

        $filter = [
            'verb' => 'save',
            'page' => [
                'size' => 20,
                'number' => 1,
            ]
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(15, $actitivies->count());

        $filter = [
            'verb' => 'save',
            'page' => [
                'size' => 20,
                'number' => 3,
            ]
        ];
        $actitivies = app('activity')->browse($filter);
        $this->assertEquals(0, $actitivies->count());
    }

    public function testAddActivity()
    {
        $user = (object)[
            'id' => 2,
            'email' => 'user@example.com',
        ];
        $scholarship = (object)[
            'id' => 3,
            'name' => 'Name College',
        ];
        $activity = app('activity')->add($user, 'save', $scholarship);
        $this->assertNotNull($activity);
        $activity = app('activity')->read($user, 'save', $scholarship);
        $this->assertNotNull($activity);

        $this->assertGreaterThanOrEqual(1, $activity->id);
        $this->assertEquals((array)$user, $activity->actor);
        $this->assertEquals(2, $activity->actor_id);
        $this->assertEquals('stdClass', $activity->actor_type);
        $this->assertEquals('save', $activity->verb);
        $this->assertEquals((array)$scholarship, $activity->object);
        $this->assertEquals(3, $activity->object_id);
        $this->assertEquals('stdClass', $activity->object_type);
        $this->assertInstanceOf(\DateTime::class, \DateTime::createFromFormat('Y-m-d H:i:s', $activity->published));
        $this->assertInstanceOf(\DateTime::class, \DateTime::createFromFormat('Y-m-d H:i:s', $activity->created_at));
        $this->assertInstanceOf(\DateTime::class, \DateTime::createFromFormat('Y-m-d H:i:s', $activity->updated_at));
        $this->assertNotInstanceOf(\DateTime::class, \DateTime::createFromFormat('Y-m-d H:i:s', $activity->id));
        $this->assertDatabaseHas('activities', ['id' => $activity->id]);
    }

    public function testDeleteActivity()
    {
        $user = (object)[
            'id' => 2,
            'email' => 'user@example.com',
        ];
        $scholarship = (object)[
            'id' => 3,
            'name' => 'Name College',
        ];
        $activity = app('activity')->add($user, 'save', $scholarship);
        $this->assertNotNull($activity);

        app('activity')->delete($user, 'save', $scholarship);

        $deleted = app('activity')->read($user, 'save', $scholarship);
        $this->assertNull($deleted);
        $this->assertDatabaseMissing('activities', ['id' => $activity->id]);
    }

    public function testReAddActivity()
    {
        $user = (object)[
            'id' => 2,
            'email' => 'user@example.com',
        ];
        $scholarship = (object)[
            'id' => 3,
            'name' => 'Name College',
        ];
        app('activity')->add($user, 'save', $scholarship);
        $one = app('activity')->read($user, 'save', $scholarship);
        $this->assertNotNull($one);

        $two = app('activity')->add($user, 'save', $scholarship);
        $this->assertNotNull($two);
        $this->assertNotEquals($one->id, $two->id);
    }

    public function testEditActivity()
    {
        $user = (object)[
            'id' => 2,
            'email' => 'user@example.com',
        ];
        $scholarship = (object)[
            'id' => 3,
            'name' => 'Name College',
        ];
        app('activity')->add($user, 'save', $scholarship);
        $one = app('activity')->read($user, 'save', $scholarship);
        $this->assertNotNull($one);

        $meta = (object)[
            'order' => 10,
        ];
        $two = app('activity')->edit($user, 'save', $scholarship, null, $meta);
        $this->assertNotNull($two);
        $this->assertEquals($one->id, $two->id);
        $this->assertNotEquals($one->toArray(), $two->toArray());
        $this->assertEquals((array)$meta, (array)$two->meta);
    }
}