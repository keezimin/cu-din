<?php

namespace PhpSoft\Activity\Tests;

use Tests\TestCase as Test;

class TestCase extends Test
{
    public function setUp()
    {
        parent::setUp();
        $this->app->singleton(\Illuminate\Database\Eloquent\Factory::class, function ($app){
            return \Illuminate\Database\Eloquent\Factory::construct($app->make(\Faker\Generator::class), base_path() . '/packages/Activity/database/factories');
        });
    }

    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }
}