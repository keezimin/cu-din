<?php

namespace PhpSoft\Activity\Services;

use PhpSoft\Activity\Models\Activity;
use PhpSoft\Activity\Repositories\Contracts\ActivityBreadInterface;

class ActivityService
{
    public $bread;

    public function __construct(ActivityBreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function browse($filter = [])
    {
        $activities = $this->bread->browse($filter);
        return $activities;
    }

    public function read($actor, $verb, $object, $target = null)
    {
        $activity = $this->bread->read(null, [
            'actor_id' => $actor->id,
            'actor_type' => $actor->object_type ?? get_class_name($actor),
            'verb' => $verb,
            'object_id' => $object->id,
            'object_type' => $object->object_type ?? get_class_name($object),
        ]);
        return $activity;
    }

    public function last($actor, $verb, $object, $target = null)
    {
        $activity = $this->bread->last(null, [
            'actor_id' => $actor->id,
            'actor_type' => $actor->object_type ?? get_class_name($actor),
            'verb' => $verb,
            'object_id' => $object->id,
            'object_type' => $object->object_type ?? get_class_name($object),
            'meta->type' => 'request',
        ]);
        return $activity;
    }

    public function add($actor, $verb, $object, $target = null, $meta = null, $status = null)
    {
        $activity = $this->bread->add(compact('actor', 'verb', 'object', 'target', 'meta', 'status'));
        return $activity;
    }

    public function edit($actor, $verb, $object, $target = null, $meta = null)
    {
        $activity = $this->read($actor, $verb, $object);
        $activity = $activity
        ? $this->bread->edit($activity->id, compact('actor', 'verb', 'object', 'target', 'meta', 'status'))
        : $this->bread->add(compact('actor', 'verb', 'object', 'target', 'meta', 'status'));
        return $activity;
    }

    public function update($filter, $data)
    {
        return $this->bread->update($filter, $data);
    }

    // public function editOne($id, $data)
    // {
    //     $activity = $this->bread->read($id);
    //     $activity && $activity = $this->bread->edit($activity->id, $data);
    //     return $activity;
    // }

    public function delete($actor, $verb, $object)
    {
        $activity = $this->read($actor, $verb, $object);
        return $activity && $this->bread->delete($activity->id);
    }
}
