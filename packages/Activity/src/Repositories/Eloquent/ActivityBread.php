<?php
namespace PhpSoft\Activity\Repositories\Eloquent;

use PhpSoft\Activity\Repositories\Contracts\ActivityBreadInterface;

class ActivityBread extends BreadRepository implements ActivityBreadInterface
{
    public function getModel()
    {
        return \PhpSoft\Activity\Models\Activity::class;
    }

    public function add($data)
    {
        $data = $this->parseData($data);
        return $this->getModel()::create($data);
    }

    public function read($id, $filter = [])
    {
        $model = $this->getModel();
        $id && $filter['id'] = $id;
        return $this->getModel()::where($filter)->first();
    }

    public function last($id, $filter = [])
    {
        $model = $this->getModel();
        $id && $filter['id'] = $id;
        return $this->getModel()::where($filter)->orderBy('id', 'desc')->first();
    }

    public function edit($id, $data)
    {
        $data = $this->parseData($data, true);
        $model = $this->getModel();
        $model::where(['id' => $id])->update($data);
        return $model::find($id);
    }

    public function update($filter, $data)
    {
        $model = $this->getModel();
        return $model::where($filter)->update($data);
    }

    public function delete($id)
    {
        $model = $this->getModel();
        return $model::where(['id' => $id])->delete($id);
    }

    private function parseData($data, $isEdit = false)
    {
        extract($data);
        $data = [];
        if ($actor) {
            $data['actor'] = $isEdit ? json_encode($actor, true) : $actor;
            $data['actor_id'] = $actor->id;
            $data['actor_type'] = $actor->object_type ?? get_class_name($actor);
        }
        if ($object) {
            $data['object'] = $isEdit ? json_encode($object, true) : $object;
            $data['object_id'] = $object->id;
            $data['object_type'] = $object->object_type ?? get_class_name($object);
        }
        if ($verb) {
            $data['verb'] = $verb;
        }
        if ($target) {
            $data['target'] = $isEdit ? json_encode($target, true) : $target;
            $data['target_id'] = $target->id;
            $data['target_type'] = $target->target_type ?? get_class_name($target);
        } else {
            $data['target'] = \DB::raw('JSON_OBJECT()');
        }
        if ($meta) {
            $data['meta'] = $isEdit ? json_encode($meta, true) : $meta;
        } else {
            $data['meta'] = \DB::raw('JSON_OBJECT()');
        }
        if (isset($status)) {
            $data['status'] = $status;
        }

        return $data;
    }
}
