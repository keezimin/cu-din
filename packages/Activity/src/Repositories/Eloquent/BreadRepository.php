<?php
namespace PhpSoft\Activity\Repositories\Eloquent;

use PhpSoft\Activity\Repositories\Contracts\BreadInterface;

abstract class BreadRepository implements BreadInterface
{
    public function browse($filter = [])
    {
        request()->merge($filter);
        $filter = request()->input();

        $size = request()->input('page.size', 10);
        $number = request()->input('page.number', 1);
        $sort = explode(',', request()->input('sort', '-id'));

        $model = $this->getModel();
        $query = $model::queryOrders($sort)->filterFields($filter);
        return $query->paginate($size, ['*'], 'page[number]', $number);
    }
}
