<?php
namespace PhpSoft\Activity\Repositories\Contracts;

interface BreadInterface
{
    public function browse($filter = []);
    public function read($id, $filter = []);
    public function edit($id, $data);
    public function add($data);
    public function delete($id);
    public function getModel();
}
