<?php

namespace PhpSoft\Activity;

use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     * Boot the service provider.
     */
    public function boot()
    {
        // Publish migrations
        $this->publishes([
          __DIR__.'/../database/migrations' => base_path('database/migrations'),
        ], 'migrations');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'PhpSoft\Activity\Repositories\Contracts\ActivityBreadInterface',
            'PhpSoft\Activity\Repositories\Eloquent\ActivityBread'
        );

        $this->app->singleton('activity', function ($app) {
            return $this->app->make(Services\ActivityService::class);
        });
    }
}
