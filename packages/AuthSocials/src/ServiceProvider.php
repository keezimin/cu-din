<?php

namespace PhpSoft\AuthSocials;

use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     * Boot the service provider.
     */
    public function boot()
    {
        // Publish migrations
        $this->publishes([
          __DIR__.'/../database/migrations' => base_path('database/migrations'),
        ], 'migrations');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('authSocial', function ($app) {
            return new Services\AuthService;
        });
    }
}
