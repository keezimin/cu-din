<?php

namespace PhpSoft\AuthSocials\Traits;

use PhpSoft\AuthSocials\Models\Social;

trait SocialsTrait
{
    public function socials()
    {
        return $this->hasMany(\PhpSoft\AuthSocials\Models\Social::class);
    }
}
