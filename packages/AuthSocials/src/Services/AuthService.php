<?php

namespace PhpSoft\AuthSocials\Services;

use PhpSoft\AuthSocials\Models\Social;

class AuthService
{
    public function getProfile($provider, $token)
    {
        try {
            return \Socialite::driver($provider)->userFromToken($token);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($provider !== 'google') {
                throw new \Illuminate\Validation\UnauthorizedException('Fail authentication');
            }

            try {
                // we try get profile from id_token
                $client = new \GuzzleHttp\Client();

                $tokenUrl = 'https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=' . $token;
                $res = $client->request('GET', $tokenUrl);
                $tokenInfo = json_decode($res->getBody(), true);

                // verify the token come from our app
                if (array_get($tokenInfo, 'audience') !== env('GOOGLE_CLIENT_ID')) {
                    throw new \Exception('Invalid Google App');
                }

                $profileUrl = 'https://www.googleapis.com/plus/v1/people/' . array_get($tokenInfo, 'user_id') . '?key=' . env('GOOGLE_API_KEY');
                $res = $client->request('GET', $profileUrl);
                $profile = json_decode($res->getBody(), true);

                return (new \Laravel\Socialite\Two\User)->setRaw($profile)->map([
                    'id' => $profile['id'], 'nickname' => array_get($profile, 'nickname'), 'name' => $profile['displayName'],
                    'email' => $tokenInfo['email'], 'avatar' => array_get($profile, 'image')['url'],
                    'avatar_original' => preg_replace('/\?sz=([0-9]+)/', '', array_get($profile, 'image')['url']),
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                throw new \Illuminate\Validation\UnauthorizedException('Fail authentication');
            } catch (\Exception $e) {
                throw new \Illuminate\Validation\UnauthorizedException($e->getMessage());
            }
        } catch (\Exception $e) {
            throw new \Illuminate\Validation\UnauthorizedException($e->getMessage());
        }
    }

    public function getSocial($provider, $id)
    {
        return Social::firstOrNew([
            'social_name' => $provider,
            'social_id' => $id,
        ]);
    }
}
