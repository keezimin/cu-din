<?php

namespace PhpSoft\AuthSocials\Facades;

use Illuminate\Support\Facades\Facade;

class AuthSocial extends Facade
{
    /**
    * Get the registered name of the component.
    *
    * @return string
    */
    protected static function getFacadeAccessor()
    {
        return 'authSocial';
    }
}
