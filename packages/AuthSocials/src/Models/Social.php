<?php

namespace PhpSoft\AuthSocials\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public $fillable = ['social_name', 'social_id'];
}
