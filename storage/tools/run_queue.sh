#!/bin/sh
#
# Pham Cong Toan <toancong1920@gmail.com>
#
# Run queue by supervisor, install supervisor is required
#

# config
PROJECT_HOME=/var/www/app

ln -fs $PROJECT_HOME/storage/tools/app_worker.conf /etc/supervisor/conf.d/app_worker.conf

* * * * * php /var/www/app/artisan schedule:run >> /tmp/debug.log 2>&1
# update and start
supervisorctl reread
supervisorctl update
supervisorctl stop app-worker:*
supervisorctl start app-worker:*
