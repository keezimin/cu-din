<?php

namespace Tests\Feature;

use Tests\TestCase;

class DistrictDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $district = factory(\App\Models\District::class)->create();
        $res = $this->deleteJson('/districts/' . $district->id);
        $res->assertStatus(401);
    }

    public function testNotFoundDistrict()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/districts/122', [], $headers);
        $res->assertStatus(404);
    }

    public function testNotAcceptDelete()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $district = factory(\App\Models\District::class)->create();
        $school = factory(\App\Models\School::class)->create([
            'name' => 'example.school',
            'district_id' => $district->id,
        ]);
        $res = $this->deleteJson('/districts/' . $district->id, [], $headers);
        $res->assertStatus(403);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $district = factory(\App\Models\District::class)->create();
        $res = $this->deleteJson('/districts/' . $district->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('districts', ['id' => $district->id]);
    }
}
