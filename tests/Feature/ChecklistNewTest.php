<?php

namespace Tests\Feature;

use Tests\TestCase;

class ChecklistNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/checklists', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/checklists', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateWithDictrictNotFound()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
        ];
        $res = $this->postJson('/checklists', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $this->assertDatabaseHas('checklists', $data);
    }

    public function testCreateSuccess()
    {
        $user = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'object_id' => $scholarship->id,
            'type' => 'scholarships',
        ];
        $res = $this->postJson('/checklists', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $data['owner_id'] = $user->id;
        unset($data['ids']);
        $this->assertDatabaseHas('checklists', $data);
    }

    public function testRelationship()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });
        $res = $this->postJson('/checklists', [
            'name' => 'example.job',
            'ids' => $ids,
        ], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'owner' => [
                        'id' => $user->id,
                        'type' => 'User',
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'User',
                    'id' => $user->id,
                ],
            ],
        ]);
    }
}
