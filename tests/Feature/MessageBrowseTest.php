<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;

/**
 * @group Message
 */
class MessageBrowseTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->getJson('/messages', []);
        $res->assertStatus(401);
    }

    public function testGetListEmpty()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 2)->create();
        $res = $this->getJson('/messages', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => []
        ]);
    }

    public function testGetList()
    {
        Queue::fake();
        $bread = app()->make('\App\Repositories\Eloquent\MessageBread');;

        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $users = factory(\App\User::class, 2)->create();
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];
        $messageIds[] = $bread->add($data, $user)->id;

        factory(\App\User::class, 4)->create();
        $data = ['target' => ['type' => 'USER_ALL'], 'content' => 'hi'];
        $messageIds[] = $bread->add($data, $user)->id;

        factory(\App\User::class, 4);
        factory(\App\User::class, 4)->create(['school_id' => $user->school_id]);
        $data = ['target' => ['type' => 'SCHOOL', 'id' => [$user->school_id]], 'content' => 'hi'];
        $messageIds[] = $bread->add($data, $user)->id;

        factory(\App\User::class, 4);
        $districts = factory(\App\Models\District::class, 3)->create()->each(function ($d) {
            factory(\App\Models\School::class, 3)->create(['district_id' => $d->id])->each(function ($s) {
                factory(\App\User::class, 4)->create(['school_id' => $s->id]);
            });
        });
        $data = ['target' => ['type' => 'DISTRICT', 'id' => [$districts[0]->id]], 'content' => 'hi'];
        $messageIds[] = $bread->add($data, $user)->id;

        $res = $this->getJson('/messages', $headers);
        $res->assertStatus(200);

        $res->assertJson([
            'meta' => [
                'totalObjects' => 4,
            ],
            'data' => [
                ['id' => $messageIds[3]],
                ['id' => $messageIds[2]],
                ['id' => $messageIds[1]],
                ['id' => $messageIds[0]],
            ]
        ]);
    }
}
