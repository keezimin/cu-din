<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PathwayDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailResourceNotfoundId()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('/pathways/2', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testDetailResourceWithoutToken()
    {
        $pathway = factory(\App\Models\Pathway::class)->create();
        $res = $this->getJson('/pathways/'. $pathway->id, []);
        $res->assertStatus(401);
    }

    public function testDetailWithUserNotActive()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $user->businessProfile()->updateOrCreate(['user_id' => $user->id], ['active' => false]);

        $careerCluster = factory(\App\Models\CareerCluster::class)->create();
        $credential = [
            'career_cluster_id' => $careerCluster->id,
            'name' => 'example name',
            'short_description' => 'example description',
            'user_id' => $user->id,
            'active' => 1,
            'about' => [
                [
                    'type' => 'image',
                    'content' => 'content'
                ]
            ]
        ];

        $res = $this->postJson('/pathways', $credential, $headers);
        $res->assertStatus(201);

        $id = self::getId($res);
        $res = $this->getJson('/pathways/'. $id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(403);
    }

    public function testDetailResourceSuccess()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $user->businessProfile()->updateOrCreate(['user_id' => $user->id], ['active' => true]);

        $careerCluster = factory(\App\Models\CareerCluster::class)->create();
        $credential = [
            'career_cluster_id' => $careerCluster->id,
            'name' => 'example name',
            'short_description' => 'example description',
            'user_id' => $user->id,
            'active' => 1,
            'about' => [
                [
                    'type' => 'image',
                    'content' => 'content'
                ]
            ]
        ];
        $res = $this->postJson('/pathways', $credential, $headers);
        $res->assertStatus(201);

        $id = self::getId($res);
        $res = $this->getJson('/pathways/'. $id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Pathway',
                'id' => '1',
                'attributes' => $credential
            ],
        ]);
    }
}
