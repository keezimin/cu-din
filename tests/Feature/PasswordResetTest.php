<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class PasswordResetTest extends TestCase
{
    use SendsPasswordResetEmails;

    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest() {
        $res = $this->postJson('/password/forgot/request', []);
        $res->assertStatus(400);
    }

    public function testResetToken() {
        $users = factory(\App\User::class,1)->create([
            'email' => 'admin@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        Mail::shouldReceive('send')->once();
        \Mockery::mock('overload:\PhpSoft\DeepLinking\GetLink')->shouldReceive('get')->andReturn('http://url');
        $res = $this->postJson('/password/forgot/request', ['email'=>'admin@example.com']);
        $res->assertStatus(204);
    }

    public function testInvalidateReset() {
        $users = factory(\App\User::class)->create([
            'email' => 'admin@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $res = $this->postJson('/password/forgot/reset', ['email'=>'admin@example.com', 'password' => '1234567', 'token' => 'abc']);
        $res->assertStatus(404);
    }

    public function testResetPasswordSuccsess() {
        $users = factory(\App\User::class)->create([
            'email' => 'admin@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $token = $this->broker()->createToken($users);
        $res = $this->postJson('/password/forgot/reset', ['email'=>'admin@example.com', 'password' => '1234567','token' => $token]);
        $res->assertStatus(204);
    }
}
