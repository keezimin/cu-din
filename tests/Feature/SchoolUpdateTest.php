<?php

namespace Tests\Feature;

use JWTAuth;
use Tests\TestCase;

class SchoolUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $school = factory(\App\Models\School::class)->create();
        $res = $this->patchJson('/schools/' . $school->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateFail()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->patchJson('/schools/122', ['name' => 'example.school'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $school = factory(\App\Models\School::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/schools/' . $school->id, ['name' => 'example.school', 'district_id' => $district->id, 'code' => 'example.code'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('schools', ['id' => $school->id, 'name' => 'example.school', 'code' => 'example.code', 'district_id' => $district->id]);
    }

    public function testUpdateWithId()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $school = factory(\App\Models\School::class)->create();
        $res = $this->patchJson('/schools/' . $school->id, ['id' => '123', 'name' => 'example.school'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('schools', ['id' => $school->id, 'name' => 'example.school']);
    }

    public function testDetailWithoutToken()
    {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->patchJson('/schools/' . $school->id, []);
        $res->assertStatus(401);
    }

    public function testRelationship()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $school = factory(\App\Models\School::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/schools/' . $school->id, ['id' => '123', 'name' => 'example.school', 'district_id' => $district->id], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'district' => [
                        'id' => $district->id,
                        'type' => 'District',
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'District',
                    'id' => $district->id,
                ],
            ],
        ]);
    }
}
