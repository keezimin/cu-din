<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\User;
use \PhpSoft\Activity\Models\Activity;
use \App\Models\Scholarship;

class ScholarshipDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailSuccess()
    {
        $token = $this->getToken();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->getJson('/scholarships/'. $scholarship->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
    }

    public function testNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/scholarships/122', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testCreateWithoutToken()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->getJson('/scholarships/'. $scholarship->id, []);
        $res->assertStatus(401);
    }

    public function testScholarshipWithActivitiesAsStudent()
    {
        $owner = factory(User::class)->create();
        $scholarship = factory(Scholarship::class)->create(['user_id' => $owner->id]);
        factory(Scholarship::class)->create(['user_id' => $owner->id]);
        // counselor save
        $ownerAct = factory(Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $owner->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);

        // student save and apply
        $user = factory(User::class)->create();
        $saveAct = factory(Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);
        $applyAct = factory(Activity::class)->create(['verb' => 'apply', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);

        //request as student
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $res = $this->getJson('/scholarships/' . $scholarship->id, $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'user' => [
                        'id' => $owner->id,
                        'type' => 'User'
                    ],
                    'activities' => [
                        [
                            'id' => $saveAct->id,
                            'type' => 'Activity'
                        ],
                        [
                            'id' => $applyAct->id,
                            'type' => 'Activity'
                        ]
                    ],
                ]
            ],
            'included' => [
                [
                    'id' => $owner->id,
                    'type' => 'User'
                ],
                [
                    'id' => $saveAct->id,
                    'type' => 'Activity'
                ],
                [
                    'id' => $applyAct->id,
                    'type' => 'Activity'
                ]
            ]
        ]);
    }
}
