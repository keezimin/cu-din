<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListSettingSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->getJson('/settings', $headers);
        $res->assertStatus(200);
    }

    public function testListSettingWithPagination()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $res = $this->getJson('/settings?page[number]=1&page[size]=3', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 3);

        $res = $this->getJson('/settings?page[number]=1&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/settings?page[number]=2&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/settings?page[number]=3&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);
    }

}
