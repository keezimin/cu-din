<?php

namespace Tests\Feature;

use Tests\TestCase;

class OrganizationMultiDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->deleteJson('/organizations', ['ids' => [$organization->id]]);
        $res->assertStatus(401);
    }

    public function testNotFoundOrganization()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/organizations', ['ids' => ['122']], $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Organization. Organization is not found',
                        'meta' => [
                            'id' => '122',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $organization1 = factory(\App\Models\Organization::class)->create();
        $organization2 = factory(\App\Models\Organization::class)->create();
        $res = $this->deleteJson('/organizations/', ['ids' => [$organization1->id, $organization2->id]], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('organizations', ['id' => [$organization1->id, $organization2->id]]);
    }

    public function testMultiWithNotAccept()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $organization1 = factory(\App\Models\Organization::class)->create();
        $organization2 = factory(\App\Models\Organization::class)->create();
        $school = factory(\App\Models\School::class)->create([
            'name' => 'example.school',
            'organization_id' => $organization1->id,
        ]);
        $res = $this->deleteJson('/organizations/', ['ids' => [$organization1->id, $organization2->id]], $headers);
        $res->assertStatus(400);
        $this->assertDatabaseMissing('organizations', ['id' => $organization2->id]);
        $this->assertDatabaseHas('schools', ['id' => $school->id, 'organization_id' => $organization1->id]);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Organization. Organization has linked with schools',
                        'meta' => [
                            'id' => $organization1->id,
                        ],
                    ],
                ],
            ],
        ]);
    }
}
