<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\BusinessProfile;
use App\Models\Profile;

class BusinessListByPathwayTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListBusinessByPathwayWithOutCareerCluster()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user1 = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();
        $career = factory(\App\Models\CareerCluster::class)->create();
        $pathway = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $career->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user1->id,
            'active' => 1
        ]);
        BusinessProfile::create(['user_id' => $user1->id, 'active' => true]);
        BusinessProfile::create(['user_id' => $user2->id, 'active' => true]);
        Profile::create(['user_id' => $user1->id]);
        Profile::create(['user_id' => $user2->id]);
        $res = $this->getJson('/businesses/pathways', $headers);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);
        $res->assertStatus(200);
    }

    public function testListBusinessByPathwayWithNotActive()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = factory(\App\User::class)->create();
        $pathway = factory(\App\Models\Pathway::class)->create();
        BusinessProfile::create(['user_id' => $user->id, 'active' => false]);
        Profile::create(['user_id' => $user->id]);
        $res = $this->getJson('/businesses/pathways', $headers);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
        $res->assertStatus(200);
    }

    public function testListBusinessByPathway()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $caree1 = factory(\App\Models\CareerCluster::class)->create();
        $caree2 = factory(\App\Models\CareerCluster::class)->create();
        $user1 = factory(\App\User::class)->create();
        BusinessProfile::create(['user_id' => $user1->id, 'active' => true]);
        Profile::create(['user_id' => $user1->id, 'contact_person' => 'u contact']);
        $user2 = factory(\App\User::class)->create();
        BusinessProfile::create(['user_id' => $user2->id, 'active' => true]);
        Profile::create(['user_id' => $user2->id, 'contact_person' => 'b contact']);
        $user3 = factory(\App\User::class)->create();
        BusinessProfile::create(['user_id' => $user3->id,'active' => false]);
        Profile::create(['user_id' => $user3->id, 'contact_person' => 'a contact']);
        $user4 = factory(\App\User::class)->create();
        BusinessProfile::create(['user_id' => $user4->id,'active' => true]);
        Profile::create(['user_id' => $user4->id, 'contact_person' => 'd contact']);
        $user5 = factory(\App\User::class)->create();
        BusinessProfile::create(['user_id' => $user5->id,'active' => true]);
        Profile::create(['user_id' => $user5->id, 'contact_person' => 'c contact']);
        $pathway1 = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $caree1->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user1->id,
            'active' => 1
        ]);
        $pathway2 = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $caree2->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user2->id,
            'active' => 1
        ]);
        $pathway3 = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $caree2->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user3->id,
            'active' => 1
        ]);
        $pathway4 = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $caree2->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user4->id,
            'active' => 1
        ]);
        $pathway5 = factory(\App\Models\Pathway::class)->create([
            'career_cluster_id' => $caree2->id,
            'name' => 'name',
            'short_description' => 'short_description',
            'user_id' => $user5->id,
            'active' => 1
        ]);
        $res = $this->getJson('/businesses/pathways?career_cluster_id='. $caree2->id , $headers);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals((int)$user2->id, (int)$responseArray->data[0]->id);
        $this->assertEquals((int)$user5->id, (int)$responseArray->data[1]->id);
        $this->assertEquals((int)$user4->id, (int)$responseArray->data[2]->id);
        $this->assertEquals(count($responseArray->data), 3);
        $res->assertStatus(200);
    }
}
