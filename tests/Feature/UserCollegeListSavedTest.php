<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserCollegeListSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/' . $user->id . '/colleges');
        $res->assertStatus(401);
    }

    public function testReadSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $res = $this->getJson('/users/' . $user->id . '/colleges', $headers);
        $res->assertStatus(200);
        $this->assertEquals(0, count(json_decode($res->getContent(), true)['data']));
    }

    public function testReadSuccessData()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        factory(\PhpSoft\Activity\Models\Activity::class, 15)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'College']);

        $res = $this->getJson('/users/' . $user->id . '/colleges', $headers);
        $res->assertStatus(200);
        $this->assertEquals(10, count(json_decode($res->getContent(), true)['data']));
    }
}
