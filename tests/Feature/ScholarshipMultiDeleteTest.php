<?php

namespace Tests\Feature;

use Tests\TestCase;

class ScholarshipMultiDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->deleteJson('/scholarships', ['id' => [$scholarship->id]]);
        $res->assertStatus(401);
    }

    public function testNotFoundResource()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        $res = $this->deleteJson('/scholarships', ['id' => [$scholarship->id, 2]], $headers);
        $res->assertStatus(200);
        $this->assertDatabaseMissing('scholarships', ['id' => [$scholarship->id]]);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Scholarship. Scholarship is not found',
                        'meta' => [
                            'id' => '2',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testMultiDeleteSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);

        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship1 = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        $scholarship2 = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        $res = $this->deleteJson('/scholarships', ['id' => [$scholarship1->id, $scholarship2->id]], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('scholarships', ['id' => [$scholarship1->id, $scholarship2->id]]);
    }
}
