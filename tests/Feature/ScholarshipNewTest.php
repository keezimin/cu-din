<?php

namespace Tests\Feature;

use Tests\TestCase;

class ScholarshipNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/scholarships', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/scholarships', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateSuccess()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $state = factory(\App\Models\State::class)->create();
        $data = [
            'name'                      => 'example.scholarship',
            'organization'              => 'example.organization',
            'application_form_file'     => 'example.application_form_file',
            'application_form_filename' => 'name',
            'application_form_link'     => 'example.application_form_link',
            'state_id'                  => $state->id,
        ];
        $res = $this->postJson('/scholarships', array_merge($data, ['school_ids' => [$user->school_id], 'district_ids' => [1]]),
            ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $data['user_id'] = $user->id;
        $this->assertDatabaseHas('scholarships', $data);
    }

    public function testCreateSchoolIdNull()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);
        $res = $this->postJson('/scholarships', ['school_id' => 1], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutSchoolId()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $school = factory(\App\Models\School::class)->create();

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);
        $res = $this->postJson('/scholarships', ['name' => 'example.scholarship', 'min_price' => 3, 'school_id' => $school->id], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'detail' => 'The school ids field is required.',
                ],
                [
                    'title' => 'Validation Error',
                    'detail' => 'The district ids field is required.',
                ],
                [
                    'title' => 'Validation Error',
                    'detail' => 'The state id field is required.',
                ],
            ],
        ]);
    }

    public function testRelationship()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $state = factory(\App\Models\State::class)->create();
        $data = [
            'name'                      => 'example.scholarship',
            'organization'              => 'example.organization',
            'application_form_file'     => 'example.application_form_file',
            'application_form_filename' => 'name',
            'application_form_link'     => 'example.application_form_link',
            'state_id'                  => $state->id,
        ];
        $res = $this->postJson('/scholarships', array_merge($data, ['school_ids' => [$user->school_id], 'district_ids' => [1]]),
            ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'user' => [
                        'id' => $user->id,
                        'type' => 'User',
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'User',
                    'id' => $user->id,
                ],
            ],
        ]);
    }
}
