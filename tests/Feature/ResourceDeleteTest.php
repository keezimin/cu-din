<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->deleteJson('/resources/'. $resource->id);
        $res->assertStatus(401);
    }

    public function testNotFoundResource()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $res = $this->deleteJson('/resources/1', [], $headers);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->deleteJson('/resources/'. $resource->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('resources', ['id' => $resource->id]);
    }
}
