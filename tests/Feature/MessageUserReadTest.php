<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;

/**
 * @group Message
 */
class MessageUserReadTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = $this->getUser();
        $res = $this->getJson('/users/' . $user->id .'/messages', []);
        $res->assertStatus(401);
    }

    public function testNotFound()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->getJson('/users/5/messages', $headers);
        $res->assertStatus(404);
    }

    public function testGetListEmpty()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 2)->create();
        $res = $this->getJson('/users/' . $user->id .'/messages', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => []
        ]);
    }

    public function testReadSuccess()
    {
        Queue::fake();
        $bread = app()->make('\App\Repositories\Eloquent\MessageBread');;

        $sender = $this->getUser();
        $token = $this->fromUser($sender);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $users = factory(\App\User::class, 2)->create();
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];
        $message = $bread->add($data, $sender);
        $messages[] = $message;
        $messageIds[] = $message->id;

        $data = ['target' => ['type' => 'USER_ALL'], 'content' => 'hi'];
        $message = $bread->add($data, $sender);
        $messages[] = $message;
        $messageIds[] = $message->id;

        factory(\App\User::class, 4);
        $districtsUsers = [];
        $districts = factory(\App\Models\District::class, 3)->create()->each(function ($d) use (&$districtsUsers) {
            factory(\App\Models\School::class, 3)->create(['district_id' => $d->id])->each(function ($s) use (&$districtsUsers) {
                $districtsUsers = array_merge($districtsUsers, factory(\App\User::class, 4)->create(['school_id' => $s->id])->all());
            });
        });
        $data = ['target' => ['type' => 'DISTRICT', 'id' => [$districts[0]->id]], 'content' => 'hi'];
        $message = $bread->add($data, $sender);
        $messages[] = $message;
        $messageIds[] = $message->id;
        // factory(\App\User::class, 4);
        // factory(\App\User::class, 4)->create(['school_id' => $user->school_id]);
        // $data = ['target' => ['type' => 'SCHOOL', 'id' => [$user->school_id]], 'content' => 'hi'];
        // $message = $bread->add($data, $sender);
        // $messages[] = $message;
        // $messageIds[] = $message->id;

        $res = $this->getJson('/users/' . $sender->id .'/messages', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'totalObjects' => 0,
            ],
            'data' => []
        ]);

        $res = $this->getJson('/users/' . array_get($messages[0], 'meta.target.id.0') .'/messages', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'totalObjects' => 2,
            ],
            'data' => [
                [
                    'relationships' => [
                        'sender' => ['id' => $sender->id, 'type' => 'User'],
                        'message' => ['id' => $messages[1]->id, 'type' => 'Message'],
                    ]
                ],
                [
                    'relationships' => [
                        'sender' => ['id' => $sender->id, 'type' => 'User'],
                        'message' => ['id' => $messages[0]->id, 'type' => 'Message'],
                    ]
                ],
            ],
            'included' => [
                ['id' => $sender->id, 'type' => 'User'],
                ['id' => $users[0]->id, 'type' => 'User'],
                ['id' => $users[1]->id, 'type' => 'User'],
                ['id' => $messages[1]->id, 'type' => 'Message'],
                ['id' => $messages[0]->id, 'type' => 'Message'],
            ]
        ]);
        $res->assertJsonMissing([
            'data' => [
                ['id' => $messages[0]->id, 'type' => 'Message'],
            ]
        ]);
        $res->assertJsonMissing([
            'data' => [
                ['id' => $messages[1]->id, 'type' => 'Message'],
            ]
        ]);

        $res = $this->getJson('/users/' . $districtsUsers[0]->id .'/messages', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'totalObjects' => 1,
            ],
            'data' => [
                [
                    'relationships' => [
                        'sender' => ['id' => $sender->id, 'type' => 'User'],
                        'message' => ['id' => $messages[2]->id, 'type' => 'Message'],
                    ]
                ],
            ],
            'included' => [
                ['id' => $sender->id, 'type' => 'User'],
                ['id' => $messages[2]->id, 'type' => 'Message'],
            ]
        ]);
        $res->assertJsonMissing([
            'data' => [
                ['id' => $messages[0]->id, 'type' => 'Message'],
            ]
        ]);
        $res->assertJsonMissing([
            'data' => [
                ['id' => $messages[1]->id, 'type' => 'Message'],
            ]
        ]);

        $res = $this->getJson('/users/' . array_get($messages[0], 'meta.target.id.0') .'/messages/statistics', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'count' => [
                    'unread_messages' => 2
                ]
            ],
        ]);

        $res = $this->postJson('/users/' . array_get($messages[0], 'meta.target.id.0') .'/messages/read', ['id' => $messages[1]->id + 1], $headers);
        $res = $this->getJson('/users/' . array_get($messages[0], 'meta.target.id.0') .'/messages/statistics', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'count' => [
                    'unread_messages' => 1
                ]
            ],
        ]);
    }
}
