<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListOrganizationSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->getJson('/organizations', $headers);
        $res->assertStatus(200);
    }

    public function testListOrganizationWithPagination()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        factory(\App\Models\Organization::class)->create([
            'name' => 'example.organization 1'
        ]);
        factory(\App\Models\Organization::class)->create([
            'name' => 'example.organization 2'
        ]);

        $res = $this->getJson('/organizations?page[number]=1&page[size]=2', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/organizations?page[number]=1&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/organizations?page[number]=2&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/organizations?page[number]=3&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

}
