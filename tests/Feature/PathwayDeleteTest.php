<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PathwayDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $resource = factory(\App\Models\Pathway::class)->create();
        $res = $this->deleteJson('/pathways',
            [
                'ids' => [2],
                'user_id' => $resource->user_id
            ]);
        $res->assertStatus(401);
    }

    public function testNotFoundPathway()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $resource = factory(\App\Models\Pathway::class)->create();
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $res = $this->deleteJson('/pathways',
            [
                'ids' => [2],
                'user_id' => $resource->user_id
            ],
            $headers
        );
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $user->businessProfile()->updateOrCreate(['user_id' => $user->id], ['active' => true]);

        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $resource = factory(\App\Models\Pathway::class)->create();
        $res = $this->deleteJson('/pathways',
            [
                'ids' => [$resource ->id],
                'user_id' => $resource->user_id
            ],
            $headers
        );
        $res->assertStatus(204);
        $this->assertDatabaseMissing('pathways', ['id' => $resource->id]);
    }
}
