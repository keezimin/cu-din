<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserCollegeDeleteSave extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->deleteJson('/users/1/colleges/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/colleges/2');
        $res->assertStatus(401);
    }

    public function testNotFoundCollege()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $res = $this->deleteJson('/users/' . $user->id . '/colleges/2', [], $headers);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $college = factory(\App\Models\College::class)->create();
        $activity = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'College', 'object_id' => $college->id]);

        $res = $this->deleteJson('/users/' . $user->id . '/colleges/' . $college->code, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('activities', ['id' => $activity->id]);
    }
}
