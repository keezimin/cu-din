<?php

namespace Tests\Feature;

use Tests\TestCase;

class BusinessInvitedCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testAuthorization()
    {
        $counselor = $this->getUser('counselor');
        $data = [
            'email' => 'owner@example.com',
            'password' => 'password',
            'first_name' => 'first name',
            'last_name' => 'last name',
            'contact_person' => 'contact person'
        ];
        // case email not in list email invited
        $res = $this->postJson('/businesses/create-invited', $data, []);
        $res->assertStatus(403);

        // case user invite not business
        $invitation = factory(\App\Models\Invitation::class)->create(['email' => 'owner@example.com', 'user_id' => $counselor->id]);
        $res = $this->postJson('/businesses/create-invited', $data, []);
        $res->assertStatus(403);
    }

    public function testInvalidateRequest()
    {
        $res = $this->postJson('/businesses/create-invited', ['email' => 'owner@example.com'], []);
        $res->assertStatus(400);
    }

    public function testCreateSuccess()
    {
        $business = $this->getUser('business_owner');
        $invitation = factory(\App\Models\Invitation::class)->create(
            [
                'email' => 'owner@example.com',
                'user_id' => $business->id
            ]
        );
        $data = [
            'email' => 'owner@example.com',
            'password' => 'password',
            'first_name' => 'first name',
            'last_name' => 'last name',
            'contact_person' => 'contact person'
        ];

        $res = $this->postJson('/businesses/create-invited', $data, []);
        $res->assertStatus(201);

        $user = \App\User::where(['email' => $data['email']])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertTrue($user->hasRole('business_owner'));
    }
}
