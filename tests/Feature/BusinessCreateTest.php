<?php

namespace Tests\Feature;

use Tests\TestCase;

class BusinessCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/businesses', []);
        $res->assertStatus(401);
    }

    public function testAuthorization()
    {
        $counselor = $this->getUser('counselor');
        $token = $this->fromUser($counselor);
        $res = $this->postJson('/businesses', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
        $district = $this->getUser('district_admin');
        $token = $this->fromUser($district);
        $res = $this->postJson('/businesses', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/businesses', ['email' => 'owner@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user_info = [
            'email' => 'owner@example.com',
            'password' => '123456',
        ];
        $credential = $user_info + [
            'first_name' => 'Name',
            'last_name' => 'Name',
            'contact_person' => 'Contact person'
        ];
        $res = $this->postJson('/businesses', $credential, $headers);
        $res->assertStatus(201);
        $user = \App\User::where(['email' => $credential['email']])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertTrue($user->hasRole('business_owner'));
        $this->assertTrue(\JWTAuth::attempt($user_info) != false);
    }
}
