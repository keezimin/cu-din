<?php

namespace Tests\Feature;

use JWTAuth;
use Tests\TestCase;

class OrganizationNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/organizations', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/organizations', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateSuccess()
    {
        $user = $this->getUser('super_admin');
        $districtAdmin = $this->getUser('district_admin');
        $token = $this->fromUser($user);
        $district = factory(\App\Models\District::class)->create();
        $data = [
            'name' => 'example',
            'district_id' => $district->id,
            'user_id' => $districtAdmin->id
        ];
        $res = $this->postJson('/organizations', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $data['updated_by'] = $user->id;
        $this->assertDatabaseHas('organizations', $data);
    }
}
