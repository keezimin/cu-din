<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserScholarshipApplyTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->postJson('/users/1/scholarships/2/applying');
        $res->assertStatus(404);
    }

    public function testNotFoundScholarship()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/scholarships/2/applying');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id . '/applying');
        $res->assertStatus(401);
    }

    public function testPostSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $user = \JWTAuth::toUser($token);

        \Carbon\Carbon::setTestNow(\Carbon\Carbon::now());
        #1
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id . '/applying', [], $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'apply',
                    'object' => [
                        'id' => $scholarship->id
                    ],
                    'object_id' => $scholarship->id,
                    'object_type' => 'Scholarship',
                    'meta' => [
                        'status' => 'applying',
                        'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
                    ],
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $scholarship->id,
            'object_type' => 'Scholarship',
            'verb' => 'apply',
        ]);

        #2
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id . '/applying', [
            'verb' => 'something',
            'meta' => [
                'test_on' => (new \Carbon\Carbon())->toIso8601String()
            ]
        ], $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'apply',
                    'object' => [
                        'id' => $scholarship->id
                    ],
                    'object_id' => $scholarship->id,
                    'object_type' => 'Scholarship',
                    'meta' => [
                        'status' => 'completed',
                        'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
                        'completed_at' => \Carbon\Carbon::now()->toIso8601String(),
                    ],
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $scholarship->id,
            'object_type' => 'Scholarship',
            'verb' => 'apply',
        ]);

        #3
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id . '/applying', [], $headers);
        $res->assertStatus(200);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'apply',
                    'object' => [
                        'id' => $scholarship->id
                    ],
                    'object_id' => $scholarship->id,
                    'object_type' => 'Scholarship',
                    'meta' => [
                        'status' => '',
                        'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
                        'completed_at' => \Carbon\Carbon::now()->toIso8601String(),
                    ],
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $scholarship->id,
            'object_type' => 'Scholarship',
            'verb' => 'apply',
        ]);
        $activity = \PhpSoft\Activity\Models\Activity::find($id);
        $this->assertEquals([
            'status' => '',
            'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            'completed_at' => \Carbon\Carbon::now()->toIso8601String(),
        ], $activity->meta);
        \Carbon\Carbon::setTestNow();
    }
}
