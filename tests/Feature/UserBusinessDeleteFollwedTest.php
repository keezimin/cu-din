<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserBusinessDeleteFollwedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->deleteJson('/users/1/follow/businesses/2');
        $res->assertStatus(404);
    }

    public function testNotFoundPathway()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/follow/businesses/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $business = $this->getUser('business');
        $res = $this->deleteJson('/users/' . $user->id . '/follow/businesses/' . $business->id);
        $res->assertStatus(401);
    }

    public function testDeleteSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $business = $this->getUser('business');
        $activity = factory(\PhpSoft\Activity\Models\Activity::class)->create(
            [
                'verb' => 'follow',
                'actor_type' => 'User',
                'actor_id' => $user->id,
                'object_type' => 'User',
                'object_id' => $business->id
            ]
        );

        $res = $this->deleteJson('/users/' . $user->id . '/follow/businesses/' . $business->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('activities', ['id' => $activity->id]);
    }
}
