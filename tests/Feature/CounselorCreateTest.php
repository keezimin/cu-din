<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CounselorCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/counselors', []);
        $res->assertStatus(401);
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/counselors', ['email' => 'counselor@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $credential = [
            'email' => 'counselor@example.com',
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'password' => '123456'
        ];
        $res = $this->postJson('/counselors', $credential, $headers);
        $res->assertStatus(201);
        $user = \App\User::where(['email' => $credential['email']])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertTrue($user->hasRole('counselor'));
        $this->assertTrue(\JWTAuth::attempt($credential) != false);
    }
}
