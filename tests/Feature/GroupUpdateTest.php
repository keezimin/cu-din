<?php

namespace Tests\Feature;

use Tests\TestCase;

class GroupUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/groups/3', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateWithoutToken()
    {
        $group = factory(\App\Models\Group::class)->create();
        $res = $this->patchJson('/groups/' . $group->id, ['name' => 'example']);
        $res->assertStatus(401);
    }

    public function testUpdateWithDictrictNotFound()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $data = [
            'name' => '',
        ];
        $res = $this->patchJson('/groups/3', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
        $this->assertDatabaseMissing('groups', $data);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('counselor');
        $group = factory(\App\Models\Group::class)->create(['owner_id' => $user->id]);
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'ids' => $ids,
        ];
        $res = $this->patchJson('/groups/' . $group->id, $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $data['owner_id'] = $user->id;
        unset($data['ids']);
        $this->assertDatabaseHas('groups', $data);
        $id = $this->getId($res);
        $this->assertDatabaseHas('group_users', ['group_id' => $id, 'user_id' => $ids[0]]);
        $this->assertDatabaseHas('group_users', ['group_id' => $id, 'user_id' => $ids[1]]);
    }
}
