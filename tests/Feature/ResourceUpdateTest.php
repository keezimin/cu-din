<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUpdateInvalidLink()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'invalid.test.com',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(400);
    }

    public function testUpdateEmptyLink()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => '',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(200);

        $this->assertDatabaseHas('resources', [
            'id' => $resource->id,
            'title' => 'update test title',
            'link' => null,
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ]);
    }

    public function testUpdateEmptyTitle()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => '',
            'link' => 'invalid.test.com',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(400);
    }

    public function testUpdateOnlyTitle()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'Update test title'
        ], ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('resources', [
            'id' => $resource->id,
            'title' => 'update test title',
            'link' => $resource->link,
            'image' => $resource->image,
            'type' => $resource->type,
        ]);
    }

    public function testUpdateEmptySummary()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => '',
            'type' => 'resource',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(400);
    }

    public function testUpdateInvalidType()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => 'test summary',
            'type' => 'invalid type',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(400);
    }

    public function testUpdateEmptyType()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => 'test summary',
            'type' => '',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(400);
    }


    public function testUpdateNotFoundId()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/resources/2', [
            'title' => 'test title',
            'link' => 'http://test.com',
            'image' => 'testimage',
            'summary' => 'test summary',
            'type' => 'news',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(404);
    }

    public function testUpdateWithoutToken()
    {
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'news',
        ]);

        $res->assertStatus(401);
    }

    public function testUpdateSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);

        $res = $this->patchJson('/resources/'. $resource->id, [
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(200);
        $this->assertDatabaseHas('resources', [
            'id' => $resource->id,
            'title' => 'update test title',
            'link' => 'http://update.test.com',
            'image' => 'update testimage',
            'summary' => 'update test summary',
            'type' => 'resource',
        ]);
    }
}
