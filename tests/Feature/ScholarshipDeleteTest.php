<?php

namespace Tests\Feature;

use Tests\TestCase;

class ScholarshipDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->deleteJson('/scholarships/' . $scholarship->id);
        $res->assertStatus(401);
    }

    public function testNotFoundScholarship()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/scholarships/0', [], $headers);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);

        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        $res = $this->deleteJson('/scholarships/' . $scholarship->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('scholarships', ['id' => $scholarship->id]);
    }
}
