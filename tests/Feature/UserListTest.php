<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailWithoutToken()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/', []);
        $res->assertStatus(401);
    }

    public function testListUserSuccess()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $token = $this->getToken();
        $res = $this->getJson('/users', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
    }

    public function testListUserWithPagination()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $token = $this->getToken();

        $res = $this->getJson('/users?page[number]=1&page[size]=2', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->getEqual($res, 2);

        $res = $this->getJson('/users?page[number]=1&page[size]=1', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->getEqual($res, 1);

        $res = $this->getJson('/users?page[number]=2&page[size]=1', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->getEqual($res, 1);

        $res = $this->getJson('/users?page[number]=3&page[size]=1', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->getEqual($res, 0);
    }

    public function getEqual($res, $number)
    {
        $responseArray = json_decode($res->getContent());
        return $this->assertEquals(count($responseArray->data), $number);
    }
}
