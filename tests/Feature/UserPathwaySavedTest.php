<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserPathwaySavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->postJson('/users/1/pathways/2');
        $res->assertStatus(404);
    }

    public function testNotFoundPathway()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/pathways/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $pathway = factory(\App\Models\Pathway::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/pathways/' . $pathway->id);
        $res->assertStatus(401);
    }

    public function testPostSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $pathway = factory(\App\Models\Pathway::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/pathways/' . $pathway->id, [], $headers);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'save',
                    'object' => [
                        'id' => $pathway->id
                    ],
                    'object_id' => $pathway->id,
                    'object_type' => 'Pathway',
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $pathway->id,
            'object_type' => 'Pathway',
        ]);
    }
}
