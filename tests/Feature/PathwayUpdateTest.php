<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PathwayUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUpdateResourceNotfoundId()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->patchJson('/pathways/2', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testAuthorization()
    {
        $userBO = $this->getUser('business_owner');
        $userTeacher = $this->getUser('teacher');

        $token = $this->fromUser($userTeacher);
        $pathway = factory(\App\Models\Pathway::class)->create();

        $res = $this->patchJson('/pathways/'. $pathway->id, [
            'short_description' => 'short description',
        ], ['HTTP_Authorization' => 'Bearer '.$token]);

        $res->assertStatus(403);
    }

    public function testUpdatelWithUserNotActive()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $user->businessProfile()->updateOrCreate(['user_id' => $user->id], ['active' => false]);

        $careerCluster = factory(\App\Models\CareerCluster::class)->create();
        $credential = [
            'career_cluster_id' => $careerCluster->id,
            'name' => 'example name',
            'short_description' => 'example description',
            'user_id' => $user->id,
            'active' => 1,
            'about' => [
                [
                    'type' => 'image',
                    'content' => 'content'
                ]
            ]
        ];

        $res = $this->postJson('/pathways', $credential, $headers);
        $res->assertStatus(201);

        $id = self::getId($res);
        $res = $this->patchJson('/pathways/'. $id, $credential, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(403);
    }

    public function testUpdateSuccess()
    {
        $userBO = $this->getUser('business_owner');
        $token = $this->fromUser($userBO);
        $userBO->businessProfile()->updateOrCreate(['user_id' => $userBO->id], ['active' => true]);

        $pathway = factory(\App\Models\Pathway::class)->create();
        $userBO->businessProfile()->updateOrCreate(['user_id' => $userBO->id], ['active' => true]);
        $data = [
            'name' => 'name example',
            'short_description' => 'short description'
        ];
        $res = $this->patchJson('/pathways/'. $pathway->id, $data, ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('pathways', $data);
    }
}
