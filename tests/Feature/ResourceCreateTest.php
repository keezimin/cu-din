<?php

namespace Tests\Feature;

use Tests\TestCase;

class ResourceCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/resources', []);
        $res->assertStatus(401);
    }

    public function testCreateSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);

        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'resource title', 'link' => 'http://google.com', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => 'news'];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(201);
        $attributes = self::getAttributes($res);
        $this->assertEquals($resource['title'], $attributes->title);
        $id = self::getId($res);
        $this->assertDatabaseHas('resources', ['id' => $id, 'title' => $resource['title'], 'owner_id' => '1']);
    }

    public function testInvalidLink()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'resource title', 'link' => 'InvalidLink', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => 'news'];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(400);
    }

    public function testEmptyTitle()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => '', 'link' => 'http://google.com', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => 'news'];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(400);
    }

    public function testEmptyLink()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'resource title', 'link' => '', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => 'news'];
        $res = $this->postJson('/resources', $resource, $headers);
        $id = self::getId($res);
        $res->assertStatus(201);
        $this->assertDatabaseHas('resources', ['id' => $id, 'title' => $resource['title'], 'owner_id' => '1', 'link' => null]);
    }

    public function testInvalidType()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'resource title', 'link' => 'http://google.com', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => 'invalid type'];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(400);
    }

    public function testEmptyType()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'resource title', 'link' => 'http://google.com', 'summary' => 'resource summary', 'image' => 'resource image', 'type' => ''];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(400);
    }

    public function testEmptySummary()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $resource = ['title' => 'title', 'link' => 'http://google.com', 'summary' => '', 'image' => 'resource image', 'type' => 'news'];
        $res = $this->postJson('/resources', $resource, $headers);
        $res->assertStatus(400);
    }
}
