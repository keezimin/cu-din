<?php

namespace Tests\Feature;

use Tests\TestCase;

class JobFeatureListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListJobUnauthenticated()
    {
        $job = factory(\App\Models\Job::class)->create();
        $res = $this->getJson('/features/jobs');
        $res->assertStatus(401);
    }

    public function testListJobSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $job = factory(\App\Models\Job::class)->create();
        $res = $this->getJson('/features/jobs', $headers);
        $res->assertStatus(200);
    }

    public function testListFeatureJobs()
    {
        // - có school trùng với school của logging student
        // - nếu có apply district và district của school trùng với district của school của logging student
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $expected = [];

        // owner create
        $s = factory(\App\Models\Job::class)->create(['user_id' => $user->id, 'school_id' => $user->school_id]);
        array_push($expected, $s->id);
        $s = factory(\App\Models\Job::class)->create(['user_id' => $user->id, 'school_id' => $user->school_id]);
        array_push($expected, $s->id);
        // create one expired
        factory(\App\Models\Job::class)->create([
            'is_applied_to_district' => true,
            'user_id' => $user->id,
            'school_id' => $user->school_id,
        ]);

        // other create
        factory(\App\Models\Job::class, 2)->create();
        factory(\App\Models\Job::class, 3)->create();

        // other create
        $s = factory(\App\Models\Job::class, 4)->create(['school_id' => $user->school_id]);
        $expected = array_merge($expected, array_pluck($s, 'id'));
        $s = factory(\App\Models\Job::class, 5)->create(['school_id' => $user->school_id]);
        $expected = array_merge($expected, array_pluck($s, 'id'));
        $s = factory(\App\Models\Job::class, 2)->create(['school_id' => $user->school_id]);

        // other create
        factory(\App\Models\District::class, 5)->create()->each(function ($d) {
            factory(\App\Models\School::class, 2)->create(['district_id' => $d->id])->each(function ($s) {
                factory(\App\Models\Job::class, 2)->create(['school_id' => $s->id]);
            });
        });

        // same district, is_applied_to_district true
        factory(\App\Models\School::class, 3)->create(['district_id' => $user->school->district_id])->each(function ($s) use (&$expected) {
            $ss = factory(\App\Models\Job::class, 3)->create(['school_id' => $s->id]);
            $expected = array_merge($expected, array_pluck($ss, 'id'));
        });

        // same district, is_applied_to_district false
        factory(\App\Models\School::class, 3)->create(['district_id' => $user->school->district_id])->each(function ($s) {
            factory(\App\Models\Job::class, 2)->create(['school_id' => $s->id]);
        });

        $res = $this->getJson('/features/jobs', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(0, $content['meta']['totalObjects']);
    }
}
