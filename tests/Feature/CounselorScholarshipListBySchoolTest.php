<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Models\Scholarship;
use \App\User;
use \PhpSoft\Activity\Models\Activity;

class CounselorScholarshipListBySchoolTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListScholarshipUnauthenticate()
    {
        $res = $this->getJson('/counselors/scholarships');
        $res->assertStatus(401);
    }

    public function testListScholarshipSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->getJson('/counselors/scholarships', $headers);
        $res->assertStatus(200);
    }

    public function testListScholarshipWithPagination()
    {
        $user = factory(User::class)->create();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(Scholarship::class, 2)->create(['user_id' => $user->id])->each(function ($sl) use (&$user) {
            $sl->schools()->attach($user->school_id);
        });
        $res = $this->getJson('/counselors/scholarships', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/counselors/scholarships?page[number]=1&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/scholarships?page[number]=2&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/scholarships?page[number]=3&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }
}
