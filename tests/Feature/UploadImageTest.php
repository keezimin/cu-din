<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use phpmock\MockBuilder;

class UploadImageTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        // Create The fake Disk
        Storage::fake('public');
    }

    public function testFileUploadExist()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $res = $this->postJson('/uploads', ['file' => UploadedFile::fake()->image('1417011228.png')], $headers);
        $res->assertStatus(200);

        $url = json_decode($res->getContent())->data->attributes->url;
        $url = explode('/', $url);

        Storage::disk()->assertExists('public/'. end($url));

        // Assert a file does not exist...

        Storage::disk('public')->assertMissing('missing.jpg');
    }

    public function testUploadSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->postJson('/uploads', ['file' => UploadedFile::fake()->image('1417011228.png')], $headers);
        $res->assertStatus(200);
    }

    public function testValidation()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->postJson('/uploads', [], $headers);
        $res->assertStatus(400);
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/uploads', []);
        $res->assertStatus(401);
    }
}
