<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardNewJobRequestTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/jobs/new-request', []);
        $res->assertStatus(401);
    }

    public function testMostCareerClusterOtherRole()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/new-request', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

    public function testGetNewPathwaysSuccess()
    {
        $job_1 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-19']));
        $job_2 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-20']));
        $job_3 = factory(\App\Models\Job::class)->create(array_merge(['status' => '1'], ['created_at' => '2019-11-18']));
        $job_4 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-25']));
        $job_5 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-27']));
        $job_6 = factory(\App\Models\Job::class)->create(array_merge(['status' => '1'], ['created_at' => '2019-11-19']));
        $job_7 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-30']));
        $job_8 = factory(\App\Models\Job::class)->create(array_merge(['status' => '0'], ['created_at' => '2019-11-29']));

        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/new-request', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[0]->created_at), $job_7->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[1]->created_at), $job_8->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[2]->created_at), $job_5->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[3]->created_at), $job_4->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[4]->created_at), $job_2->created_at);
    }
}
