<?php

namespace Tests\Feature;

use Tests\TestCase;

class OrganizationUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailWithoutToken()
    {
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->patchJson('/organizations/' . $organization->id, []);
        $res->assertStatus(401);
    }

    public function testUpdateNotfound()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/organizations/122', ['name' => 'example.organization'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $districtAdmin = $this->getUser('district_admin');
        $token = $this->fromUser($user);
        $district = factory(\App\Models\District::class)->create();
        $organization = factory(\App\Models\Organization::class)->create(
            [
                'name' => 'example',
                'district_id' => $district->id,
                'user_id' => $districtAdmin->id
            ]
        );
        $res = $this->patchJson('/organizations/' . $organization->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('super_admin');
        $districtAdmin = $this->getUser('district_admin');
        $token = $this->fromUser($user);
        $organization = factory(\App\Models\Organization::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $data = [
            'name' => 'example',
            'district_id' => $district->id,
            'user_id' => $districtAdmin->id
        ];
        $res = $this->patchJson('/organizations/' . $organization->id, $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $data['updated_by'] = $user->id;
        $data['id'] = $organization->id;
        $this->assertDatabaseHas('organizations', $data);

        $newData = [
            'user_id' => 5,
            'district_id' => 4
        ];
        $token = $this->fromUser($districtAdmin);
        $res = $this->patchJson('/organizations/' . $organization->id, $newData, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $data['updated_by'] = $user->id;
        $data['id'] = $organization->id;
        $this->assertDatabaseHas('organizations', $data);
    }
}
