<?php

namespace Tests\Feature;

use JWTAuth;
use Tests\TestCase;

class SchoolNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/schools', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/schools', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/schools', ['name' => 'example', 'code' => 'example.code'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $this->assertDatabaseHas('schools', ['name' => 'example', 'code' => 'example.code']);
    }
}
