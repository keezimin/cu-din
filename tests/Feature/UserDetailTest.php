<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailFail() {
        $token = $this->getToken();
        $res = $this->getJson('/users/122', ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(404);
    }

    public function testDetailSuccess() {
        $token = $this->getToken();
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/'. $user->id, ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(200);
    }

    public function testDetailWithoutToken() {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/'. $user->id, []);
        $res->assertStatus(401);
    }
}
