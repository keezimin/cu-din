<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListResourcesWithoutToken()
    {
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->getJson('/resources', []);
        $res->assertStatus(401);
    }

    public function testListResourcesWithPagination()
    {
        $user = $this->getUser('counselor');
        $token = $this->fromUser($user);
        $resource_1 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $resource_2 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);

        $res = $this->getJson('/resources?page[number]=1&page[size]=2', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/resources?page[number]=1&page[size]=1', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/resources?page[number]=2&page[size]=1', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/resources?page[number]=3&page[size]=1', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);

        $res = $this->getJson('/resources?page[number]=1&page[size]=3', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);
    }
    //
    // public function testListResourceWithInvalidPagination()
    // {
    //     $token = $this->getToken();
    //     $resource_1 = factory(\App\Models\Resource::class)->create();
    //     $res = $this->getJson('/resources?page[number]=invalid&page[size]=2', ['HTTP_Authorization' => 'Bearer '. $token]);
    //     $response = json_decode($res->getContent());
    //     $res->assertStatus(500);
    //     $this->assertEquals('A non-numeric value encountered', $response->errors[0]->detail);
    //
    //     $res = $this->getJson('/resources?page[number]=1&page[size]=invalid', ['HTTP_Authorization' => 'Bearer '. $token]);
    //     $response = json_decode($res->getContent());
    //     $res->assertStatus(500);
    //     $this->assertEquals('A non-numeric value encountered', $response->errors[0]->detail);
    //
    // }

    public function testListResourcesSuccess()
    {
        $user = $this->getUser('counselor');
        $token = $this->fromUser($user);
        $resource_1 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $resource_2 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->getJson('/resources', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $this->assertEquals(2, count(json_decode($res->getContent())->data));
    }
}
