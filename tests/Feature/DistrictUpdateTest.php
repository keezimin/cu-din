<?php

namespace Tests\Feature;

use Tests\TestCase;

class DistrictUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/districts/' . $district->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateFail()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/districts/122', ['name' => 'example.district'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/districts/' . $district->id, ['name' => 'example.district', 'code' => 'example.code'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('districts', ['id' => $district->id, 'name' => 'example.district', 'code' => 'example.code']);
    }

    public function testUpdateWithId()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/districts/' . $district->id, ['id' => '123', 'name' => 'example.district'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('districts', ['id' => $district->id, 'name' => 'example.district']);
    }

    public function testDetailWithoutToken()
    {
        $district = factory(\App\Models\District::class)->create();
        $res = $this->patchJson('/districts/' . $district->id, []);
        $res->assertStatus(401);
    }
}
