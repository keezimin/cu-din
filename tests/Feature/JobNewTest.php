<?php

namespace Tests\Feature;

use Tests\TestCase;

class JobNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/jobs', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/jobs', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateWithDictrictNotFound()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'organization' => 'example.organization',
            'description' => 'example.description',
            'application_form_file' => 'example.application_form_file',
            'application_form_link' => 'example.application_form_link',
            'location' => 'Da Nang',
            'application_form_filename' => 'name',
        ];
        $res = $this->postJson('/jobs', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
        $this->assertDatabaseMissing('career_jobs', $data);
    }

    public function testCreateSuccess()
    {
        $user = factory(\App\User::class)->create();
        $school_ids = [];
        $district_ids = [];

        factory(\App\Models\School::class, 2)->create()->each(function ($school) use (&$school_ids) {
            $school_ids[] = $school->id;
        });

        factory(\App\Models\District::class, 2)->create()->each(function ($district) use (&$district_ids) {
            $district_ids[] = $district->id;
        });

        $pathway = factory(\App\Models\Pathway::class)->create();
        $state = factory(\App\Models\State::class)->create();

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'organization' => 'example.organization',
            'type' => 'internship',
            'description' => 'example.description',
            'application_form_file' => 'example.application_form_file',
            'application_form_link' => 'example.application_form_link',
            'location' => 'Da Nang',
            'volunteer_hours' => null,
            'school_ids' => $school_ids,
            'application_form_filename' => 'name',
            'deadline_on' => '2017-12-12',
            'district_ids' => $district_ids,
            'pathway_id' => $pathway->id,
            'state_id' => $state->id,
            'is_assign_to_pathway' => true
        ];
        $res = $this->postJson('/jobs', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $data['owner_id'] = $user->id;
        unset($data['school_ids']);
        unset($data['district_ids']);
        $data['volunteer_hours'] = 0;
        $this->assertDatabaseHas('career_jobs', $data);
        $id = $this->getId($res);
        $this->assertDatabaseHas('schools_jobs', ['career_job_id' => $id, 'school_id' => $school_ids[0]]);
        $this->assertDatabaseHas('schools_jobs', ['career_job_id' => $id, 'school_id' => $school_ids[1]]);
        $this->assertDatabaseHas('districts_career_jobs', ['career_job_id' => $id, 'district_id' => $district_ids[0]]);
        $this->assertDatabaseHas('districts_career_jobs', ['career_job_id' => $id, 'district_id' => $district_ids[1]]);
    }

    public function testRelationship()
    {
        $user = factory(\App\User::class)->create();
        $school_ids = [];
        $district_ids = [];

        factory(\App\Models\School::class, 2)->create()->each(function ($school) use (&$school_ids) {
            $school_ids[] = $school->id;
        });

        factory(\App\Models\District::class, 2)->create()->each(function ($district) use (&$district_ids) {
            $district_ids[] = $district->id;
        });

        $pathway = factory(\App\Models\Pathway::class)->create();
        $state = factory(\App\Models\State::class)->create();

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'organization' => 'example.organization',
            'type' => 'internship',
            'description' => 'example.description',
            'application_form_file' => 'example.application_form_file',
            'application_form_link' => 'example.application_form_link',
            'location' => 'Da Nang',
            'volunteer_hours' => null,
            'school_ids' => $school_ids,
            'application_form_filename' => 'name',
            'deadline_on' => '2017-12-12',
            'district_ids' => $district_ids,
            'pathway_id' => $pathway->id,
            'state_id' => $state->id,
            'is_assign_to_pathway' => true
        ];
        $res = $this->postJson('/jobs', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'owner' => [
                        'id' => $user->id,
                        'type' => 'User',
                    ],
                    "districts"=> [
                        [
                            "id" => $district_ids[0],
                            "type" => "District"
                        ],
                        [
                            "id" => $district_ids[1],
                            "type" => "District"
                        ],        
                    ],
                    "state" => [
                        "id" => $state->id,
                        "type" => "State"
                    ],
                    "pathway" => [
                        "id" =>  $pathway->id,
                        "type" => "Pathway"
                    ]
                ],
            ],
            'included' => [
                [
                    'type' => 'User',
                    'id' => $user->id,
                ]
            ],
        ]);
    }
}
