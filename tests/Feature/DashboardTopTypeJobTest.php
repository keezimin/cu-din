<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardTopTypeJobTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/jobs/top-types', []);
        $res->assertStatus(401);
    }

    public function testTopTypeJobSuccess()
    {
        $idJobs = [];
        factory(\App\Models\Job::class, 2)->create(['type' => 'job_shadowing'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        factory(\App\Models\Job::class, 3)->create(['type' => 'mentorship'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        factory(\App\Models\Job::class, 5)->create(['type' => 'cooperative_education'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        factory(\App\Models\Job::class, 6)->create(['type' => 'internship'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        factory(\App\Models\Job::class, 1)->create(['type' => 'registered_apprenticeships'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        factory(\App\Models\Job::class, 4)->create(['type' => 'general_employment'])->each(function ($user) use (&$idJobs) {
                $idJobs[] = $user->id;
            }
        );
        
        foreach ($idJobs as $key => $id) {
            $factory = factory(\PhpSoft\Activity\Models\Activity::class);
            //key == 3 => type= mentorship
            if ($key == 3) {
                $factory = factory(\PhpSoft\Activity\Models\Activity::class, 2);
            }
            ////key == 15 => type= internship
            if ($key == 15) {
                $factory = factory(\PhpSoft\Activity\Models\Activity::class, 4);
            }
            $factory->create(
                [
                    'object_id' => $id,
                    'actor_type' => 'User',
                    'verb' => 'apply',
                    'object_type' => 'Job'
                ]
            );
        }

        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/top-types', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
        $res->assertJson(
            [
               'data' => [
                    0 => [
                        "number_job" => 6,
                        "type" => "internship",
                        "number_student" => 9
                    ],
                    1 => [
                        "number_job" => 5,
                        "type" => "cooperative_education",
                        "number_student" => 5
                    ],
                    2 => [
                        "number_job" => 4,
                        "type" => "general_employment",
                        "number_student" => 4
                    ],
                    3 => [
                        "number_job" => 3,
                        "type" => "mentorship",
                        "number_student" => 4
                    ],
                    4 => [
                        "number_job" => 2,
                        "type" => "job_shadowing",
                        "number_student" => 2
                    ],

                ]
            ]
        );
    }
}
