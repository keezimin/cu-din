<?php

namespace Tests\Feature;

use Tests\TestCase;

class JobAppliedUsersTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundjob()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/jobs/2/users');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $job = factory(\App\Models\Job::class)->create();
        $res = $this->getJson('/jobs/' . $job->id . '/users');
        $res->assertStatus(401);
    }

    public function testGetSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_admin = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'super_admin']);
        // attach roles
        $user->attachRole($role_admin);
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $job = factory(\App\Models\Job::class)->create();
        $users = factory(\App\User::class, 15)->create();
        foreach ($users as $u) {
            factory(\PhpSoft\Activity\Models\Activity::class)->create([
                'verb' => 'apply',
                'actor_type' => 'User',
                'actor_id' => $u->id,
                'object_type' => 'job',
                'object_id' => $job->id,
            ]);
            factory(\PhpSoft\Activity\Models\Activity::class)->create();
        }
        factory(\App\User::class, 5)->create();

        $res = $this->getJson('/jobs/' . $job->id . '/users', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(10, count($content['data']));
        $this->assertEquals(15, $content['meta']['totalObjects']);
    }
}
