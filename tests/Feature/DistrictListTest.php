<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DistrictListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListDistrictSuccess()
    {
        $district = factory(\App\Models\District::class)->create([
            'name' => 'example.district'
        ]);
        $res = $this->getJson('/districts');
        $res->assertStatus(200);
    }

    public function testListDistrictWithPagination()
    {
        factory(\App\Models\District::class)->create([
            'name' => 'example.district1'
        ]);
        factory(\App\Models\District::class)->create([
            'name' => 'example.district2'
        ]);

        $res = $this->getJson('/districts?page[number]=1&page[size]=2');
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/districts?page[number]=1&page[size]=1');
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/districts?page[number]=2&page[size]=1');
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/districts?page[number]=3&page[size]=1');
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

}
