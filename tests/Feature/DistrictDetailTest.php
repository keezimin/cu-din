<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DistrictDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailSuccess()
    {
        $token = $this->getToken();
        $district = factory(\App\Models\District::class)->create();
        $res = $this->getJson('/districts/'. $district->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
    }

    public function testNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/districts/122', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testCreateWithoutToken()
    {
        $district = factory(\App\Models\District::class)->create();
        $res = $this->getJson('/districts/'. $district->id, []);
        $res->assertStatus(401);
    }
}
