<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardNewPathwaysTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/pathways', []);
        $res->assertStatus(401);
    }

    public function testMostCareerClusterOtherRole()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/new-request', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

    public function testGetNewPathwaysSuccess()
    {
        $data = [
            'career_cluster_id' => function () {
                return factory(\App\Models\CareerCluster::class)->create()->id;
            },
            'name' => 'Test',
            'short_description' => 'short_description',
            'user_id' => function () {
                return factory(\App\User::class)->create()->id;
            },
            'active' => 1
        ];

        $pathway_1 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-19'], $data));
        $pathway_2 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-16'], $data));
        $pathway_3 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-20'], $data));
        $pathway_4 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-14'], $data));
        $pathway_5 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-15'], $data));
        $pathway_6 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-18'], $data));
        $pathway_7 = factory(\App\Models\Pathway::class)->create(array_merge(['created_at' => '2019-11-13'], $data));

        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/pathways', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[0]->attributes->created_at), $pathway_3->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[1]->attributes->created_at), $pathway_1->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[2]->attributes->created_at), $pathway_6->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[3]->attributes->created_at), $pathway_2->created_at);
        $this->assertEquals(\Carbon\Carbon::parse($responseArray->data[4]->attributes->created_at), $pathway_5->created_at);
    }
}
