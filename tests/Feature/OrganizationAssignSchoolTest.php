<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationAssignSchoolTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testAuthorization()
    {
        $user = $this->getUser('teacher');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $organization = factory(\App\Models\Organization::class)->create();
        $school = factory(\App\Models\School::class)->create();

        $res = $this->patchJson('/organizations/'. $organization->id. '/schools/'. $school->id. '/assign', [], $headers);
        $res->assertStatus(403);
    }

    public function testOrganizationAssignSchoolSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $organization = factory(\App\Models\Organization::class)->create();
        $school = factory(\App\Models\School::class)->create();

        $res = $this->patchJson('/organizations/'. $organization->id. '/schools/'. $school->id. '/assign', [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseHas('schools', [
            'id' => $school->id,
            'organization_id' => $organization->id
        ]);
    }
}
