<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Jobs\SendInviteEmail;
use Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployerInviteEmployerTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $employer = $this->getUser('business_owner');
        $res = $this->postJson('/businesses/'. $employer->id. '/businesses/invitation', ['email'=> ['mail1@example.com', 'mail12@example.com']], []);
        $res->assertStatus(401);
    }

    public function testWithoutEmail()
    {
        $employer = $this->getUser('business_owner');
        $token = $this->fromUser($employer);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        Queue::fake();

        $res = $this->postJson('/businesses/'. $employer->id. '/businesses/invitation', [], $headers);
        $res->assertStatus(400);
    }

    public function testInviteSuccess()
    {
        $employer = $this->getUser('business_owner');
        $token = $this->fromUser($employer);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        Queue::fake();

        $res = $this->postJson('/businesses/'. $employer->id. '/businesses/invitation', ['email'=> ['mail1@example.com', 'mail12@example.com']], $headers);
        $res->assertStatus(204);

        // Assert a job was pushed to a given queue...
        Queue::assertPushed(SendInviteEmail::class, 2);

        Mail::shouldReceive('send');
    }
}
