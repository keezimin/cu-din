<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->deleteJson('/users/' . $user->id);
        $res->assertStatus(401);
    }

    public function testDeleteAdmin()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->deleteJson('/users/' . $user->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
    }

    public function testNotFoundUser()
    {
        $token = $this->getToken();
        $res = $this->deleteJson('/users/999', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testDeleteSuccessWithOneMessage()
    {
        $admin = $this->getUser('super_admin');
        $token = $this->fromUser($admin);
        $user = $this->getUser();

        factory(\App\Models\Device::class)->create(['user_id' => $user->id]);
        $scholarship = factory(\App\Models\Scholarship::class)->create();

        // student save and apply
        $saveAct = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);
        \App\Services\UserActionService::applyScholarship($user, $scholarship);

        $m = app()->make('\App\Repositories\Eloquent\MessageBread');
        $message = $m->add([
            'target' => [
                'type' => 'USER',
                'id' => [$user->id],
            ],
            'content' => 'hi there, welcome to CUED-IN',
        ], $admin);

        // Delete
        $res = $this->deleteJson('/users/' . $user->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(204);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'deleted_at' => null]);
    }

    public function testDeleteSuccessWithMultiMessage()
    {
        $admin = $this->getUser('super_admin');
        $token = $this->fromUser($admin);
        $user = $this->getUser();
        factory(\App\User::class, 2)->create();

        factory(\App\Models\Device::class)->create(['user_id' => $user->id]);
        $scholarship = factory(\App\Models\Scholarship::class)->create();

        // student save and apply
        $saveAct = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);
        \App\Services\UserActionService::applyScholarship($user, $scholarship);

        $m = app()->make('\App\Repositories\Eloquent\MessageBread');
        $message = $m->add([
            'target' => [
                'type' => 'USER',
                'id' => [$user->id, '3', '4'],
            ],
            'content' => 'hi there, welcome to CUED-IN',
        ], $admin);

        // Delete
        $res = $this->deleteJson('/users/' . $user->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(204);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'deleted_at' => null]);
    }

    public function testDeleteSuccessWithAllMessage()
    {
        $admin = $this->getUser('super_admin');
        $token = $this->fromUser($admin);
        $user = $this->getUser();
        factory(\App\User::class, 2)->create();

        factory(\App\Models\Device::class)->create(['user_id' => $user->id]);
        $scholarship = factory(\App\Models\Scholarship::class)->create();

        // student save and apply
        $saveAct = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);
        \App\Services\UserActionService::applyScholarship($user, $scholarship);

        $m = app()->make('\App\Repositories\Eloquent\MessageBread');
        $m->add([
            'target' => [
                'type' => 'USER_ALL',
            ],
            'content' => 'hi there, welcome to CUED-IN',
        ], $admin);

        // Delete
        $res = $this->deleteJson('/users/' . $user->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(204);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'deleted_at' => null]);
    }

    public function testDeleteBusinessOwnerSucess()
    {
        $admin  = $this->getUser('super_admin');
        $token  = $this->fromUser($admin);
        $userBO = $this->getUser('business_owner');
        $userDA = $this->getUser('district_admin');

        factory(\App\Models\Scholarship::class)->create(['user_id' => $admin]);
        // scholarship BO create
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $userBO]);

        factory(\App\Models\Job::class)->create(['user_id' => $admin]);
        // job DA create
        $jobDA = factory(\App\Models\Job::class)->create(['user_id' => $userBO, 'owner_id' => $userDA]);
        factory(\App\Models\Job::class)->create(['user_id' => $userDA]);

        // job BO create
        $jobBO = factory(\App\Models\Job::class)->create(['user_id' => $userBO]);

        // Delete
        $res = $this->deleteJson('/users/' . $userBO->id, [], ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(204);
        $this->assertEquals(\App\Models\Scholarship::get()->count(), 1);
        $this->assertEquals(\App\Models\Job::get()->count(), 2);
        $this->assertDatabaseMissing('users', ['id' => $userBO->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('career_jobs', ['id' => $jobBO->id, 'user_id' => $userBO->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('career_jobs', [
            'id' => $jobDA->id, 'user_id' => $userBO->id, 'owner_id' => $userDA->id, 'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('scholarships', ['id' => $scholarship->id, 'user_id' => $userBO->id, 'deleted_at' => null]);
    }
}
