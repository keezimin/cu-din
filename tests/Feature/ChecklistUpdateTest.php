<?php

namespace Tests\Feature;

use Tests\TestCase;

class ChecklistUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/checklists/3', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateWithoutToken()
    {
        $group = factory(\App\Models\Checklist::class)->create();
        $res = $this->patchJson('/checklists/' . $group->id, ['name' => 'example']);
        $res->assertStatus(401);
    }

    public function testUpdateWithDictrictNotFound()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $data = [
            'name' => '',
        ];
        $res = $this->patchJson('/checklists/3', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
        $this->assertDatabaseMissing('checklists', $data);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('counselor');
        $checklist = factory(\App\Models\Checklist::class)->create(['owner_id' => $user->id]);
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
        ];
        $res = $this->patchJson('/checklists/' . $checklist->id, $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $data['owner_id'] = $user->id;
        unset($data['ids']);
        $this->assertDatabaseHas('checklists', $data);
    }
}
