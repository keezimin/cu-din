<?php

namespace Tests\Feature;

use Excel;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

/**
 * @group Import
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class StudentImportTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    /**
     * @group Invite
     */
    public function testImportSuccess()
    {
        $bread = app()->make('\App\Repositories\Eloquent\CounselorBread');
        $data = ['email' => 'counselor@example.com', 'password' => '123456'];
        $counselor = $bread->add($data);
        $token = $this->fromUser($counselor);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $student = [
            ['full_name', 'email'],
            ['example', 'example@example.com'],
            ['import', 'import@example.com'],
            ['import', 'import@example.com'],
        ];

        \Mockery::mock('overload:\PhpSoft\DeepLinking\GetLink')->shouldReceive('get')->andReturn('http://url');
        \Mockery::mock('overload:\App\Jobs\SendInviteEmail')->shouldReceive('dispatch')->andReturn(250);
        $file = $this->exportStudents($student);
        $file = new UploadedFile(
            $file['full'], null, null, null, null, true
        );
        $res = $this->postJson('/counselors/' . $counselor->id . '/students/import', ['file' => $file], $headers);
        $res->assertStatus(200);

        $this->assertDatabaseHas('invitations', ['fullname' => 'example', 'email' => 'example@example.com']);
        $this->assertEquals(1, \App\Models\Invitation::where('email', 'import@example.com')->count());
    }

    /**
     * @group Invite
     */
    public function testImportWithUserRegisted()
    {
        $bread = app()->make('\App\Repositories\Eloquent\CounselorBread');
        $data = ['email' => 'counselor@example.com', 'password' => '123456'];
        $counselor = $bread->add($data);
        $token = $this->fromUser($counselor);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $student = [
            ['full_name', 'email'],
            ['example', 'example@example.com'],
            ['import', 'import@example.com'],
        ];
        $file = $this->exportStudents($student);

        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password' => bcrypt('12345678'),
        ]);

        \Mockery::mock('overload:\PhpSoft\DeepLinking\GetLink')->shouldReceive('get')->andReturn('http://url');
        \Mockery::mock('overload:\App\Jobs\SendInviteEmail')->shouldReceive('dispatch')->andReturn(250);
        $file = new UploadedFile(
            $file['full'], null, null, null, null, true
        );
        $res = $this->postJson('/counselors/' . $counselor->id . '/students/import', ['file' => $file], $headers);
        $res->assertStatus(200);

        $this->assertDatabaseMissing('invitations', ['fullname' => 'example', 'email' => 'example@example.com']);
    }

    public function testImportMissingColum()
    {
        $bread = app()->make('\App\Repositories\Eloquent\CounselorBread');
        $data = ['email' => 'counselor@example.com', 'password' => '123456'];
        $counselor = $bread->add($data);
        $token = $this->fromUser($counselor);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $student = [
            ['name', 'email'],
            ['example', 'example@example.com'],
        ];
        $file = $this->exportStudents($student);

        $file = new UploadedFile(
            $file['full'], null, null, null, null, true
        );
        $res = $this->postJson('/counselors/' . $counselor->id . '/students/import', ['file' => $file], $headers);
        $res->assertStatus(422);
    }

    public function exportStudents($data = [])
    {
        return Excel::create('Filename', function ($excel) use ($data) {
            $excel->sheet('Sheetname', function ($sheet) use ($data) {
                $sheet->rows($data);
            });
        })->store('xls', storage_path('tests/exports'), true);
    }
}
