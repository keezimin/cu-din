<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserScholarshipSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->postJson('/users/1/scholarships/2');
        $res->assertStatus(404);
    }

    public function testNotFoundScholarship()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/scholarships/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id);
        $res->assertStatus(401);
    }

    public function testPostSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $scholarship->schools()->attach($user->school_id);
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id, [], $headers);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'save',
                    'object' => [
                        'id' => $scholarship->id
                    ],
                    'object_id' => $scholarship->id,
                    'object_type' => 'Scholarship',
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $scholarship->id,
            'object_type' => 'Scholarship',
        ]);
        $date = (new \Carbon\Carbon())->toIso8601String();
        $res = $this->postJson('/users/' . $user->id . '/scholarships/' . $scholarship->id, [
            'verb' => 'something',
            'meta' => [
                'test_on' => $date
            ]
        ], $headers);
        $res->assertStatus(201);
        $activity = \PhpSoft\Activity\Models\Activity::find($id);
        $this->assertEquals($user->id, $activity->actor_id);
        $this->assertEquals('User', $activity->actor_type);
        $this->assertEquals($scholarship->id, $activity->object_id);
        $this->assertEquals('Scholarship', $activity->object_type);
        $this->assertEquals([], $activity->meta);
        $this->assertEquals([], $activity->target);
        $this->assertEquals('save', $activity->verb);
    }
}
