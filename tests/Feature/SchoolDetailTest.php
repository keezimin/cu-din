<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SchoolDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailSuccess()
    {
        $token = $this->getToken();
        $school = factory(\App\Models\School::class)->create();
        $res = $this->getJson('/schools/'. $school->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
    }

    public function testNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/schools/122', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testCreateWithoutToken()
    {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->getJson('/schools/'. $school->id, []);
        $res->assertStatus(401);
    }

    public function testRelationship() {
        $token = $this->getToken();
        $district = factory(\App\Models\District::class)->create();
        $school = factory(\App\Models\School::class)->create([
            'name' => 'example.school',
            'district_id' => $district->id
        ]);
        $res = $this->getJson('/schools/'. $school->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'district' => [
                        'id' => $district->id,
                        'type' => 'District'
                    ],
                ]
            ],
            'included' => [
                [
                    'type' => 'District',
                    'id' => $district->id,
                ]
            ]
        ]);
    }
}
