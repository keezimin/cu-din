<?php

namespace Tests\Feature;

use Tests\TestCase;

class BusinessUpdateProfileTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/businesses/1', []);
        $res->assertStatus(401);
    }

    public function testNotFoundOwner()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/businesses/101', ['email' => 'owner@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testFailPermission()
    {
        $profile = [
            'photo' => 'photo.jpg',
            'logo' => 'logo.ico',
            'address' => 'da nang',
            'contact_person' => 'ha noi',
        ];

        $profile['users'] = [
            'fullname' => 'Thanh Thanh',
            'contact' => '213 Ha',
        ];
        $business = $this->getUser('business_owner');

        $district = $this->getUser('district_admin');
        $token = $this->fromUser($district);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->patchJson("/businesses/{$business->id}", $profile, $headers);
        $res->assertStatus(403);

        $counselor = $this->getUser('counselor');
        $token = $this->fromUser($counselor);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->patchJson("/businesses/{$business->id}", $profile, $headers);
        $res->assertStatus(403);
    }

    public function testUpdateSuccess()
    {
        $profile = [
            'photo' => 'photo.jpg',
            'logo' => 'logo.ico',
            'address' => 'da nang',
            'contact_person' => 'ha noi'
        ];

        $businessProfile = [
            'industry' => 'industry',
            'type' => 'type',
            'comapny_size' => 10,
            'founded' => 1997,
            'specialties' => 'specialties',
            'phone' => '12-12-1234',
            'facebook' => 'http://example.vn',
            'instagram' => 'http://example.vn',
            'linkedin' => 'http://example.vn'
        ];

        $profile['users'] = [
            'first_name' => 'Thanh',
            'last_name' => 'Thanh',
            'contact' => '213 Ha',
        ];

        $business = factory(\PhpSoft\Users\Models\User::class)->create();
        $token = $this->fromUser($business);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $res = $this->patchJson("/businesses/{$business->id}", array_merge($businessProfile, $profile), $headers);
        $res->assertStatus(200);
        $user = \App\User::with('profile')->where(['email' => $business->email])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email, 'fullname' => 'Thanh Thanh']);
        unset($profile['users']);
        $this->assertDatabaseHas('profiles', array_merge(['user_id' => $user->id], $profile));
        $this->assertDatabaseHas('business_profiles', array_merge(['user_id' => $user->id], $businessProfile));
    }
}
