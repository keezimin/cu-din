<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserPathwayListSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/' . $user->id . '/pathways');
        $res->assertStatus(401);
    }

    public function testReadSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $res = $this->getJson('/users/' . $user->id . '/pathways', $headers);
        $res->assertStatus(200);
        $this->assertEquals(0, count(json_decode($res->getContent(), true)['data']));
    }

    public function testReadSuccessData()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $pathways = factory(\App\Models\Pathway::class, 15)->create();
        foreach ($pathways as $pathway) {
            factory(\PhpSoft\Activity\Models\Activity::class)->create([
                'verb' => 'save',
                'actor_type' => 'User',
                'actor_id' => $user->id,
                'object_type' => 'Pathway',
                'object_id' => $pathway->id,
            ]);
        }
        $pathways = factory(\App\Models\Pathway::class, 5)->create();

        $res = $this->getJson('/users/' . $user->id . '/pathways', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(10, count($content['data']));
        $this->assertEquals(15, $content['meta']['totalObjects']);
    }
}
