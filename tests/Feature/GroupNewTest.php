<?php

namespace Tests\Feature;

use Tests\TestCase;

class GroupNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/groups', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/groups', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateWithDictrictNotFound()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
        ];
        $res = $this->postJson('/groups', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
        $this->assertDatabaseMissing('groups', $data);
    }

    public function testCreateSuccess()
    {
        $user = factory(\App\User::class)->create();
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'ids' => $ids,
        ];
        $res = $this->postJson('/groups', $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $data['owner_id'] = $user->id;
        unset($data['ids']);
        $this->assertDatabaseHas('groups', $data);
        $id = $this->getId($res);
        $this->assertDatabaseHas('group_users', ['group_id' => $id, 'user_id' => $ids[0]]);
        $this->assertDatabaseHas('group_users', ['group_id' => $id, 'user_id' => $ids[1]]);
    }

    public function testRelationship()
    {
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $ids = [];

        factory(\App\User::class, 2)->create()->each(function ($user) use (&$ids) {
            $ids[] = $user->id;
        });
        $res = $this->postJson('/groups', [
            'name' => 'example.job',
            'ids' => $ids,
        ], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'owner' => [
                        'id' => $user->id,
                        'type' => 'User',
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'User',
                    'id' => $user->id,
                ],
            ],
        ]);
    }
}
