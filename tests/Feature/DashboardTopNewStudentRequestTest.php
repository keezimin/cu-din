<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\BusinessProfile;

class DashboardTopNewStudentRequestTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/jobs/student-request', []);
        $res->assertStatus(401);
    }

    public function testMostCareerClusterOtherRole()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/new-request', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

    public function testTopNewStudentRequestSuccess()
    {
        $business = $this->getUser('business_owner');
        $profile = $business->profile()->create(['user_id' => $business->id, 'active' => true]);

        $idJobs = [];
        $job = factory(\App\Models\Job::class, 7)->create(['status' => 1, 'user_id' => $business->id])->each(function ($job) use (&$idJobs) {
                $idJobs[] = $job->id;
            }
        );

        foreach ($idJobs as $key => $id) {
            $factory = factory(\PhpSoft\Activity\Models\Activity::class);
            $meta = [
                'status' => 'interested',
                'applied_at' => \Carbon\Carbon::now()->toIso8601String(),
            ];

            $objectType = ($key == 2) ? 'user' : 'Job';
            $factory->create(
                [
                    'object_id' => $id,
                    'actor_type' => 'user',
                    'verb' => 'apply',
                    'object_type' => $objectType,
                    'meta' => $meta
                ]
            );
        }

        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/jobs/student-request', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
        $res->assertJson(
            [
               'data' => [
                    0 => [
                        'job_name' => $job[0]->name,
                        'type' => $job[0]->type,
                        'employer_name' => $profile->contact_person
                    ],
                    1 => [
                        'job_name' => $job[1]->name,
                        'type' => $job[1]->type,
                        'employer_name' => $profile->contact_person
                    ],
                    2 => [
                        'job_name' => $job[3]->name,
                        'type' => $job[3]->type,
                        'employer_name' => $profile->contact_person
                    ],
                    3 => [
                        'job_name' => $job[4]->name,
                        'type' => $job[4]->type,
                        'employer_name' => $profile->contact_person
                    ],
                    4 => [
                        'job_name' => $job[5]->name,
                        'type' => $job[5]->type,
                        'employer_name' => $profile->contact_person
                    ]
                ]
            ]
        );
    }
}
