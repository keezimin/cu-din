<?php

namespace Tests\Feature;

use JWTAuth;
use Tests\TestCase;

class JobUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/jobs', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateFail()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/schools/122', ['name' => 'example.school'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateSuccess()
    {
        $user = factory(\App\User::class)->create();
        $school_ids = [];
        $district_ids = [];

        factory(\App\Models\School::class, 2)->create()->each(function ($school) use (&$school_ids) {
            $school_ids[] = $school->id;
        });

        factory(\App\Models\District::class, 2)->create()->each(function ($district) use (&$district_ids) {
            $district_ids[] = $district->id;
        });
        $job = factory(\App\Models\Job::class)->create(['owner_id' => $user->id]);

        $pathway = factory(\App\Models\Pathway::class)->create();
        $state = factory(\App\Models\State::class)->create();

        $token = $this->fromUser($user);
        $data = [
            'name' => 'example.job',
            'organization' => 'example.organization',
            'type' => 'internship',
            'description' => 'example.description',
            'application_form_file' => 'example.application_form_file',
            'application_form_link' => 'example.application_form_link',
            'location' => 'Da Nang',
            'volunteer_hours' => 1,
            'school_ids' => $school_ids,
            'application_form_filename' => 'name',
            'deadline_on' => '2017-12-12',
            'district_ids' => $district_ids,
            'pathway_id' => $pathway->id,
            'state_id' => $state->id,
            'is_assign_to_pathway' => true
        ];
        $res = $this->patchJson('/jobs/' . $job->id, $data, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        unset($data['school_ids']);
        unset($data['district_ids']);
        $this->assertDatabaseHas('career_jobs', $data);
    }

    public function testUpdateWithId()
    {
        $people = factory(\App\User::class)->create();
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $school_ids = [];
        $district_ids = [];

        factory(\App\Models\School::class, 5)->create()->each(function ($school) use (&$school_ids) {
            $school_ids[] = $school->id;
        });
        factory(\App\Models\District::class, 2)->create()->each(function ($district) use (&$district_ids) {
            $district_ids[] = $district->id;
        });
        $job = factory(\App\Models\Job::class)->create([
            'name' => 'job',
            'user_id' => $user->id,
            'owner_id' => $user->id
        ]);

        $res = $this->patchJson('/jobs/' . $job->id, 
            [
                'id' => '123', 
                'name' => 'example.school', 
                'user_id' => $people->id, 
                'location' => 'Da Nang', 
                'school_ids' => $school_ids,
                'district_ids' => $district_ids,
            ], 
            [
                'HTTP_Authorization' => 'Bearer ' . $token
            ]
        );
        $res->assertStatus(200);
        $this->assertDatabaseHas('career_jobs', ['id' => $job->id, 'name' => 'example.school', 'user_id' => $people->id]);
        $this->assertDatabaseHas('schools_jobs', ['career_job_id' => $job->id, 'school_id' => $school_ids[0]]);
        $this->assertDatabaseHas('schools_jobs', ['career_job_id' => $job->id, 'school_id' => $school_ids[1]]);
        $this->assertDatabaseHas('schools_jobs', ['career_job_id' => $job->id, 'school_id' => $school_ids[2]]);
    }

    public function testUpdateFailWithId()
    {
        $people = factory(\App\User::class)->create();
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $job = factory(\App\Models\Job::class)->create([
            'name' => 'job',
            'user_id' => $user->id,
            'owner_id' => $user->id,
        ]);

        $school_ids = [];
        $district_ids = [];

        factory(\App\Models\School::class, 5)->create()->each(function ($school) use (&$school_ids) {
            $school_ids[] = $school->id;
        });
        factory(\App\Models\District::class, 2)->create()->each(function ($district) use (&$district_ids) {
            $district_ids[] = $district->id;
        });

        $res = $this->patchJson('/jobs/' . $job->id, 
            [
                'id' => '123', 
                'name' => 'example.school', 
                'user_id' => $people->id, 
                'school_ids' => array_merge($school_ids, [22, 32]),
                'district_ids' => $district_ids,
            ], 
            [
                'HTTP_Authorization' => 'Bearer ' . $token
            ]
        );

        $res->assertStatus(400);
    }

    public function testUpdateWithoutToken()
    {
        $job = factory(\App\Models\Job::class)->create();
        $res = $this->patchJson('/jobs/' . $job->id, []);
        $res->assertStatus(401);
    }
}
