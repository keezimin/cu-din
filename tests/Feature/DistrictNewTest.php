<?php

namespace Tests\Feature;

use JWTAuth;
use Tests\TestCase;

class DistrictNewTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/districts', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testCreateWithoutToken()
    {
        $res = $this->postJson('/districts', ['name' => 'example'], []);
        $res->assertStatus(401);
    }

    public function testCreateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->postJson('/districts', ['name' => 'example', 'code' => 'example.code'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(201);
        $this->assertDatabaseHas('districts', ['name' => 'example', 'code' => 'example.code']);
    }
}
