<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $res = $this->postJson('/users', []);
        $res->assertStatus(400);
    }

    public function testDataEmpty() {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->postJson('/users', [
            'email' => 'user@example.com',
            'password' => '123456',
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'school_id' => $school->id,
        ]);
        $res->assertStatus(201);
    }

    public function testCreateSuccess() {
        $school = factory(\App\Models\School::class)->create();
        $data = [
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'email' => 'user@example.com',
            'password' => '123456',
            'contact' => 'contact',
            'graduated_on' => '2017-12-12',
            'act_english' => '1',
            'act_science' => '2',
            'act_math' => '3',
            'act_composite' => '4',
            'act_reading' => '5',
            'sat_math' => '400',
            'sat_verbal' => '400',
            'school_id' => $school->id,
        ];
        $res = $this->postJson('/users', $data);
        $res->assertStatus(201);

        //check password
        $res = $this->postJson('/auth/login', ['email'=> $data['email'], 'password'=>$data['password']]);
        $res->assertStatus(200);

        unset($data['password']);
        $this->assertDatabaseHas('users', $data);
    }

    public function testRelationship() {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->postJson('/users', [
            'email' => 'user@example.com',
            'password' => '123456',
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'school_id' => $school->id
        ]);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'school' => [
                        'id' => $school->id,
                        'type' => 'School'
                    ],
                ]
            ],
            'included' => [
                [
                    'type' => 'School',
                    'id' => $school->id,
                ]
            ]
        ]);
    }

    /**
     * @group Invite
     */
    public function testCreateWithEmailInvite() {
        $school = factory(\App\Models\School::class)->create();
        $invitation = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'example',
            'email' => 'user@example.com',
        ]);

        $data = [
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'email' => 'user@example.com',
            'password' => '123456',
            'school_id' => $school->id,
        ];
        $res = $this->postJson('/users', $data);
        $res->assertStatus(201);
        $user = \App\User::where('email', $data['email'])->first();
        $this->assertDatabaseHas('invitations', ['id' => $invitation->id, 'registed_user_id' => $user['id']]);
    }
}
