<?php

namespace Tests\Feature;

use App\Models\CareerCluster;
use App\Models\Job;
use PhpSoft\Activity\Models\Activity;
use Tests\TestCase;

class DashboardMostCareerClusterTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/clusters/most-active', []);
        $res->assertStatus(401);
    }

    public function testMostCareerClusterSuccess()
    {
        $careerClusters = [];
        $careerCluster = factory(\App\Models\CareerCluster::class, 10)->create()->each(function ($careerCluster) use (&$careerClusters) {
            $careerClusters[] = $careerCluster->id;
            }
        );
    
        $idJobs = [];
        foreach ($careerClusters as $value) {
            $idJob = factory(\App\Models\Job::class, 1)->create(['career_cluster_id' => $value, 'status' => 1])->each(function ($idJob) use (&$idJobs) {
                $idJobs[] = $idJob->id;
            });
        }

        foreach ($idJobs as $value) {
            $factory = factory(\PhpSoft\Activity\Models\Activity::class, 10);
            $factory->create(
                [
                    'object_id' => $value,
                    'actor_type' => 'user',
                    'verb' => 'apply',
                    'object_type' => 'Job',
                ]
            );
        }
        
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/clusters/most-active', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
    }
}
