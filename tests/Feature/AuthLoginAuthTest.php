<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Models\Device;

class AuthLoginAuthTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $res = $this->postJson('/auth/login', []);
        $res->assertStatus(400);
    }

    public function testLoginFaild()
    {
        $res = $this->postJson('/auth/login', ['email'=>'abc@gmail.com', 'password'=>'qqqq']);
        $res->assertStatus(401);
        $res->assertJson([
            'errors' => [
                "title"=>"Authenticate Error",
            ]
        ]);
    }

    public function testLoginSuccess()
    {
        $users = factory(\App\User::class,1)->create([
            'email' => 'khanhlp@gmail.com',
            'password'=> bcrypt('12345678'),
        ]);
        $res = $this->postJson('/auth/login', ['email'=>'khanhlp@gmail.com', 'password'=>'12345678']);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);

        $user = \App\User::where(['email' => 'khanhlp@gmail.com'])->first();
        $this->assertDatabaseHas('users', ['email' => $user->email, 'password' => $user->password]);
    }

    public function testLoginSuccessWithDevice()
    {
        $users = factory(\App\User::class,1)->create([
            'email' => 'khanhlp@gmail.com',
            'password'=> bcrypt('12345678'),
        ]);
        $res = $this->postJson('/auth/login', [
            'email' => 'khanhlp@gmail.com',
            'password' => '12345678',
            'device_uuid' => 'uuid-uuid',
            'device_token' => 'token.token',
        ]);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);
        $user = \App\User::where(['email' => 'khanhlp@gmail.com'])->first();
        $this->assertDatabaseHas('users', ['email' => $user->email, 'password' => $user->password]);
        $this->assertDatabaseHas('devices', [
            'token' => 'token.token', 'uuid' => 'uuid-uuid', 'user_id' => $user->id, 'is_logging_in' => true
        ]);
        $res = $this->postJson('/auth/login', [
            'email' => 'khanhlp@gmail.com',
            'password' => '12345678',
            'device_uuid' => 'uuid-uuid',
            'device_token' => 'token.token',
        ]);
        $res->assertStatus(200);
        $this->assertEquals(1, Device::count());
    }
}
