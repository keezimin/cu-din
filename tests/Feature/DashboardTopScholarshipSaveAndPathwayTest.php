<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\BusinessProfile;

class DashboardTopScholarshipSaveAndPathwayTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testGetWithoutToken()
    {
        $res = $this->getJson('statistics/scholarships/total-saving-applying', []);
        $res->assertStatus(401);
    }

    public function testTopScholarshipSuccess()
    {
        $business = $this->getUser('business_owner');
        $profile = $business->profile()->create(['user_id' => $business->id, 'active' => true]);

        $idScholarhips = [];
        $scholarships = factory(\App\Models\Scholarship::class, 5)->create()->each(function ($scholarship) use (&$idScholarhips) {
                $idScholarhips[] = $scholarship->id;
            }
        );

        foreach ($idScholarhips as $key => $id) {
            $number = ($key == 3) ? 6 : $key + 1;
            $factory = factory(\PhpSoft\Activity\Models\Activity::class, $number);
            $verb = (in_array($key, [2, 3])) ? 'apply' : 'save';
            $factory->create(
                [
                    'object_id' => $id,
                    'actor_type' => 'user',
                    'verb' => $verb,
                    'object_type' => 'Scholarship',
                ]
            );
        }

        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->getJson('statistics/scholarships/total-saving-applying', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);
        $res->assertJson(
            [
               'data' => [
                    0 => [
                        'object_id' => $scholarships[3]->id,
                        'name' => $scholarships[3]->name,
                        'save' => 0,
                        'applying' => 6
                    ],
                    1 => [
                        'object_id' => $scholarships[4]->id,
                        'name' => $scholarships[4]->name,
                        'save' => 5,
                        'applying' => 0
                    ],
                    2 => [
                        'object_id' => $scholarships[2]->id,
                        'name' => $scholarships[2]->name,
                        'save' => 0,
                        'applying' => 3
                    ],
                    3 => [
                        'object_id' => $scholarships[1]->id,
                        'name' => $scholarships[1]->name,
                        'save' => 2,
                        'applying' => 0
                    ],
                    4 => [
                        'object_id' => $scholarships[0]->id,
                        'name' => $scholarships[0]->name,
                        'save' => 1,
                        'applying' => 0
                    ]
                ]
            ]
        );
    }
}
