<?php

namespace Tests\Feature;

use Tests\TestCase;

class SchoolDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->deleteJson('/schools/' . $school->id);
        $res->assertStatus(401);
    }

    public function testNotFoundSchool()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/schools/122', [], $headers);
        $res->assertStatus(404);
    }

    public function testNotAcceptDelete()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $school = factory(\App\Models\School::class)->create();
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password' => bcrypt('12345678'),
            'school_id' => $school->id,
        ]);
        $res = $this->deleteJson('/schools/' . $school->id, [], $headers);
        $res->assertStatus(403);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $school = factory(\App\Models\School::class)->create();
        $res = $this->deleteJson('/schools/' . $school->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('schools', ['id' => $school->id]);
    }
}
