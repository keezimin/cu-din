<?php

namespace Tests\Feature;

use Tests\TestCase;

class DistrictMultiDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $district = factory(\App\Models\District::class)->create();
        $res = $this->deleteJson('/districts', ['id' => [$district->id]]);
        $res->assertStatus(401);
    }

    public function testNotFoundDistrict()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/districts', ['id' => ['122']], $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete District. District is not found',
                        'meta' => [
                            'id' => '122',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $district1 = factory(\App\Models\District::class)->create();
        $district2 = factory(\App\Models\District::class)->create();
        $res = $this->deleteJson('/districts/', ['id' => [$district1->id, $district2->id]], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('districts', ['id' => [$district1->id, $district2->id]]);
    }

    public function testMultiWithNotAccept()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $district1 = factory(\App\Models\District::class)->create();
        $district2 = factory(\App\Models\District::class)->create();
        $school = factory(\App\Models\School::class)->create([
            'name' => 'example.school',
            'district_id' => $district1->id,
        ]);
        $res = $this->deleteJson('/districts/', ['id' => [$district1->id, $district2->id]], $headers);
        $res->assertStatus(200);
        $this->assertDatabaseMissing('districts', ['id' => $district2->id]);
        $this->assertDatabaseHas('schools', ['id' => $school->id, 'district_id' => $district1->id]);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete District. District has linked with schools',
                        'meta' => [
                            'id' => $district1->id,
                        ],
                    ],
                ],
            ],
        ]);
    }
}
