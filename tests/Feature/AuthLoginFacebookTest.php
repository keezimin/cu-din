<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Models\Device;

class AuthLoginFacebookTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $res = $this->postJson('/auth/facebook', []);
        $res->assertStatus(400);
    }

    public function testLoginWithoutEmail()
    {
        \AuthSocial::shouldReceive('getProfile')
            ->with('facebook', 'token')
            ->once()
            ->andReturn((object)[
                'id' => 123,
                'email' => null,
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', ['token' => 'token']);
        $res->assertStatus(401);
        $res->assertJson([
            'errors' => [
                [
                    'detail' => 'Invalid email',
                ]
            ]
        ]);
    }

    public function testLoginSuccess()
    {
        \Socialite::shouldReceive('driver->userFromToken')
            ->once()
            ->andReturn((object)[
                'id' => 123456,
                'email' => 'email@example.com',
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', ['token' => 'token']);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);
        $user = \App\User::where(['email' => 'email@example.com'])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertDatabaseHas('socials', [
            'social_id' => 123456, 'social_name' => 'facebook', 'user_id' => $user->id
        ]);
        $this->assertEquals(1, $user->socials()->count());
        $this->assertEquals(0, Device::count());
    }

    public function testReLoginSuccess()
    {
        \Socialite::shouldReceive('driver->userFromToken')
            ->once()
            ->andReturn((object)[
                'id' => 123456,
                'email' => 'email@example.com',
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', ['token' => 'token']);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);
        $user = \App\User::where(['email' => 'email@example.com'])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertDatabaseHas('socials', [
            'social_id' => 123456, 'social_name' => 'facebook', 'user_id' => $user->id
        ]);
        $this->assertEquals(1, $user->socials()->count());
        $this->assertEquals(0, Device::count());

        \Socialite::shouldReceive('driver->userFromToken')
            ->once()
            ->andReturn((object)[
                'id' => 123456,
                'email' => 'email@example.com',
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', ['token' => 'token']);
        $res->assertStatus(200);
        $this->assertEquals($user->toArray(), \App\User::where(['email' => 'email@example.com'])->first()->toArray());
        $this->assertEquals(1, $user->socials()->count());
        $this->assertEquals(0, Device::count());
    }

    public function testLoginSuccessWithDecice()
    {
        \Socialite::shouldReceive('driver->userFromToken')
            ->once()
            ->andReturn((object)[
                'id' => 123456,
                'email' => 'email@example.com',
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', [
            'token' => 'token',
            'device_uuid' => 'uuid-uuid',
            'device_token' => 'token.token',
        ]);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);
        $user = \App\User::where(['email' => 'email@example.com'])->first();

        $this->assertDatabaseHas('users', ['email' => $user->email]);
        $this->assertDatabaseHas('socials', [
            'social_id' => 123456, 'social_name' => 'facebook', 'user_id' => $user->id
        ]);
        $this->assertDatabaseHas('devices', [
            'token' => 'token.token', 'uuid' => 'uuid-uuid', 'user_id' => $user->id
        ]);
        $this->assertEquals(1, $user->socials()->count());
        $this->assertEquals(1, Device::count());
    }

    public function testLoginWithReadyUser()
    {
        $user = factory(\App\User::class)->create();

        \Socialite::shouldReceive('driver->userFromToken')
            ->once()
            ->andReturn((object)[
                'id' => 123456,
                'email' => $user->email,
                'name' => 'name',
            ]);
        $res = $this->postJson('/auth/facebook', ['token' => 'token']);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'type' => 'Token',
            ]
        ]);
    }
}
