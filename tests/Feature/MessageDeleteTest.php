<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PhpSoft\Activity\Models\Activity;

class MessageDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = $this->getUser();
        $bread = app()->make('\App\Repositories\Eloquent\MessageBread');
        $users = factory(\App\User::class)->create();
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];

        $messageId = $bread->add($data, $user)->id;

        $res = $this->deleteJson('/messages/'. $messageId, []);
        $res->assertStatus(401);
    }

    public function testNotFoundMessage()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $res = $this->deleteJson('/messages/122', [], $headers);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $bread = app()->make('\App\Repositories\Eloquent\MessageBread');
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $users = factory(\App\User::class, 2)->create();

        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];
        $messageId = $bread->add($data, $user)->id;
        $messageId2 = $bread->add($data, $user)->id;

        $this->assertEquals(6, Activity::count());
        $this->assertDatabaseHas('activities', ['id' => $messageId + 1, 'object_id' => $messageId]);
        $this->assertDatabaseHas('activities', ['id' => $messageId + 2, 'object_id' => $messageId]);

        $res = $this->deleteJson('/messages/' . $messageId, [], $headers);

        $this->assertDatabaseMissing('activities', ['id' => $messageId]);
        $this->assertDatabaseMissing('activities', ['id' => $messageId + 1]);
        $this->assertDatabaseMissing('activities', ['id' => $messageId + 2]);

        $this->assertDatabaseHas('activities', ['id' => $messageId2]);
        $this->assertDatabaseHas('activities', ['id' => $messageId2 + 1, 'object_id' => $messageId2]);
        $this->assertDatabaseHas('activities', ['id' => $messageId2 + 2, 'object_id' => $messageId2]);
    }
}
