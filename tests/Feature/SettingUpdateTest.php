<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->postJson('/settings', ['data' => 'setting'], $headers);
        $res->assertStatus(400);
    }

    public function testUnauthenticate()
    {
        $data = ['data' => ['app_name' => 'app-example']];
        $res = $this->postJson('/settings', $data, []);
        $res->assertStatus(401);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['data' => ['app_name' => 'app-example']];
        $res = $this->postJson('/settings', $data, $headers);
        $res->assertStatus(204);
        $this->assertDatabaseHas('settings', ['key' => 'app_name', 'value' => 'app-example']);
    }
}
