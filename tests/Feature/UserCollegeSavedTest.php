<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserCollegeSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->postJson('/users/1/colleges/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/colleges/2');
        $res->assertStatus(401);
    }

    public function testPostSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $college = factory(\App\Models\College::class)->create(['code' => 21, 'name' => 'Hello']);
        $user = \JWTAuth::toUser($token);
        $res = $this->postJson('/users/' . $user->id . '/colleges/' . $college->code, ['college_code' => $college->code, 'college_name' => $college->name], $headers);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id,
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'save',
                    'object' => [
                        'id' => $college->id,
                    ],
                    'object_id' => $college->id,
                    'object_type' => 'College',
                ],
            ],
        ]);
    }
}
