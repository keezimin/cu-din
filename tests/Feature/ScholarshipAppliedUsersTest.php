<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScholarshipAppliedUsersTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundScholarship()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/scholarships/2/users');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $scholarship->schools()->attach($user->school_id);
        $res = $this->getJson('/scholarships/' . $scholarship->id . '/users');
        $res->assertStatus(401);
    }

    public function testGetSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $scholarship->schools()->attach($user->school_id);
        $users = factory(\App\User::class, 15)->create();
        foreach($users as $u) {
            factory(\PhpSoft\Activity\Models\Activity::class)->create([
                'verb' => 'apply',
                'actor_type' => 'User',
                'actor_id' => $u->id,
                'object_type' => 'Scholarship',
                'object_id' => $scholarship->id,
            ]);
            factory(\PhpSoft\Activity\Models\Activity::class)->create();
        }
        factory(\App\User::class, 5)->create();

        $res = $this->getJson('/scholarships/' . $scholarship->id . '/users', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(10, count($content['data']));
        $this->assertEquals(15, $content['meta']['totalObjects']);
    }
}
