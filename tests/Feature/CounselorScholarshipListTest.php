<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Models\Scholarship;
use \App\User;
use \PhpSoft\Activity\Models\Activity;

class CounselorScholarshipListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $scholarship = factory(Scholarship::class)->create();
        $res = $this->getJson('/counselors/' . 10 . '/scholarships');
        $res->assertStatus(404);
    }

    public function testListScholarshipUnauthenticate()
    {
        $user = factory(User::class)->create();
        $scholarship = factory(Scholarship::class)->create(['user_id' => $user->id]);
        $res = $this->getJson('/counselors/' . $user->id . '/scholarships');
        $res->assertStatus(401);
    }

    public function testListScholarshipSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = factory(User::class)->create();
        $scholarship = factory(Scholarship::class)->create(['user_id' => $user->id]);
        $res = $this->getJson('/counselors/' . $user->id . '/scholarships', $headers);
        $res->assertStatus(200);
    }

    public function testListScholarshipWithPagination()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = factory(User::class)->create();
        $scholarship = factory(Scholarship::class, 2)->create(['user_id' => $user->id]);

        $res = $this->getJson('/counselors/' . $user->id . '/scholarships?page[number]=1&page[size]=2', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/counselors/' . $user->id . '/scholarships?page[number]=1&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/' . $user->id . '/scholarships?page[number]=2&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/' . $user->id . '/scholarships?page[number]=3&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }
}
