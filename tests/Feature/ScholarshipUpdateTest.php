<?php

namespace Tests\Feature;

use Tests\TestCase;

class ScholarshipUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/scholarships', [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateFail()
    {
        $token = $this->getToken();
        $res = $this->patchJson('/schools/122', ['name' => 'example.school'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testUpdateSuccess()
    {
        $school = factory(\App\Models\School::class)->create();
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => $school->id,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);
        $scholarship = factory(\App\Models\Scholarship::class)->create(['user_id' => $user->id]);
        $scholarship->schools()->attach($user->school_id);
        $state = factory(\App\Models\State::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $data = [
            'name'                      => 'example.scholarship',
            'organization'              => 'example.organization',
            'deadline_on'               => '2017-12-12',
            'description'               => 'Scholarship.description',
            'application_form_file'     => 'example.application_form_file',
            'application_form_filename' => 'name',
            'application_form_link'     => 'example.application_form_link',
            'state_id'                  => $state->id,
        ];
        $res = $this->patchJson('/scholarships/' . $scholarship->id,
            array_merge($data, ['school_ids' => [$user->school_id], 'district_ids' => [$district->id]]),
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(200);
        $this->assertDatabaseHas('scholarships', $data);
    }

    public function testUpdateWithId()
    {
        $people = factory(\App\User::class)->create();
        $school = factory(\App\Models\School::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => $school->id,
        ]);
        $token = $this->fromUser($user);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();

        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);

        $scholarship = factory(\App\Models\Scholarship::class)->create([
            'name' => 'scholarship',
            'user_id' => $user->id,
        ]);
        $scholarship->schools()->attach($user->school_id);

        $res = $this->patchJson('/scholarships/' . $scholarship->id,
            ['id' => '123', 'name' => 'example.school', 'user_id' => $people->id, 'school_ids' => [$school->id], 'district_ids' => [$district->id]],
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $res->assertStatus(200);
        $this->assertDatabaseHas('scholarships', ['id' => $scholarship->id, 'name' => 'example.school', 'user_id' => $user->id]);
    }

    public function testUpdateCounselorWithId()
    {
        $school = factory(\App\Models\School::class)->create();
        $district = factory(\App\Models\District::class)->create();
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => $school->id,
        ]);

        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);

        $token = $this->fromUser($user);

        $scholarship = factory(\App\Models\Scholarship::class)->create([
            'name' => 'scholarship',
            'user_id' => $user->id,
        ]);
        $scholarship->schools()->attach($user->school_id);

        $res = $this->patchJson('/scholarships/' . $scholarship->id,
            ['id' => '123', 'name' => 'example.school', 'user_id' => '111', 'school_ids' => [$school->id], 'district_ids' => [$district->id]],
            ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('scholarships', ['id' => $scholarship->id, 'name' => 'example.school', 'user_id' => $user->id]);
    }

    public function testUpdateWithoutToken()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->patchJson('/scholarships/' . $scholarship->id, []);
        $res->assertStatus(401);
    }
}
