<?php

namespace Tests\Feature;

use \App\User;
use Tests\TestCase;
use \App\Models\Job;
use \PhpSoft\Activity\Models\Activity;

class JobDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailSuccess()
    {
        $token = $this->getToken();
        $Job = factory(\App\Models\Job::class)->create();
        $res = $this->getJson('/jobs/' . $Job->id, ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
    }

    public function testNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/jobs/0', ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(404);
    }

    public function testCreateWithoutToken()
    {
        $Job = factory(\App\Models\Job::class)->create();
        $res = $this->getJson('/jobs/' . $Job->id, []);
        $res->assertStatus(401);
    }

    public function testScholarshipWithActivitiesAsStudent()
    {
        $owner = factory(User::class)->create();
        $Job = factory(Job::class)->create(['user_id' => $owner->id]);
        factory(Job::class)->create(['user_id' => $owner->id]);
        // counselor save
        $ownerAct = factory(Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $owner->id, 'object_type' => 'Job', 'object_id' => $Job->id]);

        // student save and apply
        $user = factory(User::class)->create();
        $saveAct = factory(Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Job', 'object_id' => $Job->id]);
        $applyAct = factory(Activity::class)->create(['verb' => 'apply', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Job', 'object_id' => $Job->id]);

        //request as student
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $res = $this->getJson('/jobs/' . $Job->id, $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'user' => [
                        'id' => $owner->id,
                        'type' => 'User',
                    ],
                    'activities' => [
                        [
                            'id' => $saveAct->id,
                            'type' => 'Activity',
                        ],
                        [
                            'id' => $applyAct->id,
                            'type' => 'Activity',
                        ],
                    ],
                ],
            ],
            'included' => [
                [
                    'id' => $owner->id,
                    'type' => 'User',
                ],
                [
                    'id' => $saveAct->id,
                    'type' => 'Activity',
                ],
                [
                    'id' => $applyAct->id,
                    'type' => 'Activity',
                ],
            ],
        ]);
    }
}
