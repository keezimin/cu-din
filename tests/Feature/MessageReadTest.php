<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PhpSoft\Activity\Models\Activity;

/**
 * @group Message
 */
class MessageReadTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->getJson('/messages/2', []);
        $res->assertStatus(401);
    }

    public function testNotFound()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->getJson('/messages/2', $headers);
        $res->assertStatus(404);
    }

    public function testReadSuccess()
    {
        $bread = app()->make('\App\Repositories\Eloquent\MessageBread');;

        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 2)->create();
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];
        $bread->add($data, $user)->id;

        // creator read
        $res = $this->getJson('/messages/1', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'sender' => ['id' => $user->id, 'type' => 'User'],
                ]
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
            ]
        ]);
        $res->assertJsonMissing([
            'data' => [
                'relationships' => [
                    'message' => ['id' => '1', 'type' => 'Message'],
                ]
            ],
            'included' => [
                ['id' => '1', 'type' => 'Message', 'attributes' => ['id' => '1']],
            ]
        ]);
        $content = json_decode($res->getContent(), true);
        $this->assertTrue(!empty($content['data']['attributes']['meta']['target']));

        // receiver read
        $res = $this->getJson('/messages/2', $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'data' => [
                'relationships' => [
                    'sender' => ['id' => $user->id, 'type' => 'User'],
                    'message' => ['id' => '1', 'type' => 'Message'],
                ]
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
                ['id' => $users[0]->id, 'type' => 'User', 'attributes' => ['id' => $users[0]->id]],
                ['id' => $users[1]->id, 'type' => 'User', 'attributes' => ['id' => $users[1]->id]],
                ['id' => '1', 'type' => 'Message', 'attributes' => ['id' => '1']],
            ]
        ]);
        $content = json_decode($res->getContent(), true);
        $this->assertTrue(empty($content['data']['attributes']['meta']['target']));

        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->getJson('/messages/2', $headers);
        $res->assertStatus(404);
    }
}
