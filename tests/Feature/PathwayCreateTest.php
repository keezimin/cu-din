<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Pathway;

class PathwayCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/pathways', []);
        $res->assertStatus(401);
    }

    public function testAuthorization()
    {
        $user = $this->getUser('teacher');
        $token = $this->fromUser($user);
        $res = $this->postJson('/pathways', ['email' => 'counselor@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $res = $this->postJson('/pathways', ['email' => 'counselor@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testMaxPathway()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $careerCluster = factory(\App\Models\CareerCluster::class)->create();
        $credential = [
            'career_cluster_id' => $careerCluster->id, 
            'name' => 'example name',
            'short_description' => 'example description',
            'user_id' => $user->id,
            'active' => 1,
            'about' => [
                [
                    'type' => 'image',
                    'content' => 'content'     
                ]
            ]
        ];
        for ($i = 1; $i <= 4; $i++) {
            $res = $this->postJson('/pathways', $credential, $headers);
            if ($i <= 3) {
                $res->assertStatus(201);
            }
            if ($i == 4) {
                $res->assertStatus(400);
            }                
        }    
    }

    public function testCreateSuccess()
    {
        $user = $this->getUser('business_owner');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $careerCluster = factory(\App\Models\CareerCluster::class)->create();
        $credential = [
            'career_cluster_id' => $careerCluster->id, 
            'name' => 'example name',
            'short_description' => 'example description',
            'user_id' => $user->id,
            'active' => 1,
            'about' => [
                [
                    'type' => 'image',
                    'content' => 'content'     
                ]
            ]
        ];
        $res = $this->postJson('/pathways', $credential, $headers);
        $res->assertStatus(201);
        $attributes = self::getAttributes($res);
        $this->assertEquals($credential['name'], $attributes->name);
        $id = self::getId($res);
        $this->assertDatabaseHas('pathways', 
            [
                'id' => (int)$id, 
                'name' => $credential['name'], 
                'career_cluster_id' => (int)$careerCluster->id,
                'short_description' => $credential['short_description'],
                'active' => 1
            ]
        );
    }
}
