<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailSuccess()
    {
        $token = $this->getToken();
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->getJson('/organizations/'. $organization->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
    }

    public function testNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/organizations/122', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testCreateWithoutToken()
    {
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->getJson('/organizations/'. $organization->id, []);
        $res->assertStatus(401);
    }
}
