<?php

namespace Tests\Feature;

use Tests\TestCase;

class SchoolMultiDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $school = factory(\App\Models\School::class)->create();
        $res = $this->deleteJson('/schools', ['id' => [$school->id]]);
        $res->assertStatus(401);
    }

    public function testNotFoundDistrict()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/schools', ['id' => ['122']], $headers);
        $res->assertStatus(200);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete School. School is not found',
                        'meta' => [
                            'id' => '122',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $school1 = factory(\App\Models\School::class)->create();
        $school2 = factory(\App\Models\School::class)->create();
        $res = $this->deleteJson('/schools', ['id' => [$school1->id, $school2->id]], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('schools', ['id' => [$school1->id, $school2->id]]);
    }

    public function testMultiWithNotAccept()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $school1 = factory(\App\Models\School::class)->create();
        $school2 = factory(\App\Models\School::class)->create();
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'school_id' => $school1->id,
        ]);
        $res = $this->deleteJson('/schools', ['id' => [$school1->id, $school2->id]], $headers);
        $res->assertStatus(200);
        $this->assertDatabaseMissing('schools', ['id' => $school2->id]);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'school_id' => $school1->id]);
        $res->assertJson([
            'meta' => [
                'errors' => [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete School. School has linked with users',
                        'meta' => [
                            'id' => $school1->id,
                        ],
                    ],
                ],
            ],
        ]);
    }
}
