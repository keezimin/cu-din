<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Models\Device;

class AuthLogoutTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testLogoutFail() {
        $res = $this->postJson('/auth/logout', [], []);
        $res->assertStatus(401);
    }

    public function testLogoutSuccess()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $token = $this->fromUser($user);
        $res = $this->postJson('/auth/logout', [], ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(204);
    }

    public function testLogoutSuccessWithDecice() {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $device = factory(Device::class)->create([
            'user_id' => $user->id,
            'uuid' => 'uuid-uuid',
            'token' => 'abc',
            'is_logging_in'=> true,
        ]);
        $token = $this->fromUser($user);
        $res = $this->postJson('/auth/logout', ['device_uuid' => $device->uuid], ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(204);
        $device->refresh();
        $this->assertEquals($device->is_logging_in, 0);
    }
}
