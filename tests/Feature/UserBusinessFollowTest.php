<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserBusinessFollowTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->postJson('/users/1/follow/businesses/2');
        $res->assertStatus(404);
    }

    public function testNotFoundBusiness()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->postJson('/users/' . $user->id . '/follow/businesses/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = $this->getUser('student');
        $business = $this->getUser('business');
        $res = $this->postJson('/users/' . $user->id . '/follow/businesses/' . $business->id);
        $res->assertStatus(401);
    }

    public function testPostSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $business = $this->getUser('business');
        $res = $this->postJson('/users/' . $user->id . '/follow/businesses/' . $business->id, [], $headers);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                'type' => 'Activity',
                'attributes' => [
                    'actor' => [
                        'id' => $user->id
                    ],
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'verb' => 'follow',
                    'object' => [
                        'id' => $business->id
                    ],
                    'object_id' => $business->id,
                    'object_type' => 'User',
                ],
            ]
        ]);
        $id = $this->getId($res);
        $this->assertDatabaseHas('activities', [
            'id' => $id,
            'actor_id' => $user->id,
            'actor_type' => 'User',
            'object_id' => $business->id,
            'object_type' => 'User',
        ]);
    }
}
