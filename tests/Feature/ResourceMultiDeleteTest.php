<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceMultiDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->deleteJson('/resources', ['id' => [$resource->id]]);
        $res->assertStatus(401);
    }

    public function testNotFoundResource()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);

        $resource = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $res = $this->deleteJson('/resources', ['id' => [$resource->id, 2]], $headers);
        $res->assertStatus(200);
        $this->assertDatabaseMissing('resources', ['id' => [$resource->id]]);
        $res->assertJson([
            'meta' => [
                'errors'=> [
                    [
                        'title' => 'Bad Request',
                        'detail' => 'Can\'t delete Resource. Resource is not found',
                        'meta' =>[
                            'id' => '2'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testMultiDeleteSuccess()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'example.school',
            'email' => 'user@gmail.com',
            'school_id' => null,
        ]);
        $user = \App\User::where('email', '=', 'user@gmail.com')->first();
        $role_counselor = \PhpSoft\Users\Models\Role::firstOrCreate(['name' => 'counselor']);
        // attach roles
        $user->attachRole($role_counselor);
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        $resource1 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $resource2 = factory(\App\Models\Resource::class)->create(['owner_id' => $user->id]);
        $res = $this->deleteJson('/resources', ['id' => [$resource1->id, $resource2->id]], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('resources', ['id' => [$resource1->id, $resource2->id]]);
    }
}
