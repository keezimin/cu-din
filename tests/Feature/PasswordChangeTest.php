<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PasswordChangeTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $token = $this->getToken();
        $res = $this->postJson('/password/change', [], ['HTTP_Authorization' => 'Bearer '.$token]);
        $res->assertStatus(400);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                ]
            ]
        ]);

    }

    public function testChangePassFail()
    {
        $res = $this->postJson('/password/change', ['current_password' => '12345678', 'password' => '123456', 'confirm_password' => '123456'], []);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Authenticate Error',
                ]
            ]
        ]);
        $res->assertStatus(401);
    }

    public function testChangePassSuccess()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password'=> bcrypt('12345678'),
        ]);
        $token = $this->fromUser($user);
        $this->postJson('/password/change', ['current_password' => '12345678', 'password' => '123456', 'confirm_password' => '123456'], ['HTTP_Authorization' => 'Bearer '.$token]);
        $res = $this->postJson('/auth/login', ['email'=> $user->email, 'password'=>'123456']);
        $res->assertStatus(200);
    }
}
