<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationGetSchoolTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testAuthorization()
    {
        $user = $this->getUser('teacher');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $organization = factory(\App\Models\Organization::class)->create();

        $res = $this->getJson('/organizations/'. $organization->id. '/schools', $headers);
        $res->assertStatus(403);
    }

    public function testListSchoolByOrganizationSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        $state1 = factory(\App\Models\State::class)->create();
        $state2 = factory(\App\Models\State::class)->create();

        $district1 = factory(\App\Models\District::class)->create([
            'state_id' => $state1->id
        ]);
        $district2 = factory(\App\Models\District::class)->create([
            'state_id' => $state2->id
        ]);

        $organization = factory(\App\Models\Organization::class)->create([
            'district_id' => $district1->id
        ]);

        factory(\App\Models\School::class, 3)->create(
            [
                'district_id' => $district1->id,
                'organization_id' => $organization->id
            ]
        );
        factory(\App\Models\School::class, 2)->create(
            [
                'district_id' => $district2->id,
                'organization_id' => $organization->id
            ]
        );

        $res = $this->getJson('/organizations/'. $organization->id. '/schools', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 5);

        $res = $this->getJson('/organizations/'. $organization->id. '/schools?search[county][id][]='. $district1->id. '&search[county][operator]==', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 3);

        $res = $this->getJson('/organizations/'. $organization->id. '/schools?search[county][id][]='. $district1->id. '&search[county][operator]=<>', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/organizations/'. $organization->id. '/schools?search[state][id][]='. $state1->id. '&search[state][operator]==', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 3);

        $res = $this->getJson('/organizations/'. $organization->id. '/schools?search[state][id][]='. $state1->id. '&search[state][operator]=<>', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);
    }
}
