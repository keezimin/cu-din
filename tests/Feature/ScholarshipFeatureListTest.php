<?php

namespace Tests\Feature;

use Tests\TestCase;
use \App\Models\District;

class ScholarshipFeatureListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testListScholarshipUnauthenticated()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->getJson('/features/scholarships');
        $res->assertStatus(401);
    }

    public function testListScholarshipSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->getJson('/features/scholarships', $headers);
        $res->assertStatus(200);
    }

    public function testListFeatureScholarships()
    {
        // - có school trùng với school của logging student
        // - nếu có apply district và district của school trùng với district của school của logging student
        $user = factory(\App\User::class)->create();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $expected = [];

        // case is_applied_to_state true
        $state = factory(\App\Models\State::class)->create();
        $district = District::find($user->school->district_id);
        $state = $district->state()->create(['name' => 'State Example']);
        $district->update(['state_id' => $state->id]);
        $s = factory(\App\Models\Scholarship::class)->create(['is_applied_to_state' => true, 'state_id' => $user->school->district->state_id]);
        array_push($expected, $s->id);

        // owner create
        $s = factory(\App\Models\Scholarship::class)->create(['is_applied_to_district' => false, 'user_id' => $user->id]);
        $s->schools()->attach($user->school_id);
        array_push($expected, $s->id);
        $s = factory(\App\Models\Scholarship::class)->create(['is_applied_to_district' => true, 'user_id' => $user->id]);
        $s->schools()->attach($user->school_id);
        array_push($expected, $s->id);
        // create one expired
        factory(\App\Models\Scholarship::class)->create([
            'is_applied_to_district' => true,
            'user_id' => $user->id,
            'deadline_on' => \Carbon\Carbon::yesterday(),
        ])->each(function ($sl) use (&$user) {
            $sl->schools()->attach($user->school_id);
        });

        // other create
        factory(\App\Models\Scholarship::class, 2)->create(['is_applied_to_district' => false]);
        factory(\App\Models\Scholarship::class, 3)->create(['is_applied_to_district' => true]);

        // other create
        $s = factory(\App\Models\Scholarship::class, 4)->create(['is_applied_to_district' => false])
            ->each(function ($sl) use (&$user) {
                $sl->schools()->attach($user->school_id);
            });
        $expected = array_merge($expected, array_pluck($s, 'id'));
        $s = factory(\App\Models\Scholarship::class, 5)->create(['is_applied_to_district' => true])
            ->each(function ($sl) use (&$user) {
                $sl->schools()->attach($user->school_id);
            });
        $expected = array_merge($expected, array_pluck($s, 'id'));
        $s = factory(\App\Models\Scholarship::class, 2)->create(['is_applied_to_district' => true, 'deadline_on' => date('Y-m-d', strtotime('-1 month'))])
            ->each(function ($sl) use (&$user) {
                $sl->schools()->attach($user->school_id);
            });

        // other create
        factory(\App\Models\District::class, 5)->create()->each(function ($d) use ($user, &$expected) {
            factory(\App\Models\School::class, 2)->create(['district_id' => $d->id])->each(function ($s) use ($user, &$expected) {
                $ss = factory(\App\Models\Scholarship::class, 2)->create()
                    ->each(function ($sl) use ($user) {
                        $sl->schools()->attach($user->school_id);
                    });
                $expected = array_merge($expected, array_pluck($ss, 'id'));
            });
        });

        // same district, is_applied_to_district true
        factory(\App\Models\School::class, 3)->create(['district_id' => $user->school->district_id])->each(function ($s) use (&$expected) {
            $ss = factory(\App\Models\Scholarship::class, 3)->create(['is_applied_to_district' => true])
                ->each(function ($sl) use (&$s) {
                    $sl->schools()->attach($s->id);
                });
        });

        // same district, is_applied_to_district false
        factory(\App\Models\School::class, 3)->create(['district_id' => $user->school->district_id])->each(function ($s) {
            factory(\App\Models\Scholarship::class, 2)->create(['is_applied_to_district' => false])
                ->each(function ($sl) use (&$s) {
                    $sl->schools()->attach($s->id);
                });
        });
        $res = $this->getJson('/features/scholarships', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(32, $content['meta']['totalObjects']);

        $this->assertEquals(array_slice(array_reverse($expected), 0, 10), array_pluck($content['data'], 'id'));

        $res = $this->getJson('/features/scholarships?page[number]=2', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(32, $content['meta']['totalObjects']);
        $this->assertEquals(array_slice(array_reverse($expected), 10, 10), array_pluck($content['data'], 'id'));

        $res = $this->getJson('/features/scholarships?page[number]=3', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(32, $content['meta']['totalObjects']);
        $this->assertEquals(array_slice(array_reverse($expected), 20, 10), array_pluck($content['data'], 'id'));

        $res = $this->getJson('/features/scholarships?page[number]=4', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(32, $content['meta']['totalObjects']);
        $this->assertEquals(array_slice(array_reverse($expected), 30, 10), array_pluck($content['data'], 'id'));

        $res = $this->getJson('/features/scholarships?page[number]=5', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(32, $content['meta']['totalObjects']);
        $this->assertEquals(0, count($content['data']));
    }
}
