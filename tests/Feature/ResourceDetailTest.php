<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourceDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testDetailResourceNotfoundId()
    {
        $token = $this->getToken();
        $res = $this->getJson('/resources/2', ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(404);
    }

    public function testDetailResourceWithoutToken()
    {
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->getJson('/resources/'. $resource->id, []);
        $res->assertStatus(401);
    }

    public function testDetailResourceSuccess()
    {
        $token = $this->getToken();
        $resource = factory(\App\Models\Resource::class)->create();
        $res = $this->getJson('/resources/'. $resource->id, ['HTTP_Authorization' => 'Bearer '. $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('resources', [
            'id' => $resource->id, 
            'title' => $resource->title,
            'link' => $resource->link,
            'image' => $resource->image,
            'type' => $resource->type,
        ]);
    }
}
