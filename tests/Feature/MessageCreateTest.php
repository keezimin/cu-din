<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Queue;
use PhpSoft\Activity\Models\Activity;

/**
 * @group Message
 */
class MessageCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/messages', []);
        $res->assertStatus(401);
    }

    public function testInvalidTarget()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => 'USER'];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'target',
                    ],
                ],
            ],
        ]);
    }

    public function testInvalidTargetType()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => ['type' => 'Fake']];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'target.type',
                    ],
                ],
            ],
        ]);
    }

    public function testInvalidContent()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => ['type' => 'USER', 'id' => [1]]];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'content',
                    ],
                ],
            ],
        ]);
    }

    public function testSuccessSendUser()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 2)->create();
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => 'hi'];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(3, Activity::count());
        $res->assertJson([
            'data' => [
                "type" => "Message",
                "id" => "1",
                "attributes" => [
                    'id' => "1",
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => "0",
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
            ],
        ]);
    }

    public function testSuccessSendAllUser()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        factory(\App\User::class, 4)->create();
        $data = ['target' => ['type' => 'USER_ALL'], 'content' => 'hi'];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(5, Activity::count());
        $res->assertJson([
            'data' => [
                "type" => "Message",
                "id" => "1",
                "attributes" => [
                    'id' => "1",
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => "0",
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
            ],
        ]);
    }

    public function testSuccessSendSchool()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        factory(\App\User::class, 4);
        factory(\App\User::class, 4)->create(['school_id' => $user->school_id]);
        $data = ['target' => ['type' => 'SCHOOL', 'id' => [$user->school_id]], 'content' => 'hi'];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(5, Activity::count());
        $res->assertJson([
            'data' => [
                "type" => "Message",
                "id" => "1",
                "attributes" => [
                    'id' => "1",
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => "0",
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
            ],
        ]);
    }

    public function testSuccessSendDistrict()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        factory(\App\User::class, 4);
        $districts = factory(\App\Models\District::class, 3)->create()->each(function ($d) {
            factory(\App\Models\School::class, 3)->create(['district_id' => $d->id])->each(function ($s) {
                factory(\App\User::class, 4)->create(['school_id' => $s->id]);
            });
        });

        $data = ['target' => ['type' => 'DISTRICT', 'id' => [$districts[0]->id]], 'content' => 'hi'];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(13, Activity::count());
        $res->assertJson([
            'data' => [
                "type" => "Message",
                "id" => "1",
                "attributes" => [
                    'id' => "1",
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => "0",
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
            ],
        ]);
    }

    public function testSuccessSendScholarshipWithoutContent()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 4)->create();
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create(['status' => 1])
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });

        $data = [
            'target' => ['type' => 'USER', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[0]->id]],
        ];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
    }

    public function testSuccessSendScholarshipWithEmptyContent()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 4)->create();
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create(['status' => 1])
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });

        $data = [
            'target' => ['type' => 'USER', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[0]->id]],
            'content' => '',
        ];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
    }

    public function testSuccessSendScholarship()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 4)->create();
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create(['status' => 1])
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });

        $data = [
            'target' => ['type' => 'USER', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[0]->id]],
            'content' => 'hi',
        ];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(3, Activity::count());
        $data['title'] = '{{SENDER}} has sent you a scholarship to review. {{SCHOLARSHIP}}';
        $res->assertJson([
            'data' => [
                'type' => 'Message',
                'id' => '1',
                'attributes' => [
                    'id' => '1',
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => '0',
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
                'relationships' => [
                    'sources' => [
                        ['id' => $scholarships[0]->id, 'type' => 'Scholarship'],
                    ],
                ],
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
                ['id' => $users[0]->id, 'type' => 'User', 'attributes' => ['id' => $users[0]->id]],
                ['id' => $users[1]->id, 'type' => 'User', 'attributes' => ['id' => $users[1]->id]],
                ['id' => (string) $scholarships[0]->user_id, 'type' => 'User', 'attributes' => ['id' => (string) $scholarships[0]->user_id]],
                ['id' => $scholarships[0]->id, 'type' => 'Scholarship', 'attributes' => ['id' => $scholarships[0]->id]],
            ],
        ]);
    }

    public function testSuccessSendScholarshipAll()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        factory(\App\User::class, 4)->create();
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create(['status' => 1])
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });
        $scholarships->each(function ($s) use (&$users) {
            $users = factory(\App\User::class, 4)->create(['school_id' => $s->d]);
            $users->each(function ($u) use ($s) {
                \App\Services\UserActionService::applyScholarship($u, $s);
            });
        });
        $data = [
            'target' => ['type' => 'USER_ALL', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[1]->id]],
            'content' => 'hi',
        ];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(5, Activity::where('object_type', 'Message')->count());
        $data['title'] = '{{SENDER}} has sent you a scholarship to review. {{SCHOLARSHIP}}';
        unset($data['target']['id']);
        $res->assertJson([
            'data' => [
                'type' => 'Message',
                'id' => '17',
                'attributes' => [
                    'id' => '17',
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => '0',
                    'object_type' => 'Message',
                    'meta' => $data,
                ],
                'relationships' => [
                    'sources' => [
                        ['id' => $scholarships[1]->id, 'type' => 'Scholarship'],
                    ],
                ],
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
                ['id' => (string) $scholarships[1]->user_id, 'type' => 'User', 'attributes' => ['id' => (string) $scholarships[1]->user_id]],
                ['id' => $scholarships[1]->id, 'type' => 'Scholarship', 'attributes' => ['id' => $scholarships[1]->id]],
            ],
        ]);
    }

    public function testWithTruncateContent()
    {
        Queue::fake();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 2)->create();
        $faker = \Faker\Factory::create();
        $text = $faker->sentence(200);
        $data = ['target' => ['type' => 'USER', 'id' => array_pluck($users, 'id')], 'content' => $text];
        $res = $this->postJson('/messages', $data, $headers);
        $res->assertStatus(201);
        $res->assertJson([
            'data' => [
                "type" => "Message",
                "id" => "1",
                "attributes" => [
                    'meta' => $data,
                ],
            ],
        ]);
        Queue::assertPushed(\App\Jobs\PushNotification::class, function ($job) use ($text) {
            return $job->text === str_limit($text, env('LIMIT_TEXT_MESSAGE', 200));
        });
    }
}
