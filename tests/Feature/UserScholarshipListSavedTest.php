<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserScholarshipListSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->getJson('/users/' . $user->id . '/scholarships');
        $res->assertStatus(401);
    }

    public function testReadSuccessEmpty()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $res = $this->getJson('/users/' . $user->id . '/scholarships', $headers);
        $res->assertStatus(200);
        $this->assertEquals(0, count(json_decode($res->getContent(), true)['data']));
    }

    public function testReadSuccessData()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $scholarships = factory(\App\Models\Scholarship::class, 15)->create();
        foreach ($scholarships as $s) {
            factory(\PhpSoft\Activity\Models\Activity::class)->create([
                'verb' => 'save',
                'actor_type' => 'User',
                'actor_id' => $user->id,
                'object_type' => 'Scholarship',
                'object_id' => $s->id,
            ]);
        }
        $scholarships = factory(\App\Models\Scholarship::class, 5)->create();

        $res = $this->getJson('/users/' . $user->id . '/scholarships', $headers);
        $res->assertStatus(200);
        $content = json_decode($res->getContent(), true);
        $this->assertEquals(10, count($content['data']));
        $this->assertEquals(15, $content['meta']['totalObjects']);
    }
}
