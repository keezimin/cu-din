<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Jobs\SendInviteEmail;
use Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CounselorInviteStudentTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $res = $this->postJson('/settings', ['email'=> ['mail1@example.com', 'mail12@example.com']], []);
        $res->assertStatus(401);
    }

    /**
     * @group Invite
     */
    public function testInviteSuccess()
    {
        $bread = app()->make('\App\Repositories\Eloquent\CounselorBread');
        $data = ['email' => 'counselor@example.com', 'password' => '123456'];
        $counselor = $bread->add($data);
        $token = $this->fromUser($counselor);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        Queue::fake();

        \Mockery::mock('overload:\PhpSoft\DeepLinking\GetLink')->shouldReceive('get')->andReturn('http://url');
        $res = $this->postJson('/counselors/' . $counselor->id . '/students/invitation', ['email'=> ['mail1@example.com', 'mail12@example.com']], $headers);
        $res->assertStatus(204);

        // Assert a job was pushed to a given queue...
        Queue::assertPushed(SendInviteEmail::class, 2);

        Mail::shouldReceive('send');
    }

    /**
     * @group Invite
     */
    public function testWithoutEmail()
    {
        $bread = app()->make('\App\Repositories\Eloquent\CounselorBread');
        $data = ['email' => 'counselor@example.com', 'password' => '123456'];
        $counselor = $bread->add($data);

        $token = $this->fromUser($counselor);

        $invitation = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'example',
            'email' => 'example@example.com',
            'user_id'=> $counselor->id
        ]);
        $headers = ['HTTP_Authorization' => 'Bearer '. $token];
        Queue::fake();

        \Mockery::mock('overload:\PhpSoft\DeepLinking\GetLink')->shouldReceive('get')->andReturn('http://url');
        $res = $this->postJson('/counselors/' . $counselor->id . '/students/invitation', [], $headers);
        $res->assertStatus(204);

        // Assert a job was pushed to a given queue...
        Queue::assertPushed(SendInviteEmail::class, 1);

        Mail::shouldReceive('send');
    }
}
