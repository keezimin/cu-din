<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserPathwayDeleteSavedTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->deleteJson('/users/1/pathways/2');
        $res->assertStatus(404);
    }

    public function testNotFoundPathway()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/pathways/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $pathway = factory(\App\Models\Pathway::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/pathways/' . $pathway->id);
        $res->assertStatus(401);
    }

    public function testDeleteSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $pathway = factory(\App\Models\Pathway::class)->create();
        $activity = factory(\PhpSoft\Activity\Models\Activity::class)->create(
            [
                'verb' => 'save',
                'actor_type' => 'User',
                'actor_id' => $user->id,
                'object_type' => 'Pathway',
                'object_id' => $pathway->id
            ]
        );

        $res = $this->deleteJson('/users/' . $user->id . '/pathways/' . $pathway->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('activities', ['id' => $activity->id]);
    }
}
