<?php

namespace Tests\Feature;

use Tests\TestCase;

class OrganizationDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->deleteJson('/organizations/' . $organization->id);
        $res->assertStatus(401);
    }

    public function testNotFoundOrganization()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/organizations/122', [], $headers);
        $res->assertStatus(404);
    }

    public function testNotAcceptDelete()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $organization = factory(\App\Models\Organization::class)->create();
        $school = factory(\App\Models\School::class)->create([
            'name' => 'example.school',
            'organization_id' => $organization->id,
        ]);
        $res = $this->deleteJson('/organizations/' . $organization->id, [], $headers);
        $res->assertStatus(403);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $organization = factory(\App\Models\Organization::class)->create();
        $res = $this->deleteJson('/organizations/' . $organization->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('organizations', ['id' => $organization->id]);
    }
}
