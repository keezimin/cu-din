<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserScholarshipDeleteSave extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $res = $this->deleteJson('/users/1/scholarships/2');
        $res->assertStatus(404);
    }

    public function testNotFoundScholarship()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/scholarships/2');
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $user = factory(\App\User::class)->create();
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->deleteJson('/users/' . $user->id . '/scholarships/' . $scholarship->id);
        $res->assertStatus(401);
    }

    public function testDeleteSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = \JWTAuth::toUser($token);
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $activity = factory(\PhpSoft\Activity\Models\Activity::class)->create(['verb' => 'save', 'actor_type' => 'User', 'actor_id' => $user->id, 'object_type' => 'Scholarship', 'object_id' => $scholarship->id]);

        $res = $this->deleteJson('/users/' . $user->id . '/scholarships/' . $scholarship->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('activities', ['id' => $activity->id]);
    }
}
