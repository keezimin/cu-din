<?php

namespace Tests\Feature;

use \App\User;
use Tests\TestCase;

class CounselorStudentListTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFoundUser()
    {
        $user = factory(User::class)->create();
        $res = $this->getJson('/counselors/' . 10 . '/students?invited=true');
        $res->assertStatus(404);
    }

    public function testListStudentUnauthenticate()
    {
        $user = factory(User::class)->create();
        $res = $this->getJson('/counselors/' . $user->id . '/students?invited=true');
        $res->assertStatus(401);
    }

    public function testListStudentSuccess()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = factory(User::class)->create();

        //create counselor
        $credential = [
            'email' => 'counselor@example.com',
            'password' => '123456',
            'school_id' => $user->school_id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
        ];
        $res = $this->postJson('/counselors', $credential, $headers);
        $res->assertStatus(201);
        $counselor = \App\User::where(['email' => $credential['email']])->first();

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true', $headers);
        $res->assertStatus(200);
    }

    public function testListStudentWithPagination()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user1 = factory(User::class)->create(['fullname' => 'b']);
        $user2 = factory(User::class)->create([
            'email' => 'student2@example.com', 'password' => '123456', 'school_id' => $user1->school_id, 'fullname' => 'a',
        ]);

        //create counselor
        $credential = [
            'email' => 'counselor@example.com',
            'password' => '123456',
            'school_id' => $user1->school_id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
        ];
        $res = $this->postJson('/counselors', $credential, $headers);
        $res->assertStatus(201);
        $counselor = \App\User::where(['email' => $credential['email']])->first();

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=1&page[size]=3', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=1&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=2&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 1);

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=3&page[size]=1', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 0);
    }

    /**
     * @group Invite
     */
    public function testListStudentWithInvite()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $user = factory(User::class)->create([
            'email' => 'student@example.com', 'password' => '123456', 'fullname' => 'a',
        ]);

        //create counselor
        $credential = [
            'email' => 'counselor@example.com',
            'password' => '123456',
            'school_id' => $user->school_id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
        ];
        $res = $this->postJson('/counselors', $credential, $headers);
        $res->assertStatus(201);
        $counselor = \App\User::where(['email' => $credential['email']])->first();

        //create invation
        $invitation1 = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'example',
            'email' => 'user@example.com',
            'user_id' => $counselor->id,
            'registed_user_id' => null,
        ]);
        $invitation2 = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'example',
            'email' => 'student@example.com',
            'user_id' => $counselor->id,
            'registed_user_id' => $user->id,
        ]);

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=1&page[size]=3', $headers);
        $res->assertStatus(200);
        $responseArray = json_decode($res->getContent());
        $this->assertEquals(count($responseArray->data), 2);

    }

    public function testListStudentWithInviteFilter()
    {
        $user = factory(User::class)->create([
            'email' => 'student@example.com', 'password' => '123456', 'fullname' => 'name',
        ]);
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];

        //create counselor
        $credential = [
            'email' => 'counselor@example.com',
            'password' => '123456',
            'school_id' => $user->school_id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
        ];
        $res = $this->postJson('/counselors', $credential, $headers);
        $res->assertStatus(201);
        $counselor = \App\User::where(['email' => $credential['email']])->first();

        //create invation
        $invitations[] = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'name',
            'email' => 'user@example.com',
            'user_id' => $counselor->id,
            'registed_user_id' => null,
        ]);
        $invitations[] = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'name',
            'email' => 'user2@example.com',
            'user_id' => $counselor->id,
            'registed_user_id' => null,
        ]);
        $invitations[] = factory(\App\Models\Invitation::class)->create([
            'fullname' => 'name',
            'email' => 'student@example.com',
            'user_id' => $counselor->id,
            'registed_user_id' => $user->id,
        ]);

        $res = $this->getJson('/counselors/' . $counselor->id . '/students?invited=true&page[number]=1&page[size]=3&fullname=*name*&registed_user_id[]=>0', $headers);
        $res->assertStatus(200);
        $data = array_get(json_decode($res->getContent(), true), 'data');
        $this->assertEquals(count($data), 1);
    }
}
