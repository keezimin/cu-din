<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserRestoreTest extends TestCase
{
    public function testRestoreFailureNotFound()
    {
        $counselor1 = $this->getUser('counselor');
        $counselor2 = $this->getUser('counselor');
        $token = $this->fromUser($counselor1);

        $deleted_at = date('Y-m-d H:i:s', time());
        $graduated_on = date('Y-m-d', strtotime("+1 day", time()));

        $user = factory(\App\User::class)->create(['school_id' => $counselor2->school_id, 'deleted_at' => date('Y-m-d H:i:s', time())]);

        $res = $this->postJson("/users/{$user->id}/restore", ['graduated_on' => $graduated_on], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
    }

    public function testRestoreFailureDateTime()
    {
        $counselor1 = $this->getUser('counselor');
        $counselor2 = $this->getUser('counselor');
        $token = $this->fromUser($counselor1);

        $user = factory(\App\User::class)->create(['school_id' => $counselor1->school_id, 'deleted_at' => date('Y-m-d H:i:s', time())]);

        $res = $this->postJson("/users/{$user->id}/restore", ['graduated_on' => 'user@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    // public function testRestoreFailureToday()
    // {
    //     $counselor1 = $this->getUser('counselor');
    //     $counselor2 = $this->getUser('counselor');
    //     $token = $this->fromUser($counselor1);
    //     $deleted_at = date('Y-m-d H:i:s', time());
    //     $graduated_on = date('Y-m-d', time());
    //     $user = factory(\App\User::class)->create(['school_id' => $counselor1->school_id, 'deleted_at' => $deleted_at]);
    //
    //     $res = $this->postJson("/users/{$user->id}/restore", ['graduated_on' => $graduated_on], ['HTTP_Authorization' => 'Bearer ' . $token]);
    //     $res->assertStatus(400);
    //     $this->assertDatabaseHas('users', ['id' => $user->id, 'deleted_at' => $deleted_at]);
    // }

    public function testRestoreSuccess()
    {
        $counselor1 = $this->getUser('counselor');
        $counselor2 = $this->getUser('counselor');
        $token = $this->fromUser($counselor1);
        $deleted_at = date('Y-m-d H:i:s', time());
        $graduated_on = date('Y-m-d', strtotime("+1 day", time()));
        $user = factory(\App\User::class)->create(['school_id' => $counselor1->school_id, 'deleted_at' => $deleted_at]);

        $res = $this->postJson("/users/{$user->id}/restore", ['graduated_on' => $graduated_on], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseMissing('users', ['id' => $user->id, 'deleted_at' => $deleted_at]);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'graduated_on' => $graduated_on]);
    }
}
