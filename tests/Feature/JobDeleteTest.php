<?php

namespace Tests\Feature;

use Tests\TestCase;

class JobDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testUnauthenticate()
    {
        $job = factory(\App\Models\Job::class)->create();
        $res = $this->deleteJson('/jobs/' . $job->id);
        $res->assertStatus(401);
    }

    public function testNotFoundScholarship()
    {
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $res = $this->deleteJson('/jobs/0', [], $headers);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $job = factory(\App\Models\Job::class)->create(['owner_id' => $user->id]);
        $res = $this->deleteJson('/jobs/' . $job->id, [], $headers);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('jobs', ['id' => $job->id]);
    }
}
