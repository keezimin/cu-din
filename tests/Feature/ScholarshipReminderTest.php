<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PhpSoft\Activity\Models\Activity;

/**
 * @group Message
 * @group Reminder
 */
class ScholarshipReminderTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotFound()
    {
        $res = $this->postJson('/scholarships/1/reminder', []);
        $res->assertStatus(404);
    }

    public function testUnauthenticate()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', []);
        $res->assertStatus(401);
    }

    public function testInvalidTarget()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => 'USER'];
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', $data, $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'errors'=> [
                [
                    'title' => 'Validation Error',
                    'source' =>[
                        'pointer' => 'target'
                    ]
                ]
            ]
        ]);
    }

    public function testInvalidTargetType()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => ['type' => 'Fake']];
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', $data, $headers);
        $res->assertStatus(400);
        $res->assertJson([
            'errors'=> [
                [
                    'title' => 'Validation Error',
                    'source' =>[
                        'pointer' => 'target.type'
                    ]
                ]
            ]
        ]);
    }

    public function testInvalidContent()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $token = $this->getToken();
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $data = ['target' => ['type' => 'USER', 'id' => [1]]];
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', $data, $headers);
        $res->assertStatus(201);
    }

    public function testSuccessSendScholarship()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        $users = factory(\App\User::class, 4)->create();
        $users = factory(\App\User::class, 4)->create(['school_id' => $user->school_id]);
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create()
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $scholarship->schools()->attach($user->school_id);

        $data = [
            'target' => ['type' => 'USER', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[0]->id]],
            'content' => 'hi'
        ];
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(3, Activity::count());
        $data['title'] = '{{SENDER}} sent you a reminder to complete your scholarship application that is coming due {{SCHOLARSHIP}}';
        $data['source']['id'] = [$scholarship->id]; // we expect api correct source id
        $data['type'] = 'remind';
        $res->assertJson([
            'data' => [
                'type' => 'Message',
                'id' => '1',
                'attributes' => [
                    'id' => '1',
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => '0',
                    'object_type' => 'Message',
                    'meta' => $data
                ],
                'relationships' => [
                    'sender' => ['id' => $user->id, 'type' => 'User'],
                    'targets' => [
                        ['id' => $users[0]->id, 'type' => 'User'],
                        ['id' => $users[1]->id, 'type' => 'User'],
                    ],
                    'sources' => [
                        ['id' => $scholarship->id, 'type' => 'Scholarship']
                    ]
                ]
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
                ['id' => $users[0]->id, 'type' => 'User', 'attributes' => ['id' => $users[0]->id]],
                ['id' => $users[1]->id, 'type' => 'User', 'attributes' => ['id' => $users[1]->id]],
                ['id' => (string) $scholarship->user_id, 'type' => 'User', 'attributes' => ['id' => (string) $scholarship->user_id]],
                ['id' => $scholarship->id, 'type' => 'Scholarship', 'attributes' => ['id' => $scholarship->id]],
            ]
        ]);
    }

    public function testSuccessSendScholarshipAll()
    {
        $scholarship = factory(\App\Models\Scholarship::class)->create();
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $headers = ['HTTP_Authorization' => 'Bearer ' . $token];
        factory(\App\User::class, 4)->create();
        $scholarships = factory(\App\Models\Scholarship::class, 4)->create()
            ->each(function ($sl) use ($user) {
                $sl->schools()->attach($user->school_id);
            });
        $scholarships->each(function ($s) use (&$users) {
            $users = factory(\App\User::class, 4)->create(['school_id' => $s->d]);
            $users->each(function ($u) use ($s) {
                \App\Services\UserActionService::applyScholarship($u, $s);
            });
        });
        $scholarship = $scholarships[0];
        $data = [
            'target' => ['type' => 'USER_ALL', 'id' => [$users[0]->id, $users[1]->id]],
            'source' => ['type' => 'SCHOLARSHIP', 'id' => [$scholarships[1]->id]],
            'content' => 'hi'
        ];
        $res = $this->postJson('/scholarships/' . $scholarship->id . '/reminder', $data, $headers);
        $res->assertStatus(201);
        $this->assertEquals(5, Activity::where('object_type', 'Message')->count());
        $data['title'] = '{{SENDER}} sent you a reminder to complete your scholarship application that is coming due {{SCHOLARSHIP}}';
        unset($data['target']['id']);
        $data['source']['id'] = [$scholarship->id]; // we expect api correct source id
        $data['type'] = 'remind';
        $res->assertJson([
            'data' => [
                'type' => 'Message',
                'id' => '17',
                'attributes' => [
                    'id' => '17',
                    'actor_id' => $user->id,
                    'actor_type' => 'User',
                    'object_id' => '0',
                    'object_type' => 'Message',
                    'meta' => $data
                ],
                'relationships' => [
                    'sources' => [
                        ['id' => $scholarship->id, 'type' => 'Scholarship']
                    ]
                ]
            ],
            'included' => [
                ['id' => $user->id, 'type' => 'User', 'attributes' => ['id' => $user->id]],
                ['id' => (string) $scholarship->user_id, 'type' => 'User', 'attributes' => ['id' => (string) $scholarship->user_id]],
                ['id' => $scholarship->id, 'type' => 'Scholarship', 'attributes' => ['id' => $scholarship->id]],
            ]
        ]);
    }
}
