<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['email' => ''], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(400);
    }

    public function testUpdateSuccess()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['email' => 'example@example.com', 'password' => '123456', 'contact' => 'contact.contact', 'act_english' => '2'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'email' => 'example@example.com', 'contact' => 'contact.contact', 'act_english' => '2']);
    }

    public function testCounselorUpdateSuccess()
    {
        $user = $this->getUser('counselor');
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['email' => 'example@example.com', 'password' => '123456', 'contact' => 'contact.contact', 'act_english' => '2', 'logo' => 'https://www.gstatic.com/webp/gallery3/1.png'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('users', ['id' => $user->id, 'email' => 'example@example.com', 'contact' => 'contact.contact', 'act_english' => '2']);
        $res->assertJson([
            'data' => [
                'attributes' => [
                    'profile' => [
                        'logo' => 'https://www.gstatic.com/webp/gallery3/1.png'
                    ]
                ]
            ]
        ]);
    }

    public function testDetailWithoutToken()
    {
        $user = factory(\App\User::class)->create();
        $res = $this->patchJson('/users/' . $user->id, []);
        $res->assertStatus(401);
    }

    public function testUpdateSameEmail()
    {
        $user = factory(\App\User::class)->create([
            'email' => 'example@example.com',
            'password' => bcrypt('12345678'),
        ]);
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['email' => $user->email, 'password' => '123456'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
    }

    public function testNormalUserUpdateOtherUser()
    {
        $user = $this->getUser();
        $token = $this->getToken();
        $res = $this->patchJson('/users/' . $user->id, ['fullname' => 'new name'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(403);
    }

    public function testAdminUpdateOtherUser()
    {
        $user = $this->getUser();
        $token = $this->fromUser($this->getUser('super_admin'));
        $res = $this->patchJson('/users/' . $user->id, ['fullname' => 'new name', 'logo' => 'https://www.gstatic.com/webp/gallery3/1.png'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
    }

    public function testAdminUpdateHisEmail()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['email' => 'newEmail@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
    }

    public function testAdminUpdateOtherEmail()
    {
        $user = $this->getUser();
        $token = $this->fromUser($this->getUser('super_admin'));
        $res = $this->patchJson('/users/' . $user->id, ['email' => 'newEmail@example.com'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
    }

    public function testAdminUpdateHisPassword()
    {
        $user = $this->getUser('super_admin');
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, ['password' => '123'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertTrue(\JWTAuth::attempt(['email' => $user->email, 'password' => '123']) != false);
    }

    public function testAdminUpdateOtherPassword()
    {
        $user = $this->getUser();
        $token = $this->fromUser($this->getUser('super_admin'));
        $res = $this->patchJson('/users/' . $user->id, ['password' => '123'], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertTrue(\JWTAuth::attempt(['email' => $user->email, 'password' => '123']) != false);
    }

    public function testUpdateWithEmptyFields()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, [
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'contact' => '',
            'graduated_on' => '',
            'act_english' => '',
            'act_science' => '',
            'act_math' => '',
            'act_composite' => '',
            'act_reading' => '',
            'sat_math' => '',
            'sat_verbal' => '',
        ], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'graduated_on' => null,
            'act_english' => null,
            'act_science' => null,
            'act_math' => null,
            'act_composite' => null,
            'act_reading' => null,
            'sat_math' => null,
            'sat_verbal' => null,
        ]);
    }

    public function testUpdateWithNullFields()
    {
        $user = $this->getUser();
        $token = $this->fromUser($user);
        $res = $this->patchJson('/users/' . $user->id, [
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'graduated_on' => null,
            'act_english' => null,
            'act_science' => null,
            'act_math' => null,
            'act_composite' => null,
            'act_reading' => null,
            'sat_math' => null,
            'sat_verbal' => null,
        ], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $res->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => 'Au',
            'last_name' => 'Truong',
            'graduated_on' => null,
            'act_english' => null,
            'act_science' => null,
            'act_math' => null,
            'act_composite' => null,
            'act_reading' => null,
            'sat_math' => null,
            'sat_verbal' => null,
        ]);
    }
}
