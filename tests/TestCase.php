<?php

namespace Tests;

use JWTAuth;
use PhpSoft\Users\Models\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;
    use DatabaseTransactions;

    protected function getToken($credential = null)
    {
        if (!$credential) {
            $user = factory(\App\User::class)->create();
        } else {
            $user = \App\User::where($credential)->first();
        }

        return $this->fromUser($user);
    }

    public static function getAttributes($res)
    {
        $data = json_decode($res->getContent());

        return $data->data->attributes ?? [];
    }

    public static function getId($res)
    {
        $data = json_decode($res->getContent());

        return $data->data->id ?? '';
    }

    protected function getUser($role = null)
    {
        $user = factory(\App\User::class)->create();
        $role && $user->attachRole(Role::firstOrCreate(['name' => $role]));

        return $user;
    }

    protected function fromUser($user, array $customClaims = [])
    {
        $default = [
            'password' => $user->password,
            'domain' => env('APP_URL'),
        ];
        $customClaims = array_merge($default, $customClaims);
        return JWTAuth::fromUser($user, $customClaims);
    }
}
